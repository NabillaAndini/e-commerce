<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

<style>
@import url('http://fonts.googleapis.com/css?family=Amarante');

html,body,div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline;
  outline: none;
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
html { overflow-y: scroll; }


::selection { background: #5f74a0; color: #fff; }
::-moz-selection { background: #5f74a0; color: #fff; }
::-webkit-selection { background: #5f74a0; color: #fff; }

br { display: block; line-height: 1.6em; }

article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section { display: block; }
ol, ul { list-style: none; }

input, textarea {
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  outline: none;
}

blockquote, q { quotes: none; }
blockquote:before, blockquote:after, q:before, q:after { content: ''; content: none; }
strong, b { font-weight: bold; }

table { border-collapse: collapse; border-spacing: 0; }
img { border: 0; max-width: 100%; }

h1 {
  font-family: 'Amarante', Tahoma, sans-serif;
  font-weight: bold;
  font-size: 3.6em;
  line-height: 1.7em;
  margin-bottom: 10px;
  text-align: center;
}


/** page structure **/
#wrapper {
  display: block;
  width: 1200px;
  background: #fff;
  margin: 0 auto;
  padding: 10px 17px;
  -webkit-box-shadow: 2px 2px 3px -1px rgba(0,0,0,0.35);
}

#keywords {
  margin: 0 auto;
  font-size: 1.0em;
  margin-bottom: 15px;
}


#keywords thead {
  cursor: pointer;
  background: #c9dff0;
}
#keywords thead tr th {
  font-weight: bold;
  padding: 12px 10px;
  padding-left: 32px;
}
#keywords thead tr th span {
  padding-right: 20px;
  background-repeat: no-repeat;
  background-position: 100% 100%;
}

#keywords thead tr th.headerSortUp, #keywords thead tr th.headerSortDown {
  background: #acc8dd;
}

#keywords thead tr th.headerSortUp span {
  background-image: url('http://i.imgur.com/SP99ZPJ.png');
}
#keywords thead tr th.headerSortDown span {
  background-image: url('http://i.imgur.com/RkA9MBo.png');
}


#keywords tbody tr {
  color: #555;
}
#keywords tbody tr td {
  text-align: center;
  padding: 15px 5px;
}
#keywords tbody tr td.lalign {
  text-align: left;
}
</style>
</head>



<?php include "headerAfterLogin.php";

if($_SESSION['role'] !== 'pembeli'){

 echo "<script> location.replace('dashboard.php'); </script>";

return;
}
?>


    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12 text-center">
         <div id="wrapper">
  <h1>Daftar Transaksi Shipped</h1>

  <table id="keywords" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th><span>No Invoice</span></th>
		<th><span>Nama Toko</span></th>
        <th><span>Tanggal</span></th>
        <th><span>Status</span></th>
        <th><span>Total Bayar</span></th>
        <th><span>Alamat Kirim</span></th>
		<th><span>Biaya Kirim</span></th>
		<th><span>Nomor Resi</span></th>
		<th><span>Jasa Kirim</span></th>
		<th><span>Daftar Produk</span></th>
      </tr>
    </thead>
    <tbody>
       <?php
        //$db = pg_connect('host=localhost dbname=sirima user=b10 password=basdat123');
        include 'connect.php';
        $query = "SELECT no_invoice, nama_toko, status,tanggal,total_bayar,alamat_kirim,biaya_kirim,no_resi, nama_jasa_kirim FROM TOKOKEREN.transaksi_shipped WHERE email_pembeli = '".$_SESSION['email']."' ";

        $result = pg_query($query);
        if (!$result) {
            echo "Problem with query " . $query . "<br/>";
            echo pg_last_error();
            exit();
        }





# Your array of coordinates
$coord_array = "";

# Serialize the coordinates

        while($myrow = pg_fetch_assoc($result)) {
          $bayar = "";
            if($myrow['status']=="1"){
              $bayar = "Transaksi Dilakukan";
            }else if($myrow['status']=="2"){
              $bayar = "Barang Sudah Dibayar";
            }else if($myrow['status']=="3"){
              $bayar = "Barang Sudah Dikirim";
            }else{
              $bayar = "Barang Sudah Diterima";
            }
            printf ("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>'<a href='daftar.php?invoice=".$myrow['no_invoice']."'>DAFTAR PRODUK</a></td></tr>", $myrow['no_invoice'],htmlspecialchars($myrow['nama_toko']), htmlspecialchars($myrow['tanggal']), $bayar, htmlspecialchars($myrow['total_bayar']), $myrow['alamat_kirim'], $myrow['biaya_kirim'], $myrow['no_resi'],$myrow['nama_jasa_kirim']);
        }
        ?>

      </tr>


    </tbody>
  </table>
 </div>
        </div>
      </div>

<?php include "footerAfterLogin.php"; ?>
<script>
$(document).ready(function() {
$('#keywords').DataTable();
} );
</script>
</html>
