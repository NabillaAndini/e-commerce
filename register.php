<?php
	if($_SERVER['REQUEST_METHOD'] != 'POST') {
		header("Location: index.php");
	} else {
		session_start();
		include "connect.php";

		$email = $_POST['email'];
		$password = $_POST['password1'];
		$nama = $_POST['nama'];
		$gender = $_POST['gender'];
		$telp = $_POST['telp'];
		$day = $_POST['day'];
		$month = $_POST['month'];
		$year = $_POST['year'];
		$alamat = $_POST['alamat'];

		$_SESSION['isian'] = [];
		$_SESSION['isian']['email'] = $email;
		$_SESSION['isian']['password1'] = $password;
		$_SESSION['isian']['password2'] = $_POST['password2'];
		$_SESSION['isian']['nama'] = $nama;
		$_SESSION['isian']['gender'] = $gender;
		$_SESSION['isian']['telp'] = $telp;
		$_SESSION['isian']['day'] = $day;
		$_SESSION['isian']['month'] = $month;
		$_SESSION['isian']['year'] = $year;
		$_SESSION['isian']['alamat'] = $alamat;

		$errorMsg = "";


		if($email == '') 
			$errorMsg .= "- Isian email tidak boleh kosong</br>";
		else {

			$validEmail = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
			if($validEmail != true) {
				$errorMsg .= "- Email harus dengan format yang benar<br/>";
			}

			$sql = "SELECT email FROM TOKOKEREN.pengguna WHERE email='".$_POST['email']."'";
			
			if($result = pg_query($conn, $sql)) {
				if(pg_num_rows($result) > 0) {
					$errorMsg .= "- Email sudah terdaftar, silahkan gunakan email lain<br/>";
				}
			} else {
				die("Error: $sql");
			}
		}

		if($nama == '') {
			$errorMsg .= "- Isian nama tidak boleh kosong</br>";
		}
			
		


		if($password == '') 
			$errorMsg .= "- Isian password tidak boleh kosong</br>";
			if($_POST['password2'] == '') 
				$errorMsg .= "- Isian ulangi password tidak boleh kosong</br>";
		else if($_POST['password2'] == '') 
			$errorMsg .= "- Isian ulangi password tidak boleh kosong</br>";
		else {
			if(strlen($_POST['password1']) < 6) {
				$errorMsg .= "- Kolom password dan ulangi password minimal terdiri dari 6 karakter dan bersifat case sensitive (A beda dengan a). <br/>";
			}

			if($_POST['password1'] != $_POST['password2']) {
				$errorMsg .= "- Password tidak sama<br/>";
			}
		}

		if($gender == '') 
			$errorMsg .= "- Isian gender tidak boleh kosong</br>";
		else {
			if($_POST['gender'] == "") {
				$errorMsg .= "- Isian gender harus diisi<br/>";
			}
		}

		if($day == "" || $month == "" || $year == "") {
			$errorMsg .= "- Seluruh isian tanggal lahir harus diisi</br>";
		}
		
		if($telp == '') 
			$errorMsg .= "- Isian no telepon tidak boleh kosong</br>";
		else {
			if(!preg_match("/^[0-9]*$/", $_POST['telp'])) {
			  $errorMsg .= "- Nomor telepon tidak valid!. Harus berisi angka saja<br/>";
			}
		}

		if($alamat == "") {
			$errorMsg .= "- Isian alamat harus diisi</br>";
		}

		// if ada error
		if($errorMsg != "") {	
			$_SESSION['errorMsg'] = $errorMsg;
			pg_close($conn);
			header("Location: index.php");
			die();
		} else {

			$tgl_lahir = $year."-".$month."-".$day;

			$sql = "INSERT INTO TOKOKEREN.pengguna (email, password, nama, jenis_kelamin, tgl_lahir, no_telp, alamat) VALUES ('$email', '$password', '$nama', '$gender', '$tgl_lahir', '$telp', '$alamat')";

			$sql2 = "INSERT INTO TOKOKEREN.pelanggan (email, is_penjual, nilai_reputasi, poin) VALUES ('$email', false, null, null)";

			if(!pg_query($conn, $sql)) {
				die("Error: $sql");
			}

			if(!pg_query($conn, $sql2)) {
				die("Error: $sql2");
			}

			$_SESSION['email'] = $email;
			$_SESSION['role'] = 'pembeli';
			$_SESSION['nama'] = $nama;
			header("Location: dashboard.php");
		}



		pg_close($conn);


	}
?>