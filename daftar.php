<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

<style>
@import url('http://fonts.googleapis.com/css?family=Amarante');

html,body,div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline;
  outline: none;
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
html { overflow-y: scroll; }


::selection { background: #5f74a0; color: #fff; }
::-moz-selection { background: #5f74a0; color: #fff; }
::-webkit-selection { background: #5f74a0; color: #fff; }

br { display: block; line-height: 1.6em; }

article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section { display: block; }
ol, ul { list-style: none; }

input, textarea {
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  outline: none;
}

blockquote, q { quotes: none; }
blockquote:before, blockquote:after, q:before, q:after { content: ''; content: none; }
strong, b { font-weight: bold; }

table { border-collapse: collapse; border-spacing: 0; }
img { border: 0; max-width: 100%; }

h1 {
  font-family: 'Amarante', Tahoma, sans-serif;
  font-weight: bold;
  font-size: 3.6em;
  line-height: 1.7em;
  margin-bottom: 10px;
  text-align: center;
}
h3{
	text-align : left;
}


/** page structure **/
#wrapper {
  display: block;
  width: 700px;
  background: #fff;
  margin: 0 auto;
  padding: 10px 17px;
  -webkit-box-shadow: 2px 2px 3px -1px rgba(0,0,0,0.35);
}

#keywords {
  margin: 0 auto;
  font-size: 1.0em;
  margin-bottom: 15px;
}


#keywords thead {
  cursor: pointer;
  background: black;
  font-color : white;
}
#keywords thead tr th {
  font-weight: bold;
  padding: 12px 10px;
  padding-left: 32px;
  font-color : white;
}
#keywords thead tr th span {
  padding-right: 20px;
  background-repeat: no-repeat;
  background-position: 100% 100%;
  color: white;
}

#keywords thead tr th.headerSortUp, #keywords thead tr th.headerSortDown {
  background: #acc8dd;
color : white;
}

#keywords thead tr th.headerSortUp span {
  background-image: url('http://i.imgur.com/SP99ZPJ.png');
  font-color : white;
}
#keywords thead tr th.headerSortDown span {
  background-image: url('http://i.imgur.com/RkA9MBo.png');
  color : white;
}


#keywords tbody tr {
text-color : white;
  color: #555;
}
#keywords tbody tr td {
  text-align: center;
  padding: 15px 5px;
}
#keywords tbody tr td.lalign {
  text-align: left;
}
</style>
</head>



<?php include "headerAfterLogin.php";
if($_SESSION['role'] !== 'pembeli'){

  header('location: dashboard.php');
}

?>


    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12 text-center">
         <div id="wrapper">
  <h1>Daftar Produk</h1>
  <h3> No Invoice : <?php
//    $db = pg_connect('host=localhost dbname=sirima user=b10 password=basdat123');
  include'connect.php';
$param1 = $_GET['invoice'];
echo $param1;
  ?></h3>


  <table id="keywords" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th><span>Nama Produk</span></th>
		<th><span>Berat</span></th>
        <th><span>Kuantitas</span></th>
        <th><span>Harga</span></th>
        <th><span>Subtotal</span></th>
        <th><span>Ulasan</span></th>
      </tr>
    </thead>
    <tbody>
     <?php
        //$db = pg_connect('host=localhost dbname=nabillaandini user=postgres password=candice1234');

        $query = "SELECT P.kode_produk, P.nama, L.berat, L.kuantitas, L.harga, L.sub_total FROM TOKOKEREN.produk P, TOKOKEREN.list_item L, TOKOKEREN.transaksi_shipped T  WHERE L.no_invoice='$param1' AND L.kode_produk = P.kode_produk AND T.no_invoice = L.no_invoice AND T.email_pembeli = '".$_SESSION['email']."'";

        $result = pg_query($query);
        if (!$result) {
            echo "Problem with query " . $query . "<br/>";
            echo pg_last_error();
            exit();
        }

        while($myrow = pg_fetch_assoc($result)) {
          $button = "<a href='addUlasan.php?kode=".$myrow['kode_produk']."'> <button type='button' class='btn btn-info btn-lg'>ULAS</button> </a>";
          $query = "SELECT count(*) as jumlah from TOKOKEREN. ulasan WHERE email_pembeli = '".$_SESSION['email']."' AND kode_produk = '".$myrow['kode_produk']. "';";
          $result2 = pg_query($query);

          if (!$result2) {
              echo "Problem with query " . $query . "<br/>";
              echo pg_last_error();
              exit();
          }

          $row2 = pg_fetch_assoc($result2);
          if($row2['jumlah'] > 0){
              $button = "<button type='button' class='btn btn-danger' disabled>ULAS</button>";
          }
            printf ("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>", $myrow['nama'],htmlspecialchars($myrow['berat']), htmlspecialchars($myrow['kuantitas']), $myrow['harga'], htmlspecialchars($myrow['sub_total']), $button);
        }
        ?>


    </tbody>
  </table>
 </div>
        </div>
      </div>

<?php include "footerAfterLogin.php"; ?>
<script>
$(document).ready(function() {
$('#keywords').DataTable();
} );
</script>
</html>
