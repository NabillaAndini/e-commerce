<?php
	session_start();

	include 'connect.php';
	
	$kode = pg_escape_string($_POST['kode']);
	$nama = pg_escape_string($_POST['name']);
	$harga = pg_escape_string($_POST['harga']);
	$deskripsi = pg_escape_string($_POST['desc']);
	$nominal = pg_escape_string($_POST['nominal']);

	if(!(is_numeric($harga)) || ($harga <= 0)) {
		$_SESSION["error_insert_pulsa"] = "ERROR! Can not input to database!<br>The price must be numeric and > 0";
	} else if(!(is_numeric($nominal)) || ($nominal <= 0)) {
		$_SESSION["error_insert_pulsa"] = "ERROR! Can not input to database!<br>The nominal must be numeric and > 0";
	} else {
		$sql_produk = "INSERT INTO TOKOKEREN.PRODUK (kode_produk, nama, harga, deskripsi) VALUES ('" . $kode . "', '" . $nama . "', '" . $harga . "', '" . $deskripsi . "')";
	
		$result = pg_query($conn, $sql_produk);

		if(!$result) {
			$_SESSION["error_insert_pulsa"] = "ERROR! Can not input to database!<br>There is product with your product code! <br>Product code must be unique!";
		} else {
			$_SESSION["error_insert_pulsa"] = "nope";

			$sql_pulsa = "INSERT INTO TOKOKEREN.PRODUK_PULSA (kode_produk, nominal) VALUES ('" . $kode . "', '" . $nominal . "')";
			pg_query($conn, $sql_pulsa);
		}
	}

	header("Location: addProductPulsa.php");
?>