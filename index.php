<?php
  session_start();

  // if logged in, jangan bolehin buka halaman ini
  if(isset($_SESSION['email'])) {
    header("Location: dashboard.php");
    die();
  }

  $email = "";
  $password1 = "";
  $password2 = "";
  $nama = "";
  $gender = "";
  $telp = "";
  $day = "";
  $month = "";
  $year = "";
  $alamat = "";

  if(isset($_SESSION['isian'])) {
    $email = $_SESSION['isian']['email'];
    $password1 = $_SESSION['isian']['password1'];
    $password2 = $_SESSION['isian']['password2'];
    $nama = $_SESSION['isian']['nama'];
    $gender = $_SESSION['isian']['gender'];
    $telp = $_SESSION['isian']['telp'];
    $day = $_SESSION['isian']['day'];
    $month = $_SESSION['isian']['month'];
    $year = $_SESSION['isian']['year'];
    $alamat = $_SESSION['isian']['alamat'];

    unset($_SESSION['isian']);
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>Tokokeren | Basis Data 2017</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <style>
      /* Move down content because we have a fixed navbar that is 50px tall */
      body {
        padding-top: 50px;
        padding-bottom: 20px;
      }

      .navbar-inverse{
        background-color: #c0392b;
        color: #eeeeee !important;
      }

      .navbar-inverse .navbar-brand {
        color: #eeeeee !important;  
      }
    </style>
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Toko Keren</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" method="post" action="login.php">
            <div class="form-group">
              <input type="text" name="email" placeholder="Email" class="form-control" required>
            </div>
            <div class="form-group">
              <input type="password" name="password" placeholder="Password" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-success" style="background-color: #d35400;border-color:#e74c3c">Sign in</button>
      
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-image:url('img/pattern.png');">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p>Toko Keren adalah platform yang dapat mempertemukan antara anda, para pembeli kepada anda, para penjual. Moto kami adalah <i>"Kapanpun, dimanapun"</i>. Kami tak lekang oleh waktu dan tempat, memungkinkan anda dapat bertemu dan bertransaksi kapanpun anda sedang ingin berbelanja.</p>
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
          </div>
          <div class="col-md-6">
            <h2 style="margin:0px;margin-bottom: 10px;font-size:30pt">Bergabung Sekarang!</h2>
            <p style="font-size: 12pt;">Sangat mudah bergabung dengan TokoKeren. Cukup isikan data diri anda dibawah ini!</p>

            <?php if(isset($_SESSION['errorMsg'])) { ?>
              <div class="alert alert-danger">
                <h4 style="margin:0px;margin-bottom:7px">Terjadi beberapa Kesalahan</h4>
                <?php echo $_SESSION['errorMsg']; ?>
              </div>
            <?php unset($_SESSION['errorMsg']); } ?>

            <form method="post" action="register.php" novalidate>
              <div class="input-group" style="width:70%">
                <input type="text" name="email" class="form-control" placeholder="Email" value="<?php echo $email; ?>" required> 
              </div>

              <div class="input-group" style="width:70%;margin-top:10px">
                <input type="password" name="password1" class="form-control" value="<?php echo $password1; ?>" placeholder="Password" style=" width:50%" required>

                <input type="password" name="password2" class="form-control" value="<?php echo $password2 ; ?>" placeholder="Retype" style=" width:50%" required>
              </div>

              <div class="input-group" style="width:70%;margin-top:10px">
                <input type="text" name="nama" value="<?php echo $nama; ?>" class="form-control" placeholder="Nama Lengkap" required> 
              </div>

              <div class="input-group" style="width:70%;margin-top:10px">
                <select name="gender" style="width:50%;" class="form-control" required>
                <option disabled <?php if($gender == '') echo 'selected'; ?>>Pilih Gender</option>
                <option value="L" <?php if($gender == 'L') echo "selected" ?>>Laki-laki</option>
                <option value="P" <?php if($gender == 'P') echo "selected" ?>>Perempuan</option>
                </select>

                <input type="text" name="telp" class="form-control" value="<?php echo $telp; ?>" placeholder="Telepon" style="width:50%" required>
              </div>
              
              <div class="input-group" style="width:70%;margin-top:10px">
                <select name="day" class="form-control" style="width:150px" required="">
                  <option disabled selected>Tanggal Lahir</option>
                  <?php
                    for($i = 1; $i <= 31; $i++) {
                      echo "<option value='".$i."' ".($day == $i ? "selected" : "").">".$i."</option>";
                    }
                  ?>
                </select>

                <select name="month" class="form-control" style="width:100px" required="">
                  <option disabled selected>Bulan</option>
                  <option value="01" <?php if($month == '01') echo "selected" ?>>Jan</option>
                  <option value="02" <?php if($month == '02') echo "selected" ?>>Feb</option>
                  <option value="03" <?php if($month == '03') echo "selected" ?>>Mar</option>
                  <option value="04" <?php if($month == '04') echo "selected" ?>>Apr</option>
                  <option value="05" <?php if($month == '05') echo "selected" ?>>Mei</option>
                  <option value="06" <?php if($month == '06') echo "selected" ?>>Jun</option>
                  <option value="07" <?php if($month == '07') echo "selected" ?>>Jul</option>
                  <option value="08" <?php if($month == '08') echo "selected" ?>>Ags</option>
                  <option value="09" <?php if($month == '09') echo "selected" ?>>Sept</option>
                  <option value="10" <?php if($month == '10') echo "selected" ?>>Okt</option>
                  <option value="11" <?php if($month == '11') echo "selected" ?>>Nov</option>
                  <option value="12" <?php if($month == '12') echo "selected" ?>>Des</option>
                </select>

                <select name="year" class="form-control" style="width:100px" required="">
                  <option disabled selected>Tahun</option>
                  <?php
                    for($i = 1945; $i <= 2017; $i++) {
                      echo "<option value='".$i."' ".($year == $i ? "selected" : "").">".$i."</option>";
                    }
                  ?>
                </select>
              </div>

              <div class="input-group" style="width:70%;margin-top:10px">
                <textarea class="form-control" name="alamat" placeholder="Alamat lengkap" required><?php echo $alamat; ?></textarea>
                
              </div>

              <div class="input-group" style="width:70%;margin-top:10px">
               <button type="submit" class="btn btn-success" style="background-color: #d35400;border-color:#e74c3c">Register</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Administrator</h2>
          <p>Kami sebagai administrator akan mengurus layanan marketplace Tokokeren. Kami akan terus memantau perkembangan transaksi anda, melindungi anda, meningkatkan kinerja aplikasi, dan menjadi jembatan antara penjual dan pembeli.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Pembeli</h2>
          <p>Temukan ratusan toko yang dibuka oleh ratusan penjual serta temukan berbagai barang menarik yang dapat memanjakan impian anda sekarang juga! Berbelanja di TokoKeren aman sebab kami akan melindungi dan menjaga setiap transaksi serta privasi anda. </p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <h2>Penjual</h2>
          <p>Punya barang dan susah berjualan offline? Jual barang anda di TokoKeren dan temukan lebih dari ratusan calon pembeli sekarang juga! Tokokeren menyediakan lapak dan mekanisme pembayaran yang memudahkan anda sebagai penjual dan pembeli.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; 2017 TokoKeren, Inc.</p>
      </footer>
    </div> <!-- /container -->

    <?php if(isset($_SESSION['message'])) { ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $_SESSION['judulMessage']; ?></h4>
          </div>
          <div class="modal-body">
            <p><?php echo $_SESSION['message'];?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <?php } ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    $(document).ready(function () {
      <?php if(isset($_SESSION['message'])) { ?>
      $('#myModal').modal('show');
      <?php } ?>
    });
    </script>
  </body>
</html>

<?php if(isset($_SESSION['message'])) {unset($_SESSION['message']);} ?>
