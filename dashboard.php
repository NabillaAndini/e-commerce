

<?php include "headerAfterLogin.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	
<style>

html,
body {
  width: 100%;
  height: 100%;
  overflow: hidden;
  margin: 0;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
}

div[class*=box] {
	height: 33.33%;
	width: 100%; 
  display: flex;
  justify-content: center;
  align-items: center;
}

.box-1 { background-color: #FF6766; }
.box-2 { background-color: #3C3C3C; }
.box-3 { background-color: #66A182; }

.btn {
	line-height: 50px;
	height: 50px;
	text-align: center;
	width: 250px;
	cursor: pointer;
}

/* 
========================
      BUTTON ONE
========================
*/
.btn-one {
	color: #FFF;
	transition: all 0.3s;
	position: relative;
}
.btn-one span {
	transition: all 0.3s;
}
.btn-one::before {
	content: '';
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1;
	opacity: 0;
	transition: all 0.3s;
	border-top-width: 1px;
	border-bottom-width: 1px;
	border-top-style: solid;
	border-bottom-style: solid;
	border-top-color: rgba(255,255,255,0.5);
	border-bottom-color: rgba(255,255,255,0.5);
	transform: scale(0.1, 1);
}
.btn-one:hover span {
	letter-spacing: 2px;
}
.btn-one:hover::before {
	opacity: 1;	
	transform: scale(1, 1);	
}
.btn-one::after {
	content: '';
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1;
	transition: all 0.3s;
	background-color: rgba(255,255,255,0.1);
}
.btn-one:hover::after {
	opacity: 0;	
	transform: scale(0.1, 1);
}


/* 
========================
      BUTTON THREE
========================
*/
.btn-three {
	color: #FFF;
	transition: all 0.5s;
	position: relative;
}
.btn-three::before {
	content: '';
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1;
	background-color: rgba(255,255,255,0.1);
	transition: all 0.3s;
}
.btn-three:hover::before {
	opacity: 0 ;
	transform: scale(0.5,0.5);
}
.btn-three::after {
	content: '';
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1;
	opacity: 0;
	transition: all 0.3s;
	border: 1px solid rgba(255,255,255,0.5);
	transform: scale(1.2,1.2);
}
.btn-three:hover::after {
	opacity: 1;
	transform: scale(1,1);
}

</style>
</head>
    <div class="container" style="font-family: 'Open Sans Condensed', sans-serif;">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12 text-center">

        <?php if($_SESSION['role'] != 'admin') { ?>

          <h2 style="color:#444444; opacity: 0.7;">What do you want to buy?</h2>
       <!-- Hover #1 -->
<div class="box-1" style="margin-top: 20px">
<a href="buyPulsa.php">
  <div class="btn btn-one">
    <span>PRODUK PULSA</span>
  </div>
  </a>
</div>


<!-- Hover #3 -->
<div class="box-3">
	<a href="selectToko.php">
  <div class="btn btn-three" data-toggle="modal" data-target="#myModal">
    <span>SHIPPED PRODUCT</span>
  </div>
  </a>
</div>
<h2 style="color:#444444; opacity: 0.7;">See what's in your cart!</h2>
<a href="cart.php">
<img src="http://www.cleowholesale.com/front/images/cart.png">
</a>
<?php } else { ?>

	<h2>You're an admin</h2>
	<a href="addCategory.php" style="color:white">
	<div class="box-1" style="margin-top: 20px">
  <div class="btn btn-one">
    <span>Add Category</span>
  </div>

</div></a>


<!-- Hover #3 -->
<a href="addJasaKirim.php" style="color:white">
<div class="box-3">
	
  <div class="btn btn-three" data-toggle="modal" data-target="#myModal">
    <span>Membuat Jasa Kirim</span>
  </div>
 
</div></a>


<a href="addPromo.php" style="color:white">
<div class="box-1">
  <div class="btn btn-one">
    <span>Membuat Promo</span>
  </div>

</div></a>


<!-- Hover #3 -->
<a href="addProductPulsa.php" style="color:white">
<div class="box-3">
	
  <div class="btn btn-three">
    <span>Menambah Produk</span>
  </div>
 
</div></a>

<a href="logout.php" style="color:white">
<div class="box-1">
  <div class="btn btn-one">
    <span>Logout</span>
  </div>

</div>
</a>
<?php } ?>
        </div>
      </div>

<?php include "footerAfterLogin.php"; ?>