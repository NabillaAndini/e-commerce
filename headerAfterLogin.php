<?php
  session_start();

  // if not logged in, jangan bolehin buka halaman ini
  if(!isset($_SESSION['email'])) {
    $_SESSION['message'] = "<h2>Harap login terlebih dahulu</h2>";
    $_SESSION['judulMessage'] = "Oops...";
    header("Location: index.php");
    die();
  }

  $page = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
  $defaultPage = ['dashboard.php', 'index.php'];
  $accessAdmin = ['addCategory.php', 'addJasaKirim.php', 'addPromo.php', 'addProductPulsa.php'];

  
  if(!in_array($page, $defaultPage)) {
    // admin cmn bisa buka halaman admin aja
    if($_SESSION['role'] == 'admin') {
      if(!in_array($page, $accessAdmin)) {
        header("Location: dashboard.php");
        die();
      }
    }

    // kalo bukan admin tp coba2 buka halaman admin
    if($_SESSION['role'] != 'admin') {
      if(in_array($page, $accessAdmin)) {
        header("Location: dashboard.php");
        die();
      } 
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Tokokeren | Basis Data 2017</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/tokokeren-css.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Philosopher" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <style>
      /* Move down content because we have a fixed navbar that is 50px tall */
      body {
        padding-top: 50px;
        background-image: url("img/pattern2.png");
        background-color: #ffffff;
      }

      .navbar-inverse{
        background-color: #c0392b;
        color: #eeeeee !important;
      }

      .navbar-inverse .navbar-brand {
        color: #eeeeee !important;  
      }
	  .myButton {
	-moz-box-shadow:inset 0px 1px 0px 0px #f7c5c0;
	-webkit-box-shadow:inset 0px 1px 0px 0px #f7c5c0;
	box-shadow:inset 0px 1px 0px 0px #f7c5c0;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #fc8d83), color-stop(1, #e4685d));
	background:-moz-linear-gradient(top, #fc8d83 5%, #e4685d 100%);
	background:-webkit-linear-gradient(top, #fc8d83 5%, #e4685d 100%);
	background:-o-linear-gradient(top, #fc8d83 5%, #e4685d 100%);
	background:-ms-linear-gradient(top, #fc8d83 5%, #e4685d 100%);
	background:linear-gradient(to bottom, #fc8d83 5%, #e4685d 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fc8d83', endColorstr='#e4685d',GradientType=0);
	background-color:#fc8d83;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #d83526;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #b23e35;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #e4685d), color-stop(1, #fc8d83));
	background:-moz-linear-gradient(top, #e4685d 5%, #fc8d83 100%);
	background:-webkit-linear-gradient(top, #e4685d 5%, #fc8d83 100%);
	background:-o-linear-gradient(top, #e4685d 5%, #fc8d83 100%);
	background:-ms-linear-gradient(top, #e4685d 5%, #fc8d83 100%);
	background:linear-gradient(to bottom, #e4685d 5%, #fc8d83 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e4685d', endColorstr='#fc8d83',GradientType=0);
	background-color:#e4685d;
  text-decoration: none;
}
.myButton:active {
	position:relative;
	top:1px;
}
.modalDialog {
    position: fixed;
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0, 0, 0, 0.8);
    z-index: 99999;
    opacity:0;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
	
}
.modalDialog:target {
    opacity:1;
    pointer-events: auto;
}
.modalDialog > div {
    width: 400px;
    position: relative;
    margin: 10% auto;
    padding: 5px 20px 13px 20px;
    border-radius: 10px;
    background: #fff;
    background: -moz-linear-gradient(#fff, #999);
    background: -webkit-linear-gradient(#fff, #999);
    background: -o-linear-gradient(#fff, #999);
}
.close {
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: -12px;
    text-align: center;
    top: -10px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    -moz-box-shadow: 1px 1px 3px #000;
    -webkit-box-shadow: 1px 1px 3px #000;
    box-shadow: 1px 1px 3px #000;
}
.close:hover {
    background: #00d9ff;
}
    </style>
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Toko Keren</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#" style="color:#eeeeee">Hello, <?php echo $_SESSION['nama']." (".$_SESSION['role'].")";?></a></li>
           
<li><a href="dashboard.php" style="color: white;">Home</a></li>
            <?php if($_SESSION['role'] != 'admin') {?>

			<li><a href="#openModal" style="color: white;">Transaction</a></li>
      <?php if($_SESSION['role'] == 'pembeli') { ?>
        <li><a href="addStore.php" style="color: white;">Membuat Toko</a></li>
      <?php } else { ?>
        <li><a href="addShippedProduct.php" style="color: white;">Menambah Produk</a></li>
        <li><a href="addStore.php" style="color: white;">Membuat Toko</a></li>
      <?php } ?>
			<div id="openModal" class="modalDialog">
    <div>	<a href="#close" title="Close" class="close">X</a>

        	<h2>What do you want to pay?</h2>
 <a href="pulsa.php" class="myButton">Transaksi Pulsa</a>
<a href="shipped.php" class="myButton">Transaksi Shipped</a>
    </div>
</div>
        <?php } ?>
            <li><a href="logout.php" style="color:#eeeeee">Logout <span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
          </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>