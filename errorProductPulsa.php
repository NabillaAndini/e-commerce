<?php include "headerAfterLogin.php"; 
	if(!isset($_SESSION["email"])) {
		echo("<script> location.replace('index.php'); </script>");
	}

  	if(isset($_SESSION["role"]) && ($_SESSION["role"] == "admin")) {
		echo("<script> location.replace('addProductPulsa.php'); </script>");
	}
?>

<div class="container-fluid" style="bottom: 0;">
	<div class="row" id="productPulsa">
		<div class="container" style="margin-top: 25px;">
	        <div class="col-md-6" style="color: white;">
	          	<div style="margin-top: 30px; background-color: #228b96; padding-left: 30px; padding-bottom: 20px;"> 
	          		<p style="padding-top: 15px; padding-bottom: 8px; font-size: 20px; font-family: 'Philosopher', sans-serif;">Sorry, this form for admin only! </p>
	          		<button onclick="goBack()" id="back"> Back </button> 
	          	</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-5" style="color: white; font-family: 'Philosopher', sans-serif; font-size: 15px;">
				<div class="row">
					<h2 style="margin-bottom: 20px;"> Top telecom companies in Indonesia </h2>
					<p style="margin-bottom: 30px;"> The telecommunications sector in Indonesia has started a phase of development after quite a long period of uncertainty. Although any country with a population like Indonesia’s (240,000,000 people) is a large potential market, the country needs to carry on with the building of the telecom infrastructure it needs to support a large population spread over its unique geography. Today, Indonesia has started to embrace broadband, and some of its telecommunications companies are among the most important ones in the world. Here they are: </p>
				</div>
				<div class="row">
					<div class="col-md-4" style="background-image: url('img/axis.png'); background-repeat: no-repeat; background-size: 100px; min-height: 15vh; background-position: center"></div>
					<div class="col-md-4" style="background-image: url('img/hutchison.png'); background-repeat: no-repeat; background-size: 150px; min-height: 15vh; background-position: center"></div>
					<div class="col-md-4" style="background-image: url('img/xl.png'); background-repeat: no-repeat; background-size: 100px; min-height: 15vh; background-position: center"></div>
				</div>
				<div class="row">
					<div class="col-md-6" style="background-image: url('img/im3.png'); background-repeat: no-repeat; background-size: 200px; min-height: 20vh;"></div>
					<div class="col-md-6" style="background-image: url('img/telkomsel.png'); background-repeat: no-repeat; background-size: 200px; min-height: 20vh; margin-top: 14px;"></div>
				</div>
				<p> Source : http://zeendo.com/info/top-telecom-companies-in-indonesia/ </p>
			</div>
		</div>
	</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
  	<script>
		function goBack() {
    		window.history.back();
		}
	</script>
</body>
</html>