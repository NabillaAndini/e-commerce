<?php
	session_start();
	
	include 'connect.php';
	if (isset($_SESSION["role"]) && ($_SESSION["role"] == 'penjual')) {
		echo("<script> location.replace('errorAddStore2.php'); </script>");
		exit();
	}

	$name = pg_escape_string($_POST['name']);
	$deskripsi = pg_escape_string($_POST['desc']);
	$slogan = pg_escape_string($_POST['slogan']);
	$lokasi = pg_escape_string($_POST['location']);
	$email_penjual = $_SESSION['email'];

	$sql_cek_nama = "SELECT nama FROM TOKOKEREN.TOKO WHERE nama = '" . $name . "'" ;
	$result = pg_query($conn, $sql_cek_nama);

	if(pg_fetch_row($result) == 0) {
		$sql = "INSERT INTO TOKOKEREN.TOKO(nama, deskripsi, slogan, lokasi, email_penjual) VALUES ('" . $name . "', '" . $deskripsi . "', '" . $slogan . "', '" . $lokasi . "', '" . $email_penjual . "')";
		$result = pg_query($conn, $sql);

		if(!$result) {
			$_SESSION["error_insert_store"] = "yes";
		} else {
			$_SESSION["error_insert_store"] = "nope";
			$_SESSION["error_insert_jasa"] = "nope";
		
			$i=1;
	
			while(isset($_POST["jasa" . $i . ""])) {
				$sql_jasa = "INSERT INTO TOKOKEREN.TOKO_JASA_KIRIM(nama_toko, jasa_kirim) VALUES ('" . $name . "', '" . $_POST["jasa" . $i . ""]. "')";

				$result = pg_query($conn, $sql_jasa);

				if(!$result) {
					$errormessage = pg_last_error();
					$_SESSION["error_insert_jasa"] = "yes";
				}

				$i++;
			}
		}
	} else {
		$_SESSION["error_insert_store"] = "yes";
	}

	if(isset($_SESSION["error_insert_jasa"])) {
		if($_SESSION["error_insert_jasa"] == "nope") {
			$sql_update_penjual = "UPDATE TOKOKEREN.PELANGGAN SET is_penjual=true WHERE email = '" . $_SESSION['email'] . "'";
			pg_query($conn, $sql_update_penjual);
		} else if ($_SESSION["error_insert_jasa"] == "yes"){
			$sql_delete_store = "DELETE FROM TOKOKEREN.TOKO WHERE nama = '" . $name . "'" ;
			$sql_delete_jasa = "DELETE FROM TOKOKEREN.TOKO_JASA_KIRIM WHERE nama_toko = '" . $name . "'" ;
			pg_query($conn, $sql_delete_jasa);
			pg_query($conn, $sql_delete_store);
		}

	}
	header("Location: addStore.php");
?>

