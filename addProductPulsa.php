<?php include "headerAfterLogin.php"; 

  	if(isset($_SESSION["role"]) && !($_SESSION["role"] == "admin")) {
		echo("<script> location.replace('errorProductPulsa.php'); </script>");
	}
?>

<div class="container-fluid" style="bottom: 0;">
	<div class="row" id="productPulsa">
	<?php
		if(isset($_SESSION["error_insert_pulsa"])){
	  		if($_SESSION["error_insert_pulsa"] == "nope") {
	    			echo(" 
  						<div id='messages' class= 'alert alert-success alert-dismissible' role='alert' style='position: absolute; z-index: 1000;top: 50%; left: 50%; margin-top: -9em; margin-left: -15em; padding: 70px; background-color: #bdecf0; border: 1px solid #3dc7d4; font-family: 'Slabo 27px', serif;'>
          					<div><button type='button' class='close' data-dismiss='alert' aria-label='Close'>&times;</button>
          				 	Success! Prepay Product Has Been Added! </div>
        				</div>");
			} else {
				echo(" 
  						<div id='messages' class= 'alert alert-success alert-dismissible' role='alert' style='position: absolute; z-index: 1000;top: 50%; left: 50%; margin-top: -9em; margin-left: -15em; padding: 30px; background-color: #f0c1bd; border: 1px solid red;'>
          					<button type='button' class='close' data-dismiss='alert' aria-label='Close' style='margin-left: 300px; color: red;'>&times;</button>
					<div style='font-size: 17px; color: red;'>" . $_SESSION['error_insert_pulsa'] . " </div>
    			</div> ");

			}
			unset($_SESSION["error_insert_pulsa"]);

		}

	?>
		<div class="container" style="margin-top: 25px;">
	        <div class="col-md-6" style="color: white;">
	          	<h2 id="judul" style="text-align: center; margin-top: 30px; background-color: #228b96; font-size: 27px; padding-top: 10px; padding-bottom: 10px;"> Add Prepay Product </h2>
				<form action="cekPulsa.php" method="post" style="margin-top: 25px;">
					<div class="form-group">
						<label for="kode">Product Code:</label>
						<input type="text" class="form-control" id="kode" placeholder="Enter product code" name="kode" required oninvalid="this.setCustomValidity('Please insert the product code')" oninput="setCustomValidity('')">
					</div>
					<div class="form-group">
						<label for="name">Product Name:</label>
						<input type="text" class="form-control" id="name" placeholder="Enter product name" name="name" required oninvalid="this.setCustomValidity('Please insert the product name')" oninput="setCustomValidity('')">
					</div>
					<div class="form-group">
						<label for="harga">Price:</label>
						<input type="text" class="form-control" id="harga" placeholder="Enter product price" name="harga" required oninvalid="this.setCustomValidity('Please insert the price of product')" oninput="setCustomValidity('')">
					</div>
					<div class="form-group">
						<label for="description">Description:</label>
						<textarea class="form-control" rows="5" id="desc" name="desc"></textarea>
					</div>
					<div class="form-group">
						<label for="nominal">Nominal:</label>
						<input type="text" class="form-control" id="nominal" placeholder="Enter product nominal" name="nominal" required oninvalid="this.setCustomValidity('Please insert the nominal of product')" oninput="setCustomValidity('')">
					</div>
					<div style="text-align:center; margin-top: 30px;">
						<button type="submit" class="btn btn-default" style="padding-left: 100px; padding-right: 100px; background-color: #1d7881; color: white; font-family: 'Philosopher', sans-serif; font-size: 15px;">Submit</button>
					</div>
				</form>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-5" style="color: white; font-family: 'Philosopher', sans-serif; font-size: 15px;">
				<div class="row">
					<h2 style="margin-bottom: 20px;"> Top telecom companies in Indonesia </h2>
					<p style="margin-bottom: 30px;"> The telecommunications sector in Indonesia has started a phase of development after quite a long period of uncertainty. Although any country with a population like Indonesia’s (240,000,000 people) is a large potential market, the country needs to carry on with the building of the telecom infrastructure it needs to support a large population spread over its unique geography. Today, Indonesia has started to embrace broadband, and some of its telecommunications companies are among the most important ones in the world. Here they are: </p>
				</div>
				<div class="row">
					<div class="col-md-4" style="background-image: url('img/axis.png'); background-repeat: no-repeat; background-size: 100px; min-height: 15vh; background-position: center"></div>
					<div class="col-md-4" style="background-image: url('img/hutchison.png'); background-repeat: no-repeat; background-size: 150px; min-height: 15vh; background-position: center"></div>
					<div class="col-md-4" style="background-image: url('img/xl.png'); background-repeat: no-repeat; background-size: 100px; min-height: 15vh; background-position: center"></div>
				</div>
				<div class="row">
					<div class="col-md-6" style="background-image: url('img/im3.png'); background-repeat: no-repeat; background-size: 200px; min-height: 20vh;"></div>
					<div class="col-md-6" style="background-image: url('img/telkomsel.png'); background-repeat: no-repeat; background-size: 200px; min-height: 20vh; margin-top: 14px;"></div>
				</div>
				<p> Source : http://zeendo.com/info/top-telecom-companies-in-indonesia/ </p>
			</div>
		</div>
	</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>