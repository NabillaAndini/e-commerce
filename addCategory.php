

<?php include "headerAfterLogin.php"; ?>


    <div class="container" style="margin-top:35px">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-6">
          <h2 style="margin-top:0px">Menambahkan Kategori</h2>
          <form action="" method="post">
            <div class="input-group" style="width:70%;margin-bottom:20px">
              <label>Kode Kategori Utama</label>
              <input type="text" name="kodekategori" class="form-control" placeholder="ex: K001" required><br/>

              <label>Nama Kategori Utama</label>
              <input type="text" name="kategori" class="form-control" placeholder="ex: Sepatu" required> 
            </div>

            <div id="sub">

              <div class="input-group" id="sub1" style="width:70%;margin-bottom:20px">
                <label>Kode SubKategori 1</label>
                <input type="text" name="kodesub1" class="form-control" placeholder="ex: SK001" required><br/>

                <label id="labelsub1">Sub Kategori 1</label>
                <input id="subsub1" type="text" name="sub1" class="form-control" placeholder="ex: Sepatu Pria" required> 
              </div>

            </div>
            <a href="#" class="btn btn-primary" id="add-sub"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add SubCategory</a>
          </form>
        </div>
        <div class="col-md-6 well">
          <h4 style="margin-top:0px;  ">3 Simple Tips for Site Navigation and Category Structure</h4>
          by: Jacob King<br/><br/>

          <p><b>Start at the Beginning</b><br/>

            Your customer’s ability to easily find what they’re looking for largely depends on what they’re shown the minute they arrive on your homepage. It’s important to naturally categorize your products or services at the very top of the page. For example, if you sell a variety of athletic rehab supplies, then a natural, top-level category structure would probably separate braces from muscle creams. This not only makes it easy for customers to know which path they need to take; it also helps reinforce the keyword structure of your website so search engines know to look deeper than the homepage.</p>

            <p><b>Funnel Down to the Specifics</b><br/>

            Now that your homepage menu options are in tip-top shape, we need to keep sending customers down the right purchasing funnel. What secondary ways would you segment your selection of products? Would it be by color? By material? Or by specific parts of the body? </p>

            <p><b>Ask Customers What They Thought</b><br/>

            Once you’ve completed that transaction, the order has shipped, and a week or so has passed, be sure to send follow-up emails to see what customers thought of the purchasing process. Was it easy to find the right category from the start? What about subcategories? Were products well organized on the page? You work on your store so often that you’re bound to miss details that a neutral third party will pick up, and having that objective eye is important.</p><br/>

          <i>source: http://www.volusion.com/ecommerce-blog/articles/easy-tips-for-site-navigation-and-category-structure/</i>
        </div>
      </div>

<?php include "footerAfterLogin.php"; ?>