
      <hr>

      <footer>
        <p>&copy; 2017 TokoKeren, Inc.</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src= "https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"> </script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js" type="text/javascript"> </script>

    <script>
      $(document).ready(function() {
        var i = 1;
        $("#add-sub").click(function() {
          i++;
          $("#sub").append('<hr><div class="input-group" id="sub'+i+'" style="width:70%;margin-bottom:20px"><label>Kode SubKategori '+i+'</label><input type="text" name="kodesub'+i+'" class="form-control" placeholder="ex: SK001" required><br/><label id="labelsub'+i+'">Sub Kategori '+i+'</label> <input id="subsub'+i+'" type="text" name="sub'+i+'" class="form-control" placeholder="ex: Sepatu Pria" required> </div>');
        });
      });
    </script>
  </body>
</html>
