<?php
session_start();
if(!isset($_SESSION['role']) or $_SESSION['role'] !== 'pembeli'){
  echo 'not authenticated';
  return;
}
?>

<?php if(isset($_POST['kategori']) and isset($_POST['kategori']) and $_POST['kategori'] !== "" and $_POST['subkategori'] !== ""): ?>

  <?php
  //$db = pg_connect('host=localhost dbname=sirima user=b10 password=basdat123');
 include 'connect.php';
  $query = "SELECT p.kode_produk, p.nama, p.harga, p.deskripsi, s.is_asuransi, s.stok, s.is_baru, s.harga_grosir FROM TOKOKEREN.shipped_produk s, TOKOKEREN.produk p WHERE s.kode_produk = p.kode_produk AND kategori = '".$_POST['subkategori']."' and nama_toko = '".$_POST['toko']."'";
  $result = pg_query($query);

  if (!$result) {
      echo "Problem with query " . $query . "<br/>";
      echo pg_last_error();
      exit();
  }
  if(pg_num_rows($result) < 1){
    echo "tidak ada barang dari toko " . $_POST['toko'] . " dengan kategori tersebut";
    return;
  }
  ?>
  <table id="keywords" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th><span>Kode Produk</span></th>
        <th><span>Nama Produk</span></th>
        <th><span>Harga</span></th>
        <th><span>Deskripsi</span></th>
        <th><span>Is_asuransi</span></th>
        <th><span>Stok</span></th>
        <th><span>Is Baru</span></th>
        <th><span>Harga Grosir</span></th>
        <th><span>Beli</span></th>
      </tr>
    </thead>
    <tbody>
      <?php while($row = pg_fetch_assoc($result)) { ?>
      <tr>
        <td class="lalign"><?php echo $row['kode_produk']; ?></td>
        <td><?php echo $row['nama']; ?></td>
        <td id="harga_<?php echo $row['kode_produk']; ?>"><?php echo $row['harga']; ?></td>
        <td><?php echo $row['deskripsi']; ?></td>
        <td ><?php if($row['is_asuransi'] == "t") echo "TRUE"; else echo "FALSE";?></td>
        <td id="stok_<?php echo $row['kode_produk']; ?>"><?php echo $row['stok']; ?></td>
        <td><?php if($row['is_baru'] == "t") echo "TRUE"; else echo "FALSE";?></td>
        <td><?php echo $row['harga_grosir']; ?></td>
        <td id="button_<?php echo $row['kode_produk']; ?>">
          <?php
          $query = "SELECT kode_produk  FROM TOKOKEREN.keranjang_belanja WHERE kode_produk = '".$row['kode_produk']."' AND pembeli = '".$_SESSION['email']."'";
          $result2 = pg_query($query);

          if (!$result2) {
              echo "Problem with query " . $query . "<br/>";
              echo pg_last_error();
              exit();
          }
          if(pg_num_rows($result2) > 0){
            echo '<button type="button" class="btn btn-danger " disabled>ADA DI CART</button>';
          }else if($row['stok'] <1){
            echo '<button type="button" class="btn btn-danger " disabled>KOSONG</button>';
          }else {
            echo'<button type="button" class="btn btn-info " onClick="beli(\''.$row['kode_produk'].'\')">BELI</button>';
          }
          ?>
        </td>

      </tr>
      <?php }?>

    </tbody>
  </table>
<?php else: ?>
  harap pilih kategori terlebih dahulu
<?php endif; ?>
