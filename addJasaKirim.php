<?php include "headerAfterLogin.php"; 
  include 'connect.php';
  pg_query($conn, "set search_path to tokokeren");
  $namajasakirim_has_error= "";
  $namajasakirim_feedback= "";
  $namajasakirim_message= "";
  $namajasakirim_value = "";

  $lamapengiriman_has_error= "";
  $lamapengiriman_feedback= "";
  $lamapengiriman_message= "";
  $lamapengiriman_value = "";

  $tarif_has_error= "";
  $tarif_feedback= "";
  $tarif_message= "";
  $tarif_value = "";

  $alert = "";
  if(isset($_POST['namajasakirim'])){
    $has_error = false;

    $namajasakirim_value = $_POST['namajasakirim'];
    $lamapengiriman_value = $_POST['lamapengiriman'];
    $tarif_value = $_POST['tarif'];

    $namajasakirim = pg_escape_string($conn, $namajasakirim_value);
    $lamapengiriman = pg_escape_string($conn, $lamapengiriman_value);
    $tarif = pg_escape_string($conn, $tarif_value);

    if($namajasakirim == ""){
      $has_error = true;
      $namajasakirim_has_error= "has-error has-feedback";
      $namajasakirim_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $namajasakirim_message= '<span class="help-block">Nama Jasa Kirim Tidak Boleh Kosong!</span>';
    }else {
      $query = "SELECT * from jasa_kirim where nama='".$namajasakirim."';";
      $result = pg_query($conn, $query);
      if(pg_num_rows($result) > 0){
        $has_error = true;
        $namajasakirim_has_error= "has-error has-feedback";
        $namajasakirim_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
        $namajasakirim_message= '<span class="help-block">Nama Jasa Kirim Sudah Ada!</span>';
      }else {
        $namajasakirim_has_error= "has-success has-feedback";
        $namajasakirim_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';  
      }
    }

    if($lamapengiriman == ""){
      $has_error = true;
      $lamapengiriman_has_error= "has-error has-feedback";
      $lamapengiriman_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $lamapengiriman_message= '<span class="help-block">Lama Pengiriman Tidak Boleh Kosong!</span>';
    }else if(strlen($lamapengiriman) > 10){
      $has_error = true;
      $lamapengiriman_has_error= "has-error has-feedback";
      $lamapengiriman_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $lamapengiriman_message= '<span class="help-block">Lama Pengiriman Terlalu Panjang!</span>';
    }else {
      $lamapengiriman_has_error= "has-success has-feedback";
      $lamapengiriman_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';
    }

    if($tarif == ""){
      $has_error = true;
      $tarif_has_error= "has-error has-feedback";
      $tarif_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $tarif_message= '<span class="help-block">Tarif Tidak Boleh Kosong!</span>';
    }else if (!is_numeric($tarif)) {
      $has_error = true;
      $tarif_has_error= "has-error has-feedback";
      $tarif_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $tarif_message= '<span class="help-block">Masukkan tarif dalam format angka!</span>';
    }else {
      $tarif_has_error= "has-success has-feedback";
      $tarif_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';
    }

    if(!$has_error){
      $query = "INSERT INTO jasa_kirim (nama, lama_kirim, tarif) 
                VALUES ('".$namajasakirim."', '".$lamapengiriman."', '".$tarif."');";
      
      $result = pg_query($conn, $query);


      if($result){
        $alert = '<div style="width:70%"class="alert alert-success alert-dismissable">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> Berhasil Menambahkan Jasa Kirim '.$namajasakirim_value.'.
          </div>
        ';
      }

      $namajasakirim_has_error= "";
      $namajasakirim_feedback= "";
      $namajasakirim_message= "";
      $namajasakirim_value = "";

      $lamapengiriman_has_error= "";
      $lamapengiriman_feedback= "";
      $lamapengiriman_message= "";
      $lamapengiriman_value = "";

      $tarif_has_error= "";
      $tarif_feedback= "";
      $tarif_message= "";
      $tarif_value = "";
    }
  }
?>

<div class="container" style="margin-top:35px">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-6">
          <h2 style="margin-top:0px">Menambahkan Jasa Kirim</h2>
          <?php echo $alert; ?>
          <form action="" method="post">
            <div class="form-group <?php echo $namajasakirim_has_error;?>" style="width:70%">
              <label>Nama Jasa Kirim</label>
              <input type="text" name="namajasakirim" class="form-control" value="<?php echo $namajasakirim_value;?>" placeholder="ex: JNE Reguler" >
              <?php echo $namajasakirim_feedback;?>
              <?php echo $namajasakirim_message;?>
            </div>
            <div class="form-group <?php echo $lamapengiriman_has_error;?>" style="width:70%;margin-bottom:20px">
              <label>Lama Pengiriman (dalam hari)</label>
              <input type="text" name="lamapengiriman" value="<?php echo $lamapengiriman_value;?>" class="form-control" placeholder="ex:1-2 "> 
              <?php echo $lamapengiriman_feedback;?>
              <?php echo $lamapengiriman_message;?>
            </div>
            <div class="form-group <?php echo $tarif_has_error;?>" style="width:70%;margin-bottom:20px">
              <label>Tarif</label> 
              <input type="text" name="tarif" class="form-control" value="<?php echo $tarif_value;?>" placeholder="ex: 8000.00"> 
              <?php echo $tarif_feedback;?>
              <?php echo $tarif_message;?>
            </div>

            <button class="btn btn-primary" type="submit" id="add-sub"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Jasa Kirim</button>
          </form>
        </div>
    </div>
    
<?php include "footerAfterLogin.php"; ?>