<?php
		if(!isset($_GET['kode'])){
            header('location: dashboard.php');
		}


  
  include 'connect.php';
  $kode = pg_escape_string($conn, $_GET['kode']);
  pg_query($conn, "set search_path to tokokeren");
  $result = pg_query($conn, "select kode_produk from produk where kode_produk = '".$kode."';");
?>

<?php include "headerAfterLogin.php"; 
 
  if($row=pg_fetch_row($result)){
    $result = pg_query($conn, "select kode_produk from list_item l, transaksi_shipped s where s.no_invoice = l.no_invoice and s.email_pembeli = '".$_SESSION['email']."' and kode_produk = '".$kode."';");
    
    $row=pg_fetch_row($result);
    if(!$row){
      
        echo "<script>alert('anda tidak pernah membeli produk ini, anda hanya bisa mengulas produk yang sudah pernah anda beli'); location.replace('dashboard.php')</script>";
    }
    $result = pg_query($conn, "select kode_produk from ulasan where email_pembeli = '".$_SESSION['email']."' and kode_produk = '".$kode."';");

    $row=pg_fetch_row($result);

    if($row){
      echo "<script>alert('anda sudah pernah mengulas produk ini'); location.replace('dashboard.php')</script>";
      return;
    }
  }else {
     echo "<script>location.replace('dashboard.php')</script>";
     return;
  }

  $rating_has_error= "";
  $rating_feedback= "";
  $rating_message= "";
  $rating_value = "";

  $komentar_has_error= "";
  $komentar_feedback= "";
  $komentar_message= "";
  $komentar_value = "";

  if(isset($_POST['rating']) and isset($_POST['komentar']) ){
    $rating_value = $_POST['rating'];
    $komentar_value = $_POST['komentar'];

    $rating = pg_escape_string($conn, $rating_value);
    $komentar = pg_escape_string($conn, $komentar_value);

    $has_error = false;

    if($rating == ""){
      $has_error = true;
      $rating_has_error= "has-error has-feedback";
      $rating_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $rating_message= '<span class="help-block">Pilih rating terlebih dahulu!</span>';
    }else if($rating != "1" and $rating != "2" and $rating != "3" and $rating != "4" and $rating != "5"){
      $has_error = true;
      $rating_has_error= "has-error has-feedback";
      $rating_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $rating_message= '<span class="help-block">Isi raing dengan nilat 1 - 5!</span>';
    }else {
      $rating_has_error= "has-success has-feedback";
      $rating_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';   
    }

    if($komentar == ""){
      $has_error = true;
      $komentar_has_error= "has-error has-feedback";
      $komentar_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $komentar_message= '<span class="help-block">Komentar tidak boleh kosong!</span>';
    }else {
      $komentar_has_error= "has-success has-feedback";
      $komentar_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';   
    }

    if(!$has_error){
      $result = pg_query($conn, "INSERT INTO ulasan(email_pembeli, kode_produk, tanggal, rating, komentar) VALUES ('".$_SESSION['email']."','".$_GET['kode']."', '".date('Y-m-d')."', '".$rating."','".$komentar."')");

      if($result){
        echo "<script>alert('sukses mengulas produk ".$_GET['kode']."'); location.replace('dashboard.php')</script>";
      }else {
        return;
      }
    }
  } 


?>

<div class="container" style="margin-top:35px">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-6">
          <h2 style="margin-top:0px">Form Ulasan</h2>
          <form action="" method="post">
            <div class="input-group <?php echo $kodeproduk_has_error;?>" style="width:70%;">
              <label>Kode produk</label>
              <input type="text" name="kodeproduk" class="form-control" value="<?php echo $_GET['kode'];?>" placeholder="ex: S0000046 " readonly>
            </div>
            <div class="input-group <?php echo $rating_has_error;?>" style="width:70%;">
              <label>Rating</label>
               <select class="form-control" name="rating" value="" id="dropdown_rating" >
                <option value="">Silahkan Pilih rating</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
              <?php echo $rating_feedback;?>
              <?php echo $rating_message;?>
            </div>
            <div class="input-group <?php echo $komentar_has_error;?>" style="width:70%;">
               <label>Komentar</label>
              <input type="text" name="komentar" class="form-control" value="<?php echo $komentar_value;?>" placeholder="ex: Terjamin terbaik" > 
              <?php echo $komentar_feedback;?>
              <?php echo $komentar_message;?>
            </div>
            <input type="submit" class="btn btn-primary" id="add-sub">
          </form>
        </div>
    </div>
    
<?php include "footerAfterLogin.php"; ?>
<script>
  $(document).ready(function(){
    $('#dropdown_rating').val("<?php echo $rating_value; ?>");
  });
</script>