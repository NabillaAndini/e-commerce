<?php include "headerAfterLogin.php"; 
	
	if(isset($_SESSION["role"])) {
	  	if($_SESSION["role"] == "pembeli") {
			echo("<script> location.replace('addStore.php'); </script>");
		}
	}
?>

<div class="container" style="margin-top:35px;">
	<div class="row">
        <div class="col-md-6" style="background: rgba(0, 0, 0, 0.7); padding-bottom: 30px; padding-right: 15px; border-radius: 10px;">
          	<p style="color: white; margin-top: 30px; font-family: 'Philosopher', sans-serif; font-size: 19px;"> Sorry, this form for customer only! Admin prohibited! </p>
          	<button onclick="goBack()" id="back"> Back </button>
		</div>

		<div class="col-md-1"></div>

        <div class="col-md-5 well">
          	<h4 style="margin-top:0px; font-family: 'Slabo 27px', serif; font-size: 25px;"> Makes Your Own Amazing Store !</h4>

          	<div class="panel-group" id="accordion">
          		<div class="panel panel-default">
          			<div class="panel-heading" id="button">
          				<h4 class="panel-title">
          					<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" style="text-decoration: none; font-family: 'Slabo 27px', serif; font-size: 18px;">Step 1: The Business Plan</a>
          				</h4>
          			</div>
          			<div id="collapse1" class="panel-collapse collapse in">
          				<div class="panel-body">
            				Would you open a brick and mortar business without a business plan? You should not attempt to start an online retail store without one either. A business plan is critical to developing the roadmap and framework necessary to establish a successful business. A business plan for an online business should include the approach that will be used for financing, marketing, and advertising of the business. It should also establish a clear idea of how it will reach success. If you need funding, you will also require this business plan to get venture capitalists on board with your ideas. These elements don't become any less important just because you're saving overhead on facility costs.
            			</div>
            		</div>
        		</div>
	        	<div class="panel panel-default">
	        		<div class="panel-heading" id="button">
	        			<h4 class="panel-title">
	            			<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" style="text-decoration: none; font-family: 'Slabo 27px', serif; font-size: 18px;">Step 2: Choosing an Online Retail Business Structure</a>
	            		</h4>
	            	</div>
	            	<div id="collapse2" class="panel-collapse collapse">
	        			<div class="panel-body">
	            			After establishing a plan for your online business, you will need to choose an appropriate business structure. If you are unsure of the structure that is best for your company, consider the tax benefits and requirements of each. The IRS website can help you with this decision. After making this decision, you will be able to file all of the documentation to make your business and its name legal. This is generally the end of the offline process. Now, it’s time to take the steps to get your store established online. That's why you should choose TOKOKEREN and fill the form beside!
	            		</div>
	            	</div>
	            </div>
	            <div class="panel panel-default">
	            	<div class="panel-heading" id="button">
	            		<h4 class="panel-title">
	            			<a data-toggle="collapse" data-parent="#accordion" href="#collapse3" style="text-decoration: none; font-family: 'Slabo 27px', serif; font-size: 18px;">Step 3: Choosing the Products You Will Sell</a>
	            		</h4>
	            	</div>
	            	<div id="collapse3" class="panel-collapse collapse">
	        			<div class="panel-body">
	            			Once you’ve completed that transaction, the order has shipped, and a week or so has passed, be sure to send follow-up emails to see what customers thought of the purchasing process. Was it easy to find the right category from the start? What about subcategories? Were products well organized on the page? You work on your store so often that you’re bound to miss details that a neutral third party will pick up, and having that objective eye is important. And to add your product here, go to "add product" on menu bar!
	            		</div>
	            	</div>
	            </div>

	            <div class="panel panel-default">
	            	<div class="panel-heading" id="button">
	            		<h4 class="panel-title">
	            			<a data-toggle="collapse" data-parent="#accordion" href="#collapse4" style="text-decoration: none; font-family: 'Slabo 27px', serif; font-size: 18px;">Step 4: Getting Bar Codes for Your Products</a>
	           			</h4>
	            	</div>
	            	<div id="collapse4" class="panel-collapse collapse">
	        			<div class="panel-body">
	            			To legally sell products to consumers through an online store, you will need UPC bar codes for each item you are selling. Does your product come in different sizes, colors, or patterns? You will need a separate barcode for each product, with each combination of features. Luckily, purchasing barcodes can usually be done easily online. You can even buy large batches of consecutive codes at a discount through various online services.
	            		</div>
	            	</div>
	            </div>

	            <div class="panel panel-default">
	            	<div class="panel-heading" id="button">
	            		<h4 class="panel-title">
	            			<a data-toggle="collapse" data-parent="#accordion" href="#collapse5" style="text-decoration: none; font-family: 'Slabo 27px', serif; font-size: 18px;">Step 5: Building Inventory</a>
	           			</h4>
	            	</div>
	            	<div id="collapse5" class="panel-collapse collapse">
	        			<div class="panel-body">
	            			If you are just establishing yourself online, you will need to have a decent sized inventory of products. This is true regardless of whether your ecommerce store is your only storefront or if you have a physical location as well. Nothing can make an online business lose money quite like having a surplus of backed up orders. In addition to establishing your inventory, you will need to make decisions about storage. The good news is that even if you do not have the money for a warehouse yourself, many companies will store your items and ship them out as orders come in for an affordable rate.<br><br>
	           				All step sumber : http://ecommerce-platforms.com/ecommerce-selling-advice/10-easy-steps-to-establishing-an-online-retail-store
	           			</div>
	           		</div>
	           	</div>
	        </div>
	    </div>
    </div>
</div>

<script src="js/jquery.js"></script>
  	<script>
		function goBack() {
    		window.history.back();
		}
	</script>


<?php include "footerAfterLogin.php"; ?>