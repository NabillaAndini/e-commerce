

<?php include "headerAfterLogin.php";
	if($_SESSION['role'] !== 'pembeli'){
		header('location: dashboard.php');
		echo' <script> location.replace("dashboard.php"); </script>';
		return;
	}
	if(!isset($_GET['toko'])){
		echo' <script> location.replace("selectToko.php"); </script>';
		return;
	}
	//$db = pg_connect('host=localhost dbname=sirima user=b10 password=basdat123');
	include 'connect.php';
	$query = "SELECT nama FROM TOKOKEREN.toko WHERE nama = '".$_GET['toko']."'";
	$result = pg_query($query);
	if (!$result) {
			echo "Problem with query " . $query . "<br/>";
			echo pg_last_error();
			exit();
	}

	if(pg_num_rows($result) <= 0){
		echo' <script> location.replace("selectToko.php"); </script>';
		return;
	}

	$query = "SELECT K.kode as kode_kategori, K.nama as nama_kategori, S.kode as kode_sub, S.nama as nama_sub FROM TOKOKEREN.kategori_utama K, TOKOKEREN.sub_kategori S  WHERE k.kode = s.kode_kategori";


	$result = pg_query($query);
	if (!$result) {
			echo "Problem with query " . $query . "<br/>";
			echo pg_last_error();
			exit();
	}
	$categories = array();
	while($row = pg_fetch_assoc($result)){
		if(!isset($categories[$row['kode_kategori']])){
			$categories[$row['kode_kategori']] = array();
			$categories[$row['kode_kategori']]['nama'] = $row['nama_kategori'];
			$categories[$row['kode_kategori']]['subs'] = array();
		}
		$categories[$row['kode_kategori']]['subs'][$row['kode_sub']] = $row['nama_sub'];
	}


?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>

body{
  margin:0;
}
select[disabled]{
  color:#aaa;
}
h1{color:#563d7c;}
.navbar-inverse{
        background-color: #c0392b;
        color: #eeeeee !important;
      }

      .navbar-inverse .navbar-brand {
        color: #eeeeee !important;
      }

.form-style-1 {
    margin:10px auto;
    max-width: 400px;
    padding: 20px 12px 10px 20px;
    font: 13px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
}
.form-style-1 li {
    padding: 0;
    display: block;
    list-style: none;
    margin: 10px 0 0 0;
}
.form-style-1 label{
    margin:0 0 3px 0;
    padding:0px;
    display:block;
    font-weight: bold;
}
.form-style-1 input[type=text],
.form-style-1 input[type=date],
.form-style-1 input[type=datetime],
.form-style-1 input[type=number],
.form-style-1 input[type=search],
.form-style-1 input[type=time],
.form-style-1 input[type=url],
.form-style-1 input[type=email],
textarea,
select{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    border:1px solid #BEBEBE;
    padding: 7px;
    margin:0px;
    -webkit-transition: all 0.30s ease-in-out;
    -moz-transition: all 0.30s ease-in-out;
    -ms-transition: all 0.30s ease-in-out;
    -o-transition: all 0.30s ease-in-out;
    outline: none;
}
.form-style-1 input[type=text]:focus,
.form-style-1 input[type=date]:focus,
.form-style-1 input[type=datetime]:focus,
.form-style-1 input[type=number]:focus,
.form-style-1 input[type=search]:focus,
.form-style-1 input[type=time]:focus,
.form-style-1 input[type=url]:focus,
.form-style-1 input[type=email]:focus,
.form-style-1 textarea:focus,
.form-style-1 select:focus{
    -moz-box-shadow: 0 0 8px #88D5E9;
    -webkit-box-shadow: 0 0 8px #88D5E9;
    box-shadow: 0 0 8px #88D5E9;
    border: 1px solid #88D5E9;
}
.form-style-1 .field-divided{
    width: 49%;
}

.form-style-1 .field-long{
    width: 100%;
}
.form-style-1 .field-select{
    width: 100%;
}
.form-style-1 .field-textarea{
    height: 100px;
}
.form-style-1 input[type=submit], .form-style-1 input[type=button]{
    background: #4B99AD;
    padding: 8px 15px 8px 15px;
    border: none;
    color: #fff;
}
.form-style-1 input[type=submit]:hover, .form-style-1 input[type=button]:hover{
    background: #4691A4;
    box-shadow:none;
    -moz-box-shadow:none;
    -webkit-box-shadow:none;
}
.form-style-1 .required{
    color:red;
}
@import 'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300';
#keywords {
  margin: 0 auto;
  font-size: 0.8em;
  margin-bottom: 15px;
}


#keywords thead {
  cursor: pointer;
  background: #c9dff0;
}
#keywords thead tr th {
  font-weight: bold;
  padding: 12px 20px;
  padding-left: 12px;
}
#keywords thead tr th span {
  padding-right: 10px;
  background-repeat: no-repeat;
  background-position: 100% 100%;
}

#keywords thead tr th.headerSortUp, #keywords thead tr th.headerSortDown {
  background: #acc8dd;
}

#keywords thead tr th.headerSortUp span {
  background-image: url('http://i.imgur.com/SP99ZPJ.png');
}
#keywords thead tr th.headerSortDown span {
  background-image: url('http://i.imgur.com/RkA9MBo.png');
}


#keywords tbody tr {
  color: #555;
}
#keywords tbody tr td {
  text-align: center;
  padding: 15px 10px;
}
#keywords tbody tr td.lalign {
  text-align: left;
}
.form-style-5{
    max-width: 900px;
    padding: 10px 20px;
    background: #f4f7f8;
    margin: 10px auto;
    padding: 20px;
    background: #f4f7f8;
    border-radius: 8px;
    font-family: Georgia, "Times New Roman", Times, serif;
}
.form-style-5 fieldset{
    border: none;
}
.form-style-5 legend {
    font-size: 1.4em;
    margin-bottom: 10px;
}
.form-style-5 label {
    display: block;
    margin-bottom: 8px;
}
.form-style-5 input[type="text"],
.form-style-5 input[type="date"],
.form-style-5 input[type="datetime"],
.form-style-5 input[type="email"],
.form-style-5 input[type="number"],
.form-style-5 input[type="search"],
.form-style-5 input[type="time"],
.form-style-5 input[type="url"],
.form-style-5 textarea,
.form-style-5 select {
    font-family: Georgia, "Times New Roman", Times, serif;
    background: rgba(255,255,255,.1);
    border: none;
    border-radius: 4px;
    font-size: 16px;
    margin: 0;
    outline: 0;
    padding: 7px;
    width: 100%;
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    background-color: #e8eeef;
    color:#8a97a0;
    -webkit-box-shadow: 0 1px 0 rgba(0,0,0,0.03) inset;
    box-shadow: 0 1px 0 rgba(0,0,0,0.03) inset;
    margin-bottom: 30px;

}
.form-style-5 input[type="text"]:focus,
.form-style-5 input[type="date"]:focus,
.form-style-5 input[type="datetime"]:focus,
.form-style-5 input[type="email"]:focus,
.form-style-5 input[type="number"]:focus,
.form-style-5 input[type="search"]:focus,
.form-style-5 input[type="time"]:focus,
.form-style-5 input[type="url"]:focus,
.form-style-5 textarea:focus,
.form-style-5 select:focus{
    background: #d2d9dd;
}
.form-style-5 select{
    -webkit-appearance: menulist-button;
    height:35px;
}
.form-style-5 .number {
    background: #1abc9c;
    color: #fff;
    height: 30px;
    width: 30px;
    display: inline-block;
    font-size: 0.8em;
    margin-right: 4px;
    line-height: 30px;
    text-align: center;
    text-shadow: 0 1px 0 rgba(255,255,255,0.2);
    border-radius: 15px 15px 15px 0px;
}

.form-style-5 input[type="submit"],
.form-style-5 input[type="button"]
{
    position: relative;
    display: block;
    padding: 19px 39px 18px 39px;
    color: #FFF;
    margin: 0 auto;
    background: #1abc9c;
    font-size: 18px;
    text-align: center;
    font-style: normal;
    width: 100%;
    border: 1px solid #16a085;
    border-width: 1px 1px 3px;
    margin-bottom: 10px;
}
.form-style-5 input[type="submit"]:hover,
.form-style-5 input[type="button"]:hover
{
    background: #109177;
}

</style>
</head>
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12 text-center">
        	<div class="form-style-5">


<form id="kategori_form">
<fieldset>

<legend><span class="number">2</span> DAFTAR SHIPPED PRODUK</legend>
<label for="kategori">Kategori:</label>
<select id="kategori" name="kategori">

</select>


</select>

<label for="subkategori">Subkategori:</label>
<select id="subkategori" name="subkategori">


</select>
<input type="text" name="toko" value="<?php echo $_GET['toko']; ?>" hidden/>
    <input type="submit"   id="submitKategori" value="List Produk">
</fieldset>
<div id="products">

</div>

  <a href="cart.php?toko=<?php echo $_GET['toko'] ?>"> <input type="button" value="Lihat Keranjang Belanja" href="cart.php"/> </a>
  <script type="text/javascript">
    $('#my_modal').on('show.bs.modal', function(e) {

    //get data-id attribute of the clicked element
    var bookId = $(e.relatedTarget).data('book-id');

    //populate the textbox
    $(e.currentTarget).find('input[name="bookId"]').val(bookId);
});
  </script>
 </div>

</form>
</div>
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"></button>
          <h4 class="modal-title">FORM KUANTITAS BARANG</h4>
        </div>
        <div class="modal-body">
     <form action="" id="beli_form">
<ul class="form-style-1">
    <li><label>Kode Produk <span class="required"></span></label>
    <input type="Nomor" name="kode_beli" id="kode_beli" class="field-long" readonly/>
    </li>
    <li>
        <label>Kuantitas <span class="required">*</span></label>
        <input type="text" name="kuantitas_beli" id="kuantitas_beli" class="field-long"required />
				<span id="kuantitas_error" class="required">*Kuantitas harus dalam bentuk angka</span>
    </li>
		<li>
        <label>Berat per barang (dalam kg) <span class="required">*</span></label>
        <input type="text" name="berat_beli"  id="berat_beli" class="field-long" required />
				<span id="berat_error" class="required">*Berat barang harus dalam bentuk angka</span>
		</li>
        <input type="text" name="harga_beli"  id="harga_beli" class="field-long" readonly hidden />

    <li >
        <input id="button_beli" type="submit" value="Tambah ke keranjang!" />
    </li>
</ul>

        </div>
        <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


        </div>

</select><p />
      </div>
    </div>
  </div>
        </div>
      </div>

<?php include "footerAfterLogin.php"; ?>
<script>
	var categories = <?php echo json_encode($categories); ?>;

	$(document).ready(function(){
		var dropdown = "<option value =''> silahkan pilih kategori </option>";
		$.each(categories, function(key,value){
			dropdown += "<option value ='"+key+"'>"+value.nama+" </option>"
		});


		$('#kategori').html(dropdown);
		$('#subkategori').html("<option value =''> harap pilih kategori terlebih dahulu </option>");
		$('#subkategori').attr('disabled', true);
	});

	$('#kategori').change(function(){
		var kategori = this.value;
		if(kategori == ""){
			$('#subkategori').html("<option value =''> harap pilih kategori terlebih dahulu </option>");
			$('#subkategori').attr('disabled', true);
		}else {
			var dropdown = "<option value =''> silahkan pilih sub kategori </option>";
			$.each(categories[kategori]['subs'], function(key,value){
				dropdown += "<option value ='"+key+"'>"+value+" </option>"
			});
			$('#subkategori').html(dropdown);
			$('#subkategori').attr('disabled', false);
		}
	});

	$("#kategori_form").submit(function(event) {
     var ajaxRequest;

    event.preventDefault();

    /* Clear result div*/
    $("#products").html('Loading list of products...');
    /* Get from elements values */

    var values = $(this).serialize();
		$('#subkategori').attr('disabled', true);
		$('#kategori').attr('disabled', true);
		$('#submitKategori').attr('disabled', true);
       ajaxRequest= $.ajax({
            url: "productAJAX.php",
            type: "post",
            data: values
        });


     ajaxRequest.done(function (response, textStatus, jqXHR){
          // show successfully for submit message
          $("#products").html(response);
					$('#kategori').attr('disabled', false);
					if($('#kategori').val() !== "")
						$('#subkategori').attr('disabled', false);
					$('#submitKategori').attr('disabled', false);
					$('#keywords').DataTable();
     });

     /* On failure of request this function will be called  */
     ajaxRequest.fail(function (){

       // show error
       $("#products	").html('There is error while submit');
			 if($('#kategori').val() != "")
			 	$('#subkategori').attr('disabled', false);
			 $('#kategori').attr('disabled', false);
			 $('#submitKategori').attr('disabled', false);
     });
  });

	function isInt(value) {
	  return !isNaN(value) &&
	         parseInt(Number(value)) == value &&
	         !isNaN(parseInt(value, 10));
	}

 	$("#beli_form").submit(function(event) {
      var ajaxRequest;

			$('#kuantitas_error').html("");
			$('#berat_error').html("");
			var error = false;
			var errorMessage = "Terdapat kesalahan:";
			if(!isInt($('#kuantitas_beli').val())){
				error = true;
				$('#kuantitas_error').html("*Kuantitas harus dalam bentuk angka");
				errorMessage += "\nKuantitas harus dalam bentuk angka";
			}else if(parseInt($('#kuantitas_beli').val()) > parseInt($('#stok_'+$('#kode_beli').val()).html())) {
				error = true;
				$('#kuantitas_error').html("*Kuantitas harus tidak melebihi stok");
				errorMessage += "\nKuantitas harus tidak melebihi stok";
			}

			if(!isInt($('#berat_beli').val())){
				error = true;
				$('#berat_error').html("*Berat harus dalam bentuk angka");
				errorMessage += "\nBerat harus dalam bentuk angka";
			}


     event.preventDefault();

		 if(error){
			 alert(errorMessage);
			 return;
		 }

		  var values = $(this).serialize();
 		$('#kuantitas_beli').attr('disabled', true);
 		$('#kode_beli').attr('disabled', true);
		$('#berat_beli').attr('disabled', true);
 		$('#button_beli').attr('disabled', true);
        ajaxRequest= $.ajax({
             url: "beliAjax.php",
             type: "post",
             data: values
         });


      ajaxRequest.done(function (response, textStatus, jqXHR){
           // show successfully for submit message

			 		$('#kuantitas_beli').attr('disabled', false);
			 		$('#kode_beli').attr('disabled', false);
					$('#berat_beli').attr('disabled', false);
			 		$('#button_beli').attr('disabled', false);
					if(response.trim() === "sukses"){
						alert("Sukses menambahkan produk "+ $('#kode_beli').val() +" ke keranjang.");
						$('#button_'+$('#kode_beli').val()).html('<button type="button" class="btn btn-danger " disabled>ADA DI CART</button>');
						$('#myModal').modal('hide');
					}else {
						alert("Terjadi kesalahan. Gagal menambahkan ke keranjang.");
					}
      });

      /* On failure of request this function will be called  */
      ajaxRequest.fail(function (){

        // show error
				$('#kuantitas_beli').attr('disabled', false);
				$('#kode_beli').attr('disabled', false);
				$('#kuantitas_beli').attr('disabled', false);
				$('#button_beli').attr('disabled', false);

				alert("Terjadi kesalahan. Gagal menambahkan ke keranjang.");
      });
 	 });

	 function beli(kode_produk){
		 $('#kode_beli').val(kode_produk);
		 $('#berat_beli').val(1);
		 $('#kuantitas_beli').val(1);
		 $('#kuantitas_error').html("");
		 $('#berat_error').html("");
		 $('#kuantitas_beli').attr('max',  $('#stok_'+kode_produk).html());
		 $('#harga_beli').val( $('#harga_'+kode_produk).html());
		 $('#myModal').modal({
 		    backdrop: 'static',
 		    keyboard: false
 		});
	 }
</script>
