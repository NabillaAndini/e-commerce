<?php 
  
  include 'connect.php';
  pg_query($conn, "set search_path to tokokeren");

  $result = pg_query($conn, "SELECT * from kategori_utama;");
  $categories = array();

  while($row = pg_fetch_assoc($result)){
    $categories[$row['kode']] = array();
    $categories[$row['kode']]['nama'] = $row['nama'];
    $categories[$row['kode']]['subs'] = array();

    $result2 = pg_query($conn, "SELECT kode, nama from sub_kategori 
                                          where kode_kategori = '". $row['kode']."';");
    while($row2 = pg_fetch_assoc($result2)){
      $categories[$row['kode']]['subs'][$row2['kode']] = $row2['nama'];

    }
  }

  $deskripsi_has_error= "";
  $deskripsi_feedback= "";
  $deskripsi_message= "";
  $deskripsi_value = "";

  $periodeawal_has_error= "";
  $periodeawal_feedback= "";
  $periodeawal_message= "";
  $periodeawal_value = "";

  $periodeakhir_has_error= "";
  $periodeakhir_feedback= "";
  $periodeakhir_message= "";
  $periodeakhir_value = "";

  $kodepromo_has_error= "";
  $kodepromo_feedback= "";
  $kodepromo_message= "";
  $kodepromo_value = "";

  $kategori_has_error= "";
  $kategori_feedback= "";
  $kategori_message= "";
  $kategori_value = "";

  $subkategori_has_error= "";
  $subkategori_feedback= "";
  $subkategori_message= "";
  $subkategori_value = "";

  $alert ="";

  if(isset($_POST['deskripsi'])){
    $has_error = false;
    $deskripsi_value = $_POST['deskripsi'];
    $periodeawal_value = $_POST['periodeawal'];
    $periodeakhir_value = $_POST['periodeakhir'];
    $kodepromo_value = $_POST['kodepromo'];
    $kategori_value = "";
    if(isset($_POST['kategori'])){
      $kategori_value = $_POST['kategori'];  
    }


    $subkategori_value = '';
    if(isset($_POST['subkategori'])){
      $subkategori_value = $_POST['subkategori'];  
    }
    

    $deskripsi = pg_escape_string($conn, $deskripsi_value);
    $periodeawal = pg_escape_string($conn, $periodeawal_value);
    $periodeakhir = pg_escape_string($conn, $periodeakhir_value);
    $kodepromo = pg_escape_string($conn, $kodepromo_value);
    $kategori = pg_escape_string($conn, $kategori_value);
    $subkategori = pg_escape_string($conn, $subkategori_value);

    if($deskripsi == ""){
      $has_error = true;
      $deskripsi_has_error= "has-error has-feedback";
      $deskripsi_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $deskripsi_message= '<span class="help-block">Deskripsi Tidak Boleh Kosong!</span>';
    }else {
      $deskripsi_has_error= "has-success has-feedback";
      $deskripsi_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';
    }

    if($periodeawal == ""){
      $has_error = true;
      $periodeawal_has_error= "has-error has-feedback";
      $periodeawal_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $periodeawal_message= '<span class="help-block">Periode Awal Tidak Boleh Kosong!</span>';
    }else {
      $date = $periodeawal;
      $t = strtotime($date);
      $d = date('Y-m-d', $t);
      if(!($d && $d === $date)){
        $has_error = true;
        $periodeawal_has_error= "has-error has-feedback";
        $periodeawal_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
        $periodeawal_message= '<span class="help-block">Harap Masukkan Sesuai Format</span>';
      }
      else{
        $periodeawal_has_error= "has-success has-feedback";
        $periodeawal_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';
      }
    }

    if($periodeakhir == ""){
      $has_error = true;
      $periodeakhir_has_error= "has-error has-feedback";
      $periodeakhir_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $periodeakhir_message= '<span class="help-block">Periode Akhir Tidak Boleh Kosong!</span>';
    }else {
      $date = $periodeakhir;
      $t = strtotime($date);
      $d = date('Y-m-d', $t);
      if(!($d && $d === $date)){
        $has_error = true;
        $periodeakhir_has_error= "has-error has-feedback";
        $periodeakhir_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
        $periodeakhir_message= '<span class="help-block">Harap Masukkan Sesuai Format</span>';
      }
      else{
        if($periodeawal !== ""){
          $date = $periodeawal;
          $t2 = strtotime($date);
          $d = date('Y-m-d', $t2);
          if(($d && $d === $date)){
            if($t2 >= $t){
              $has_error = true;
              $periodeakhir_has_error= "has-error has-feedback";
              $periodeakhir_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
              $periodeakhir_message= '<span class="help-block">Periode akhir harus lebih lama dari periode awal</span>';
            }else {
              $periodeakhir_has_error= "has-success has-feedback";
              $periodeakhir_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';  
            }
          }else {
            $periodeakhir_has_error= "has-success has-feedback";
            $periodeakhir_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';  
          }
        }else {
          $periodeakhir_has_error= "has-success has-feedback";
          $periodeakhir_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';  
        }
      }
    }

    if($kodepromo == ""){
      $has_error = true;
      $kodepromo_has_error= "has-error has-feedback";
      $kodepromo_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $kodepromo_message= '<span class="help-block">Kode Promo Tidak Boleh Kosong!</span>';
    }else {
      $result = pg_query($conn, "SELECT * FROM PROMO WHERE kode ='".$kodepromo."';");
      if($row = pg_fetch_assoc($result)){
        $kodepromo_has_error= "has-error has-feedback";
        $kodepromo_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
        $kodepromo_message= '<span class="help-block">Kode Promo Sudah Ada!</span>';
      }else {
        $kodepromo_has_error= "has-success has-feedback";
        $kodepromo_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';  
      }
      
    }

    if($kategori == ""){
      $has_error = true;
      $kategori_has_error= "has-error has-feedback";
      $kategori_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $kategori_message= '<span class="help-block">Harap Pilih Kategori!</span>';
    }else {
      $kategori_has_error= "has-success has-feedback";
      $kategori_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';
    }

    if($subkategori == ""){
      $has_error = true;
      $subkategori_has_error= "has-error has-feedback";
      $subkategori_feedback= '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
      $subkategori_message= '<span class="help-block">Harap Pilih Sub Kategori!</span>';
    }else {
      $subkategori_has_error= "has-success has-feedback";
      $subkategori_feedback= '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';
    }

    if(!$has_error){
      $result = pg_query($conn, "SELECT count(*) as jumlah FROM PROMO ;");
      $row = pg_fetch_assoc($result);
      $last_id = intval($row['jumlah']) ;
      $current_id = sprintf("R%05d", $last_id +1);

      $result = pg_query($conn, "INSERT INTO promo (id, deskripsi, periode_awal, periode_akhir, kode) VALUES ('".$current_id."','".$deskripsi."','".$periodeawal."','".$periodeakhir."','".$kodepromo."') ;");
      
      $result = pg_query($conn, "SELECT kode_produk  FROM shipped_produk WHERE kategori = '". $subkategori."' ;");
      while($row = pg_fetch_assoc($result)){
        $result2 = pg_query($conn, "INSERT INTO promo_produk (id_promo, kode_produk) VALUES ('".$current_id."','".$row['kode_produk']."') ;");  
      }
      $alert = '<div style="width:70%"class="alert alert-success alert-dismissable">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> Berhasil Menambahkan Promo '.$kodepromo_value.'.
          </div>
        ';

    }

  }

  

?>


<?php include "headerAfterLogin.php"; ?>

<div class="container" style="margin-top:35px">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-6">
          <h2 style="margin-top:0px">Form Promo</h2>
          <form action="" method="post">
          <?php echo $alert; ?>
            <div class="form-group <?php echo $deskripsi_has_error;?>" style="width:70%;">
              <label>Deskripsi</label>
              <input type="text" name="deskripsi" class="form-control" value="<?php echo $deskripsi_value;?>" placeholder="ex: Diskon 50% untuk semua item" >
              <?php echo $deskripsi_feedback;?>
              <?php echo $deskripsi_message;?>
            </div>  
            <div class="form-group <?php echo $periodeawal_has_error;?>" style="width:70%;">
          		<label>Periode Awal</label>
              <input type="date" name="periodeawal" class="form-control" value="<?php echo $periodeawal_value;?>" >
              <?php echo $periodeawal_feedback;?>
              <?php echo $periodeawal_message;?>
            </div>
              <div class="form-group <?php echo $periodeakhir_has_error;?>" style="width:70%;">
        		<label>Periode Akhir</label>
    	       	<input type="date" name="periodeakhir" class="form-control" value="<?php echo $periodeakhir_value;?>" >
              <?php echo $periodeakhir_feedback;?>
              <?php echo $periodeakhir_message;?>
            </div>
	        <div class="form-group <?php echo $kodepromo_has_error;?>" style="width:70%;">
	            <label>Kode promo</label>
	            <input type="text" name="kodepromo" class="form-control" value="<?php echo $kodepromo_value;?>" placeholder="ex: SEMUASERBASETENGAH" >
              <?php echo $kodepromo_feedback;?>
              <?php echo $kodepromo_message;?>
	        </div>
	        <div class="form-group <?php echo $kategori_has_error;?>" style="width:70%;">
              <label>Kategori</label>
              <select class="form-control" name="kategori" value="" id="dropdown_kategori">
          
              </select>
              <?php echo $kategori_feedback;?>
              <?php echo $kategori_message;?>
          </div>
	        <div class="form-group <?php echo $subkategori_has_error;?>" style="width:70%;">
	            <label>Sub Kategori</label>
              <select class="form-control" name="subkategori" id="dropdown_subkategori">
          
              </select>  
              <?php echo $subkategori_feedback;?>
              <?php echo $subkategori_message;?>      
          </div>

            <button type="submit" class="btn btn-primary" id="add-sub"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Submit </button>
          </form>
        </div>
    </div>

<?php include "footerAfterLogin.php"; ?>
        <!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


<script>
  var categories = <?php echo json_encode($categories); ?>;
  console.log(categories);
</script>
<script>
    $(document).ready(function(){
      var date_input=$('input[name="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
      var selected = "<?php echo $kategori_value; ?>";
      var dropdown = "<option value=''>Silahkan Pilih Kategori</option>";
      var flag = false;
      $.each(categories, function(key, value){

        if(selected === key){
          flag = true;
          dropdown += "<option value='"+key+"' selected>"+value.nama+"</option>";  
        }else {
          dropdown += "<option value='"+key+"'>"+value.nama+"</option>";
        }
      });  
      $("#dropdown_kategori").html(dropdown);
      if(!flag){
        
        $("#dropdown_subkategori").html("<option value=''>Pilih kategori terlebih dahulu</option>");
        $("#dropdown_subkategori").prop("disabled", true);  
      }else {
        var dropdown = "<option value=''>Silahkan Pilih Sub Kategori</option>";
        var kategori = selected;
        $.each(categories[kategori].subs, function(key, value){       
           dropdown += "<option value='"+key+"'>"+value+"</option>";
        });  
        $("#dropdown_subkategori").html(dropdown);
        $("#dropdown_subkategori").prop("disabled", false);
        
      }
      
    })

    $("#dropdown_kategori").change(function () {
        var dropdown = "<option value=''>Silahkan Pilih Sub Kategori</option>";
        var kategori = this.value;
        if(kategori !== ""){
          $.each(categories[kategori].subs, function(key, value){       
             dropdown += "<option value='"+key+"'>"+value+"</option>";
          });  
          $("#dropdown_subkategori").html(dropdown);
          $("#dropdown_subkategori").prop("disabled", false);
        }else {
          $("#dropdown_subkategori").html("<option value=''>Pilih kategori terlebih dahulu</option>");
          $("#dropdown_subkategori").prop("disabled", true);    
        }
    });
</script>

