--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.6
-- Dumped by pg_dump version 9.5.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: tokokeren; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tokokeren;


ALTER SCHEMA tokokeren OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: check_point(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION check_point() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
total NUMERIC(10,2);
poins INT;
BEGIN
IF(TG_OP = 'UPDATE') THEN 
SELECT TS.total_bayar INTO total
FROM TRANSAKSI_SHIPPED TS 
WHERE NEW.no_invoice = TS.no_invoice;
IF(NEW.status = 4) THEN
SELECT poin INTO poins
FROM PELANGGAN
WHERE NEW.email_pembeli = email;
IF (OLD.STATUS != 4) THEN
IF(NEW.status = 4 AND poins IS NULL) THEN
UPDATE PELANGGAN SET poin = (0.01 * total)
WHERE NEW.email_pembeli = email;
ELSE IF(NEW.status = 4 AND poins IS NOT NULL) THEN
UPDATE PELANGGAN SET poin = (0.01 * total) + poin
WHERE NEW.email_pembeli = email;
END IF;
RETURN NEW;
END IF;
RETURN NEW;
END IF;
RETURN NEW;
END IF;
RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION public.check_point() OWNER TO postgres;

--
-- Name: stock_after_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION stock_after_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
stock INT;
BEGIN	
IF(TG_OP='INSERT') THEN
SELECT stok INTO stock
FROM SHIPPED_PRODUK
WHERE kode_produk = NEW.kode_produk;	
IF(NEW.kuantitas > stock) THEN
UPDATE SHIPPED_PRODUK
SET stok = 0
WHERE kode_produk = NEW.kode_produk;
NEW.kuantitas := stock;
ELSE
UPDATE SHIPPED_PRODUK
SET stok = stok - NEW.kuantitas
WHERE kode_produk = NEW.kode_produk;
END IF;
ELSIF(TG_OP='DELETE') THEN
UPDATE SHIPPED_PRODUK
SET stok = stok + OLD.kuantitas
WHERE kode_produk = OLD.kode_produk;
RETURN OLD;
ElSIF(TG_OP='UPDATE') THEN
UPDATE SHIPPED_PRODUK
SET stok = stok + OLD.kuantitas
WHERE kode_produk = OLD.kode_produk;
SELECT stok INTO stock
FROM SHIPPED_PRODUK
WHERE kode_produk = NEW.kode_produk;
IF(NEW.kuantitas > stock) THEN
UPDATE SHIPPED_PRODUK
SET stok = 0
WHERE kode_produk = NEW.kode_produk;
NEW.kuantitas := stock;
ELSE
UPDATE SHIPPED_PRODUK
SET stok = stok - NEW.kuantitas
WHERE kode_produk = NEW.kode_produk;
END IF;
END IF;
RETURN NEW;
END
$$;


ALTER FUNCTION public.stock_after_update() OWNER TO postgres;

SET search_path = tokokeren, pg_catalog;

--
-- Name: check_point(); Type: FUNCTION; Schema: tokokeren; Owner: postgres
--

CREATE FUNCTION check_point() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
total NUMERIC(10,2);
poins INT;
BEGIN
IF(TG_OP = 'UPDATE') THEN 
SELECT TS.total_bayar INTO total
FROM TRANSAKSI_SHIPPED TS 
WHERE NEW.no_invoice = TS.no_invoice;
IF(NEW.status = 4) THEN
SELECT poin INTO poins
FROM PELANGGAN
WHERE NEW.email_pembeli = email;
IF (OLD.STATUS != 4) THEN
IF(NEW.status = 4 AND poins IS NULL) THEN
UPDATE PELANGGAN SET poin = (0.01 * total)
WHERE NEW.email_pembeli = email;
ELSE IF(NEW.status = 4 AND poins IS NOT NULL) THEN
UPDATE PELANGGAN SET poin = (0.01 * total) + poin
WHERE NEW.email_pembeli = email;
END IF;
RETURN NEW;
END IF;
RETURN NEW;
END IF;
RETURN NEW;
END IF;
RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION tokokeren.check_point() OWNER TO postgres;

--
-- Name: stock_after_update(); Type: FUNCTION; Schema: tokokeren; Owner: postgres
--

CREATE FUNCTION stock_after_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
stock INT;
BEGIN	
IF(TG_OP='INSERT') THEN
SELECT stok INTO stock
FROM SHIPPED_PRODUK
WHERE kode_produk = NEW.kode_produk;	
IF(NEW.kuantitas > stock) THEN
UPDATE SHIPPED_PRODUK
SET stok = 0
WHERE kode_produk = NEW.kode_produk;
NEW.kuantitas := stock;
ELSE
UPDATE SHIPPED_PRODUK
SET stok = stok - NEW.kuantitas
WHERE kode_produk = NEW.kode_produk;
END IF;
ELSIF(TG_OP='DELETE') THEN
UPDATE SHIPPED_PRODUK
SET stok = stok + OLD.kuantitas
WHERE kode_produk = OLD.kode_produk;
RETURN OLD;
ElSIF(TG_OP='UPDATE') THEN
UPDATE SHIPPED_PRODUK
SET stok = stok + OLD.kuantitas
WHERE kode_produk = OLD.kode_produk;
SELECT stok INTO stock
FROM SHIPPED_PRODUK
WHERE kode_produk = NEW.kode_produk;
IF(NEW.kuantitas > stock) THEN
UPDATE SHIPPED_PRODUK
SET stok = 0
WHERE kode_produk = NEW.kode_produk;
NEW.kuantitas := stock;
ELSE
UPDATE SHIPPED_PRODUK
SET stok = stok - NEW.kuantitas
WHERE kode_produk = NEW.kode_produk;
END IF;
END IF;
RETURN NEW;
END
$$;


ALTER FUNCTION tokokeren.stock_after_update() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: jasa_kirim; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE jasa_kirim (
    nama character varying(100) NOT NULL,
    lama_kirim character varying(10) NOT NULL,
    tarif numeric(10,2) NOT NULL
);


ALTER TABLE jasa_kirim OWNER TO postgres;

--
-- Name: kategori_utama; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE kategori_utama (
    kode character(3) NOT NULL,
    nama character varying(100) NOT NULL
);


ALTER TABLE kategori_utama OWNER TO postgres;

--
-- Name: keranjang_belanja; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE keranjang_belanja (
    pembeli character varying(50) NOT NULL,
    kode_produk character(8) NOT NULL,
    berat integer NOT NULL,
    kuantitas integer NOT NULL,
    harga numeric(10,2) NOT NULL,
    sub_total numeric(10,2) NOT NULL
);


ALTER TABLE keranjang_belanja OWNER TO postgres;

--
-- Name: komentar_diskusi; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE komentar_diskusi (
    pengirim character varying(50) NOT NULL,
    penerima character varying(50) NOT NULL,
    waktu timestamp without time zone NOT NULL,
    komentar text NOT NULL
);


ALTER TABLE komentar_diskusi OWNER TO postgres;

--
-- Name: list_item; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE list_item (
    no_invoice character(10) NOT NULL,
    kode_produk character(8) NOT NULL,
    berat integer NOT NULL,
    kuantitas integer NOT NULL,
    harga numeric(10,2) NOT NULL,
    sub_total numeric(10,2) NOT NULL
);


ALTER TABLE list_item OWNER TO postgres;

--
-- Name: pelanggan; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE pelanggan (
    email character varying(50) NOT NULL,
    is_penjual boolean NOT NULL,
    nilai_reputasi numeric(10,1),
    poin integer
);


ALTER TABLE pelanggan OWNER TO postgres;

--
-- Name: pengguna; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE pengguna (
    email character varying(50) NOT NULL,
    password character varying(20) NOT NULL,
    nama character varying(100) NOT NULL,
    jenis_kelamin character(1) NOT NULL,
    tgl_lahir date NOT NULL,
    no_telp character varying(20) NOT NULL,
    alamat text NOT NULL,
    CONSTRAINT jk_check CHECK (((jenis_kelamin = 'L'::bpchar) OR (jenis_kelamin = 'P'::bpchar)))
);


ALTER TABLE pengguna OWNER TO postgres;

--
-- Name: produk; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE produk (
    kode_produk character(8) NOT NULL,
    nama character varying(100) NOT NULL,
    harga numeric(10,2) NOT NULL,
    deskripsi text
);


ALTER TABLE produk OWNER TO postgres;

--
-- Name: produk_pulsa; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE produk_pulsa (
    kode_produk character(8) NOT NULL,
    nominal integer NOT NULL
);


ALTER TABLE produk_pulsa OWNER TO postgres;

--
-- Name: promo; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE promo (
    id character(6) NOT NULL,
    deskripsi text NOT NULL,
    periode_awal date NOT NULL,
    periode_akhir date NOT NULL,
    kode character varying(20) NOT NULL
);


ALTER TABLE promo OWNER TO postgres;

--
-- Name: promo_produk; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE promo_produk (
    id_promo character(6) NOT NULL,
    kode_produk character(8) NOT NULL
);


ALTER TABLE promo_produk OWNER TO postgres;

--
-- Name: shipped_produk; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE shipped_produk (
    kode_produk character(8) NOT NULL,
    kategori character(5) NOT NULL,
    nama_toko character varying(100) NOT NULL,
    is_asuransi boolean NOT NULL,
    stok integer NOT NULL,
    is_baru boolean NOT NULL,
    min_order integer NOT NULL,
    min_grosir integer NOT NULL,
    max_grosir integer NOT NULL,
    harga_grosir numeric(10,2) NOT NULL,
    foto character varying(100) NOT NULL
);


ALTER TABLE shipped_produk OWNER TO postgres;

--
-- Name: sub_kategori; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE sub_kategori (
    kode character(5) NOT NULL,
    kode_kategori character(3) NOT NULL,
    nama character varying(100) NOT NULL
);


ALTER TABLE sub_kategori OWNER TO postgres;

--
-- Name: toko; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE toko (
    nama character varying(100) NOT NULL,
    deskripsi text,
    slogan character varying(100),
    lokasi text NOT NULL,
    email_penjual character varying(50) NOT NULL
);


ALTER TABLE toko OWNER TO postgres;

--
-- Name: toko_jasa_kirim; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE toko_jasa_kirim (
    nama_toko character varying(100) NOT NULL,
    jasa_kirim character varying(100) NOT NULL
);


ALTER TABLE toko_jasa_kirim OWNER TO postgres;

--
-- Name: transaksi_pulsa; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE transaksi_pulsa (
    no_invoice character(10) NOT NULL,
    tanggal date NOT NULL,
    waktu_bayar timestamp without time zone NOT NULL,
    status smallint,
    total_bayar numeric(10,2) NOT NULL,
    email_pembeli character varying(50) NOT NULL,
    nominal integer NOT NULL,
    nomor character varying(20) NOT NULL,
    kode_produk character(8) NOT NULL,
    CONSTRAINT sts_check CHECK (((status = 1) OR (status = 2)))
);


ALTER TABLE transaksi_pulsa OWNER TO postgres;

--
-- Name: transaksi_shipped; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE transaksi_shipped (
    no_invoice character(10) NOT NULL,
    tanggal date NOT NULL,
    waktu_bayar timestamp without time zone,
    status smallint NOT NULL,
    total_bayar numeric(10,2) NOT NULL,
    email_pembeli character varying(50) NOT NULL,
    nama_toko character varying(100) NOT NULL,
    alamat_kirim text NOT NULL,
    biaya_kirim numeric(10,2) NOT NULL,
    no_resi character(16),
    nama_jasa_kirim character varying(16) NOT NULL,
    CONSTRAINT status_check CHECK (((status = 1) OR (status = 2) OR (status = 3) OR (status = 4)))
);


ALTER TABLE transaksi_shipped OWNER TO postgres;

--
-- Name: ulasan; Type: TABLE; Schema: tokokeren; Owner: postgres
--

CREATE TABLE ulasan (
    email_pembeli character varying(50) NOT NULL,
    kode_produk character(8) NOT NULL,
    tanggal date NOT NULL,
    rating integer NOT NULL,
    komentar text
);


ALTER TABLE ulasan OWNER TO postgres;

--
-- Data for Name: jasa_kirim; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY jasa_kirim (nama, lama_kirim, tarif) FROM stdin;
JNE REGULER	1-3	8000.00
JNE YES	1	15000.00
JNE OKE	2-4	5000.00
TIKI REGULER	1-3	9000.00
POS PAKET BIASA	1-3	7000.00
POS PAKET KILAT	1-2	12000.00
WAHANA	1-3	8000.00
J&T EXPRESS	1-2	14000.00
PAHAL	2-3	7000.00
LION PARCEL	1-3	10000.00
\.


--
-- Data for Name: kategori_utama; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY kategori_utama (kode, nama) FROM stdin;
K01	Fashion Wanita
K02	Fashion Pria
K03	Fashion Muslim
K04	Fashion Anak
K05	Kecantikan
K06	Kesehatan
K07	Ibu & Bayi
K08	Rumah Tangga
K09	Handphone & Tablet
K10	Laptop & Aksesoris
K11	Komputer & Aksesoris
K12	Elektronik
K13	Kamera, Foto & Video
K14	Otomotif
K15	Olahraga
K16	Film, Musik & Game
K17	Dapur
K18	Office & Stationery
K19	Souvenir, Kado & Hadiah
K20	Mainan & Hobi
K21	Makanan & Minuman
K22	Buku
\.


--
-- Data for Name: keranjang_belanja; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY keranjang_belanja (pembeli, kode_produk, berat, kuantitas, harga, sub_total) FROM stdin;
halilil@wuturo.net	S0000001	5	2	349000.00	698000.00
tu@pevu.edu	S0000006	4	3	417000.00	1251000.00
hu@eha.co.uk	S0000011	2	3	86000.00	258000.00
pidcem@bucfauru.com	S0000016	4	2	66000.00	132000.00
vanesu@juwhuzu.gov	S0000021	2	1	65000.00	65000.00
subzola@kacjis.edu	S0000026	3	1	18000.00	18000.00
jiwuol@vi.io	S0000031	4	1	59000.00	59000.00
mewugek@sojkatta.io	S0000036	5	1	43000.00	43000.00
ze@erejanek.com	S0000041	3	1	549000.00	549000.00
il@up.co.uk	S0000046	5	2	168000.00	336000.00
eto@cenjiv.co.uk	S0000051	5	1	266000.00	266000.00
zu@guji.gov	S0000056	1	2	56000.00	112000.00
canehiler@enlo.edu	S0000061	1	1	55000.00	55000.00
ija@ub.net	S0000066	5	3	988000.00	2964000.00
fura@upipu.gov	S0000071	3	2	922000.00	1844000.00
tipdabit@ogazov.com	S0000076	3	2	79000.00	158000.00
loh@cetumi.com	S0000081	2	3	758000.00	2274000.00
ivduse@zuhew.org	S0000086	1	1	58000.00	58000.00
cito@no.edu	S0000091	3	3	76000.00	228000.00
war@fohrahsin.net	S0000096	2	1	95000.00	95000.00
faopidu@uhi.com	S0000101	5	3	66000.00	198000.00
hisudo@ohlu.edu	S0000106	3	1	911000.00	911000.00
hazut@ezhofid.co.uk	S0000111	3	3	92000.00	276000.00
ciraofi@wikvuut.io	S0000116	2	2	44000.00	88000.00
satme@lir.gov	S0000121	2	1	993000.00	993000.00
je@eno.net	S0000126	2	3	73000.00	219000.00
getat@cotehef.org	S0000131	5	2	134000.00	268000.00
fokjar@ril.gov	S0000136	3	2	339000.00	678000.00
no@ugu.com	S0000141	2	3	328000.00	984000.00
jaolo@fihotnem.net	S0000146	3	2	245000.00	490000.00
tufilco@luri.edu	S0000151	3	3	33000.00	99000.00
jude@fok.edu	S0000156	2	1	82000.00	82000.00
efuis@uc.co.uk	S0000161	4	3	69000.00	207000.00
ditka@bajwo.gov	S0000166	2	2	96000.00	192000.00
uwa@tagokraw.gov	S0000171	5	3	58000.00	174000.00
ju@zo.com	S0000176	1	3	343000.00	1029000.00
ewebeduc@heceh.co.uk	S0000181	4	3	552000.00	1656000.00
papa@jorisuz.co.uk	S0000186	1	2	81000.00	162000.00
weheb@isoho.com	S0000191	1	2	245000.00	490000.00
wimfi@derwejgen.co.uk	S0000196	4	3	368000.00	1104000.00
kehec@ebbucja.net	S0000201	3	2	734000.00	1468000.00
costoja@waw.gov	S0000206	5	2	926000.00	1852000.00
fasepjev@ogafi.net	S0000211	2	2	126000.00	252000.00
jumfel@nulueh.net	S0000216	5	2	283000.00	566000.00
tuvnej@le.io	S0000221	2	3	749000.00	2247000.00
ewe@manog.co.uk	S0000226	1	2	978000.00	1956000.00
pagmu@ka.edu	S0000231	3	3	774000.00	2322000.00
se@tedu.io	S0000236	1	1	569000.00	569000.00
miwesa@ros.org	S0000241	2	2	41000.00	82000.00
hed@cingi.org	S0000246	5	1	43000.00	43000.00
\.


--
-- Data for Name: komentar_diskusi; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY komentar_diskusi (pengirim, penerima, waktu, komentar) FROM stdin;
halilil@wuturo.net	evkosa@seh.net	2016-08-20 20:43:49	Selamat malam. Kenapa toko ini menyediakan banyak promo?
evkosa@seh.net	halilil@wuturo.net	2016-08-21 02:04:43	Siapapun siap tidak menyerah
tocepu@wela.co.uk	ugsava@ubruffal.co.uk	2016-09-02 07:08:52	Selamat malam. Bagaimana cara menukar barang?
ugsava@ubruffal.co.uk	tocepu@wela.co.uk	2016-09-03 16:01:21	Kita bisa tidak menyerah
guz@tuve.com	wed@ihienucus.com	2016-08-23 05:28:53	Selamat pagi. Kenapa produk ini tidak bisa dipakai?
wed@ihienucus.com	guz@tuve.com	2016-08-24 09:13:28	Siapapun siap benar
kehu@pom.edu	rofik@uz.edu	2016-08-01 11:58:56	Assalamualikum. Apakah barang bisa ditukar tambah?
rofik@uz.edu	kehu@pom.edu	2016-08-02 18:20:43	Anda siap menerima segalanya
na@tazevze.com	nanotat@huthu.co.uk	2016-08-05 12:16:31	Hai, apa kabar?. Apakah uang bisa di refund?
nanotat@huthu.co.uk	na@tazevze.com	2016-08-06 19:22:27	Kita siap menerima segalanya
tu@pevu.edu	oh@nip.org	2016-08-13 00:21:10	Hai semuanya. Apakah barang bisa ditukar tambah?
oh@nip.org	tu@pevu.edu	2016-08-14 18:31:06	Semua bisa benar sukses
uc@zepunu.org	migtunor@gu.edu	2016-09-11 22:58:02	Selamat sore. Kenapa toko ini menyediakan banyak promo?
migtunor@gu.edu	uc@zepunu.org	2016-09-12 03:08:54	Kita dapat tidak menyerah
sijtu@ja.io	zuzro@finapore.org	2016-09-25 18:56:33	Selamat siang. Kenapa produk ini tidak bisa dipakai?
zuzro@finapore.org	sijtu@ja.io	2016-09-26 13:31:39	Kita akan jadi jutawan
cel@map.io	niltafli@jeewedi.io	2016-09-24 06:54:28	Hai, sudah lama tidak berjumpa!. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
niltafli@jeewedi.io	cel@map.io	2016-09-25 02:22:49	Semua akan tidak menyerah
teuzu@va.net	egven@di.net	2016-10-03 05:41:36	Hai, kamu cantik hari ini. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
egven@di.net	teuzu@va.net	2016-10-04 22:43:20	Anda ingin tidak menyerah
hu@eha.co.uk	ikwuwne@kap.co.uk	2016-08-15 16:55:10	Halo. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
ikwuwne@kap.co.uk	hu@eha.co.uk	2016-08-16 03:55:20	Seluruhnya bisa tidak menyerah
dimkerra@lebog.co.uk	pid@ke.io	2016-10-19 15:05:24	Halo. Kenapa toko ini menyediakan banyak promo?
pid@ke.io	dimkerra@lebog.co.uk	2016-10-20 14:00:23	Kita akan menerima segalanya
oficaznaf@lomu.org	topa@geztik.org	2016-08-25 23:05:11	Assalamualikum. Apakah saya dapat membeli barang dengan ketentuan ini?
topa@geztik.org	oficaznaf@lomu.org	2016-08-25 23:40:02	Siapapun siap tidak menyerah
ucetaeb@zola.co.uk	liev@ravwu.co.uk	2016-10-19 06:07:49	Hai, apa kabar?. Kenapa produk ini tidak bisa dipakai?
liev@ravwu.co.uk	ucetaeb@zola.co.uk	2016-10-20 08:01:28	Anda akan dapat barang bagus
ijages@lomulel.io	fi@idarof.gov	2016-09-20 06:29:12	Selamat malam. Apakah barang bisa ditukar tambah?
fi@idarof.gov	ijages@lomulel.io	2016-09-21 00:17:48	Kita dapat menerima segalanya
pidcem@bucfauru.com	he@kodizi.co.uk	2016-10-11 14:23:58	Halo, gimana kabarnya?. Bagaimana cara menukar barang?
he@kodizi.co.uk	pidcem@bucfauru.com	2016-10-12 09:41:46	Siapapun akan jadi jutawan
anowuw@sabapmi.org	caoko@jufonima.org	2016-07-31 10:32:39	Halo, gimana kabarnya?. Kenapa produk ini tidak bisa dipakai?
caoko@jufonima.org	anowuw@sabapmi.org	2016-08-01 11:02:58	Semua ingin menerima segalanya
sadhupac@zon.edu	fiuco@po.gov	2016-09-16 11:45:07	Halo. Apakah uang bisa di refund?
fiuco@po.gov	sadhupac@zon.edu	2016-09-17 21:22:44	Semua akan benar sukses
noge@wib.com	pijejaze@bop.com	2016-09-16 20:41:07	Selamat malam. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
pijejaze@bop.com	noge@wib.com	2016-09-17 18:08:17	Seluruhnya dapat benar sukses
po@fahida.co.uk	ces@fefeit.edu	2016-10-05 19:25:48	Selamat pagi. Bagaimana cara menukar barang?
ces@fefeit.edu	po@fahida.co.uk	2016-10-06 07:11:46	Seluruhnya ingin menerima segalanya
vanesu@juwhuzu.gov	nocmafi@judda.net	2016-08-27 15:21:03	Assalamualikum. Bagaimana cara menukar barang?
nocmafi@judda.net	vanesu@juwhuzu.gov	2016-08-28 16:01:23	Seluruhnya siap benar sukses
sisi@povzurudo.io	vama@zi.net	2016-08-14 19:20:27	Hellow guys. Apakah pembelian barang dapat di cicil?
vama@zi.net	sisi@povzurudo.io	2016-08-15 21:45:58	Semua siap menerima segalanya
hovarwuh@sup.io	wiofu@ruafvu.gov	2016-10-14 04:09:50	Selamat malam. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
wiofu@ruafvu.gov	hovarwuh@sup.io	2016-10-15 00:06:40	Siapapun ingin tidak menyerah
inu@fo.gov	mavvo@jeniv.org	2016-10-09 05:34:06	Selamat sore. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
mavvo@jeniv.org	inu@fo.gov	2016-10-10 03:05:34	Anda ingin benar
pi@kekbu.io	fazgif@dir.co.uk	2016-09-23 14:38:21	Hellow guys. Apakah saya dapat membeli barang dengan ketentuan ini?
fazgif@dir.co.uk	pi@kekbu.io	2016-09-24 14:08:33	Seluruhnya ingin benar
subzola@kacjis.edu	adwior@kionanu.net	2016-08-16 05:19:02	Halo. Kenapa produk ini tidak bisa dipakai?
adwior@kionanu.net	subzola@kacjis.edu	2016-08-17 11:49:33	Kita ingin menerima segalanya
si@behab.co.uk	luzub@ep.org	2016-09-24 18:35:42	Selamat siang. Bagaimana cara menukar barang?
luzub@ep.org	si@behab.co.uk	2016-09-25 07:03:41	Anda siap benar sukses
ruemo@ru.co.uk	cor@jawip.co.uk	2016-07-26 16:35:54	Halo, gimana kabarnya?. Berapa banyak kesempatan saya memenangkan hadiah?
cor@jawip.co.uk	ruemo@ru.co.uk	2016-07-27 02:38:28	Seluruhnya bisa benar sukses
pufvep@zagicmac.io	lopvu@zudpaki.co.uk	2016-08-09 11:57:45	Selamat pagi. Kenapa produk ini tidak bisa dipakai?
lopvu@zudpaki.co.uk	pufvep@zagicmac.io	2016-08-10 10:47:48	Anda dapat menerima segalanya
nu@erajod.org	po@cahgamuji.io	2016-08-29 10:57:40	Selamat sore. Berapa banyak kesempatan saya memenangkan hadiah?
po@cahgamuji.io	nu@erajod.org	2016-08-30 19:42:12	Seluruhnya akan jadi jutawan
jiwuol@vi.io	facehfe@pidmu.com	2016-08-30 08:12:13	Hellow guys. Kenapa produk ini tidak bisa dipakai?
facehfe@pidmu.com	jiwuol@vi.io	2016-08-31 20:39:03	Seluruhnya ingin tidak menyerah
vuc@ifef.net	mek@funsako.edu	2016-07-26 01:28:23	Selamat siang. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
mek@funsako.edu	vuc@ifef.net	2016-07-27 03:20:00	Siapapun ingin benar
gaupe@uf.org	buahe@ismulgam.org	2016-07-29 04:33:25	Assalamualikum. Kenapa produk ini tidak bisa dipakai?
buahe@ismulgam.org	gaupe@uf.org	2016-07-30 20:59:07	Siapapun bisa menerima segalanya
dup@ejmameh.io	ovzedi@azpapvuc.net	2016-07-27 07:20:57	Hai, sudah lama tidak berjumpa!. Apakah barang bisa ditukar tambah?
ovzedi@azpapvuc.net	dup@ejmameh.io	2016-07-28 10:10:13	Semua ingin dapat barang bagus
erfi@zokiw.co.uk	ci@hom.com	2016-09-04 00:00:33	Selamat sore. Kenapa produk ini tidak bisa dipakai?
ci@hom.com	erfi@zokiw.co.uk	2016-09-05 02:28:57	Anda bisa benar
mewugek@sojkatta.io	dupbajov@lidhugmal.io	2016-08-21 00:40:55	Selamat sore. Kenapa produk ini tidak bisa dipakai?
dupbajov@lidhugmal.io	mewugek@sojkatta.io	2016-08-22 12:41:14	Seluruhnya siap benar
jawantu@siziin.io	how@rizaur.edu	2016-08-02 05:50:07	Hai, apa kabar?. Apakah pembelian barang dapat di cicil?
how@rizaur.edu	jawantu@siziin.io	2016-08-03 17:42:39	Semua ingin tidak menyerah
ced@ego.net	efuepiec@ow.gov	2016-09-10 11:52:01	Selamat pagi. Apakah saya dapat membeli barang dengan ketentuan ini?
efuepiec@ow.gov	ced@ego.net	2016-09-11 21:58:46	Kita bisa dapat barang bagus
eptew@os.org	azakopibo@fuew.gov	2016-10-24 16:06:40	Hai semuanya. Kenapa produk ini tidak bisa dipakai?
azakopibo@fuew.gov	eptew@os.org	2016-10-25 03:58:15	Seluruhnya ingin benar sukses
jotugve@oclekuju.io	he@huow.co.uk	2016-07-28 04:15:46	Selamat sore. Apakah uang bisa di refund?
he@huow.co.uk	jotugve@oclekuju.io	2016-07-29 20:59:00	Siapapun ingin benar
ze@erejanek.com	fi@wadbiwlo.gov	2016-08-11 01:26:24	Halo, gimana kabarnya?. Apakah barang disini semuanya original?
fi@wadbiwlo.gov	ze@erejanek.com	2016-08-12 15:45:37	Anda ingin benar
vim@kieh.gov	pisopu@fomabako.co.uk	2016-10-20 13:31:53	Selamat siang. Apakah pembelian barang dapat di cicil?
pisopu@fomabako.co.uk	vim@kieh.gov	2016-10-21 06:43:23	Anda dapat tidak menyerah
evisep@uwre.net	ut@movpuwom.net	2016-09-11 08:04:44	Halo. Apakah saya dapat membeli barang dengan ketentuan ini?
ut@movpuwom.net	evisep@uwre.net	2016-09-12 11:40:13	Siapapun dapat dapat barang bagus
hu@zusrusiw.edu	detcogu@war.edu	2016-10-23 20:19:14	Selamat sore. Kenapa toko ini menyediakan banyak promo?
detcogu@war.edu	hu@zusrusiw.edu	2016-10-24 14:53:32	Kita ingin jadi jutawan
ji@kozko.co.uk	uzpude@fisojelev.io	2016-10-24 13:45:01	Selamat siang. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
uzpude@fisojelev.io	ji@kozko.co.uk	2016-10-25 20:26:54	Semua bisa benar
il@up.co.uk	uwbew@wircihhat.gov	2016-09-25 16:45:30	Selamat siang. Apakah barang bisa ditukar tambah?
uwbew@wircihhat.gov	il@up.co.uk	2016-09-26 03:24:09	Siapapun bisa menerima segalanya
obofac@fel.edu	bebuh@sot.gov	2016-10-17 03:47:05	Halo. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
bebuh@sot.gov	obofac@fel.edu	2016-10-18 13:09:16	Seluruhnya siap dapat barang bagus
rohoci@muza.io	zab@eje.net	2016-09-25 18:56:57	Halo, gimana kabarnya?. Bagaimana cara menukar barang?
zab@eje.net	rohoci@muza.io	2016-09-25 23:32:44	Anda siap dapat barang bagus
adaerop@ono.net	adoujkum@gavro.net	2016-09-29 22:14:12	Selamat pagi. Bagaimana jika saya ingin memberikan hadiah ke teman saya?
adoujkum@gavro.net	adaerop@ono.net	2016-09-30 07:37:21	Kita bisa benar sukses
waruk@tu.org	jezrubki@masna.co.uk	2016-09-03 06:57:52	Selamat sore. Kenapa produk ini tidak bisa dipakai?
jezrubki@masna.co.uk	waruk@tu.org	2016-09-04 22:28:39	Anda ingin benar
\.


--
-- Data for Name: list_item; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY list_item (no_invoice, kode_produk, berat, kuantitas, harga, sub_total) FROM stdin;
V000000001	S0000086	3	1	58000.00	58000.00
V000000001	S0000085	1	2	864000.00	1728000.00
V000000002	S0000176	2	1	343000.00	343000.00
V000000002	S0000175	3	3	96000.00	288000.00
V000000003	S0000225	3	1	798000.00	798000.00
V000000003	S0000223	5	3	172000.00	516000.00
V000000004	S0000264	3	1	415000.00	415000.00
V000000004	S0000262	4	3	31000.00	93000.00
V000000005	S0000299	3	1	83000.00	83000.00
V000000005	S0000300	2	2	131000.00	262000.00
V000000006	S0000193	4	1	128000.00	128000.00
V000000006	S0000194	1	2	96000.00	192000.00
V000000007	S0000291	3	2	65000.00	130000.00
V000000007	S0000290	4	2	89000.00	178000.00
V000000008	S0000187	2	1	36000.00	36000.00
V000000008	S0000189	2	2	11000.00	22000.00
V000000009	S0000210	5	1	82000.00	82000.00
V000000009	S0000208	4	1	525000.00	525000.00
V000000010	S0000187	2	2	36000.00	72000.00
V000000010	S0000189	5	1	11000.00	11000.00
V000000011	S0000121	3	2	993000.00	1986000.00
V000000011	S0000122	3	3	43000.00	129000.00
V000000012	S0000297	3	3	34000.00	102000.00
V000000012	S0000296	2	3	142000.00	426000.00
V000000013	S0000238	5	2	79000.00	158000.00
V000000013	S0000239	2	3	13000.00	39000.00
V000000014	S0000191	4	3	245000.00	735000.00
V000000014	S0000190	4	2	744000.00	1488000.00
V000000015	S0000041	3	3	549000.00	1647000.00
V000000015	S0000040	1	2	44000.00	88000.00
V000000016	S0000247	4	2	87000.00	174000.00
V000000016	S0000248	3	3	33000.00	99000.00
V000000017	S0000006	2	2	417000.00	834000.00
V000000017	S0000005	4	3	961000.00	2883000.00
V000000018	S0000284	3	2	345000.00	690000.00
V000000018	S0000285	3	3	478000.00	1434000.00
V000000019	S0000078	4	2	49000.00	98000.00
V000000019	S0000077	3	2	866000.00	1732000.00
V000000020	S0000033	3	2	375000.00	750000.00
V000000020	S0000032	2	1	649000.00	649000.00
V000000021	S0000086	1	1	58000.00	58000.00
V000000021	S0000087	2	3	57000.00	171000.00
V000000022	S0000196	5	2	368000.00	736000.00
V000000022	S0000197	4	1	52000.00	52000.00
V000000023	S0000101	3	3	66000.00	198000.00
V000000023	S0000100	1	3	637000.00	1911000.00
V000000024	S0000232	5	2	99000.00	198000.00
V000000024	S0000234	2	2	29000.00	58000.00
V000000025	S0000089	2	3	44000.00	132000.00
V000000025	S0000090	2	2	51000.00	102000.00
V000000026	S0000211	3	2	126000.00	252000.00
V000000026	S0000212	3	1	82000.00	82000.00
V000000027	S0000123	1	2	747000.00	1494000.00
V000000027	S0000122	4	3	43000.00	129000.00
V000000028	S0000123	1	2	747000.00	1494000.00
V000000028	S0000122	1	2	43000.00	86000.00
V000000029	S0000117	5	1	22000.00	22000.00
V000000029	S0000115	4	2	178000.00	356000.00
V000000030	S0000020	5	3	612000.00	1836000.00
V000000030	S0000019	3	2	32000.00	64000.00
V000000031	S0000005	1	1	961000.00	961000.00
V000000031	S0000006	2	1	417000.00	417000.00
V000000032	S0000084	5	3	676000.00	2028000.00
V000000032	S0000082	5	3	147000.00	441000.00
V000000033	S0000185	2	1	35000.00	35000.00
V000000033	S0000186	1	3	81000.00	243000.00
V000000034	S0000163	3	2	534000.00	1068000.00
V000000034	S0000165	3	2	567000.00	1134000.00
V000000035	S0000119	3	3	11000.00	33000.00
V000000035	S0000118	2	3	371000.00	1113000.00
V000000036	S0000169	4	3	272000.00	816000.00
V000000036	S0000170	3	3	29000.00	87000.00
V000000037	S0000232	4	2	99000.00	198000.00
V000000037	S0000233	4	1	918000.00	918000.00
V000000038	S0000150	4	3	87000.00	261000.00
V000000038	S0000149	1	3	95000.00	285000.00
V000000039	S0000235	3	2	53000.00	106000.00
V000000039	S0000236	1	3	569000.00	1707000.00
V000000040	S0000060	3	1	24000.00	24000.00
V000000040	S0000059	1	2	69000.00	138000.00
V000000041	S0000259	1	2	53000.00	106000.00
V000000041	S0000261	1	3	753000.00	2259000.00
V000000042	S0000083	3	3	446000.00	1338000.00
V000000042	S0000084	3	3	676000.00	2028000.00
V000000043	S0000195	3	3	162000.00	486000.00
V000000043	S0000194	2	1	96000.00	96000.00
V000000044	S0000187	3	2	36000.00	72000.00
V000000044	S0000189	5	1	11000.00	11000.00
V000000045	S0000035	3	3	596000.00	1788000.00
V000000045	S0000036	2	2	43000.00	86000.00
V000000046	S0000096	2	1	95000.00	95000.00
V000000046	S0000095	1	1	824000.00	824000.00
V000000047	S0000025	4	2	91000.00	182000.00
V000000047	S0000026	1	2	18000.00	36000.00
V000000048	S0000143	4	1	33000.00	33000.00
V000000048	S0000142	4	2	156000.00	312000.00
V000000049	S0000279	5	3	687000.00	2061000.00
V000000049	S0000278	3	2	27000.00	54000.00
V000000050	S0000053	4	3	92000.00	276000.00
V000000050	S0000052	1	2	164000.00	328000.00
V000000051	S0000261	1	1	753000.00	753000.00
V000000051	S0000259	3	3	53000.00	159000.00
V000000052	S0000208	2	2	525000.00	1050000.00
V000000052	S0000209	2	1	88000.00	88000.00
V000000053	S0000240	3	2	81000.00	162000.00
V000000053	S0000239	5	2	13000.00	26000.00
V000000054	S0000062	1	1	31000.00	31000.00
V000000054	S0000063	1	2	288000.00	576000.00
V000000055	S0000189	5	3	11000.00	33000.00
V000000055	S0000188	5	2	43000.00	86000.00
V000000056	S0000179	2	3	37000.00	111000.00
V000000056	S0000180	1	1	62000.00	62000.00
V000000057	S0000213	3	2	26000.00	52000.00
V000000057	S0000212	3	2	82000.00	164000.00
V000000058	S0000016	5	2	66000.00	132000.00
V000000058	S0000018	1	3	469000.00	1407000.00
V000000059	S0000284	5	3	345000.00	1035000.00
V000000059	S0000283	3	2	938000.00	1876000.00
V000000060	S0000018	1	3	469000.00	1407000.00
V000000060	S0000017	5	2	728000.00	1456000.00
V000000061	S0000266	4	2	24000.00	48000.00
V000000061	S0000267	5	1	17000.00	17000.00
V000000062	S0000239	2	1	13000.00	13000.00
V000000062	S0000240	3	3	81000.00	243000.00
V000000063	S0000228	1	3	993000.00	2979000.00
V000000063	S0000227	2	1	88000.00	88000.00
V000000064	S0000189	2	2	11000.00	22000.00
V000000064	S0000187	4	1	36000.00	36000.00
V000000065	S0000080	2	2	42000.00	84000.00
V000000065	S0000081	2	3	758000.00	2274000.00
V000000066	S0000080	2	3	42000.00	126000.00
V000000066	S0000081	3	1	758000.00	758000.00
V000000067	S0000208	3	2	525000.00	1050000.00
V000000067	S0000210	3	1	82000.00	82000.00
V000000068	S0000180	5	1	62000.00	62000.00
V000000068	S0000179	5	3	37000.00	111000.00
V000000069	S0000004	3	3	94000.00	282000.00
V000000069	S0000005	5	3	961000.00	2883000.00
V000000070	S0000117	3	2	22000.00	44000.00
V000000070	S0000116	2	2	44000.00	88000.00
V000000071	S0000124	3	3	22000.00	66000.00
V000000071	S0000125	1	2	28000.00	56000.00
V000000072	S0000019	4	3	32000.00	96000.00
V000000072	S0000020	5	3	612000.00	1836000.00
V000000073	S0000111	2	3	92000.00	276000.00
V000000073	S0000109	1	2	595000.00	1190000.00
V000000074	S0000269	2	1	351000.00	351000.00
V000000074	S0000270	2	1	82000.00	82000.00
V000000075	S0000151	4	1	33000.00	33000.00
V000000075	S0000152	2	3	36000.00	108000.00
V000000076	S0000276	4	2	21000.00	42000.00
V000000076	S0000274	1	2	743000.00	1486000.00
V000000077	S0000057	5	2	961000.00	1922000.00
V000000077	S0000055	2	1	446000.00	446000.00
V000000078	S0000193	1	1	128000.00	128000.00
V000000078	S0000195	1	2	162000.00	324000.00
V000000079	S0000218	4	3	244000.00	732000.00
V000000079	S0000219	1	1	892000.00	892000.00
V000000080	S0000264	5	3	415000.00	1245000.00
V000000080	S0000262	4	2	31000.00	62000.00
V000000081	S0000221	4	3	749000.00	2247000.00
V000000081	S0000222	5	1	567000.00	567000.00
V000000082	S0000095	2	3	824000.00	2472000.00
V000000082	S0000094	3	3	364000.00	1092000.00
V000000083	S0000217	4	2	12000.00	24000.00
V000000083	S0000219	4	3	892000.00	2676000.00
V000000084	S0000143	1	3	33000.00	99000.00
V000000084	S0000142	1	2	156000.00	312000.00
V000000085	S0000090	3	2	51000.00	102000.00
V000000085	S0000088	2	1	87000.00	87000.00
V000000086	S0000283	3	2	938000.00	1876000.00
V000000086	S0000285	3	2	478000.00	956000.00
V000000087	S0000165	5	3	567000.00	1701000.00
V000000087	S0000163	4	2	534000.00	1068000.00
V000000088	S0000090	5	1	51000.00	51000.00
V000000088	S0000088	1	1	87000.00	87000.00
V000000089	S0000148	2	3	771000.00	2313000.00
V000000089	S0000149	1	1	95000.00	95000.00
V000000090	S0000212	2	1	82000.00	82000.00
V000000090	S0000213	1	2	26000.00	52000.00
V000000091	S0000247	4	1	87000.00	87000.00
V000000091	S0000248	5	3	33000.00	99000.00
V000000092	S0000158	3	2	452000.00	904000.00
V000000092	S0000159	4	3	14000.00	42000.00
V000000093	S0000106	3	1	911000.00	911000.00
V000000093	S0000107	5	2	36000.00	72000.00
V000000094	S0000206	3	2	926000.00	1852000.00
V000000094	S0000207	1	2	59000.00	118000.00
V000000095	S0000116	2	2	44000.00	88000.00
V000000095	S0000115	3	2	178000.00	356000.00
V000000096	S0000086	3	1	58000.00	58000.00
V000000096	S0000085	3	1	864000.00	864000.00
V000000097	S0000148	2	2	771000.00	1542000.00
V000000097	S0000149	4	3	95000.00	285000.00
V000000098	S0000138	4	3	28000.00	84000.00
V000000098	S0000136	4	3	339000.00	1017000.00
V000000099	S0000020	5	2	612000.00	1224000.00
V000000099	S0000021	1	1	65000.00	65000.00
V000000100	S0000082	3	3	147000.00	441000.00
V000000100	S0000083	5	1	446000.00	446000.00
V000000101	S0000277	1	1	499000.00	499000.00
V000000101	S0000279	3	2	687000.00	1374000.00
V000000102	S0000247	1	2	87000.00	174000.00
V000000102	S0000248	5	1	33000.00	33000.00
V000000103	S0000273	5	2	393000.00	786000.00
V000000103	S0000271	5	1	83000.00	83000.00
V000000104	S0000250	3	1	57000.00	57000.00
V000000104	S0000252	2	3	96000.00	288000.00
V000000105	S0000040	1	3	44000.00	132000.00
V000000105	S0000041	4	2	549000.00	1098000.00
V000000106	S0000205	1	3	328000.00	984000.00
V000000106	S0000207	1	3	59000.00	177000.00
V000000107	S0000158	5	2	452000.00	904000.00
V000000107	S0000159	2	3	14000.00	42000.00
V000000108	S0000168	2	2	61000.00	122000.00
V000000108	S0000166	3	1	96000.00	96000.00
V000000109	S0000256	5	3	15000.00	45000.00
V000000109	S0000257	2	1	695000.00	695000.00
V000000110	S0000130	4	1	27000.00	27000.00
V000000110	S0000132	4	3	25000.00	75000.00
V000000111	S0000089	1	1	44000.00	44000.00
V000000111	S0000088	2	3	87000.00	261000.00
V000000112	S0000043	4	3	136000.00	408000.00
V000000112	S0000044	4	2	776000.00	1552000.00
V000000113	S0000242	4	2	68000.00	136000.00
V000000113	S0000243	4	2	37000.00	74000.00
V000000114	S0000066	4	2	988000.00	1976000.00
V000000114	S0000064	5	1	14000.00	14000.00
V000000115	S0000138	3	3	28000.00	84000.00
V000000115	S0000136	4	2	339000.00	678000.00
V000000116	S0000177	4	3	442000.00	1326000.00
V000000116	S0000175	2	1	96000.00	96000.00
V000000117	S0000011	1	1	86000.00	86000.00
V000000117	S0000012	4	2	95000.00	190000.00
V000000118	S0000175	4	2	96000.00	192000.00
V000000118	S0000176	3	3	343000.00	1029000.00
V000000119	S0000159	2	3	14000.00	42000.00
V000000119	S0000157	5	1	35000.00	35000.00
V000000120	S0000266	4	2	24000.00	48000.00
V000000120	S0000265	3	2	59000.00	118000.00
V000000121	S0000052	2	1	164000.00	164000.00
V000000121	S0000054	1	2	63000.00	126000.00
V000000122	S0000230	5	3	729000.00	2187000.00
V000000122	S0000231	2	2	774000.00	1548000.00
V000000123	S0000045	3	2	222000.00	444000.00
V000000123	S0000043	4	1	136000.00	136000.00
V000000124	S0000201	3	2	734000.00	1468000.00
V000000124	S0000200	4	1	65000.00	65000.00
V000000125	S0000118	2	3	371000.00	1113000.00
V000000125	S0000119	4	2	11000.00	22000.00
V000000126	S0000128	5	1	27000.00	27000.00
V000000126	S0000127	3	1	764000.00	764000.00
V000000127	S0000168	2	2	61000.00	122000.00
V000000127	S0000166	3	3	96000.00	288000.00
V000000128	S0000259	3	2	53000.00	106000.00
V000000128	S0000260	3	1	893000.00	893000.00
V000000129	S0000025	2	2	91000.00	182000.00
V000000129	S0000027	3	1	345000.00	345000.00
V000000130	S0000260	1	2	893000.00	1786000.00
V000000130	S0000259	1	2	53000.00	106000.00
V000000131	S0000174	4	1	47000.00	47000.00
V000000131	S0000172	5	2	652000.00	1304000.00
V000000132	S0000200	3	3	65000.00	195000.00
V000000132	S0000201	4	2	734000.00	1468000.00
V000000133	S0000018	1	3	469000.00	1407000.00
V000000133	S0000016	1	3	66000.00	198000.00
V000000134	S0000222	3	1	567000.00	567000.00
V000000134	S0000221	4	1	749000.00	749000.00
V000000135	S0000294	5	1	37000.00	37000.00
V000000135	S0000292	3	1	13000.00	13000.00
V000000136	S0000176	1	2	343000.00	686000.00
V000000136	S0000177	4	3	442000.00	1326000.00
V000000137	S0000068	4	1	514000.00	514000.00
V000000137	S0000069	2	2	959000.00	1918000.00
V000000138	S0000151	5	3	33000.00	99000.00
V000000138	S0000152	3	2	36000.00	72000.00
V000000139	S0000106	2	2	911000.00	1822000.00
V000000139	S0000107	5	3	36000.00	108000.00
V000000140	S0000249	3	3	73000.00	219000.00
V000000140	S0000248	1	3	33000.00	99000.00
V000000141	S0000197	3	1	52000.00	52000.00
V000000141	S0000196	1	1	368000.00	368000.00
V000000142	S0000283	5	3	938000.00	2814000.00
V000000142	S0000285	1	1	478000.00	478000.00
V000000143	S0000019	2	3	32000.00	96000.00
V000000143	S0000020	3	2	612000.00	1224000.00
V000000144	S0000174	3	1	47000.00	47000.00
V000000144	S0000172	5	3	652000.00	1956000.00
V000000145	S0000286	1	1	273000.00	273000.00
V000000145	S0000288	3	3	166000.00	498000.00
V000000146	S0000206	2	1	926000.00	926000.00
V000000146	S0000205	3	3	328000.00	984000.00
V000000147	S0000051	3	1	266000.00	266000.00
V000000147	S0000050	2	2	11000.00	22000.00
V000000148	S0000029	4	3	83000.00	249000.00
V000000148	S0000030	1	2	147000.00	294000.00
V000000149	S0000026	2	3	18000.00	54000.00
V000000149	S0000027	2	3	345000.00	1035000.00
V000000150	S0000280	2	1	362000.00	362000.00
V000000150	S0000281	3	1	913000.00	913000.00
V000000151	S0000163	3	1	534000.00	534000.00
V000000151	S0000165	4	3	567000.00	1701000.00
V000000152	S0000123	3	2	747000.00	1494000.00
V000000152	S0000121	3	2	993000.00	1986000.00
V000000153	S0000131	4	2	134000.00	268000.00
V000000153	S0000132	2	2	25000.00	50000.00
V000000154	S0000172	5	2	652000.00	1304000.00
V000000154	S0000174	4	2	47000.00	94000.00
V000000155	S0000090	3	1	51000.00	51000.00
V000000155	S0000088	1	2	87000.00	174000.00
V000000156	S0000105	2	2	92000.00	184000.00
V000000156	S0000104	2	1	368000.00	368000.00
V000000157	S0000111	1	3	92000.00	276000.00
V000000157	S0000109	2	3	595000.00	1785000.00
V000000158	S0000248	4	1	33000.00	33000.00
V000000158	S0000247	2	1	87000.00	87000.00
V000000159	S0000219	5	3	892000.00	2676000.00
V000000159	S0000217	5	1	12000.00	12000.00
V000000160	S0000202	1	1	81000.00	81000.00
V000000160	S0000203	5	3	917000.00	2751000.00
V000000161	S0000005	3	3	961000.00	2883000.00
V000000161	S0000004	2	2	94000.00	188000.00
V000000162	S0000029	2	2	83000.00	166000.00
V000000162	S0000028	5	1	169000.00	169000.00
V000000163	S0000212	4	1	82000.00	82000.00
V000000163	S0000213	4	3	26000.00	78000.00
V000000164	S0000001	2	2	349000.00	698000.00
V000000164	S0000002	5	3	352000.00	1056000.00
V000000165	S0000146	5	1	245000.00	245000.00
V000000165	S0000147	4	3	142000.00	426000.00
V000000166	S0000154	2	1	739000.00	739000.00
V000000166	S0000155	1	2	38000.00	76000.00
V000000167	S0000162	2	2	452000.00	904000.00
V000000167	S0000161	2	3	69000.00	207000.00
V000000168	S0000025	3	2	91000.00	182000.00
V000000168	S0000026	3	3	18000.00	54000.00
V000000169	S0000256	1	2	15000.00	30000.00
V000000169	S0000258	5	2	54000.00	108000.00
V000000170	S0000028	5	3	169000.00	507000.00
V000000170	S0000030	4	2	147000.00	294000.00
V000000171	S0000045	4	3	222000.00	666000.00
V000000171	S0000044	3	1	776000.00	776000.00
V000000172	S0000299	1	2	83000.00	166000.00
V000000172	S0000300	4	1	131000.00	131000.00
V000000173	S0000292	1	3	13000.00	39000.00
V000000173	S0000294	4	3	37000.00	111000.00
V000000174	S0000141	3	2	328000.00	656000.00
V000000174	S0000139	5	3	83000.00	249000.00
V000000175	S0000265	4	2	59000.00	118000.00
V000000175	S0000267	3	2	17000.00	34000.00
V000000176	S0000282	3	1	632000.00	632000.00
V000000176	S0000280	5	3	362000.00	1086000.00
V000000177	S0000062	3	2	31000.00	62000.00
V000000177	S0000061	4	2	55000.00	110000.00
V000000178	S0000123	3	3	747000.00	2241000.00
V000000178	S0000122	5	2	43000.00	86000.00
V000000179	S0000128	2	1	27000.00	27000.00
V000000179	S0000129	2	2	44000.00	88000.00
V000000180	S0000177	1	2	442000.00	884000.00
V000000180	S0000175	1	1	96000.00	96000.00
V000000181	S0000197	4	2	52000.00	104000.00
V000000181	S0000196	3	2	368000.00	736000.00
V000000182	S0000160	4	2	52000.00	104000.00
V000000182	S0000162	1	1	452000.00	452000.00
V000000183	S0000012	1	1	95000.00	95000.00
V000000183	S0000010	1	1	99000.00	99000.00
V000000184	S0000227	2	1	88000.00	88000.00
V000000184	S0000226	4	3	978000.00	2934000.00
V000000185	S0000070	4	1	541000.00	541000.00
V000000185	S0000072	3	3	575000.00	1725000.00
V000000186	S0000054	2	3	63000.00	189000.00
V000000186	S0000053	3	1	92000.00	92000.00
V000000187	S0000222	5	2	567000.00	1134000.00
V000000187	S0000221	1	2	749000.00	1498000.00
V000000188	S0000216	3	1	283000.00	283000.00
V000000188	S0000215	3	2	286000.00	572000.00
V000000189	S0000236	4	1	569000.00	569000.00
V000000189	S0000237	4	3	925000.00	2775000.00
V000000190	S0000278	2	3	27000.00	81000.00
V000000190	S0000279	3	3	687000.00	2061000.00
V000000191	S0000131	3	3	134000.00	402000.00
V000000191	S0000130	5	3	27000.00	81000.00
V000000192	S0000172	4	3	652000.00	1956000.00
V000000192	S0000173	3	2	329000.00	658000.00
V000000193	S0000202	1	3	81000.00	243000.00
V000000193	S0000203	1	1	917000.00	917000.00
V000000194	S0000240	4	2	81000.00	162000.00
V000000194	S0000239	2	1	13000.00	13000.00
V000000195	S0000284	4	1	345000.00	345000.00
V000000195	S0000285	5	2	478000.00	956000.00
V000000196	S0000122	2	2	43000.00	86000.00
V000000196	S0000123	5	1	747000.00	747000.00
V000000197	S0000187	4	1	36000.00	36000.00
V000000197	S0000188	4	1	43000.00	43000.00
V000000198	S0000001	5	3	349000.00	1047000.00
V000000198	S0000002	5	3	352000.00	1056000.00
V000000199	S0000023	5	2	336000.00	672000.00
V000000199	S0000022	3	1	121000.00	121000.00
V000000200	S0000208	4	3	525000.00	1575000.00
V000000200	S0000210	2	1	82000.00	82000.00
V000000201	S0000066	4	1	988000.00	988000.00
V000000201	S0000064	3	2	14000.00	28000.00
V000000202	S0000092	5	2	98000.00	196000.00
V000000202	S0000093	4	1	579000.00	579000.00
V000000203	S0000108	1	2	68000.00	136000.00
V000000203	S0000107	5	1	36000.00	36000.00
V000000204	S0000069	4	1	959000.00	959000.00
V000000204	S0000067	2	2	188000.00	376000.00
V000000205	S0000029	4	1	83000.00	83000.00
V000000205	S0000030	1	1	147000.00	147000.00
V000000206	S0000110	1	3	95000.00	285000.00
V000000206	S0000109	5	3	595000.00	1785000.00
V000000207	S0000149	3	3	95000.00	285000.00
V000000207	S0000148	4	1	771000.00	771000.00
V000000208	S0000058	1	2	643000.00	1286000.00
V000000208	S0000060	3	1	24000.00	24000.00
V000000209	S0000057	1	2	961000.00	1922000.00
V000000209	S0000056	1	3	56000.00	168000.00
V000000210	S0000147	1	3	142000.00	426000.00
V000000210	S0000145	2	2	86000.00	172000.00
V000000211	S0000204	4	2	889000.00	1778000.00
V000000211	S0000202	1	2	81000.00	162000.00
V000000212	S0000195	2	2	162000.00	324000.00
V000000212	S0000194	3	1	96000.00	96000.00
V000000213	S0000235	1	3	53000.00	159000.00
V000000213	S0000237	5	2	925000.00	1850000.00
V000000214	S0000164	1	2	18000.00	36000.00
V000000214	S0000163	1	3	534000.00	1602000.00
V000000215	S0000141	5	2	328000.00	656000.00
V000000215	S0000139	4	2	83000.00	166000.00
V000000216	S0000204	4	1	889000.00	889000.00
V000000216	S0000203	4	1	917000.00	917000.00
V000000217	S0000123	1	3	747000.00	2241000.00
V000000217	S0000121	5	2	993000.00	1986000.00
V000000218	S0000125	4	3	28000.00	84000.00
V000000218	S0000126	1	2	73000.00	146000.00
V000000219	S0000072	3	2	575000.00	1150000.00
V000000219	S0000070	5	3	541000.00	1623000.00
V000000220	S0000193	2	1	128000.00	128000.00
V000000220	S0000194	4	3	96000.00	288000.00
V000000221	S0000126	4	1	73000.00	73000.00
V000000221	S0000125	2	3	28000.00	84000.00
V000000222	S0000166	4	1	96000.00	96000.00
V000000222	S0000167	2	2	146000.00	292000.00
V000000223	S0000201	5	2	734000.00	1468000.00
V000000223	S0000200	5	3	65000.00	195000.00
V000000224	S0000108	5	1	68000.00	68000.00
V000000224	S0000106	5	3	911000.00	2733000.00
V000000225	S0000202	1	2	81000.00	162000.00
V000000225	S0000203	1	1	917000.00	917000.00
V000000226	S0000273	1	3	393000.00	1179000.00
V000000226	S0000272	4	2	74000.00	148000.00
V000000227	S0000033	1	3	375000.00	1125000.00
V000000227	S0000031	3	3	59000.00	177000.00
V000000228	S0000250	4	2	57000.00	114000.00
V000000228	S0000252	1	3	96000.00	288000.00
V000000229	S0000234	2	1	29000.00	29000.00
V000000229	S0000233	4	1	918000.00	918000.00
V000000230	S0000191	3	1	245000.00	245000.00
V000000230	S0000192	3	3	575000.00	1725000.00
V000000231	S0000093	1	3	579000.00	1737000.00
V000000231	S0000092	1	1	98000.00	98000.00
V000000232	S0000134	2	3	21000.00	63000.00
V000000232	S0000135	4	1	43000.00	43000.00
V000000233	S0000189	5	3	11000.00	33000.00
V000000233	S0000188	1	3	43000.00	129000.00
V000000234	S0000006	3	1	417000.00	417000.00
V000000234	S0000004	5	3	94000.00	282000.00
V000000235	S0000158	1	3	452000.00	1356000.00
V000000235	S0000157	5	1	35000.00	35000.00
V000000236	S0000043	2	3	136000.00	408000.00
V000000236	S0000044	5	2	776000.00	1552000.00
V000000237	S0000292	2	3	13000.00	39000.00
V000000237	S0000293	2	1	32000.00	32000.00
V000000238	S0000204	1	3	889000.00	2667000.00
V000000238	S0000203	4	2	917000.00	1834000.00
V000000239	S0000217	4	3	12000.00	36000.00
V000000239	S0000219	3	1	892000.00	892000.00
V000000240	S0000055	4	2	446000.00	892000.00
V000000240	S0000056	4	1	56000.00	56000.00
V000000241	S0000103	5	2	45000.00	90000.00
V000000241	S0000104	2	3	368000.00	1104000.00
V000000242	S0000137	5	2	71000.00	142000.00
V000000242	S0000138	1	3	28000.00	84000.00
V000000243	S0000084	2	1	676000.00	676000.00
V000000243	S0000082	5	2	147000.00	294000.00
V000000244	S0000140	2	3	359000.00	1077000.00
V000000244	S0000139	3	3	83000.00	249000.00
V000000245	S0000032	3	3	649000.00	1947000.00
V000000245	S0000033	1	3	375000.00	1125000.00
V000000246	S0000278	3	1	27000.00	27000.00
V000000246	S0000279	3	3	687000.00	2061000.00
V000000247	S0000034	4	1	466000.00	466000.00
V000000247	S0000035	5	1	596000.00	596000.00
V000000248	S0000240	5	1	81000.00	81000.00
V000000248	S0000239	4	2	13000.00	26000.00
V000000249	S0000133	1	3	868000.00	2604000.00
V000000249	S0000135	3	3	43000.00	129000.00
V000000250	S0000006	5	3	417000.00	1251000.00
V000000250	S0000005	1	2	961000.00	1922000.00
V000000251	S0000002	3	2	352000.00	704000.00
V000000251	S0000003	5	2	27000.00	54000.00
V000000252	S0000298	1	1	329000.00	329000.00
V000000252	S0000299	2	1	83000.00	83000.00
V000000253	S0000205	4	1	328000.00	328000.00
V000000253	S0000206	5	2	926000.00	1852000.00
V000000254	S0000172	4	1	652000.00	652000.00
V000000254	S0000173	2	3	329000.00	987000.00
V000000255	S0000230	3	1	729000.00	729000.00
V000000255	S0000229	3	2	758000.00	1516000.00
V000000256	S0000070	5	1	541000.00	541000.00
V000000256	S0000071	4	1	922000.00	922000.00
V000000257	S0000042	2	2	177000.00	354000.00
V000000257	S0000040	2	3	44000.00	132000.00
V000000258	S0000139	3	3	83000.00	249000.00
V000000258	S0000141	2	1	328000.00	328000.00
V000000259	S0000018	5	3	469000.00	1407000.00
V000000259	S0000017	3	2	728000.00	1456000.00
V000000260	S0000286	1	1	273000.00	273000.00
V000000260	S0000287	3	3	95000.00	285000.00
V000000261	S0000122	3	2	43000.00	86000.00
V000000261	S0000123	4	1	747000.00	747000.00
V000000262	S0000075	2	2	643000.00	1286000.00
V000000262	S0000073	2	3	44000.00	132000.00
V000000263	S0000296	2	3	142000.00	426000.00
V000000263	S0000297	5	1	34000.00	34000.00
V000000264	S0000261	5	1	753000.00	753000.00
V000000264	S0000260	4	2	893000.00	1786000.00
V000000265	S0000123	4	1	747000.00	747000.00
V000000265	S0000121	4	2	993000.00	1986000.00
V000000266	S0000231	5	1	774000.00	774000.00
V000000266	S0000229	3	2	758000.00	1516000.00
V000000267	S0000294	3	1	37000.00	37000.00
V000000267	S0000293	2	3	32000.00	96000.00
V000000268	S0000208	2	2	525000.00	1050000.00
V000000268	S0000210	1	1	82000.00	82000.00
V000000269	S0000291	5	2	65000.00	130000.00
V000000269	S0000290	4	2	89000.00	178000.00
V000000270	S0000177	2	2	442000.00	884000.00
V000000270	S0000175	5	3	96000.00	288000.00
V000000271	S0000142	1	1	156000.00	156000.00
V000000271	S0000144	2	1	187000.00	187000.00
V000000272	S0000212	4	2	82000.00	164000.00
V000000272	S0000213	1	3	26000.00	78000.00
V000000273	S0000182	3	1	79000.00	79000.00
V000000273	S0000181	5	2	552000.00	1104000.00
V000000274	S0000043	4	3	136000.00	408000.00
V000000274	S0000045	2	2	222000.00	444000.00
V000000275	S0000176	2	2	343000.00	686000.00
V000000275	S0000175	2	1	96000.00	96000.00
V000000276	S0000077	3	3	866000.00	2598000.00
V000000276	S0000078	2	1	49000.00	49000.00
V000000277	S0000238	1	1	79000.00	79000.00
V000000277	S0000240	2	3	81000.00	243000.00
V000000278	S0000166	5	3	96000.00	288000.00
V000000278	S0000168	1	2	61000.00	122000.00
V000000279	S0000184	4	3	25000.00	75000.00
V000000279	S0000185	3	1	35000.00	35000.00
V000000280	S0000044	5	3	776000.00	2328000.00
V000000280	S0000045	3	3	222000.00	666000.00
V000000281	S0000147	4	3	142000.00	426000.00
V000000281	S0000146	3	3	245000.00	735000.00
V000000282	S0000219	4	1	892000.00	892000.00
V000000282	S0000217	5	3	12000.00	36000.00
V000000283	S0000083	1	1	446000.00	446000.00
V000000283	S0000084	3	3	676000.00	2028000.00
V000000284	S0000028	2	1	169000.00	169000.00
V000000284	S0000030	5	1	147000.00	147000.00
V000000285	S0000080	1	3	42000.00	126000.00
V000000285	S0000081	1	3	758000.00	2274000.00
V000000286	S0000072	3	3	575000.00	1725000.00
V000000286	S0000070	3	1	541000.00	541000.00
V000000287	S0000015	4	3	553000.00	1659000.00
V000000287	S0000013	1	3	969000.00	2907000.00
V000000288	S0000189	1	1	11000.00	11000.00
V000000288	S0000188	1	3	43000.00	129000.00
V000000289	S0000232	4	3	99000.00	297000.00
V000000289	S0000234	5	2	29000.00	58000.00
V000000290	S0000107	3	2	36000.00	72000.00
V000000290	S0000108	3	1	68000.00	68000.00
V000000291	S0000176	3	1	343000.00	343000.00
V000000291	S0000175	2	3	96000.00	288000.00
V000000292	S0000262	1	2	31000.00	62000.00
V000000292	S0000264	2	1	415000.00	415000.00
V000000293	S0000237	3	3	925000.00	2775000.00
V000000293	S0000235	2	1	53000.00	53000.00
V000000294	S0000147	1	3	142000.00	426000.00
V000000294	S0000145	4	3	86000.00	258000.00
V000000295	S0000091	4	2	76000.00	152000.00
V000000295	S0000093	3	1	579000.00	579000.00
V000000296	S0000259	4	1	53000.00	53000.00
V000000296	S0000261	1	3	753000.00	2259000.00
V000000297	S0000285	2	3	478000.00	1434000.00
V000000297	S0000283	3	1	938000.00	938000.00
V000000298	S0000255	1	3	274000.00	822000.00
V000000298	S0000254	3	2	416000.00	832000.00
V000000299	S0000256	3	1	15000.00	15000.00
V000000299	S0000257	1	2	695000.00	1390000.00
V000000300	S0000288	5	1	166000.00	166000.00
V000000300	S0000287	1	1	95000.00	95000.00
V000000301	S0000067	5	1	188000.00	188000.00
V000000301	S0000068	1	2	514000.00	1028000.00
V000000302	S0000250	2	3	57000.00	171000.00
V000000302	S0000251	3	2	617000.00	1234000.00
V000000303	S0000020	3	2	612000.00	1224000.00
V000000303	S0000019	2	3	32000.00	96000.00
V000000304	S0000030	1	1	147000.00	147000.00
V000000304	S0000029	4	2	83000.00	166000.00
V000000305	S0000099	4	1	34000.00	34000.00
V000000305	S0000098	2	1	688000.00	688000.00
V000000306	S0000107	4	3	36000.00	108000.00
V000000306	S0000106	5	3	911000.00	2733000.00
V000000307	S0000220	1	2	637000.00	1274000.00
V000000307	S0000221	5	2	749000.00	1498000.00
V000000308	S0000096	5	3	95000.00	285000.00
V000000308	S0000095	2	1	824000.00	824000.00
V000000309	S0000089	3	2	44000.00	88000.00
V000000309	S0000088	5	1	87000.00	87000.00
V000000310	S0000019	1	3	32000.00	96000.00
V000000310	S0000020	3	3	612000.00	1836000.00
V000000311	S0000036	3	2	43000.00	86000.00
V000000311	S0000035	1	1	596000.00	596000.00
V000000312	S0000163	4	3	534000.00	1602000.00
V000000312	S0000165	1	2	567000.00	1134000.00
V000000313	S0000166	3	1	96000.00	96000.00
V000000313	S0000167	4	1	146000.00	146000.00
V000000314	S0000018	2	3	469000.00	1407000.00
V000000314	S0000017	3	2	728000.00	1456000.00
V000000315	S0000204	3	1	889000.00	889000.00
V000000315	S0000202	3	2	81000.00	162000.00
V000000316	S0000015	3	3	553000.00	1659000.00
V000000316	S0000013	2	2	969000.00	1938000.00
V000000317	S0000116	5	3	44000.00	132000.00
V000000317	S0000117	2	2	22000.00	44000.00
V000000318	S0000103	1	2	45000.00	90000.00
V000000318	S0000104	2	2	368000.00	736000.00
V000000319	S0000055	4	1	446000.00	446000.00
V000000319	S0000057	1	2	961000.00	1922000.00
V000000320	S0000068	4	1	514000.00	514000.00
V000000320	S0000067	1	1	188000.00	188000.00
V000000321	S0000219	1	1	892000.00	892000.00
V000000321	S0000218	4	3	244000.00	732000.00
V000000322	S0000152	1	1	36000.00	36000.00
V000000322	S0000151	1	3	33000.00	99000.00
V000000323	S0000195	2	3	162000.00	486000.00
V000000323	S0000193	2	2	128000.00	256000.00
V000000324	S0000284	3	1	345000.00	345000.00
V000000324	S0000283	5	2	938000.00	1876000.00
V000000325	S0000222	4	3	567000.00	1701000.00
V000000325	S0000221	1	1	749000.00	749000.00
V000000326	S0000002	5	2	352000.00	704000.00
V000000326	S0000001	1	3	349000.00	1047000.00
V000000327	S0000063	2	1	288000.00	288000.00
V000000327	S0000061	2	3	55000.00	165000.00
V000000328	S0000099	2	2	34000.00	68000.00
V000000328	S0000098	5	1	688000.00	688000.00
V000000329	S0000213	1	2	26000.00	52000.00
V000000329	S0000212	2	3	82000.00	246000.00
V000000330	S0000069	5	3	959000.00	2877000.00
V000000330	S0000067	1	2	188000.00	376000.00
V000000331	S0000271	5	2	83000.00	166000.00
V000000331	S0000273	1	1	393000.00	393000.00
V000000332	S0000083	4	2	446000.00	892000.00
V000000332	S0000084	3	2	676000.00	1352000.00
V000000333	S0000177	2	1	442000.00	442000.00
V000000333	S0000175	5	1	96000.00	96000.00
V000000334	S0000166	3	1	96000.00	96000.00
V000000334	S0000167	5	3	146000.00	438000.00
V000000335	S0000104	4	2	368000.00	736000.00
V000000335	S0000103	4	3	45000.00	135000.00
V000000336	S0000273	4	3	393000.00	1179000.00
V000000336	S0000271	5	3	83000.00	249000.00
V000000337	S0000141	5	3	328000.00	984000.00
V000000337	S0000139	2	3	83000.00	249000.00
V000000338	S0000296	5	3	142000.00	426000.00
V000000338	S0000297	1	2	34000.00	68000.00
V000000339	S0000033	3	3	375000.00	1125000.00
V000000339	S0000032	2	3	649000.00	1947000.00
V000000340	S0000131	5	1	134000.00	134000.00
V000000340	S0000132	3	3	25000.00	75000.00
V000000341	S0000242	5	3	68000.00	204000.00
V000000341	S0000241	5	3	41000.00	123000.00
V000000342	S0000061	4	1	55000.00	55000.00
V000000342	S0000063	4	1	288000.00	288000.00
V000000343	S0000090	1	1	51000.00	51000.00
V000000343	S0000088	4	2	87000.00	174000.00
V000000344	S0000229	1	2	758000.00	1516000.00
V000000344	S0000230	1	3	729000.00	2187000.00
V000000345	S0000003	1	2	27000.00	54000.00
V000000345	S0000002	1	1	352000.00	352000.00
V000000346	S0000287	2	2	95000.00	190000.00
V000000346	S0000286	3	3	273000.00	819000.00
V000000347	S0000065	5	2	485000.00	970000.00
V000000347	S0000064	2	1	14000.00	14000.00
V000000348	S0000278	5	2	27000.00	54000.00
V000000348	S0000279	3	2	687000.00	1374000.00
V000000349	S0000067	5	3	188000.00	564000.00
V000000349	S0000068	4	3	514000.00	1542000.00
V000000350	S0000110	1	2	95000.00	190000.00
V000000350	S0000111	4	1	92000.00	92000.00
V000000351	S0000245	4	1	892000.00	892000.00
V000000351	S0000244	5	3	16000.00	48000.00
V000000352	S0000114	3	3	874000.00	2622000.00
V000000352	S0000112	4	2	81000.00	162000.00
V000000353	S0000173	2	2	329000.00	658000.00
V000000353	S0000174	1	1	47000.00	47000.00
V000000354	S0000170	3	2	29000.00	58000.00
V000000354	S0000169	3	2	272000.00	544000.00
V000000355	S0000292	5	1	13000.00	13000.00
V000000355	S0000294	5	2	37000.00	74000.00
V000000356	S0000101	3	2	66000.00	132000.00
V000000356	S0000102	3	3	75000.00	225000.00
V000000357	S0000146	4	2	245000.00	490000.00
V000000357	S0000147	3	3	142000.00	426000.00
V000000358	S0000073	4	2	44000.00	88000.00
V000000358	S0000075	2	2	643000.00	1286000.00
V000000359	S0000015	1	3	553000.00	1659000.00
V000000359	S0000013	1	2	969000.00	1938000.00
V000000360	S0000265	5	1	59000.00	59000.00
V000000360	S0000266	5	3	24000.00	72000.00
V000000361	S0000122	5	2	43000.00	86000.00
V000000361	S0000123	2	3	747000.00	2241000.00
V000000362	S0000225	4	3	798000.00	2394000.00
V000000362	S0000224	5	1	98000.00	98000.00
V000000363	S0000093	4	3	579000.00	1737000.00
V000000363	S0000092	3	2	98000.00	196000.00
V000000364	S0000222	2	1	567000.00	567000.00
V000000364	S0000221	4	3	749000.00	2247000.00
V000000365	S0000241	5	3	41000.00	123000.00
V000000365	S0000242	5	2	68000.00	136000.00
V000000366	S0000132	3	3	25000.00	75000.00
V000000366	S0000130	5	2	27000.00	54000.00
V000000367	S0000170	3	2	29000.00	58000.00
V000000367	S0000169	5	2	272000.00	544000.00
V000000368	S0000031	3	2	59000.00	118000.00
V000000368	S0000032	2	3	649000.00	1947000.00
V000000369	S0000296	3	1	142000.00	142000.00
V000000369	S0000297	2	1	34000.00	34000.00
V000000370	S0000274	4	2	743000.00	1486000.00
V000000370	S0000276	5	1	21000.00	21000.00
V000000371	S0000120	1	2	412000.00	824000.00
V000000371	S0000119	3	3	11000.00	33000.00
V000000372	S0000066	1	3	988000.00	2964000.00
V000000372	S0000064	2	1	14000.00	14000.00
V000000373	S0000238	1	2	79000.00	158000.00
V000000373	S0000239	2	1	13000.00	13000.00
V000000374	S0000032	2	3	649000.00	1947000.00
V000000374	S0000031	2	1	59000.00	59000.00
V000000375	S0000085	1	2	864000.00	1728000.00
V000000375	S0000086	5	2	58000.00	116000.00
V000000376	S0000038	4	3	14000.00	42000.00
V000000376	S0000037	5	2	383000.00	766000.00
V000000377	S0000297	3	3	34000.00	102000.00
V000000377	S0000296	3	1	142000.00	142000.00
V000000378	S0000126	1	1	73000.00	73000.00
V000000378	S0000125	4	1	28000.00	28000.00
V000000379	S0000234	4	1	29000.00	29000.00
V000000379	S0000233	5	2	918000.00	1836000.00
V000000380	S0000125	1	1	28000.00	28000.00
V000000380	S0000124	4	3	22000.00	66000.00
V000000381	S0000117	1	1	22000.00	22000.00
V000000381	S0000115	5	2	178000.00	356000.00
V000000382	S0000281	5	1	913000.00	913000.00
V000000382	S0000280	5	1	362000.00	362000.00
V000000383	S0000249	3	1	73000.00	73000.00
V000000383	S0000248	3	3	33000.00	99000.00
V000000384	S0000230	5	2	729000.00	1458000.00
V000000384	S0000231	3	2	774000.00	1548000.00
V000000385	S0000218	5	3	244000.00	732000.00
V000000385	S0000219	5	2	892000.00	1784000.00
V000000386	S0000064	2	1	14000.00	14000.00
V000000386	S0000066	2	3	988000.00	2964000.00
V000000387	S0000190	2	3	744000.00	2232000.00
V000000387	S0000192	3	3	575000.00	1725000.00
V000000388	S0000022	1	1	121000.00	121000.00
V000000388	S0000024	1	3	86000.00	258000.00
V000000389	S0000100	5	3	637000.00	1911000.00
V000000389	S0000102	5	1	75000.00	75000.00
V000000390	S0000251	2	2	617000.00	1234000.00
V000000390	S0000250	1	1	57000.00	57000.00
V000000391	S0000230	1	1	729000.00	729000.00
V000000391	S0000229	4	2	758000.00	1516000.00
V000000392	S0000220	5	2	637000.00	1274000.00
V000000392	S0000221	1	1	749000.00	749000.00
V000000393	S0000160	2	1	52000.00	52000.00
V000000393	S0000161	2	3	69000.00	207000.00
V000000394	S0000187	2	3	36000.00	108000.00
V000000394	S0000188	5	1	43000.00	43000.00
V000000395	S0000192	5	1	575000.00	575000.00
V000000395	S0000190	1	3	744000.00	2232000.00
V000000396	S0000195	5	1	162000.00	162000.00
V000000396	S0000193	4	2	128000.00	256000.00
V000000397	S0000199	1	2	81000.00	162000.00
V000000397	S0000200	4	1	65000.00	65000.00
V000000398	S0000137	5	3	71000.00	213000.00
V000000398	S0000136	3	2	339000.00	678000.00
V000000399	S0000232	5	3	99000.00	297000.00
V000000399	S0000233	4	1	918000.00	918000.00
V000000400	S0000215	5	3	286000.00	858000.00
V000000400	S0000214	1	1	438000.00	438000.00
V000000401	S0000015	1	1	553000.00	553000.00
V000000401	S0000013	5	1	969000.00	969000.00
V000000402	S0000208	4	3	525000.00	1575000.00
V000000402	S0000209	3	1	88000.00	88000.00
V000000403	S0000220	2	1	637000.00	637000.00
V000000403	S0000222	4	1	567000.00	567000.00
V000000404	S0000185	5	3	35000.00	105000.00
V000000404	S0000186	3	2	81000.00	162000.00
V000000405	S0000100	4	2	637000.00	1274000.00
V000000405	S0000102	4	1	75000.00	75000.00
V000000406	S0000080	2	1	42000.00	42000.00
V000000406	S0000079	1	3	84000.00	252000.00
V000000407	S0000082	4	2	147000.00	294000.00
V000000407	S0000084	1	1	676000.00	676000.00
V000000408	S0000123	5	3	747000.00	2241000.00
V000000408	S0000122	4	3	43000.00	129000.00
V000000409	S0000170	5	2	29000.00	58000.00
V000000409	S0000169	5	3	272000.00	816000.00
V000000410	S0000084	5	1	676000.00	676000.00
V000000410	S0000083	3	1	446000.00	446000.00
V000000411	S0000145	4	1	86000.00	86000.00
V000000411	S0000146	1	2	245000.00	490000.00
V000000412	S0000282	3	1	632000.00	632000.00
V000000412	S0000280	1	3	362000.00	1086000.00
V000000413	S0000299	1	2	83000.00	166000.00
V000000413	S0000298	1	2	329000.00	658000.00
V000000414	S0000220	1	1	637000.00	637000.00
V000000414	S0000221	4	1	749000.00	749000.00
V000000415	S0000233	3	3	918000.00	2754000.00
V000000415	S0000232	1	2	99000.00	198000.00
V000000416	S0000173	5	1	329000.00	329000.00
V000000416	S0000174	1	1	47000.00	47000.00
V000000417	S0000134	5	1	21000.00	21000.00
V000000417	S0000133	5	3	868000.00	2604000.00
V000000418	S0000053	4	3	92000.00	276000.00
V000000418	S0000052	1	2	164000.00	328000.00
V000000419	S0000183	2	1	95000.00	95000.00
V000000419	S0000181	5	3	552000.00	1656000.00
V000000420	S0000182	3	2	79000.00	158000.00
V000000420	S0000183	3	2	95000.00	190000.00
V000000421	S0000059	1	2	69000.00	138000.00
V000000421	S0000060	3	2	24000.00	48000.00
V000000422	S0000210	4	3	82000.00	246000.00
V000000422	S0000208	3	2	525000.00	1050000.00
V000000423	S0000139	4	1	83000.00	83000.00
V000000423	S0000141	3	3	328000.00	984000.00
V000000424	S0000206	1	2	926000.00	1852000.00
V000000424	S0000207	5	1	59000.00	59000.00
V000000425	S0000268	1	2	75000.00	150000.00
V000000425	S0000269	2	2	351000.00	702000.00
V000000426	S0000176	5	1	343000.00	343000.00
V000000426	S0000177	2	1	442000.00	442000.00
V000000427	S0000017	2	1	728000.00	728000.00
V000000427	S0000018	4	1	469000.00	469000.00
V000000428	S0000211	2	3	126000.00	378000.00
V000000428	S0000213	2	2	26000.00	52000.00
V000000429	S0000222	3	2	567000.00	1134000.00
V000000429	S0000220	1	1	637000.00	637000.00
V000000430	S0000079	3	1	84000.00	84000.00
V000000430	S0000080	2	2	42000.00	84000.00
V000000431	S0000282	1	2	632000.00	1264000.00
V000000431	S0000281	1	2	913000.00	1826000.00
V000000432	S0000168	3	2	61000.00	122000.00
V000000432	S0000167	1	1	146000.00	146000.00
V000000433	S0000087	3	1	57000.00	57000.00
V000000433	S0000085	1	2	864000.00	1728000.00
V000000434	S0000217	4	2	12000.00	24000.00
V000000434	S0000219	1	2	892000.00	1784000.00
V000000435	S0000195	4	3	162000.00	486000.00
V000000435	S0000193	3	2	128000.00	256000.00
V000000436	S0000012	1	1	95000.00	95000.00
V000000436	S0000011	5	2	86000.00	172000.00
V000000437	S0000185	3	3	35000.00	105000.00
V000000437	S0000186	3	1	81000.00	81000.00
V000000438	S0000109	2	2	595000.00	1190000.00
V000000438	S0000110	5	2	95000.00	190000.00
V000000439	S0000178	1	3	82000.00	246000.00
V000000439	S0000180	3	3	62000.00	186000.00
V000000440	S0000257	3	1	695000.00	695000.00
V000000440	S0000258	4	2	54000.00	108000.00
V000000441	S0000226	4	3	978000.00	2934000.00
V000000441	S0000228	4	2	993000.00	1986000.00
V000000442	S0000107	2	2	36000.00	72000.00
V000000442	S0000106	1	3	911000.00	2733000.00
V000000443	S0000080	4	2	42000.00	84000.00
V000000443	S0000081	3	2	758000.00	1516000.00
V000000444	S0000188	1	2	43000.00	86000.00
V000000444	S0000189	4	1	11000.00	11000.00
V000000445	S0000032	1	2	649000.00	1298000.00
V000000445	S0000031	1	3	59000.00	177000.00
V000000446	S0000050	4	1	11000.00	11000.00
V000000446	S0000051	2	3	266000.00	798000.00
V000000447	S0000006	4	3	417000.00	1251000.00
V000000447	S0000004	5	3	94000.00	282000.00
V000000448	S0000015	3	1	553000.00	553000.00
V000000448	S0000014	4	2	29000.00	58000.00
V000000449	S0000185	2	1	35000.00	35000.00
V000000449	S0000184	1	3	25000.00	75000.00
V000000450	S0000223	4	3	172000.00	516000.00
V000000450	S0000225	4	1	798000.00	798000.00
V000000451	S0000271	2	2	83000.00	166000.00
V000000451	S0000273	3	2	393000.00	786000.00
V000000452	S0000035	4	3	596000.00	1788000.00
V000000452	S0000036	2	1	43000.00	43000.00
V000000453	S0000165	3	1	567000.00	567000.00
V000000453	S0000164	3	3	18000.00	54000.00
V000000454	S0000221	4	1	749000.00	749000.00
V000000454	S0000222	5	3	567000.00	1701000.00
V000000455	S0000057	3	2	961000.00	1922000.00
V000000455	S0000056	2	1	56000.00	56000.00
V000000456	S0000044	2	1	776000.00	776000.00
V000000456	S0000043	5	1	136000.00	136000.00
V000000457	S0000131	3	2	134000.00	268000.00
V000000457	S0000130	5	2	27000.00	54000.00
V000000458	S0000004	2	1	94000.00	94000.00
V000000458	S0000005	4	3	961000.00	2883000.00
V000000459	S0000119	4	1	11000.00	11000.00
V000000459	S0000118	4	2	371000.00	742000.00
V000000460	S0000037	3	2	383000.00	766000.00
V000000460	S0000039	2	2	12000.00	24000.00
V000000461	S0000175	3	2	96000.00	192000.00
V000000461	S0000176	1	3	343000.00	1029000.00
V000000462	S0000049	5	1	595000.00	595000.00
V000000462	S0000051	1	2	266000.00	532000.00
V000000463	S0000239	3	3	13000.00	39000.00
V000000463	S0000240	5	1	81000.00	81000.00
V000000464	S0000271	5	1	83000.00	83000.00
V000000464	S0000272	2	1	74000.00	74000.00
V000000465	S0000046	4	2	168000.00	336000.00
V000000465	S0000047	3	2	921000.00	1842000.00
V000000466	S0000038	3	3	14000.00	42000.00
V000000466	S0000039	1	1	12000.00	12000.00
V000000467	S0000272	3	2	74000.00	148000.00
V000000467	S0000273	3	3	393000.00	1179000.00
V000000468	S0000161	2	2	69000.00	138000.00
V000000468	S0000162	1	1	452000.00	452000.00
V000000469	S0000051	2	2	266000.00	532000.00
V000000469	S0000049	1	1	595000.00	595000.00
V000000470	S0000255	5	1	274000.00	274000.00
V000000470	S0000254	3	1	416000.00	416000.00
V000000471	S0000300	4	1	131000.00	131000.00
V000000471	S0000299	2	2	83000.00	166000.00
V000000472	S0000152	3	1	36000.00	36000.00
V000000472	S0000153	3	2	268000.00	536000.00
V000000473	S0000244	5	3	16000.00	48000.00
V000000473	S0000245	4	1	892000.00	892000.00
V000000474	S0000114	3	3	874000.00	2622000.00
V000000474	S0000112	3	2	81000.00	162000.00
V000000475	S0000180	2	1	62000.00	62000.00
V000000475	S0000179	2	2	37000.00	74000.00
V000000476	S0000074	4	1	68000.00	68000.00
V000000476	S0000073	4	3	44000.00	132000.00
V000000477	S0000254	1	3	416000.00	1248000.00
V000000477	S0000253	1	1	87000.00	87000.00
V000000478	S0000290	1	1	89000.00	89000.00
V000000478	S0000289	1	2	49000.00	98000.00
V000000479	S0000205	1	2	328000.00	656000.00
V000000479	S0000206	5	3	926000.00	2778000.00
V000000480	S0000050	1	1	11000.00	11000.00
V000000480	S0000049	1	1	595000.00	595000.00
V000000481	S0000168	3	2	61000.00	122000.00
V000000481	S0000167	1	1	146000.00	146000.00
V000000482	S0000017	1	1	728000.00	728000.00
V000000482	S0000016	5	1	66000.00	66000.00
V000000483	S0000007	1	1	246000.00	246000.00
V000000483	S0000009	4	2	297000.00	594000.00
V000000484	S0000220	2	2	637000.00	1274000.00
V000000484	S0000222	5	3	567000.00	1701000.00
V000000485	S0000249	4	1	73000.00	73000.00
V000000485	S0000248	1	1	33000.00	33000.00
V000000486	S0000255	4	3	274000.00	822000.00
V000000486	S0000253	5	3	87000.00	261000.00
V000000487	S0000035	4	3	596000.00	1788000.00
V000000487	S0000034	1	2	466000.00	932000.00
V000000488	S0000173	1	2	329000.00	658000.00
V000000488	S0000174	4	1	47000.00	47000.00
V000000489	S0000053	3	2	92000.00	184000.00
V000000489	S0000054	1	1	63000.00	63000.00
V000000490	S0000185	1	1	35000.00	35000.00
V000000490	S0000184	1	1	25000.00	25000.00
V000000491	S0000200	3	2	65000.00	130000.00
V000000491	S0000201	4	3	734000.00	2202000.00
V000000492	S0000249	4	2	73000.00	146000.00
V000000492	S0000247	2	1	87000.00	87000.00
V000000493	S0000083	1	3	446000.00	1338000.00
V000000493	S0000082	2	3	147000.00	441000.00
V000000494	S0000100	2	3	637000.00	1911000.00
V000000494	S0000102	3	1	75000.00	75000.00
V000000495	S0000147	1	1	142000.00	142000.00
V000000495	S0000146	4	1	245000.00	245000.00
V000000496	S0000231	2	2	774000.00	1548000.00
V000000496	S0000230	3	2	729000.00	1458000.00
V000000497	S0000129	3	3	44000.00	132000.00
V000000497	S0000127	2	2	764000.00	1528000.00
V000000498	S0000247	3	3	87000.00	261000.00
V000000498	S0000248	2	1	33000.00	33000.00
V000000499	S0000266	4	1	24000.00	24000.00
V000000499	S0000265	5	1	59000.00	59000.00
V000000500	S0000220	1	2	637000.00	1274000.00
V000000500	S0000221	2	2	749000.00	1498000.00
\.


--
-- Data for Name: pelanggan; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY pelanggan (email, is_penjual, nilai_reputasi, poin) FROM stdin;
evkosa@seh.net	t	\N	\N
ugsava@ubruffal.co.uk	t	\N	\N
wed@ihienucus.com	t	\N	\N
rofik@uz.edu	t	\N	\N
nanotat@huthu.co.uk	t	\N	\N
oh@nip.org	t	\N	\N
migtunor@gu.edu	t	\N	\N
zuzro@finapore.org	t	\N	\N
niltafli@jeewedi.io	t	\N	\N
egven@di.net	t	\N	\N
ikwuwne@kap.co.uk	t	\N	\N
pid@ke.io	t	\N	\N
topa@geztik.org	t	\N	\N
liev@ravwu.co.uk	t	\N	\N
fi@idarof.gov	t	\N	\N
he@kodizi.co.uk	t	\N	\N
caoko@jufonima.org	t	\N	\N
fiuco@po.gov	t	\N	\N
pijejaze@bop.com	t	\N	\N
ces@fefeit.edu	t	\N	\N
nocmafi@judda.net	t	\N	\N
vama@zi.net	t	\N	\N
wiofu@ruafvu.gov	t	\N	\N
mavvo@jeniv.org	t	\N	\N
fazgif@dir.co.uk	t	\N	\N
adwior@kionanu.net	t	\N	\N
luzub@ep.org	t	\N	\N
cor@jawip.co.uk	t	\N	\N
lopvu@zudpaki.co.uk	t	\N	\N
po@cahgamuji.io	t	\N	\N
facehfe@pidmu.com	t	\N	\N
mek@funsako.edu	t	\N	\N
buahe@ismulgam.org	t	\N	\N
ovzedi@azpapvuc.net	t	\N	\N
ci@hom.com	t	\N	\N
dupbajov@lidhugmal.io	t	\N	\N
how@rizaur.edu	t	\N	\N
efuepiec@ow.gov	t	\N	\N
azakopibo@fuew.gov	t	\N	\N
he@huow.co.uk	t	\N	\N
fi@wadbiwlo.gov	t	\N	\N
pisopu@fomabako.co.uk	t	\N	\N
ut@movpuwom.net	t	\N	\N
detcogu@war.edu	t	\N	\N
uzpude@fisojelev.io	t	\N	\N
uwbew@wircihhat.gov	t	\N	\N
bebuh@sot.gov	t	\N	\N
zab@eje.net	t	\N	\N
adoujkum@gavro.net	t	\N	\N
jezrubki@masna.co.uk	t	\N	\N
gap@wemihir.org	t	\N	\N
pinku@kerro.edu	t	\N	\N
renicul@pazuse.net	t	\N	\N
rigic@lob.net	t	\N	\N
susgijic@at.org	t	\N	\N
cirif@eciipi.edu	t	\N	\N
ko@fuvlujom.edu	t	\N	\N
gukeroc@juihade.co.uk	t	\N	\N
copzocnid@lu.gov	t	\N	\N
dafhol@asawike.org	t	\N	\N
jazuggo@zeb.co.uk	t	\N	\N
ovhoco@pe.org	t	\N	\N
baftoj@li.gov	t	\N	\N
zobeh@les.gov	t	\N	\N
leztigjod@rehenas.edu	t	\N	\N
wedujiw@guh.edu	t	\N	\N
lalenir@tajuwne.net	t	\N	\N
lidkoc@ze.gov	t	\N	\N
ankez@dusfug.org	t	\N	\N
fajigag@igwuz.io	t	\N	\N
bopoc@meksiw.net	t	\N	\N
kuleboj@ozwecwav.co.uk	t	\N	\N
zenu@morwulok.edu	t	\N	\N
jak@kevkupdof.co.uk	t	\N	\N
bugaw@or.edu	t	\N	\N
cepwasfa@habik.co.uk	t	\N	\N
viz@nevhu.net	t	\N	\N
mus@uja.gov	t	\N	\N
milcetwo@zutenjo.edu	t	\N	\N
dis@pavejus.edu	t	\N	\N
ihi@gabtanto.org	t	\N	\N
ruhtad@vavihza.net	t	\N	\N
coje@wuf.io	t	\N	\N
pedfur@ze.co.uk	t	\N	\N
jedmew@cihuwde.co.uk	t	\N	\N
pacajti@uj.edu	t	\N	\N
ohijazren@izfu.net	t	\N	\N
wihut@judhindiz.net	t	\N	\N
zoju@zururub.net	t	\N	\N
akehihi@zoji.gov	t	\N	\N
vezi@jocow.co.uk	t	\N	\N
haul@roama.net	t	\N	\N
mehrulihu@riribkuk.net	t	\N	\N
tagu@ajaehagis.com	t	\N	\N
bu@diadu.gov	t	\N	\N
do@ki.io	t	\N	\N
puz@feumu.net	t	\N	\N
deem@tuvir.co.uk	t	\N	\N
ta@elbuple.net	t	\N	\N
ac@nettiguj.edu	t	\N	\N
halilil@wuturo.net	f	\N	\N
tocepu@wela.co.uk	f	\N	\N
guz@tuve.com	f	\N	\N
kehu@pom.edu	f	\N	\N
na@tazevze.com	f	\N	\N
tu@pevu.edu	f	\N	\N
uc@zepunu.org	f	\N	\N
sijtu@ja.io	f	\N	\N
cel@map.io	f	\N	\N
teuzu@va.net	f	\N	\N
hu@eha.co.uk	f	\N	\N
dimkerra@lebog.co.uk	f	\N	\N
oficaznaf@lomu.org	f	\N	\N
ucetaeb@zola.co.uk	f	\N	\N
ijages@lomulel.io	f	\N	\N
pidcem@bucfauru.com	f	\N	\N
anowuw@sabapmi.org	f	\N	\N
sadhupac@zon.edu	f	\N	\N
noge@wib.com	f	\N	\N
po@fahida.co.uk	f	\N	\N
vanesu@juwhuzu.gov	f	\N	\N
sisi@povzurudo.io	f	\N	\N
hovarwuh@sup.io	f	\N	\N
inu@fo.gov	f	\N	\N
pi@kekbu.io	f	\N	\N
subzola@kacjis.edu	f	\N	\N
si@behab.co.uk	f	\N	\N
ruemo@ru.co.uk	f	\N	\N
pufvep@zagicmac.io	f	\N	\N
nu@erajod.org	f	\N	\N
jiwuol@vi.io	f	\N	\N
vuc@ifef.net	f	\N	\N
gaupe@uf.org	f	\N	\N
dup@ejmameh.io	f	\N	\N
erfi@zokiw.co.uk	f	\N	\N
mewugek@sojkatta.io	f	\N	\N
jawantu@siziin.io	f	\N	\N
ced@ego.net	f	\N	\N
eptew@os.org	f	\N	\N
jotugve@oclekuju.io	f	\N	\N
ze@erejanek.com	f	\N	\N
vim@kieh.gov	f	\N	\N
evisep@uwre.net	f	\N	\N
hu@zusrusiw.edu	f	\N	\N
ji@kozko.co.uk	f	\N	\N
il@up.co.uk	f	\N	\N
obofac@fel.edu	f	\N	\N
rohoci@muza.io	f	\N	\N
adaerop@ono.net	f	\N	\N
waruk@tu.org	f	\N	\N
eto@cenjiv.co.uk	f	\N	\N
fiw@mamko.co.uk	f	\N	\N
gukum@ficoofo.co.uk	f	\N	\N
ho@jesopi.gov	f	\N	\N
hemvofvod@osulojmov.com	f	\N	\N
zu@guji.gov	f	\N	\N
uk@kiko.co.uk	f	\N	\N
powjajrak@el.io	f	\N	\N
ige@ocwope.net	f	\N	\N
jodluf@soklizkec.org	f	\N	\N
canehiler@enlo.edu	f	\N	\N
teguc@pohon.io	f	\N	\N
zonur@vocumotoz.net	f	\N	\N
povioge@na.gov	f	\N	\N
jazurot@kentefe.net	f	\N	\N
ija@ub.net	f	\N	\N
udne@donazmuv.io	f	\N	\N
imsebap@oci.io	f	\N	\N
ta@roc.io	f	\N	\N
mir@ru.org	f	\N	\N
fura@upipu.gov	f	\N	\N
solodsi@vinepmid.org	f	\N	\N
jumovbot@ul.edu	f	\N	\N
ba@nod.gov	f	\N	\N
ur@ru.com	f	\N	\N
tipdabit@ogazov.com	f	\N	\N
tumog@heovazor.edu	f	\N	\N
jidfow@ditiduze.com	f	\N	\N
kub@etdav.org	f	\N	\N
ji@ihmomih.io	f	\N	\N
loh@cetumi.com	f	\N	\N
nimzin@guznerepi.edu	f	\N	\N
uc@rebufjen.edu	f	\N	\N
ejoha@acsudo.net	f	\N	\N
gita@parwaj.net	f	\N	\N
ivduse@zuhew.org	f	\N	\N
vobvi@vo.org	f	\N	\N
cin@dorpaz.org	f	\N	\N
jujrecwo@veofowe.com	f	\N	\N
rulsiw@lecwuzla.gov	f	\N	\N
cito@no.edu	f	\N	\N
esoli@velmurap.io	f	\N	\N
tomva@kej.gov	f	\N	\N
neloke@lu.edu	f	\N	\N
sud@ul.org	f	\N	\N
war@fohrahsin.net	f	\N	\N
ilam@hadjaus.org	f	\N	\N
dipcoc@ziszipmes.io	f	\N	\N
wano@liwjumu.com	f	\N	\N
malvo@gigdelkun.com	f	\N	\N
faopidu@uhi.com	f	\N	\N
zeb@fuz.io	f	\N	\N
lud@wubra.gov	f	\N	\N
cafvadpoj@miwoda.co.uk	f	\N	\N
vig@dehede.org	f	\N	\N
hisudo@ohlu.edu	f	\N	\N
mezhi@te.edu	f	\N	\N
wa@ejadu.gov	f	\N	\N
ez@upzi.com	f	\N	\N
if@azo.gov	f	\N	\N
hazut@ezhofid.co.uk	f	\N	\N
meiru@dofigoj.org	f	\N	\N
cej@juz.co.uk	f	\N	\N
rofuhbe@vetidi.co.uk	f	\N	\N
bavit@ji.org	f	\N	\N
ciraofi@wikvuut.io	f	\N	\N
juciddu@difuci.org	f	\N	\N
map@levuv.io	f	\N	\N
vacawhep@kalagwo.com	f	\N	\N
kodon@jeej.com	f	\N	\N
satme@lir.gov	f	\N	\N
igupu@ogezegpod.gov	f	\N	\N
le@lopdooc.com	f	\N	\N
zi@mimpu.com	f	\N	\N
ku@titdotres.gov	f	\N	\N
je@eno.net	f	\N	\N
ginid@unfioc.org	f	\N	\N
idudovje@wadmisre.co.uk	f	\N	\N
nifu@lotedpoh.gov	f	\N	\N
sepmajbo@racupmib.io	f	\N	\N
getat@cotehef.org	f	\N	\N
it@kaf.io	f	\N	\N
va@bebim.net	f	\N	\N
oceob@codetcuh.com	f	\N	\N
ikruj@sapanti.com	f	\N	\N
fokjar@ril.gov	f	\N	\N
bod@zij.edu	f	\N	\N
giwjufu@mahuh.net	f	\N	\N
ew@egzo.gov	f	\N	\N
vuim@zadjul.co.uk	f	\N	\N
no@ugu.com	f	\N	\N
hewunul@rigfuda.org	f	\N	\N
ofi@veubuda.org	f	\N	\N
sulovsi@ewigi.edu	f	\N	\N
cileiji@nek.edu	f	\N	\N
jaolo@fihotnem.net	f	\N	\N
boobaca@we.gov	f	\N	\N
uvogew@onumi.io	f	\N	\N
tinubwi@ewo.com	f	\N	\N
jibloc@ol.co.uk	f	\N	\N
tufilco@luri.edu	f	\N	\N
gigat@saf.co.uk	f	\N	\N
is@fe.com	f	\N	\N
murgupo@hiiluhe.net	f	\N	\N
efo@ran.com	f	\N	\N
jude@fok.edu	f	\N	\N
woatva@wodlopwep.edu	f	\N	\N
ji@fogukhu.com	f	\N	\N
laligni@hukru.io	f	\N	\N
sivugal@opnulaj.gov	f	\N	\N
efuis@uc.co.uk	f	\N	\N
ivnu@viset.edu	f	\N	\N
mireg@nevwacko.edu	f	\N	\N
ijo@sisfopbup.org	f	\N	\N
il@caguri.edu	f	\N	\N
ditka@bajwo.gov	f	\N	\N
zut@sazul.co.uk	f	\N	\N
paejne@com.edu	f	\N	\N
re@wumop.com	f	\N	\N
gabiro@sez.net	f	\N	\N
uwa@tagokraw.gov	f	\N	\N
upu@owfogna.com	f	\N	\N
ra@midgabe.edu	f	\N	\N
jukuctig@bucolza.org	f	\N	\N
jejme@me.com	f	\N	\N
ju@zo.com	f	\N	\N
tol@tukon.edu	f	\N	\N
leluwi@honwi.com	f	\N	\N
nograhcuc@kauno.edu	f	\N	\N
fiom@si.org	f	\N	\N
ewebeduc@heceh.co.uk	f	\N	\N
tata@meetpe.gov	f	\N	\N
lupadiv@ge.com	f	\N	\N
ejiflo@amdof.io	f	\N	\N
ciwimnim@wolihvo.io	f	\N	\N
papa@jorisuz.co.uk	f	\N	\N
heip@cani.com	f	\N	\N
owagij@gingetek.io	f	\N	\N
fo@pa.io	f	\N	\N
lefosmo@fawriil.co.uk	f	\N	\N
weheb@isoho.com	f	\N	\N
gucas@vi.net	f	\N	\N
evdivcu@jak.io	f	\N	\N
ewnor@koc.org	f	\N	\N
ummi@dur.gov	f	\N	\N
wimfi@derwejgen.co.uk	f	\N	\N
eribip@dim.edu	f	\N	\N
ciznamluf@penkono.gov	f	\N	\N
enuji@benkutja.gov	f	\N	\N
vedowur@loowojac.io	f	\N	\N
kehec@ebbucja.net	f	\N	\N
aveju@ezihocle.co.uk	f	\N	\N
veizovo@dewuv.org	f	\N	\N
ca@jon.io	f	\N	\N
mocsib@cirgo.io	f	\N	\N
costoja@waw.gov	f	\N	\N
goufaut@luwab.co.uk	f	\N	\N
uphagfa@reluba.io	f	\N	\N
su@ri.net	f	\N	\N
paweifa@gusak.com	f	\N	\N
fasepjev@ogafi.net	f	\N	\N
kibiri@koftere.io	f	\N	\N
ponuv@howup.co.uk	f	\N	\N
cuberuta@biepi.co.uk	f	\N	\N
dah@cuz.org	f	\N	\N
jumfel@nulueh.net	f	\N	\N
tog@siv.org	f	\N	\N
liru@op.co.uk	f	\N	\N
opevar@utfebe.com	f	\N	\N
evomobmi@sepete.com	f	\N	\N
tuvnej@le.io	f	\N	\N
cobkom@bo.org	f	\N	\N
co@lep.gov	f	\N	\N
remuw@jopuru.io	f	\N	\N
tegesemo@roenvuj.edu	f	\N	\N
ewe@manog.co.uk	f	\N	\N
jemoh@kawbur.gov	f	\N	\N
socoma@laomigez.io	f	\N	\N
kanaz@mema.org	f	\N	\N
hesnaji@ve.net	f	\N	\N
pagmu@ka.edu	f	\N	\N
wemufmi@vigurija.gov	f	\N	\N
dup@nazepad.net	f	\N	\N
ap@gavakeog.gov	f	\N	\N
ijlo@vicot.com	f	\N	\N
se@tedu.io	f	\N	\N
kon@dow.edu	f	\N	\N
idfolo@kori.co.uk	f	\N	\N
arpil@nasvunub.com	f	\N	\N
ceiwosak@kucuaj.net	f	\N	\N
miwesa@ros.org	f	\N	\N
gurab@suvelelub.io	f	\N	\N
zup@ab.com	f	\N	\N
gurezi@folcib.gov	f	\N	\N
joboav@un.io	f	\N	\N
hed@cingi.org	f	\N	\N
nepic@zolozwuf.gov	f	\N	\N
uduhet@pomwic.org	f	\N	\N
fub@nuwok.io	f	\N	\N
deretjaw@gek.edu	f	\N	\N
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY pengguna (email, password, nama, jenis_kelamin, tgl_lahir, no_telp, alamat) FROM stdin;
esilo@veteku.io	qEWOIIAO7IHIrIE	Milissa Cottone	L	1992-02-17	087811745935	800 Ridgewood Drive?
onruv@ijpobo.net	lAFEmIOEgAZO3AY	Keila Grasty	P	1991-01-21	087733649892	Bardstown, KY 40004
netejcem@cuzkisiv.co.uk	FIFAFAcIPUEUDOm	Lianne Winston	L	1991-10-04	088968223131	8218 Court St.?
zolozu@wubsod.io	AElAFAVIxIvILU1	Nettie Whitelaw	P	1991-01-10	082556382434	Hoffman Estates, IL 60169
ozikibvu@wor.io	zAEOyA0I8ILElOp	Mariko Seibold	P	1991-08-12	087673961267	665 Kingston Dr.?
evkosa@seh.net	jIJE9A9U0E9AHUh	Jaye Porta	L	1991-12-07	082249875228	Glendora, CA 91740
ugsava@ubruffal.co.uk	xAoOJOyA5IzUNAO	Tatum Lagunas	P	1991-03-04	085424387329	895 Pumpkin Hill Dr.?
wed@ihienucus.com	mEEAQU7I5UnOfAW	Nobuko Castaneda	L	1990-10-07	081457629431	Little Falls, NJ 07424
rofik@uz.edu	tUjE8EhE3OwUkIM	Louise Lopresti	P	1991-08-06	084339914783	8114 Roberts Street?
nanotat@huthu.co.uk	kUOEpEyOeApAwOb	Cassaundra Lockamy	P	1991-09-11	081192151697	Encino, CA 91316
oh@nip.org	hI5OoEuATAlIpI0	Christena Tracy	P	1991-10-03	082134935531	819 Smoky Hollow Lane?
migtunor@gu.edu	aAsEEIKU2UaAoER	Ken Linen	P	1992-01-28	089963255736	Woodstock, GA 30188
zuzro@finapore.org	6OxOEIgAjUxUvIz	Layne Sierra	L	1991-01-17	082511266267	37 S. Brewery Dr.?
niltafli@jeewedi.io	hIvUJAqEUA1OSO7	Tien Drinnon	P	1991-01-02	085298394146	Glendale Heights, IL 60139
egven@di.net	fEnOiUAUYEtITIH	Landon Shine	P	1991-12-16	088129155976	9698 Cedar Swamp Street?
ikwuwne@kap.co.uk	VIuIEAaULEREfEr	Terrence Coletta	L	1991-02-23	084367461982	Saint Louis, MO 63109
pid@ke.io	NOYUiAAI3OjEjUV	Mathew Landeros	L	1991-09-20	081161195338	117 Brookside Ave.?
topa@geztik.org	3AvIPOoOBAYUZAB	Ina Davin	L	1990-05-03	087962289993	West Lafayette, IN 47906
liev@ravwu.co.uk	zIUIBEcEpEvI5OY	Oswaldo Kardos	L	1990-12-24	081342577712	44 N. Branch Lane?
fi@idarof.gov	GOAUrAFEiE7AVIy	Keeley Losoya	L	1990-08-07	087382317134	Glen Allen, VA 23059
he@kodizi.co.uk	CA9IBU6AxU1IUA7	George Chiasson	L	1990-07-06	089718124931	152 El Dorado Court?
caoko@jufonima.org	5A5IhEEAFEcIXEh	Geralyn Tenney	L	1991-07-05	083358592721	Lacey, WA 98503
fiuco@po.gov	QIpOZE7OcULIROh	Louann Ord	P	1992-05-05	087325779134	491 N. College Court?
pijejaze@bop.com	lU8UKUZI6U1ElEJ	Carmon Michalowski	P	1991-01-17	082222641351	Middleton, WI 53562
ces@fefeit.edu	KUKOUAfOQEUA6EW	Isela Class	P	1992-03-26	086675978688	638 High Road?
nocmafi@judda.net	1UnIuUKU6EkUNAY	Nadene Rosenau	L	1990-10-10	087778146691	Wooster, OH 44691
vama@zi.net	hA2E2UhEPEMUFIK	Keisha Hathaway	P	1991-09-17	086447234578	8 Ketch Harbour Lane?
wiofu@ruafvu.gov	OECUFEsAzEkUnAH	Ryan Shippy	P	1992-01-15	081217822978	Bayonne, NJ 07002
mavvo@jeniv.org	VE9IcIJOuU0AcIo	Kimiko Hage	P	1991-11-22	084942929452	3 Paris Hill Ave.?
fazgif@dir.co.uk	DUWEVUqUkO7AEAo	Mariela Strope	L	1992-06-23	089619492818	Fall River, MA 02720
adwior@kionanu.net	JABOwAtOEIgAPEo	Rubi Mannon	P	1992-04-01	087214668429	30 Race Rd.?
luzub@ep.org	IADUlEBAHOGAAIu	Andree Sloat	P	1990-10-13	086514947663	Commack, NY 11725
cor@jawip.co.uk	yU2IFUFOIAMARI6	Steffanie Peebles	P	1991-11-03	084965786624	47 Cedar Ave.?
lopvu@zudpaki.co.uk	zA1AkA5IfIKUtOs	Gertha Rawls	L	1991-06-30	086955916286	Brick, NJ 08723
po@cahgamuji.io	jATAZAyOhALEMIt	Beatriz Fuhr	P	1991-10-04	089331581367	19 Surrey St.?
facehfe@pidmu.com	dAtI4EuEyI2IPOS	Adrienne Suh	L	1990-09-10	081564692516	Findlay, OH 45840
mek@funsako.edu	TO6IqOqUgUGUHA2	Felix Gallman	P	1990-06-11	088818513152	9963 Wayne St.?
buahe@ismulgam.org	TO1EFEdI3ODEsIh	Nannie Heiss	L	1991-09-21	084343587854	Jamestown, NY 14701
ovzedi@azpapvuc.net	LUmUKUIOfAGEaO0	Mohamed Toenjes	L	1992-01-01	087961646963	8516 Snake Hill Street?
ci@hom.com	hAjOHOrU9IBO4A7	Foster Danziger	P	1992-05-13	088143416676	Point Pleasant Beach, NJ 08742
dupbajov@lidhugmal.io	ZOtUTO1ILOeOvED	Harold Wyrick	P	1992-03-21	087746296961	78 Parker Ave.?
how@rizaur.edu	lESEPUeEeOsIyEg	Michael Onorato	P	1990-05-02	084612133834	Natick, MA 01760
efuepiec@ow.gov	gUEOME4OjUhAHOq	Spring Bassi	L	1990-06-24	083356322881	984 Elm Ave.?
azakopibo@fuew.gov	HOPIXOPOfISInI4	Desmond Faucette	P	1991-12-29	081751867965	Port Charlotte, FL 33952
he@huow.co.uk	0UtOQUZIgAKE9Aj	Rayna Laffey	P	1991-08-31	084266269469	9 Theatre Lane?
fi@wadbiwlo.gov	kUbEoOXEBAwOVEn	Dexter Dickinson	L	1991-05-23	089928376237	Buffalo Grove, IL 60089
pisopu@fomabako.co.uk	DEGImUeU1ElEkIy	Rubin Gerry	P	1990-09-15	089323459346	493 Bishop Street?
ut@movpuwom.net	QUfIOIAO8AjIvIe	Evonne Dejarnette	P	1991-11-06	082815352134	Corona, NY 11368
detcogu@war.edu	NEpAyApECEqUeO1	Chau Lama	L	1991-09-04	088787972317	10 W. Windfall Ave.?
uzpude@fisojelev.io	yOsUOI8ANAgUNUQ	Malena Schulte	P	1991-06-12	085187592372	Port Jefferson Station, NY 11776
uwbew@wircihhat.gov	2IUU3OqAPI6UFI0	Marleen Raiford	P	1990-08-11	088542282885	18 Pacific Circle?
bebuh@sot.gov	SEvOlU9ImAHEYOR	Taina Bertolini	L	1990-12-23	081418171924	Yonkers, NY 10701
zab@eje.net	UAbOaAiOdU3OiEb	Tonya Mcneilly	L	1990-08-19	088347442979	24 Devonshire Ave.?
adoujkum@gavro.net	iIOIvUAEBUDOJES	Jane Corle	P	1991-11-09	088223564276	Howell, NJ 07731
jezrubki@masna.co.uk	QOVAPEUAbAXONEF	Rayna Heeren	P	1990-05-08	081386898545	7754 Chapel Street?
gap@wemihir.org	2OOOGEhI4IwItEs	Alayna Redus	P	1990-09-13	088478681896	Iowa City, IA 52240
pinku@kerro.edu	zOsUFIWEpOYAzAM	Amber Twellman	P	1991-03-05	089478782553	9801 Andover Street?
renicul@pazuse.net	uIWAeAqOAAnIRUT	Graciela Elser	L	1992-02-07	082459492377	Muskogee, OK 74403
rigic@lob.net	kUREgO7E3IuIYUg	Hulda Vicini	P	1990-11-17	081496296523	39 Carpenter St.?
susgijic@at.org	zIjOhIqIUAmUvEI	Breanna Hickey	L	1991-10-17	089463257786	Gloucester, MA 01930
cirif@eciipi.edu	MELUKE9IlOeIIEC	Jacquiline Christo	P	1991-09-20	088283892617	8006 Randall Mill St.?
ko@fuvlujom.edu	0EsOFILIrUdEGIq	Zora Amante	P	1991-09-23	083252856347	Lombard, IL 60148
gukeroc@juihade.co.uk	MIXUVUWEjAkESEV	Emile Sharrow	L	1990-05-23	083844323678	128 Durham St.?
copzocnid@lu.gov	II7O1AhIZIRUfEZ	Evangelina Days	L	1991-04-24	084385277986	Port Saint Lucie, FL 34952
dafhol@asawike.org	AUDA2OBIaIQEAEb	Jason James	L	1991-09-06	081544697269	7755 South Deerfield Street?
jazuggo@zeb.co.uk	OAOE8InEQEtOZId	Zaida Bellantoni	L	1990-12-08	085397134252	Panama City, FL 32404
ovhoco@pe.org	3I2ECUuIfU9OxO1	Reba Hunsinger	P	1991-03-14	088186752783	8508 East George Dr.?
baftoj@li.gov	iItOdEZEVIhOSOJ	Elise Vandiver	P	1992-05-01	082727124247	Wayne, NJ 07470
zobeh@les.gov	uOtAQE8OiEKIoIW	Demetrius Pauls	L	1990-06-13	081137781163	8923 Hall Lane?
leztigjod@rehenas.edu	LIsOFUsESAEU0EH	Ligia Guinyard	L	1990-11-05	086178722531	Champlin, MN 55316
wedujiw@guh.edu	ZUFExEmUSI2OmUZ	Ardell Werts	P	1990-07-21	088556791789	20 Golf Dr.?
lalenir@tajuwne.net	8O6ARUaERAjAzO6	Inger Steve	P	1990-09-17	084927971614	Crofton, MD 21114
lidkoc@ze.gov	QIeOKU6UDAAEmOy	Phil Ruybal	P	1990-08-22	088567967797	62 Shipley St.?
ankez@dusfug.org	sU0IVISANUZUCUQ	Milissa Coston	L	1992-03-23	086489752474	Chandler, AZ 85224
fajigag@igwuz.io	3AyIXODELA7U4An	Norene Fallis	P	1992-06-11	082858618445	7788 Pleasant Rd.?
bopoc@meksiw.net	DEXE4U5OhACOfE9	Tiffaney Sedlacek	L	1992-02-16	089772899743	Elkton, MD 21921
kuleboj@ozwecwav.co.uk	uInAEOVAjOzIFEG	Danny Riesgo	P	1992-04-13	088263176174	830 Beech Street?
zenu@morwulok.edu	PU7I7I1ONAdEKUL	Rosaline Guilbert	P	1992-06-15	083823117739	Georgetown, SC 29440
jak@kevkupdof.co.uk	HAwI5AfUbUEUXUf	Edith Kearse	P	1992-06-20	086413149127	96 Depot Lane?
bugaw@or.edu	RITApURIeUWIHOz	Hans Burkhardt	P	1990-11-18	083511142617	Catonsville, MD 21228
cepwasfa@habik.co.uk	PA5UxUoAEAOITIB	Signe Silvia	L	1992-06-02	088148424726	8099 Rock Maple Ave.?
viz@nevhu.net	9ArAHU7OuU1IuAs	Kami Sirmans	P	1990-10-19	089446241187	Barrington, IL 60010
mus@uja.gov	AEgIcAcEuUQUXAL	Kathline Fenton	P	1990-06-27	089186534812	68 Baker Rd.?
milcetwo@zutenjo.edu	9OhUFIhOlE4UKOS	Stewart Moitoso	L	1991-11-09	082141767312	Carlisle, PA 17013
dis@pavejus.edu	jOmUhAkAzUAORUb	Galina Given	L	1992-01-25	088789189642	97 Henry Dr.?
ihi@gabtanto.org	vOPIyI6ELOnEfEd	Richie Bultman	L	1991-04-17	081369951317	Saint Joseph, MI 49085
ruhtad@vavihza.net	oAyI4InUXUHOJIa	Adam Gehlert	P	1992-02-07	088782819327	5 S. Hilldale Ave.?
coje@wuf.io	COPEiO7IvEgAZI0	Donny Reller	L	1990-07-27	086598721415	Terre Haute, IN 47802
pedfur@ze.co.uk	ZAGIRI6EIAWEtUx	Latesha Caron	L	1990-05-01	082141817849	9811 Arcadia Street?
jedmew@cihuwde.co.uk	dO0AxOuUwEoEwAg	Patrina Balas	L	1992-01-07	086817681684	Dothan, AL 36301
pacajti@uj.edu	OENOYO5OuUOO6IS	Branden Haygood	L	1991-04-02	089174146214	6 Wild Horse Lane?
ohijazren@izfu.net	HOHI6I4O4EdUxOh	Carter Whitfield	L	1992-05-22	088514663452	Maplewood, NJ 07040
wihut@judhindiz.net	UOuUtOZOKO0EZUl	Edwin Killebrew	P	1991-09-30	082974257137	9899 Goldfield Drive?
zoju@zururub.net	OIWAwA6ALISAXUU	Marianne Kobel	P	1992-06-05	084162675589	West Palm Beach, FL 33404
akehihi@zoji.gov	CEAEGOZEAOVEJOT	Petronila Rhein	P	1990-12-28	089132929341	679 Evergreen Street?
vezi@jocow.co.uk	GEAOgOAEAIuOmAa	Diana Tift	L	1990-11-30	084937321975	Chapel Hill, NC 27516
haul@roama.net	bU4AoUNE2IFEgAX	Del Dieter	P	1990-06-01	089711397186	624 Ashley Ave.?
mehrulihu@riribkuk.net	MENInAjEkULETAP	Christa Pozo	P	1992-06-18	084779992619	Rockville Centre, NY 11570
tagu@ajaehagis.com	9UIIwAZOBAtUbOu	Zenia Meggs	P	1992-03-03	084889388686	30 Shirley St.?
bu@diadu.gov	lA7EHUNEpUWE8En	Dudley Brophy	P	1991-07-15	085469154232	Petersburg, VA 23803
do@ki.io	XIYObUQAtEMUAUp	Ruthe Shorts	L	1991-02-28	084615878837	19 Peachtree Ave.?
puz@feumu.net	jAJOwOcIQEIOOAF	Josephine Dearmond	P	1990-07-19	082242146915	Augusta, GA 30906
deem@tuvir.co.uk	IAzE1EIOEEMUpIR	Jacqulyn Ritter	L	1990-10-20	083944234643	2 John Lane?
ta@elbuple.net	vIfEFUdOLUjAIOr	Mika Bortle	L	1990-12-13	087416399235	Oklahoma City, OK 73112
ac@nettiguj.edu	KULErOGE3IeUMUl	Teresita Mizelle	L	1991-01-04	088333665644	8 Madison Lane?
halilil@wuturo.net	4EHUYAKEQUsEFIR	Silvana Holtz	L	1991-05-18	088756398873	Marietta, GA 30008
tocepu@wela.co.uk	lAIEpAwI9IiIoUk	Alfredo Gorney	L	1990-08-15	088963568843	7388 Hill Field Court?
guz@tuve.com	iEkOEOsIGUfONIb	Emelia Whitehurst	P	1990-05-14	085574235677	Reading, MA 01867
kehu@pom.edu	aUGIwEdOVE9U2A0	Brittani Defore	P	1991-10-04	085997282174	7394 Monroe Dr.?
na@tazevze.com	yEGOOONELAKUVU6	Arla Coryell	P	1991-03-30	087522163664	Zionsville, IN 46077
tu@pevu.edu	qUUIFIXUoIEEQIm	Brandee Santerre	P	1991-09-23	085869773768	21 Helen Circle?
uc@zepunu.org	cOUU4AGUcE8EsUt	Brigid Redel	L	1992-06-15	082997255589	Ossining, NY 10562
sijtu@ja.io	UUqIZAMAMEgEZA7	Sharika Bastin	L	1991-06-27	082892837939	97 East Airport Dr.?
cel@map.io	tEOOZI9UbAUISI8	Shalon Pillow	P	1991-09-09	088235125789	Pottstown, PA 19464
teuzu@va.net	0IdO6U8EcIrAPE4	Kirstie Lush	L	1992-06-04	083829216189	1 Bishop St.?
hu@eha.co.uk	zAuOQEKEUEUE0O2	Ayesha Ro	P	1992-05-19	083964621362	Rossville, GA 30741
dimkerra@lebog.co.uk	aUiU4A8IkUcImAX	Hellen Catto	P	1991-04-13	088433692748	904 Glen Eagles Street?
oficaznaf@lomu.org	BA9ABIXU8U1UOOi	Latasha Guttierrez	P	1992-03-01	088891427856	Lebanon, PA 17042
ucetaeb@zola.co.uk	fAsOJA3UCUaUmOD	Loren Kao	P	1991-05-06	088951138283	942 Columbia Dr.?
ijages@lomulel.io	EUrICIhIBApIxAM	Nannette Lashbrook	P	1990-06-14	082927863221	Uniontown, PA 15401
pidcem@bucfauru.com	kAYEpUwA8UlItEm	Kayla Nicoletti	L	1990-11-10	089336167416	486 Queen Ave.?
anowuw@sabapmi.org	gO0OfArIoUmUoId	Etta Feld	L	1991-07-29	087347832657	Elizabethton, TN 37643
sadhupac@zon.edu	xEXEjOJIYOAOrUO	Daron Gerow	L	1991-07-31	088734247292	8217 Creekside Dr.?
noge@wib.com	BOBOkABODOzESA2	Pearlene Stapler	P	1991-03-29	082592456772	Millington, TN 38053
po@fahida.co.uk	jOJIKOzOMEfU6UQ	Eva Spells	P	1991-12-14	081331397341	32 Hillcrest Ave.?
vanesu@juwhuzu.gov	3IuEHAVEuOtEIIi	Lisa Heineman	L	1991-01-15	086568193686	Menasha, WI 54952
sisi@povzurudo.io	BEhEWICEOOPEnIP	Marjory Kellogg	P	1991-12-22	089251589522	66 Wentworth Dr.?
hovarwuh@sup.io	TUFO7IDUvI8IfAb	Bobbie Feinstein	L	1990-12-19	084483636822	Manchester Township, NJ 08759
inu@fo.gov	ZIkEGApEHUREIO2	Emory Baize	P	1992-02-27	086278668459	1 Whitemarsh Drive?
pi@kekbu.io	MEcO9IdUQIAIcU3	Sidney Sandhu	L	1992-05-14	087887958597	Lithonia, GA 30038
subzola@kacjis.edu	9E2O4AeEdUHIJA3	Lean Mcmains	P	1990-12-13	082625867873	8734 Winding Way Drive?
si@behab.co.uk	DIVUjAgU3U2E1I9	Tinisha Lembke	L	1992-01-14	084251991812	Doylestown, PA 18901
ruemo@ru.co.uk	WElUnE4OLUpEMUZ	Dorthea Nelms	L	1992-05-01	084936825693	7255 Campfire Dr.?
pufvep@zagicmac.io	aAyUhEoEYOJEuUY	Bethel Siewert	L	1990-09-28	088951272311	Longview, TX 75604
nu@erajod.org	JEfUaOrO4E1ECOw	May Mascio	L	1991-08-13	082718925273	9817 Hill St.?
jiwuol@vi.io	UAuOkIUUQOAAdUn	Doloris Buerger	P	1991-02-03	084181967955	Saint Petersburg, FL 33702
vuc@ifef.net	bOXAoIUOHOPAxOx	Eun Strasburg	L	1992-02-29	086266286691	7888 W. Plumb Branch Street?
gaupe@uf.org	PEqOfIOO3AzEOOQ	Cassaundra Maricle	P	1991-02-11	084331844346	Hoffman Estates, IL 60169
dup@ejmameh.io	mAYUiISIbIoOMAO	Magen Nolen	L	1992-06-04	084319642311	49 Court Road?
erfi@zokiw.co.uk	FUqIaIrOjEzO2AD	Vena Sarles	L	1991-07-19	086322623457	Endicott, NY 13760
mewugek@sojkatta.io	EUCIMAGAJUoOuIJ	Bill Stocker	L	1991-03-03	084276517578	510 East Rose St.?
jawantu@siziin.io	pUcIOAaIUOeOzAQ	Emmanuel Mcbeth	P	1990-08-22	085587839247	Edison, NJ 08817
ced@ego.net	PA4EWIeIjOAEgAe	Gracia Matchett	L	1991-11-04	083628712251	85 East Avenue?
eptew@os.org	ZEKATO2AJUgEJEx	Aurore Politte	L	1991-09-02	084543177987	Salisbury, MD 21801
jotugve@oclekuju.io	uOeUsUVI3OrA7U6	Crissy Wrona	L	1990-08-19	086922486849	9578 Campfire Rd.?
ze@erejanek.com	oOxE4EyUbEtUHAR	Kyle Mcgonigal	P	1991-10-15	085162836248	Glen Ellyn, IL 60137
vim@kieh.gov	hOgU4U3OYINOLI6	Arnoldo Claude	L	1991-02-05	082869743679	179 Sunset Street?
evisep@uwre.net	WU2ANErI8OIE8Id	Cameron Golay	L	1991-08-13	087372928895	West Roxbury, MA 02132
hu@zusrusiw.edu	GObEqOmUUABIXU7	Shemika Mogan	L	1991-10-13	083596838437	500 Center Lane?
ji@kozko.co.uk	ROUETOrUdEuE0OB	Aliza Addy	L	1991-12-15	081355673883	Soddy Daisy, TN 37379
il@up.co.uk	oIDApAjIoEpOEAd	Modesto Klann	L	1990-12-01	081739195412	66 Bay Lane?
obofac@fel.edu	cArUcACU9EIO3Iw	Drusilla Balas	P	1992-04-16	087261146677	Atlanta, GA 30303
rohoci@muza.io	bOGOGAmODA2EoAK	Barry Stalcup	P	1990-09-16	089545225679	8712 Lilac Drive?
adaerop@ono.net	MAfOUANIfEUAgUA	Luke Harling	L	1992-06-02	086354176689	Bergenfield, NJ 07621
waruk@tu.org	gOjUuIUI9E0OwU9	Elvin Valletta	P	1992-03-25	085235593768	146 Woodside St.?
eto@cenjiv.co.uk	FAoAuUCE1OXEFOt	Jarred Troupe	P	1992-01-18	083652648325	Gurnee, IL 60031
fiw@mamko.co.uk	XUqIWEPOtOuU5IT	Twanna Deanda	P	1990-10-15	089981622761	644 Purple Finch Rd.?
gukum@ficoofo.co.uk	dAnARA4UEE2U7Oi	Chanell Nellis	P	1991-07-27	083783748687	Bristow, VA 20136
ho@jesopi.gov	WABATAoEfA0OQUz	Jayme Gosselin	P	1991-09-06	083981475717	895 El Dorado Ave.?
hemvofvod@osulojmov.com	bEQI4U9IPUdIlOT	Teena Dagen	P	1991-05-02	084174532111	Jackson Heights, NY 11372
zu@guji.gov	9EuEkA1O9OVAxEU	Stephania Gang	L	1991-07-02	089597287124	8803 East Chapel Rd.?
uk@kiko.co.uk	ZAfEBAAUGAMAAEW	Jene Dulmage	L	1990-05-15	088823979931	San Lorenzo, CA 94580
powjajrak@el.io	wAhUUOHAUEhO9OL	Edmund Lafollette	P	1990-08-28	083664799781	4 Philmont Drive?
ige@ocwope.net	OUCANUHAuUIUBUY	Alaina Pearman	L	1991-05-30	086254826984	Wakefield, MA 01880
jodluf@soklizkec.org	zAIIiA8ERULAvAD	Mickey Menefee	L	1991-04-09	088211434162	7497 Thomas Ave.?
canehiler@enlo.edu	OIXEaEnOLEKImEN	Katlyn Lang	P	1991-01-01	086683364345	Annandale, VA 22003
teguc@pohon.io	eAjIJIjUkUlAKI2	Ria Shipman	L	1991-06-24	087951798979	589 Arrowhead St.?
zonur@vocumotoz.net	EACO3UfOUANOzAy	Alexis Stonge	L	1990-12-10	085432924857	Billerica, MA 01821
povioge@na.gov	MO2UyOfUxUpA3OD	Ai Frame	P	1991-08-07	081852291179	683 Briarwood Ave.?
jazurot@kentefe.net	9UUEYUGAAOGOIOb	Kazuko Sandhu	L	1990-09-19	088124853623	Wisconsin Rapids, WI 54494
ija@ub.net	xIdUoECU2OFItO4	Wanita Demmer	L	1992-01-13	088165549253	228 Pennington St.?
udne@donazmuv.io	YAaIIA2OpEGA6ID	Georgia Ratley	P	1991-11-07	089385753397	Unit 8?
imsebap@oci.io	jI4U2ETUnE9OmIG	Logan Stecklein	L	1990-06-19	087732673828	Elizabeth, NJ 07202
ta@roc.io	YA6IvEyUnI1USAV	Sasha Hassel	L	1992-03-25	089658326411	8913 Orchard St.?
mir@ru.org	qOMUREVIEOnUqIf	Cordie Monarrez	P	1991-10-08	081834637235	Clermont, FL 34711
fura@upipu.gov	DIjE5EVEbEwIHEV	Timothy Tabron	L	1991-11-02	089373146882	8854 N. Smoky Hollow Drive?
solodsi@vinepmid.org	rA9A6UGI5O8OQI5	Desiree Bird	P	1991-01-26	083814726138	Ft Mitchell, KY 41017
jumovbot@ul.edu	qOmEzU2OeUSOxUr	Dean Yuan	L	1992-03-16	088353195485	51 Beacon Circle?
ba@nod.gov	aAqODI0E9UgOBIt	Scottie Adamek	L	1990-09-25	088462878989	Levittown, NY 11756
ur@ru.com	JEEUXAyE9OlIcAl	Patrina Brawley	L	1991-04-04	088235459285	22 Essex St.?
tipdabit@ogazov.com	MAYOGUyAZOIUnEd	Kalyn Kina	P	1991-09-27	082575721643	Mesa, AZ 85203
tumog@heovazor.edu	VE7A8EBAtAoAYEA	Francine Cheatam	P	1991-06-17	086655242682	860 Madison Avenue?
jidfow@ditiduze.com	iIkEcUxIfEDOYEy	Von Chadwell	L	1991-06-19	083554638627	Bergenfield, NJ 07621
kub@etdav.org	oONOmApEIUvUWIJ	Dorothea Binder	P	1991-08-06	088734754197	796 Poor House Lane?
ji@ihmomih.io	6EyAMA5EQAEI1Eu	Lashaunda Honaker	P	1990-07-12	087825982635	Ambler, PA 19002
loh@cetumi.com	FAuEDA0E6IuIsU9	Thelma Arthur	L	1990-07-02	082449261371	9 Selby Ave.?
nimzin@guznerepi.edu	LEWI8ACIAUaI2Uo	Michelle Duffy	P	1991-01-26	085135326399	Ashburn, VA 20147
uc@rebufjen.edu	EEMU7IkIfUIAuIv	Ali Hutsell	P	1992-02-28	086965738861	6 N. Santa Clara Dr.?
ejoha@acsudo.net	1AEOjEQEAE1UrOj	Nona Quaid	L	1991-02-15	083256167351	Hazleton, PA 18201
gita@parwaj.net	HADA9EaIrOOUkEi	Silas Longerbeam	L	1990-07-20	084648336285	27 Miller Ave.?
ivduse@zuhew.org	SI5AiAYU2I2AHIT	Sammie Guillemette	P	1991-04-20	083651227954	Niles, MI 49120
vobvi@vo.org	1OIUzOjIoAlEoU5	Hildegard Bridgeman	P	1991-10-29	087265658851	804 Foster Drive?
cin@dorpaz.org	UUlAcOXEXUVAeIn	Arlyne Rego	P	1991-02-07	086989153346	Teaneck, NJ 07666
jujrecwo@veofowe.com	zEDABOiIoEeAOUq	Keenan Mencer	P	1991-01-13	089474269559	856 Woodland St.?
rulsiw@lecwuzla.gov	PUvIgOYASAHOYAz	Leah Pouliot	L	1991-10-29	086112511558	Apple Valley, CA 92307
cito@no.edu	VO0OgUiAzUKOjIx	Ida Shortt	P	1990-12-18	083672427763	7019 Vale St.?
esoli@velmurap.io	lAsUsEbIfU1IfAY	Madalene Duryea	L	1990-10-14	087863947394	Fitchburg, MA 01420
tomva@kej.gov	6U9I5EVI3U6ImOW	Velma Keltz	L	1991-07-25	088919153475	8367 Lincoln Road?
neloke@lu.edu	MOEErOgUDUPU1AU	Danette Klump	L	1991-08-30	088729885189	Elkton, MD 21921
sud@ul.org	9O6IpIyO7UjO6U1	Danica Fritze	P	1990-04-29	084142334698	2 Old Princess Rd.?
war@fohrahsin.net	WUOIZOUIPEsU5On	Berta Harley	L	1990-12-16	084374987394	Rockville, MD 20850
ilam@hadjaus.org	FEWIoA1EHUZEOA0	Almeda Colby	L	1991-08-31	084469964342	491 Sherwood Ave.?
dipcoc@ziszipmes.io	tOtEOAlARImIHOg	Tamesha Trunnell	L	1991-09-13	083622919137	Sevierville, TN 37876
wano@liwjumu.com	nAdAwAcOiEKU0AK	Marlin Norman	L	1990-11-04	081827482454	9623 Manhattan Drive?
malvo@gigdelkun.com	CA2OPAQUXE5IqIm	Melodee Fudge	P	1991-06-14	083692698322	Fond Du Lac, WI 54935
faopidu@uhi.com	ZUJU8OLAPE9OKUC	Gricelda Cumpston	P	1991-02-15	082867529387	524 Penn St.?
zeb@fuz.io	AEUO7OtALE7UmEb	Timika Perrone	L	1990-11-15	085952855122	Clayton, NC 27520
lud@wubra.gov	kAmUjUzUhInUrIg	Ahmad Kurek	L	1990-11-09	082214455263	633 Pin Oak St.?
cafvadpoj@miwoda.co.uk	tUJEXAREwOuISIh	Jerrod Verrill	L	1992-06-16	086368635428	Alabaster, AL 35007
vig@dehede.org	4IbIVIJEyI7UVEk	Jade Mclawhorn	P	1990-05-01	086825237933	791 Fawn Ave.?
hisudo@ohlu.edu	kOIOFOgAUI3EaUj	Willodean Nakano	L	1991-03-06	081267682357	Windsor Mill, MD 21244
mezhi@te.edu	jIgOWU2OAE3UiIJ	Sunny Harsh	L	1991-03-23	086761862611	293 Hartford Ave.?
wa@ejadu.gov	4IQE1AkOQU6OLEI	Antone Medlen	P	1990-08-02	086194697692	East Orange, NJ 07017
ez@upzi.com	AUNUdIlINEEAdEQ	Criselda Nash	L	1992-01-19	088214939561	8522 W. Pacific St.?
if@azo.gov	cODUKO3OwUDOBOw	Kamilah Dragon	L	1991-11-28	081592841411	Statesville, NC 28625
hazut@ezhofid.co.uk	rIOAiOFORAbE4EG	Salena Decuir	P	1990-11-15	082939171855	9533 Ivy Ave.?
meiru@dofigoj.org	NEUO5OTOkO8U1A7	Celesta Quinlan	L	1991-03-11	085487246377	Stoughton, MA 02072
cej@juz.co.uk	PUsIiAPIrA4IHUt	Keenan Brockway	P	1991-10-29	084425437782	830 Oak Street?
rofuhbe@vetidi.co.uk	LOMOpE6ULEdAzUw	Cristine Dison	P	1990-05-05	081693553195	Flemington, NJ 08822
bavit@ji.org	hOxORUMAKUCEzOQ	Stefanie Trammell	P	1990-06-05	085288917748	174 W. Kent Dr.?
ciraofi@wikvuut.io	AUjItOlEZUmEqA3	Rocky Vandam	L	1992-04-27	084248458787	Longwood, FL 32779
juciddu@difuci.org	LO4EcIkURUKEiAI	Grisel Cashwell	L	1990-05-16	088844638877	506 1st Lane?
map@levuv.io	bEoUmO2AmUzEVA5	Yuk Warwick	L	1990-12-22	087359971957	Shrewsbury, MA 01545
vacawhep@kalagwo.com	wO7U0IWAuAXIaUJ	Sage Mazon	L	1990-10-03	088282595887	24 Lafayette St.?
kodon@jeej.com	cIzOQUrOaOtIrEK	Charlyn Phillippi	L	1991-11-21	088485446438	Easley, SC 29640
satme@lir.gov	0InOzEGIoEAIiUq	Kamala Molino	L	1991-04-05	081733211377	16 Bald Hill Ave.?
igupu@ogezegpod.gov	sIoA6AaOCAiIyUU	Cheree Wakeman	L	1991-07-05	087678467496	Lake Worth, FL 33460
le@lopdooc.com	oOpUCO7UbIRICEq	Zachariah Bilbrey	L	1991-05-29	089496574512	9149 Ryan Street?
zi@mimpu.com	3UxEyAoAXIOIfOP	Marcel Charity	L	1991-03-05	085472973487	Rockford, MI 49341
ku@titdotres.gov	DAVORA2OLE3A1Ej	Cyrus Derose	L	1991-04-30	088354192694	833 New Lane?
je@eno.net	cIbAGElUiAWIvUl	Wonda Giambrone	P	1990-09-01	082215344952	Bel Air, MD 21014
ginid@unfioc.org	yAlOiEjOhAhUbOc	Truman Dancy	P	1990-08-04	085148692692	285 West Olive Ave.?
idudovje@wadmisre.co.uk	1APIAAKEKESAZUs	Gilda Dresel	P	1990-05-25	084378529779	Stow, OH 44224
nifu@lotedpoh.gov	QA1IVUbUoO1ITOG	Cathleen Rotunno	P	1991-11-18	087957749196	46 Silver Spear St.?
sepmajbo@racupmib.io	vUJAoIOUtUVUGEO	Judi Worrall	L	1991-08-06	083396326191	Beloit, WI 53511
getat@cotehef.org	fIPUaOoAZAiUkAI	Karey Buckalew	P	1992-01-12	086687967355	8020 Lawrence Rd.?
it@kaf.io	MUgAwAVIEECO2UJ	Veola Mcewen	P	1991-08-13	086772482338	Hanover, PA 17331
va@bebim.net	gIIIdIuAQIyUUE5	Reid Gayles	P	1991-06-01	088326532557	7139 Country Club Ave.?
oceob@codetcuh.com	dIKEqUXOvO5ApEz	Berry Luna	L	1990-08-22	089688442988	Wake Forest, NC 27587
ikruj@sapanti.com	KUTUXEVIPAhU0Ar	Julian Carrero	L	1991-06-24	085531668194	93 Wellington Avenue?
fokjar@ril.gov	6EiIeUlOgUSEfI3	Noelle Smyers	P	1991-04-08	081649878436	Woodbridge, VA 22191
bod@zij.edu	HAvADEzEMAGAaAu	Lourdes Dailey	L	1991-10-06	085265666716	8 Chapel St.?
giwjufu@mahuh.net	7IaOvASOWI4AgUh	Shanna Navarette	L	1990-06-16	086257994223	Freeport, NY 11520
ew@egzo.gov	uEPExO9E1UsInEf	Hattie Stack	P	1990-06-20	089989388487	966 Lakeview Lane?
vuim@zadjul.co.uk	MO8UdIDUeUOAYAs	Junita Groh	P	1991-12-17	081598288337	Perth Amboy, NJ 08861
no@ugu.com	kUvAvUmOtU2U6I6	Lyle Lalor	P	1991-05-05	084765314259	17 Pineknoll Dr.?
hewunul@rigfuda.org	zErAhUIE5AMA7Aq	Lorita Loudin	L	1991-11-07	086762277756	Ottawa, IL 61350
ofi@veubuda.org	rUIIAEBAkAJIVIZ	Shira Renfrow	P	1991-10-16	086469864169	839 North Richardson Ave.?
sulovsi@ewigi.edu	sEfIaEBE7E0AVUX	Diego Vides	L	1990-10-26	083996295776	East Haven, CT 06512
cileiji@nek.edu	4OtIfULOnIfIQUC	Romona Mcdevitt	P	1990-12-25	087824297964	25 Amherst Avenue?
jaolo@fihotnem.net	OUME6AtI2OUEeOd	Nicholle Staggers	P	1991-10-11	086396969241	Columbus, GA 31904
boobaca@we.gov	EUIIPIGEvAiASU2	Khalilah Ptacek	L	1990-08-03	083728798547	189 Washington Rd.?
uvogew@onumi.io	gU4E7A9OSAMUPES	Shakita Mcmaster	L	1991-02-14	089553928917	Mableton, GA 30126
tinubwi@ewo.com	KEAAgInUwI7IMAn	Cathern Tidd	P	1991-08-07	085441229698	80 S. Addison St.?
jibloc@ol.co.uk	0IVA0ExE4A6AXIs	Audie Cutting	L	1992-03-06	082549274418	Cottage Grove, MN 55016
tufilco@luri.edu	aAZOzO1ABUDIjEM	Blanche Highsmith	L	1990-12-02	085759525417	44 Addison St.?
gigat@saf.co.uk	COoINE0OqARA6AY	Wan Schell	L	1990-07-30	083429719388	Painesville, OH 44077
is@fe.com	aE4I0EjIJOJEAO7	Annice Schaber	P	1992-02-05	081577191761	778 Carpenter St.?
murgupo@hiiluhe.net	VIcOcEsEVO0AUOb	Miesha Holmon	P	1991-06-02	088175241277	Summerville, SC 29483
efo@ran.com	gA4IHIQELArItID	Caitlyn Dahlin	P	1991-01-17	084449324542	822 Hillside St.?
jude@fok.edu	nIkEyI3ODEuAxE2	Josefine Scheidegger	L	1990-05-07	087915787139	Falls Church, VA 22041
woatva@wodlopwep.edu	2OxAiU8U1AyOOIj	Tish Uriarte	P	1991-04-30	082376971177	7227 Creekside St.?
ji@fogukhu.com	mIUOIE7UWAFIsEc	Noemi Amburn	L	1990-06-30	082882529435	Phoenixville, PA 19460
laligni@hukru.io	uEUA7AVOAUjElEf	Malika Self	P	1991-08-22	085664732853	42 NE. Nut Swamp Rd.?
sivugal@opnulaj.gov	aEfA1UbIvUFOeIn	Carlo Price	L	1991-06-14	082659296173	Piscataway, NJ 08854
efuis@uc.co.uk	9OvOQEmAQAYU8En	Charlott Younger	P	1990-07-31	083496787769	51 Green Drive?
ivnu@viset.edu	eOxIFUtU6EbEuOJ	Georgeanna Moors	L	1992-05-29	082684611295	Jersey City, NJ 07302
mireg@nevwacko.edu	JAIAxUhU4EREFAP	Almeta Effler	P	1991-06-15	082976416239	452 Lancaster Street?
ijo@sisfopbup.org	fU2IHU9UwAoOfEp	Jule Maisonet	P	1992-06-07	083376434524	Trenton, NJ 08610
il@caguri.edu	UUZUfEQAxUQAPIe	Jeri Doom	P	1992-01-01	087124684116	8135 Eagle Court?
ditka@bajwo.gov	QEFUlAsAPAdAOIf	Larue Irizarry	L	1991-09-22	081399121688	Oxford, MS 38655
zut@sazul.co.uk	cEuIXO0EzE5AyI9	Jenni Pellett	P	1991-06-14	085787458882	89 E. Devonshire Ave.?
paejne@com.edu	5IkA5AQO0ISAuOP	Emanuel Lambrecht	P	1990-05-05	084647754413	Pittsburgh, PA 15206
re@wumop.com	aUlImA5UYUyOqO3	Delana Koren	P	1992-03-03	086569981879	9803 Amerige Street?
gabiro@sez.net	3AzU6AjA7UDOLOa	Leilani Pearcy	P	1991-02-02	087558872489	Reading, MA 01867
uwa@tagokraw.gov	kUiEHApUPE2EsE2	Providencia Fujimoto	P	1990-11-19	086196821445	7844 Thomas Dr.?
upu@owfogna.com	yUiAiU6A8AsIYOX	Louisa Mains	L	1991-12-31	086734191714	Twin Falls, ID 83301
ra@midgabe.edu	vAoUTAAEhOCE9Uh	Cami Colin	L	1991-12-03	081142464731	812 Creekside Drive?
jukuctig@bucolza.org	PAkUEAeIqEUI6IQ	Jannie Borum	L	1991-08-03	083313274431	Hamburg, NY 14075
jejme@me.com	IO5OWAzUgErEKUf	Karma Whitt	L	1991-06-05	086335354996	69 N. Fieldstone Ave.?
ju@zo.com	mExUDUpU2UWAqIM	Hortense Egli	L	1990-12-21	081996964823	Indianapolis, IN 46201
tol@tukon.edu	SEzIOUdOYUdUAIC	Erlinda Chapin	L	1991-08-18	088222633141	541 Maple Ave.?
leluwi@honwi.com	fE9URIsUUOtEEUU	Loni Dell	L	1992-05-18	083619519912	Peachtree City, GA 30269
nograhcuc@kauno.edu	AIzIlE9UYESOFAI	Manda Kimberly	L	1991-02-16	083453158485	267 Linda Road?
fiom@si.org	JOuAeUPO9ERIlEH	Charley Stoecker	L	1991-06-09	085281869531	Louisville, KY 40207
ewebeduc@heceh.co.uk	IU6ASE1INElAbA2	Ok Hinerman	P	1991-07-22	089177944171	8408 Fieldstone St.?
tata@meetpe.gov	4IOOUEwAuImUzA9	Mauro Blassingame	L	1991-04-23	084115664342	Dundalk, MD 21222
lupadiv@ge.com	METEwEHI9AoI3EN	Scarlet Rancourt	L	1990-09-07	082822399581	112 Laurel St.?
ejiflo@amdof.io	JAOIrOQIFUFA4E9	Alfred Dimaggio	P	1992-04-25	083739168397	Long Branch, NJ 07740
ciwimnim@wolihvo.io	4IJILI5ARUmU8Ox	Laci Perham	L	1990-09-29	088568398543	278 Helen Court?
papa@jorisuz.co.uk	OEaUqOdArIFUYEp	Dalton Sanchez	L	1992-06-19	082583982979	New Kensington, PA 15068
heip@cani.com	7UGA4IzEzOgILOq	Nicola Pacifico	P	1990-05-20	089382395212	7958 Schoolhouse Drive?
owagij@gingetek.io	dEaE5OPIEAcUgEL	Celine Sather	L	1990-10-27	086473346527	Gaithersburg, MD 20877
fo@pa.io	pARIlIUIoIBEIIB	Homer Brogdon	P	1991-08-10	086273835742	81 County St.?
lefosmo@fawriil.co.uk	XOPUYUeEYAaAWU8	Bong Valvo	P	1991-06-21	087452916331	Reston, VA 20191
weheb@isoho.com	FOpEfONApO1UWEL	Julianne Jahnke	P	1990-05-04	084176589752	585 Valley Court?
gucas@vi.net	TA1ATERIvEiOfUb	Yuriko Fecteau	L	1991-04-28	086756813474	Saint Johns, FL 32259
evdivcu@jak.io	HEFIwOTEYUMOcEw	Illa Furguson	P	1991-10-03	089687513858	73 Lakewood Lane?
ewnor@koc.org	wUBUBELEXAMAyOL	Dina Charrier	L	1991-03-02	087253438385	Fargo, ND 58102
ummi@dur.gov	FAUOME5IXEXECOV	Eda Benard	P	1990-11-10	081866675817	393 Parker St.?
wimfi@derwejgen.co.uk	zEZEhALU1U0EqAl	Wen Sabatino	P	1991-06-20	084129699436	Everett, MA 02149
eribip@dim.edu	1UDA3ObOxUrIRUm	Pasquale Milici	P	1991-07-25	089967232934	965 Van Dyke Dr.?
ciznamluf@penkono.gov	TEJO2IQI1IWUuOo	Carlita Guth	L	1991-03-26	088383911122	Des Moines, IA 50310
enuji@benkutja.gov	nAkOHOPUVOKUcIQ	Ehtel Drennen	P	1991-02-24	082877563225	684 Lakeshore St.?
vedowur@loowojac.io	CIbAkEtUYAZUJUy	Johnson Borey	P	1991-01-27	083811439864	Asbury Park, NJ 07712
kehec@ebbucja.net	HI6ISEOIGAgOFUS	Delsie Oberlin	P	1991-09-23	089622967153	93 Littleton Ave.?
aveju@ezihocle.co.uk	fE3ItUuExEMAVE0	Tammara Hoffman	L	1992-01-13	086583714398	Waukegan, IL 60085
veizovo@dewuv.org	1UFIJAQULO1OEI5	Xiao Dewees	P	1992-06-23	088531661352	677 Wakehurst Dr.?
ca@jon.io	sUAOkEzOPOaODA0	Demetria Cressman	L	1991-09-30	087841949427	Tallahassee, FL 32303
mocsib@cirgo.io	zAFIuUbOqI6UqAv	Willow Eastland	P	1991-11-16	083414754357	66 Wentworth Rd.?
costoja@waw.gov	yAIU0ATE4UeEQIz	Joanie Harbert	P	1991-01-03	083866748345	Oconomowoc, WI 53066
goufaut@luwab.co.uk	eAoA3EMI9UREqAx	Lydia Laprade	P	1990-12-21	081825125344	81 Hill Field St.?
uphagfa@reluba.io	AO2IGI3OXOTUbEj	Ada Joe	L	1990-07-22	083228848414	Ridgecrest, CA 93555
su@ri.net	vORUlAdIGAFAvUx	Merry Hanberry	P	1990-09-19	083345194526	7058 Illinois St.?
paweifa@gusak.com	xE0OZOwUREpA9Ui	Geneva Rutan	P	1992-06-29	085381973767	Deland, FL 32720
fasepjev@ogafi.net	rI0OdOYOEUgI3Eu	Rikki Texada	P	1991-05-10	084727583253	7013 West St Paul St.?
kibiri@koftere.io	5EsIHOwIuIdEEUS	Jeanene Lock	L	1990-07-20	083237582723	Shrewsbury, MA 01545
ponuv@howup.co.uk	LA4EfEXIhARUXON	Porter Shewmake	P	1991-10-31	087247728353	2 Liberty Lane?
cuberuta@biepi.co.uk	GIzOTOFIUEmUIU0	Ambrose Hackworth	L	1991-08-22	083284186144	Oxford, MS 38655
dah@cuz.org	aOmI7IGAOEZANEY	Kathline Machnik	L	1990-08-12	087568795882	8483 Oak Meadow St.?
jumfel@nulueh.net	0AaAgAeUWIXUbEg	Desiree Hane	P	1991-06-20	086216374839	Pompano Beach, FL 33060
tog@siv.org	oI6OBIROCIxIJA5	Jami Fahy	L	1991-05-17	087663765253	368 Durham St.?
liru@op.co.uk	FIyOWEeAfObIhIe	Livia Corsi	P	1990-10-08	087766498237	Deerfield, IL 60015
opevar@utfebe.com	aIGEzIwU8ExUnAr	Sharika Jessop	L	1992-02-22	087698774349	178 Philmont St.?
evomobmi@sepete.com	KEqIzA7EuImOMAp	Joyce Mckitrick	L	1990-11-11	084855259552	Langhorne, PA 19047
tuvnej@le.io	5OhE0O7OROnIfIp	Phung Bohon	P	1991-01-22	081799739472	21 North Beech Lane?
cobkom@bo.org	kOpUEUWEMO0IWAs	Jewell Petrick	L	1991-06-04	082287582933	Chelmsford, MA 01824
co@lep.gov	NEtECABUMEYI9OQ	My Pettis	P	1990-05-19	086553362222	7756 Highland St.?
remuw@jopuru.io	FEnUEEpIEEPO3UM	Lorene Magee	P	1990-11-24	088526173488	Woodside, NY 11377
tegesemo@roenvuj.edu	UETUVUjEtAtEHEq	Katherina Manchester	L	1991-08-05	082872484793	912 Sherwood St.?
ewe@manog.co.uk	vEBURO4EVIoO1OO	Azucena Strout	L	1992-05-22	089735682135	Dearborn, MI 48124
jemoh@kawbur.gov	KIvE7I3OZAcEFOU	Riley Gengler	L	1991-11-27	087545822851	194 Liberty St.?
socoma@laomigez.io	lECAZAKImIUO9O8	Celesta Brockington	P	1991-05-19	087694741235	Goose Creek, SC 29445
kanaz@mema.org	ZEuEJUnOaEdAYEQ	Brenda Martens	P	1991-05-24	084893632521	7042 Rockaway Ave.?
hesnaji@ve.net	bU5EPE1OzU0UtUg	Leroy Ahl	L	1992-06-21	089725412485	King Of Prussia, PA 19406
pagmu@ka.edu	OEmOLAKU9AoOcUc	Oscar Najera	L	1990-09-05	084718697424	88 Indian Spring Street?
wemufmi@vigurija.gov	WAwEAIdUJIHUgOh	Oralee Seaberg	L	1992-06-19	088576795884	North Andover, MA 01845
dup@nazepad.net	ME9AQInAOOOODI4	Lanie Stalling	L	1991-12-08	081258872413	702 Smoky Hollow St.?
ap@gavakeog.gov	pAVOuO2U5AdITEf	Margot Gurley	P	1991-06-18	086367267944	Lancaster, NY 14086
ijlo@vicot.com	WU5UeUnEhE0IFOp	Elli Dauphinais	P	1991-03-19	086866717668	195 College St.?
se@tedu.io	fOjE2O0OgEuAGOw	Dillon Lorence	P	1991-02-21	084139389455	Ada, OK 74820
kon@dow.edu	gAiUbImE0AeAwIA	Mackenzie Reeves	P	1990-10-05	087755412577	7512 Randall Mill Street?
idfolo@kori.co.uk	MOsAEUyOuU8AKAa	Jonell Shiba	P	1991-07-29	089768761427	Goldsboro, NC 27530
arpil@nasvunub.com	MI6OGIeOzIUAUAD	Brittni Valasquez	P	1992-07-02	088465233348	80 8th St.?
ceiwosak@kucuaj.net	sUwEmORUQIuA9Eo	Clifford Maffucci	L	1990-07-27	085692629138	Woburn, MA 01801
miwesa@ros.org	FUqUeONIGAFAUIN	Wendi Sandman	P	1991-11-20	083537356632	96 West Fordham Ave.?
gurab@suvelelub.io	kExOUEcI9EIErI6	Masako Littlejohn	L	1992-05-31	088257312139	Frankfort, KY 40601
zup@ab.com	ZUxUKA9EMIxUmUT	Blanch Cooney	L	1991-10-28	081676785858	8408 Bank St.?
gurezi@folcib.gov	VAkUJOaUCInOMER	Trudi Ismail	P	1990-06-24	086543672483	Newport News, VA 23601
joboav@un.io	OAZIbEROvIoEUUf	Wallace Sepe	L	1990-12-23	086556251586	16 Bank Court?
hed@cingi.org	TU1U9UhOzEwEmO8	Andreas Mccombs	L	1990-11-06	087499217269	Cornelius, NC 28031
nepic@zolozwuf.gov	0EYULU1OaOdACUO	Dwayne Ashurst	L	1990-12-12	081486541655	7602 University Rd.?
uduhet@pomwic.org	jITO0OeOfIHIME5	Cinderella Hamiton	L	1991-02-04	081795766196	Glenside, PA 19038
fub@nuwok.io	6ATUZUfIVI9OEOf	Lorene Searcy	P	1990-08-27	082714965876	634 Homestead Court?
deretjaw@gek.edu	vO3O3OiIOUfUsAx	Madelyn Seybert	L	1991-06-29	084274786211	Helotes, TX 78023
\.


--
-- Data for Name: produk; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY produk (kode_produk, nama, harga, deskripsi) FROM stdin;
S0000001	Dengan ini adanya Celana Sangat bagus	349000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, beli 1 gratis 10 , cicilan 0%, fast respond!
S0000002	Dijual Jeans Nomor 1 seIndinesia	352000.00	ayo jangan sampai kehabisan!, miliki saat ini juga, beli 1 gratis 10 , bonus piring cantik, dijamin lulus 3 tahun
S0000003	Dijual Mobil Keren sekali	27000.00	ayo jangan sampai kehabisan!, nanti!, dijamin basdat A, cicilan 0%, juga ngedate bareng jihan
S0000004	Disewakan Celana Biasa saja	94000.00	anda harus jadi prioritas!, nanti!, beli 1 gratis 10 , gak datang  2 kali, fast respond!
S0000005	Disewakan Vasmina Keren sekali	961000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, beli 1 gratis 10 , langsung tanpa DP, tanpa basa basi
S0000006	Menghadirkan Baju Keren sekali	417000.00	anda harus jadi prioritas!, nanti!, hari ini terakhir, langsung tanpa DP, tanpa tipu tipu
S0000007	[WTS] Baju Tidak ada tandingannya	246000.00	ayo jangan sampai kehabisan!, dapatkan segera, bonus nomor telfon jihan, dapatkan kesempatan, juga ngedate bareng jihan
S0000008	Dibeli sekarang juga HP Tidak ada tandingannya	95000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, gocengan doang, langsung tanpa DP, tanpa basa basi
S0000009	Dijual Celana Berkualitas	297000.00	gapunya? gakeren lo!, dapatkan segera, dijamin basdat A, bonus piring cantik, original asli!
S0000010	Dijual Jeans Berkualitas	99000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, hari ini terakhir, tanpa diundi, tanpa tipu tipu
S0000011	Dijual Baju Pantai Sangat bagus	86000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, gocengan doang, dapatkan kesempatan, dijamin lulus 3 tahun
S0000012	Menghadirkan Celana Internasional	95000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, bonus nomor telfon jihan, bonus piring cantik, original asli!
S0000013	Menghadirkan Jeans Tak terkalahkan	969000.00	gapunya? gakeren lo!, nanti!, hari ini terakhir, bonus piring cantik, fast respond!
S0000014	Disewakan Sepatu Nomor 1 seIndinesia	29000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, dijamin basdat A, gak datang  2 kali, original asli!
S0000015	[WTS] Jaket Berkualitas	553000.00	gapunya? gakeren lo!, dapatkan segera, hari ini terakhir, langsung tanpa DP, tanpa basa basi
S0000016	Menghadirkan Kunci Mobil Sangat bagus	66000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, gocengan doang, langsung tanpa DP, tanpa basa basi
S0000017	Disewakan Sweater Tidak Jelek	728000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, bonus nomor telfon jihan, tanpa diundi, tanpa tipu tipu
S0000018	Dengan ini adanya Celana Berkualitas	469000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, bonus kelereng juga, cicilan 0%, original asli!
S0000019	Dengan ini adanya Mobil Nomor 1 seIndinesia	32000.00	anda harus jadi prioritas!, nanti!, dijamin basdat A, tanpa diundi, fast respond!
S0000020	[WTS] HP Tak terkalahkan	612000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, bonus nomor telfon jihan, langsung tanpa DP, juga ngedate bareng jihan
S0000021	Menghadirkan Kemeja Keren sekali	65000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, beli 1 gratis 10 , langsung tanpa DP, juga ngedate bareng jihan
S0000022	Dibeli sekarang juga Jeans Biasa saja	121000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, bonus kelereng juga, tanpa diundi, original asli!
S0000023	Menghadirkan Kunci Mobil Sangat bagus	336000.00	anda harus jadi prioritas!, miliki saat ini juga, gocengan doang, langsung tanpa DP, juga ngedate bareng jihan
S0000024	[WTS] Kemeja Tidak ada tandingannya	86000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, gocengan doang, gak datang  2 kali, fast respond!
S0000025	Disewakan HP Tak terkalahkan	91000.00	ayo jangan sampai kehabisan!, nanti!, dijamin basdat A, tanpa diundi, juga ngedate bareng jihan
S0000026	Menghadirkan Kunci Mobil Berkualitas	18000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, bonus nomor telfon jihan, gak datang  2 kali, tanpa basa basi
S0000027	Dijual Vasmina Tidak Jelek	345000.00	gapunya? gakeren lo!, miliki saat ini juga, bonus nomor telfon jihan, dapatkan kesempatan, fast respond!
S0000028	Disewakan Sweater Biasa saja	169000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, gocengan doang, langsung tanpa DP, juga ngedate bareng jihan
S0000029	Disewakan Mobil Berkualitas	83000.00	gapunya? gakeren lo!, miliki saat ini juga, beli 1 gratis 10 , bonus piring cantik, juga ngedate bareng jihan
S0000030	Menghadirkan Jeans Sangat bagus	147000.00	anda harus jadi prioritas!, dapatkan segera, hari ini terakhir, dapatkan kesempatan, original asli!
S0000031	Dengan ini adanya Celana Biasa saja	59000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, bonus kelereng juga, dapatkan kesempatan, tanpa tipu tipu
S0000032	Dengan ini adanya Kunci Mobil Tak terkalahkan	649000.00	anda harus jadi prioritas!, miliki saat ini juga, dijamin basdat A, gak datang  2 kali, dijamin lulus 3 tahun
S0000033	Dengan ini adanya Kemeja Internasional	375000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, hari ini terakhir, dapatkan kesempatan, original asli!
S0000034	Dengan ini adanya HP Berkualitas	466000.00	ayo jangan sampai kehabisan!, nanti!, gocengan doang, dapatkan kesempatan, juga ngedate bareng jihan
S0000035	Dibeli sekarang juga Sweater Tidak Jelek	596000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, bonus kelereng juga, cicilan 0%, original asli!
S0000036	[WTS] Celana Keren sekali	43000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, bonus nomor telfon jihan, gak datang  2 kali, dijamin lulus 3 tahun
S0000037	Dijual Mobil Keren sekali	383000.00	anda harus jadi prioritas!, tunggu apa lagi, beli 1 gratis 10 , cicilan 0%, tanpa basa basi
S0000038	Dengan ini adanya Baju Nomor 1 seIndinesia	14000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, hari ini terakhir, langsung tanpa DP, tanpa basa basi
S0000039	Dibeli sekarang juga Baju Biasa saja	12000.00	anda harus jadi prioritas!, tunggu apa lagi, beli 1 gratis 10 , bonus piring cantik, tanpa basa basi
S0000040	Dibeli sekarang juga Kemeja Biasa saja	44000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, bonus nomor telfon jihan, tanpa diundi, fast respond!
S0000041	Dibeli sekarang juga Celana Sangat bagus	549000.00	anda harus jadi prioritas!, miliki saat ini juga, hari ini terakhir, cicilan 0%, fast respond!
S0000042	Disewakan Kemeja Sangat bagus	177000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, gocengan doang, gak datang  2 kali, tanpa tipu tipu
S0000043	Dijual Baju Pantai Berkualitas	136000.00	ayo jangan sampai kehabisan!, mau tunggu sampai kapan!, hari ini terakhir, tanpa diundi, dijamin lulus 3 tahun
S0000044	Disewakan Sepatu Biasa saja	776000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, gocengan doang, gak datang  2 kali, original asli!
S0000045	Dijual Kunci Mobil Berkualitas	222000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, bonus nomor telfon jihan, cicilan 0%, original asli!
S0000046	Disewakan Kemeja Keren sekali	168000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, hari ini terakhir, bonus piring cantik, juga ngedate bareng jihan
S0000047	[WTS] Kemeja Keren sekali	921000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, bonus nomor telfon jihan, tanpa diundi, tanpa basa basi
S0000048	[WTS] Sepatu Sangat bagus	137000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, gocengan doang, dapatkan kesempatan, tanpa tipu tipu
S0000049	Dijual Baju Tak terkalahkan	595000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, dijamin basdat A, dapatkan kesempatan, original asli!
S0000050	Dengan ini adanya Sepatu Nomor 1 seIndinesia	11000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, bonus kelereng juga, langsung tanpa DP, dijamin lulus 3 tahun
S0000051	Menghadirkan Jaket Nomor 1 seIndinesia	266000.00	anda harus jadi prioritas!, dapatkan segera, gocengan doang, bonus piring cantik, tanpa basa basi
S0000052	Menghadirkan Sepatu Nomor 1 seIndinesia	164000.00	anda harus jadi prioritas!, miliki saat ini juga, dijamin basdat A, gak datang  2 kali, dijamin lulus 3 tahun
S0000053	[WTS] Baju Pantai Berkualitas	92000.00	anda harus jadi prioritas!, tunggu apa lagi, beli 1 gratis 10 , dapatkan kesempatan, juga ngedate bareng jihan
S0000054	Menghadirkan Jaket Tak terkalahkan	63000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, hari ini terakhir, langsung tanpa DP, fast respond!
S0000055	Dengan ini adanya Sepatu Tidak Jelek	446000.00	ayo jangan sampai kehabisan!, nanti!, bonus nomor telfon jihan, tanpa diundi, tanpa basa basi
S0000056	Dibeli sekarang juga Jaket Sangat bagus	56000.00	anda harus jadi prioritas!, tunggu apa lagi, gocengan doang, langsung tanpa DP, tanpa basa basi
S0000057	Dijual Kunci Mobil Tak terkalahkan	961000.00	gapunya? gakeren lo!, tunggu apa lagi, gocengan doang, dapatkan kesempatan, fast respond!
S0000058	Dibeli sekarang juga Sepatu Tidak Jelek	643000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, gocengan doang, cicilan 0%, fast respond!
S0000059	Menghadirkan Kemeja Internasional	69000.00	anda harus jadi prioritas!, tunggu apa lagi, gocengan doang, dapatkan kesempatan, tanpa basa basi
S0000060	Dijual Mobil Tidak Jelek	24000.00	ayo jangan sampai kehabisan!, nanti!, hari ini terakhir, dapatkan kesempatan, dijamin lulus 3 tahun
S0000061	Dijual HP Tidak ada tandingannya	55000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, beli 1 gratis 10 , langsung tanpa DP, juga ngedate bareng jihan
S0000062	Dijual Kunci Mobil Biasa saja	31000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, dijamin basdat A, tanpa diundi, tanpa tipu tipu
S0000063	Dijual Sepatu Tidak ada tandingannya	288000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, beli 1 gratis 10 , cicilan 0%, original asli!
S0000064	Menghadirkan Vasmina Internasional	14000.00	beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi, beli 1 gratis 10 , dapatkan kesempatan, juga ngedate bareng jihan
S0000065	Dibeli sekarang juga Baju Keren sekali	485000.00	ayo jangan sampai kehabisan!, nanti!, bonus nomor telfon jihan, bonus piring cantik, juga ngedate bareng jihan
S0000066	Disewakan Jaket Berkualitas	988000.00	ayo jangan sampai kehabisan!, nanti!, gocengan doang, gak datang  2 kali, dijamin lulus 3 tahun
S0000067	Disewakan Jeans Biasa saja	188000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, beli 1 gratis 10 , bonus piring cantik, juga ngedate bareng jihan
S0000068	Dibeli sekarang juga Kemeja Keren sekali	514000.00	ayo jangan sampai kehabisan!, nanti!, dijamin basdat A, gak datang  2 kali, fast respond!
S0000069	Dengan ini adanya Sepatu Berkualitas	959000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, hari ini terakhir, langsung tanpa DP, original asli!
S0000070	Dijual Kunci Mobil Nomor 1 seIndinesia	541000.00	gapunya? gakeren lo!, tunggu apa lagi, hari ini terakhir, dapatkan kesempatan, juga ngedate bareng jihan
S0000071	Disewakan Celana Tidak Jelek	922000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, beli 1 gratis 10 , langsung tanpa DP, tanpa basa basi
S0000072	Disewakan Vasmina Tidak ada tandingannya	575000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, beli 1 gratis 10 , tanpa diundi, tanpa tipu tipu
S0000073	[WTS] Baju Pantai Tidak ada tandingannya	44000.00	anda harus jadi prioritas!, nanti!, dijamin basdat A, tanpa diundi, original asli!
S0000074	[WTS] Baju Pantai Sangat bagus	68000.00	gapunya? gakeren lo!, tunggu apa lagi, gocengan doang, bonus piring cantik, tanpa basa basi
S0000075	Menghadirkan HP Tak terkalahkan	643000.00	anda harus jadi prioritas!, miliki saat ini juga, gocengan doang, langsung tanpa DP, dijamin lulus 3 tahun
S0000076	[WTS] Celana Keren sekali	79000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, bonus nomor telfon jihan, dapatkan kesempatan, fast respond!
S0000077	Dengan ini adanya Kemeja Tidak Jelek	866000.00	anda harus jadi prioritas!, miliki saat ini juga, bonus kelereng juga, tanpa diundi, tanpa tipu tipu
S0000078	Disewakan Sweater Keren sekali	49000.00	anda harus jadi prioritas!, miliki saat ini juga, beli 1 gratis 10 , bonus piring cantik, original asli!
S0000079	Menghadirkan Kemeja Nomor 1 seIndinesia	84000.00	anda harus jadi prioritas!, dapatkan segera, bonus nomor telfon jihan, cicilan 0%, juga ngedate bareng jihan
S0000080	Dijual Baju Keren sekali	42000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, hari ini terakhir, tanpa diundi, original asli!
S0000081	Dibeli sekarang juga Sweater Berkualitas	758000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, beli 1 gratis 10 , langsung tanpa DP, fast respond!
S0000082	Menghadirkan Vasmina Keren sekali	147000.00	anda harus jadi prioritas!, tunggu apa lagi, dijamin basdat A, tanpa diundi, original asli!
S0000083	Menghadirkan Sweater Nomor 1 seIndinesia	446000.00	anda harus jadi prioritas!, nanti!, dijamin basdat A, bonus piring cantik, tanpa tipu tipu
S0000084	Dengan ini adanya Jaket Tidak ada tandingannya	676000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, bonus nomor telfon jihan, langsung tanpa DP, fast respond!
P0000046	Pulsa Smartfren 70ribu	77000.00	Jaminan 100%, digunakan ketika dibutuhkan, gapunya? gakeren lo!, beli sekarang, jangan nanti
S0000085	Menghadirkan Baju Pantai Internasional	864000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, bonus kelereng juga, tanpa diundi, dijamin lulus 3 tahun
S0000086	Dijual Mobil Sangat bagus	58000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, bonus kelereng juga, gak datang  2 kali, tanpa basa basi
S0000087	Dijual Jaket Tidak ada tandingannya	57000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, dijamin basdat A, langsung tanpa DP, fast respond!
S0000088	Dijual Sweater Tak terkalahkan	87000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, dijamin basdat A, dapatkan kesempatan, dijamin lulus 3 tahun
S0000089	[WTS] Baju Tak terkalahkan	44000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, hari ini terakhir, dapatkan kesempatan, tanpa basa basi
S0000090	Dibeli sekarang juga Sweater Keren sekali	51000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, gocengan doang, dapatkan kesempatan, original asli!
S0000091	Menghadirkan Vasmina Tidak Jelek	76000.00	gapunya? gakeren lo!, dapatkan segera, gocengan doang, tanpa diundi, tanpa basa basi
S0000092	[WTS] Kunci Mobil Nomor 1 seIndinesia	98000.00	gapunya? gakeren lo!, nanti!, beli 1 gratis 10 , cicilan 0%, tanpa tipu tipu
S0000093	Disewakan Baju Pantai Tidak ada tandingannya	579000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, bonus kelereng juga, langsung tanpa DP, tanpa basa basi
S0000094	Disewakan Sweater Nomor 1 seIndinesia	364000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, dijamin basdat A, bonus piring cantik, original asli!
S0000095	Dengan ini adanya Baju Pantai Tidak ada tandingannya	824000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, beli 1 gratis 10 , gak datang  2 kali, fast respond!
S0000096	Dibeli sekarang juga Mobil Berkualitas	95000.00	ayo jangan sampai kehabisan!, dapatkan segera, bonus nomor telfon jihan, bonus piring cantik, fast respond!
S0000097	[WTS] Mobil Tidak Jelek	958000.00	anda harus jadi prioritas!, tunggu apa lagi, bonus nomor telfon jihan, langsung tanpa DP, tanpa basa basi
S0000098	Menghadirkan Jaket Keren sekali	688000.00	anda harus jadi prioritas!, tunggu apa lagi, gocengan doang, tanpa diundi, tanpa basa basi
S0000099	Dibeli sekarang juga Jaket Keren sekali	34000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, beli 1 gratis 10 , tanpa diundi, original asli!
S0000100	Disewakan Jeans Sangat bagus	637000.00	gapunya? gakeren lo!, nanti!, hari ini terakhir, cicilan 0%, fast respond!
S0000101	Dengan ini adanya Kemeja Tidak ada tandingannya	66000.00	anda harus jadi prioritas!, tunggu apa lagi, beli 1 gratis 10 , langsung tanpa DP, dijamin lulus 3 tahun
S0000102	Menghadirkan Baju Pantai Sangat bagus	75000.00	anda harus jadi prioritas!, nanti!, bonus kelereng juga, gak datang  2 kali, fast respond!
S0000103	Menghadirkan Jeans Tidak Jelek	45000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, dijamin basdat A, dapatkan kesempatan, tanpa tipu tipu
S0000104	[WTS] Baju Pantai Berkualitas	368000.00	anda harus jadi prioritas!, tunggu apa lagi, hari ini terakhir, langsung tanpa DP, juga ngedate bareng jihan
S0000105	[WTS] Vasmina Sangat bagus	92000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, beli 1 gratis 10 , cicilan 0%, tanpa tipu tipu
S0000106	Menghadirkan Sweater Tidak Jelek	911000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, dijamin basdat A, langsung tanpa DP, juga ngedate bareng jihan
S0000107	Dijual Jeans Tak terkalahkan	36000.00	ayo jangan sampai kehabisan!, dapatkan segera, bonus kelereng juga, tanpa diundi, tanpa basa basi
S0000108	Disewakan Mobil Tidak ada tandingannya	68000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, bonus kelereng juga, bonus piring cantik, original asli!
S0000109	Dibeli sekarang juga Kemeja Tak terkalahkan	595000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, hari ini terakhir, cicilan 0%, dijamin lulus 3 tahun
S0000110	Menghadirkan Jeans Internasional	95000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, hari ini terakhir, bonus piring cantik, original asli!
S0000111	Dibeli sekarang juga Mobil Berkualitas	92000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, beli 1 gratis 10 , bonus piring cantik, juga ngedate bareng jihan
S0000112	Menghadirkan Vasmina Internasional	81000.00	gapunya? gakeren lo!, dapatkan segera, gocengan doang, cicilan 0%, dijamin lulus 3 tahun
S0000113	Menghadirkan Sepatu Tidak Jelek	12000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, hari ini terakhir, cicilan 0%, tanpa basa basi
S0000114	Disewakan Jaket Berkualitas	874000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, hari ini terakhir, cicilan 0%, tanpa tipu tipu
S0000115	Dijual Jaket Berkualitas	178000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, hari ini terakhir, langsung tanpa DP, fast respond!
S0000116	Menghadirkan Kunci Mobil Tidak Jelek	44000.00	anda harus jadi prioritas!, tunggu apa lagi, gocengan doang, cicilan 0%, fast respond!
S0000117	Dijual Kemeja Tidak ada tandingannya	22000.00	gapunya? gakeren lo!, dapatkan segera, hari ini terakhir, dapatkan kesempatan, tanpa tipu tipu
S0000118	[WTS] Jaket Tidak ada tandingannya	371000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, bonus kelereng juga, cicilan 0%, original asli!
S0000119	Dengan ini adanya Mobil Keren sekali	11000.00	anda harus jadi prioritas!, tunggu apa lagi, beli 1 gratis 10 , langsung tanpa DP, fast respond!
S0000120	[WTS] Baju Pantai Tidak ada tandingannya	412000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, bonus nomor telfon jihan, langsung tanpa DP, tanpa tipu tipu
S0000121	Menghadirkan Jaket Berkualitas	993000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, hari ini terakhir, gak datang  2 kali, fast respond!
S0000122	Dijual Baju Pantai Nomor 1 seIndinesia	43000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, beli 1 gratis 10 , tanpa diundi, original asli!
S0000123	Dijual HP Keren sekali	747000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, dijamin basdat A, langsung tanpa DP, tanpa basa basi
S0000124	Dijual Sweater Biasa saja	22000.00	gapunya? gakeren lo!, nanti!, hari ini terakhir, dapatkan kesempatan, dijamin lulus 3 tahun
S0000125	[WTS] Vasmina Sangat bagus	28000.00	ayo jangan sampai kehabisan!, dapatkan segera, bonus nomor telfon jihan, bonus piring cantik, dijamin lulus 3 tahun
S0000126	[WTS] HP Sangat bagus	73000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, gocengan doang, tanpa diundi, dijamin lulus 3 tahun
S0000127	Dengan ini adanya Vasmina Berkualitas	764000.00	ayo jangan sampai kehabisan!, dapatkan segera, bonus nomor telfon jihan, tanpa diundi, fast respond!
S0000128	Dijual Jeans Sangat bagus	27000.00	gapunya? gakeren lo!, dapatkan segera, hari ini terakhir, dapatkan kesempatan, tanpa basa basi
S0000129	Disewakan Sweater Tidak ada tandingannya	44000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, bonus nomor telfon jihan, langsung tanpa DP, dijamin lulus 3 tahun
S0000130	[WTS] Kemeja Nomor 1 seIndinesia	27000.00	gapunya? gakeren lo!, miliki saat ini juga, bonus nomor telfon jihan, langsung tanpa DP, original asli!
S0000131	Dibeli sekarang juga HP Keren sekali	134000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, bonus nomor telfon jihan, bonus piring cantik, tanpa tipu tipu
S0000132	Disewakan Kemeja Tak terkalahkan	25000.00	beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi, beli 1 gratis 10 , tanpa diundi, tanpa tipu tipu
S0000133	[WTS] Baju Tidak ada tandingannya	868000.00	anda harus jadi prioritas!, dapatkan segera, gocengan doang, langsung tanpa DP, tanpa tipu tipu
S0000134	Menghadirkan Sepatu Keren sekali	21000.00	gapunya? gakeren lo!, nanti!, bonus kelereng juga, cicilan 0%, fast respond!
S0000135	Dibeli sekarang juga Sweater Nomor 1 seIndinesia	43000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, beli 1 gratis 10 , gak datang  2 kali, tanpa tipu tipu
S0000136	Disewakan Sepatu Tidak Jelek	339000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, dijamin basdat A, bonus piring cantik, original asli!
S0000137	Dijual Baju Pantai Biasa saja	71000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, gocengan doang, gak datang  2 kali, original asli!
S0000138	Menghadirkan Sepatu Tidak Jelek	28000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, hari ini terakhir, bonus piring cantik, tanpa tipu tipu
S0000139	[WTS] Kunci Mobil Tidak Jelek	83000.00	anda harus jadi prioritas!, dapatkan segera, bonus kelereng juga, bonus piring cantik, tanpa basa basi
S0000140	Dengan ini adanya Mobil Internasional	359000.00	ayo jangan sampai kehabisan!, mau tunggu sampai kapan!, dijamin basdat A, langsung tanpa DP, original asli!
S0000141	Dijual HP Internasional	328000.00	anda harus jadi prioritas!, tunggu apa lagi, hari ini terakhir, langsung tanpa DP, original asli!
S0000142	[WTS] Kemeja Nomor 1 seIndinesia	156000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, bonus nomor telfon jihan, tanpa diundi, original asli!
S0000143	Dijual Jaket Berkualitas	33000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, gocengan doang, langsung tanpa DP, tanpa basa basi
S0000144	[WTS] Baju Biasa saja	187000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, beli 1 gratis 10 , cicilan 0%, fast respond!
S0000145	Dijual Baju Tak terkalahkan	86000.00	anda harus jadi prioritas!, dapatkan segera, bonus nomor telfon jihan, gak datang  2 kali, juga ngedate bareng jihan
S0000146	Dibeli sekarang juga Baju Pantai Sangat bagus	245000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, dijamin basdat A, gak datang  2 kali, fast respond!
S0000147	Dengan ini adanya Celana Keren sekali	142000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, bonus kelereng juga, cicilan 0%, fast respond!
S0000148	Dibeli sekarang juga HP Internasional	771000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, beli 1 gratis 10 , bonus piring cantik, fast respond!
S0000149	Disewakan Vasmina Sangat bagus	95000.00	anda harus jadi prioritas!, miliki saat ini juga, beli 1 gratis 10 , langsung tanpa DP, tanpa tipu tipu
S0000150	Dibeli sekarang juga Mobil Berkualitas	87000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, gocengan doang, cicilan 0%, tanpa basa basi
S0000151	Disewakan Baju Keren sekali	33000.00	anda harus jadi prioritas!, nanti!, gocengan doang, langsung tanpa DP, fast respond!
S0000152	Dijual Baju Pantai Tidak ada tandingannya	36000.00	gapunya? gakeren lo!, nanti!, dijamin basdat A, gak datang  2 kali, juga ngedate bareng jihan
S0000153	Dengan ini adanya Kunci Mobil Nomor 1 seIndinesia	268000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, beli 1 gratis 10 , bonus piring cantik, fast respond!
S0000154	[WTS] Celana Internasional	739000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, gocengan doang, tanpa diundi, original asli!
S0000155	[WTS] Sweater Tidak ada tandingannya	38000.00	ayo jangan sampai kehabisan!, nanti!, gocengan doang, gak datang  2 kali, tanpa basa basi
S0000156	[WTS] HP Biasa saja	82000.00	anda harus jadi prioritas!, miliki saat ini juga, dijamin basdat A, bonus piring cantik, tanpa tipu tipu
S0000157	Dijual Baju Berkualitas	35000.00	gapunya? gakeren lo!, tunggu apa lagi, bonus kelereng juga, tanpa diundi, original asli!
S0000158	Menghadirkan Mobil Internasional	452000.00	anda harus jadi prioritas!, tunggu apa lagi, gocengan doang, bonus piring cantik, dijamin lulus 3 tahun
S0000159	Menghadirkan Mobil Internasional	14000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, bonus nomor telfon jihan, tanpa diundi, dijamin lulus 3 tahun
S0000160	Disewakan Baju Pantai Berkualitas	52000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, dijamin basdat A, bonus piring cantik, dijamin lulus 3 tahun
S0000161	Menghadirkan HP Tidak Jelek	69000.00	anda harus jadi prioritas!, tunggu apa lagi, gocengan doang, cicilan 0%, fast respond!
S0000162	Dibeli sekarang juga Kemeja Sangat bagus	452000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, beli 1 gratis 10 , gak datang  2 kali, original asli!
S0000163	Dengan ini adanya Celana Nomor 1 seIndinesia	534000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, bonus kelereng juga, cicilan 0%, original asli!
S0000164	Disewakan Baju Pantai Nomor 1 seIndinesia	18000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, dijamin basdat A, bonus piring cantik, tanpa basa basi
S0000165	Disewakan Baju Internasional	567000.00	anda harus jadi prioritas!, nanti!, dijamin basdat A, langsung tanpa DP, original asli!
S0000166	Disewakan Jeans Biasa saja	96000.00	ayo jangan sampai kehabisan!, dapatkan segera, gocengan doang, cicilan 0%, fast respond!
S0000167	[WTS] Mobil Berkualitas	146000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, gocengan doang, cicilan 0%, dijamin lulus 3 tahun
S0000168	Dengan ini adanya Vasmina Sangat bagus	61000.00	anda harus jadi prioritas!, miliki saat ini juga, dijamin basdat A, dapatkan kesempatan, fast respond!
S0000169	Menghadirkan HP Tidak Jelek	272000.00	ayo jangan sampai kehabisan!, mau tunggu sampai kapan!, bonus kelereng juga, dapatkan kesempatan, tanpa basa basi
S0000170	Dengan ini adanya Celana Internasional	29000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, beli 1 gratis 10 , cicilan 0%, original asli!
S0000171	Menghadirkan Kunci Mobil Tidak ada tandingannya	58000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, bonus nomor telfon jihan, dapatkan kesempatan, tanpa basa basi
S0000172	[WTS] Mobil Tidak Jelek	652000.00	beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi, bonus kelereng juga, langsung tanpa DP, fast respond!
S0000173	[WTS] Baju Biasa saja	329000.00	ayo jangan sampai kehabisan!, miliki saat ini juga, bonus kelereng juga, cicilan 0%, tanpa tipu tipu
S0000174	Dengan ini adanya HP Tidak ada tandingannya	47000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, gocengan doang, tanpa diundi, tanpa basa basi
S0000175	Dijual Celana Tidak Jelek	96000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, hari ini terakhir, gak datang  2 kali, original asli!
S0000176	Dengan ini adanya Jaket Tak terkalahkan	343000.00	gapunya? gakeren lo!, tunggu apa lagi, bonus nomor telfon jihan, gak datang  2 kali, fast respond!
S0000177	[WTS] Kunci Mobil Keren sekali	442000.00	ayo jangan sampai kehabisan!, nanti!, beli 1 gratis 10 , bonus piring cantik, fast respond!
S0000178	Dibeli sekarang juga HP Berkualitas	82000.00	ayo jangan sampai kehabisan!, dapatkan segera, hari ini terakhir, dapatkan kesempatan, fast respond!
S0000179	[WTS] Kemeja Keren sekali	37000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, dijamin basdat A, dapatkan kesempatan, juga ngedate bareng jihan
S0000180	Dengan ini adanya Sweater Keren sekali	62000.00	anda harus jadi prioritas!, miliki saat ini juga, hari ini terakhir, langsung tanpa DP, fast respond!
S0000181	[WTS] Kemeja Nomor 1 seIndinesia	552000.00	anda harus jadi prioritas!, tunggu apa lagi, gocengan doang, dapatkan kesempatan, original asli!
S0000182	Dibeli sekarang juga Celana Tak terkalahkan	79000.00	gapunya? gakeren lo!, miliki saat ini juga, dijamin basdat A, bonus piring cantik, tanpa tipu tipu
S0000183	Disewakan Jaket Sangat bagus	95000.00	gapunya? gakeren lo!, tunggu apa lagi, hari ini terakhir, dapatkan kesempatan, fast respond!
S0000184	Disewakan Jeans Biasa saja	25000.00	anda harus jadi prioritas!, miliki saat ini juga, bonus nomor telfon jihan, dapatkan kesempatan, juga ngedate bareng jihan
S0000185	Dibeli sekarang juga Jaket Nomor 1 seIndinesia	35000.00	anda harus jadi prioritas!, miliki saat ini juga, gocengan doang, dapatkan kesempatan, dijamin lulus 3 tahun
S0000186	Menghadirkan Sepatu Nomor 1 seIndinesia	81000.00	anda harus jadi prioritas!, tunggu apa lagi, bonus nomor telfon jihan, langsung tanpa DP, original asli!
S0000187	[WTS] Sepatu Biasa saja	36000.00	anda harus jadi prioritas!, tunggu apa lagi, dijamin basdat A, gak datang  2 kali, dijamin lulus 3 tahun
S0000188	Dibeli sekarang juga Vasmina Keren sekali	43000.00	anda harus jadi prioritas!, dapatkan segera, bonus kelereng juga, langsung tanpa DP, original asli!
S0000189	Disewakan Sweater Biasa saja	11000.00	gapunya? gakeren lo!, tunggu apa lagi, hari ini terakhir, tanpa diundi, original asli!
S0000190	[WTS] Sweater Biasa saja	744000.00	anda harus jadi prioritas!, miliki saat ini juga, beli 1 gratis 10 , bonus piring cantik, dijamin lulus 3 tahun
S0000191	Dibeli sekarang juga Jeans Tidak Jelek	245000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, dijamin basdat A, tanpa diundi, juga ngedate bareng jihan
S0000192	Menghadirkan Mobil Biasa saja	575000.00	gapunya? gakeren lo!, dapatkan segera, bonus kelereng juga, tanpa diundi, original asli!
S0000193	Menghadirkan Celana Tidak Jelek	128000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, hari ini terakhir, cicilan 0%, dijamin lulus 3 tahun
S0000194	Dibeli sekarang juga Vasmina Tidak ada tandingannya	96000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, gocengan doang, langsung tanpa DP, tanpa tipu tipu
S0000195	Dengan ini adanya Celana Keren sekali	162000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, beli 1 gratis 10 , cicilan 0%, original asli!
S0000196	Menghadirkan Jaket Internasional	368000.00	gapunya? gakeren lo!, tunggu apa lagi, bonus kelereng juga, bonus piring cantik, juga ngedate bareng jihan
S0000197	Dengan ini adanya Mobil Tidak ada tandingannya	52000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, dijamin basdat A, gak datang  2 kali, dijamin lulus 3 tahun
S0000198	Dibeli sekarang juga Jeans Internasional	27000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, bonus nomor telfon jihan, gak datang  2 kali, fast respond!
S0000199	Disewakan Vasmina Berkualitas	81000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, bonus nomor telfon jihan, bonus piring cantik, original asli!
S0000200	Menghadirkan Mobil Keren sekali	65000.00	gapunya? gakeren lo!, dapatkan segera, hari ini terakhir, langsung tanpa DP, original asli!
S0000201	Dijual Kunci Mobil Tidak Jelek	734000.00	ayo jangan sampai kehabisan!, nanti!, hari ini terakhir, dapatkan kesempatan, tanpa basa basi
S0000202	Dijual Baju Pantai Tidak ada tandingannya	81000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, bonus nomor telfon jihan, gak datang  2 kali, tanpa basa basi
S0000203	[WTS] Mobil Nomor 1 seIndinesia	917000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, dijamin basdat A, langsung tanpa DP, tanpa tipu tipu
S0000204	Dibeli sekarang juga Baju Sangat bagus	889000.00	gapunya? gakeren lo!, nanti!, bonus nomor telfon jihan, cicilan 0%, tanpa basa basi
S0000205	Dibeli sekarang juga Celana Sangat bagus	328000.00	beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi, bonus nomor telfon jihan, dapatkan kesempatan, juga ngedate bareng jihan
S0000206	Dibeli sekarang juga Jaket Berkualitas	926000.00	ayo jangan sampai kehabisan!, dapatkan segera, bonus kelereng juga, bonus piring cantik, fast respond!
S0000207	Menghadirkan Jaket Sangat bagus	59000.00	gapunya? gakeren lo!, miliki saat ini juga, bonus nomor telfon jihan, tanpa diundi, dijamin lulus 3 tahun
S0000208	Dijual Kemeja Keren sekali	525000.00	anda harus jadi prioritas!, tunggu apa lagi, beli 1 gratis 10 , cicilan 0%, tanpa tipu tipu
S0000209	Dibeli sekarang juga Mobil Tidak ada tandingannya	88000.00	ayo jangan sampai kehabisan!, miliki saat ini juga, hari ini terakhir, gak datang  2 kali, original asli!
S0000210	Disewakan Baju Pantai Tidak ada tandingannya	82000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, bonus nomor telfon jihan, dapatkan kesempatan, tanpa basa basi
S0000211	Menghadirkan Sweater Tidak ada tandingannya	126000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, dijamin basdat A, cicilan 0%, tanpa tipu tipu
S0000212	Menghadirkan HP Tidak Jelek	82000.00	ayo jangan sampai kehabisan!, dapatkan segera, gocengan doang, cicilan 0%, original asli!
P0000047	Pulsa Hepi 40ribu	42000.00	Bisa digunakan kapanpun, Bisa dinego, anda harus jadi prioritas!, dapatkan segera
S0000213	Dibeli sekarang juga Kunci Mobil Biasa saja	26000.00	gapunya? gakeren lo!, tunggu apa lagi, bonus kelereng juga, dapatkan kesempatan, fast respond!
S0000214	Dibeli sekarang juga Mobil Keren sekali	438000.00	gapunya? gakeren lo!, nanti!, hari ini terakhir, gak datang  2 kali, dijamin lulus 3 tahun
S0000215	Dibeli sekarang juga Baju Pantai Tidak ada tandingannya	286000.00	anda harus jadi prioritas!, tunggu apa lagi, bonus nomor telfon jihan, dapatkan kesempatan, tanpa basa basi
S0000216	Dibeli sekarang juga HP Tak terkalahkan	283000.00	anda harus jadi prioritas!, miliki saat ini juga, hari ini terakhir, dapatkan kesempatan, dijamin lulus 3 tahun
S0000217	Dijual Baju Pantai Sangat bagus	12000.00	ayo jangan sampai kehabisan!, nanti!, hari ini terakhir, bonus piring cantik, tanpa tipu tipu
S0000218	Dibeli sekarang juga Kunci Mobil Tidak Jelek	244000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, bonus kelereng juga, dapatkan kesempatan, dijamin lulus 3 tahun
S0000219	Dijual Jaket Tidak Jelek	892000.00	beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi, bonus kelereng juga, tanpa diundi, dijamin lulus 3 tahun
S0000220	Disewakan Kunci Mobil Berkualitas	637000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, gocengan doang, tanpa diundi, dijamin lulus 3 tahun
S0000221	Dengan ini adanya Sepatu Biasa saja	749000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, hari ini terakhir, cicilan 0%, dijamin lulus 3 tahun
S0000222	Dijual Jeans Tidak Jelek	567000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, gocengan doang, langsung tanpa DP, juga ngedate bareng jihan
S0000223	Dibeli sekarang juga Sweater Tidak ada tandingannya	172000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, beli 1 gratis 10 , dapatkan kesempatan, tanpa tipu tipu
S0000224	[WTS] Baju Pantai Tak terkalahkan	98000.00	gapunya? gakeren lo!, tunggu apa lagi, gocengan doang, gak datang  2 kali, tanpa tipu tipu
S0000225	Menghadirkan Vasmina Berkualitas	798000.00	gapunya? gakeren lo!, dapatkan segera, bonus nomor telfon jihan, langsung tanpa DP, original asli!
S0000226	[WTS] Kunci Mobil Internasional	978000.00	gapunya? gakeren lo!, nanti!, bonus nomor telfon jihan, cicilan 0%, fast respond!
S0000227	[WTS] Sweater Berkualitas	88000.00	gapunya? gakeren lo!, nanti!, dijamin basdat A, bonus piring cantik, original asli!
S0000228	Dijual Baju Tak terkalahkan	993000.00	anda harus jadi prioritas!, nanti!, beli 1 gratis 10 , dapatkan kesempatan, juga ngedate bareng jihan
S0000229	Dibeli sekarang juga Kunci Mobil Nomor 1 seIndinesia	758000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, hari ini terakhir, langsung tanpa DP, juga ngedate bareng jihan
S0000230	Disewakan Jaket Tidak Jelek	729000.00	ayo jangan sampai kehabisan!, mau tunggu sampai kapan!, bonus kelereng juga, cicilan 0%, fast respond!
S0000231	Dengan ini adanya Sepatu Biasa saja	774000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, gocengan doang, langsung tanpa DP, juga ngedate bareng jihan
S0000232	Dijual Baju Pantai Nomor 1 seIndinesia	99000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, bonus nomor telfon jihan, bonus piring cantik, dijamin lulus 3 tahun
S0000233	Menghadirkan Baju Berkualitas	918000.00	anda harus jadi prioritas!, tunggu apa lagi, gocengan doang, langsung tanpa DP, tanpa basa basi
S0000234	Dengan ini adanya Sweater Internasional	29000.00	anda harus jadi prioritas!, dapatkan segera, hari ini terakhir, langsung tanpa DP, juga ngedate bareng jihan
S0000235	Dijual Sepatu Biasa saja	53000.00	gapunya? gakeren lo!, tunggu apa lagi, hari ini terakhir, tanpa diundi, original asli!
S0000236	Dibeli sekarang juga Mobil Internasional	569000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, gocengan doang, cicilan 0%, dijamin lulus 3 tahun
S0000237	Dibeli sekarang juga Sweater Biasa saja	925000.00	gapunya? gakeren lo!, miliki saat ini juga, hari ini terakhir, dapatkan kesempatan, fast respond!
S0000238	Menghadirkan Vasmina Keren sekali	79000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, hari ini terakhir, dapatkan kesempatan, tanpa tipu tipu
S0000239	Dibeli sekarang juga Sweater Biasa saja	13000.00	gapunya? gakeren lo!, tunggu apa lagi, gocengan doang, bonus piring cantik, dijamin lulus 3 tahun
S0000240	Dengan ini adanya Mobil Sangat bagus	81000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, beli 1 gratis 10 , langsung tanpa DP, fast respond!
S0000241	Dijual HP Nomor 1 seIndinesia	41000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, gocengan doang, tanpa diundi, fast respond!
S0000242	Dijual Jeans Tak terkalahkan	68000.00	gapunya? gakeren lo!, nanti!, bonus kelereng juga, dapatkan kesempatan, original asli!
S0000243	Dijual Sweater Berkualitas	37000.00	gapunya? gakeren lo!, dapatkan segera, bonus nomor telfon jihan, bonus piring cantik, tanpa tipu tipu
S0000244	Dengan ini adanya Baju Keren sekali	16000.00	gapunya? gakeren lo!, nanti!, hari ini terakhir, gak datang  2 kali, fast respond!
S0000245	Dibeli sekarang juga HP Tidak Jelek	892000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, dijamin basdat A, gak datang  2 kali, tanpa basa basi
S0000246	Menghadirkan Sweater Tidak ada tandingannya	43000.00	beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi, dijamin basdat A, dapatkan kesempatan, juga ngedate bareng jihan
S0000247	Disewakan Baju Internasional	87000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, dijamin basdat A, dapatkan kesempatan, tanpa tipu tipu
S0000248	Dibeli sekarang juga Kunci Mobil Berkualitas	33000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, beli 1 gratis 10 , langsung tanpa DP, juga ngedate bareng jihan
S0000249	Dijual Mobil Biasa saja	73000.00	gapunya? gakeren lo!, miliki saat ini juga, dijamin basdat A, cicilan 0%, original asli!
S0000250	Menghadirkan Baju Pantai Internasional	57000.00	ayo jangan sampai kehabisan!, mau tunggu sampai kapan!, hari ini terakhir, tanpa diundi, tanpa basa basi
S0000251	Dengan ini adanya Jeans Internasional	617000.00	ayo jangan sampai kehabisan!, mau tunggu sampai kapan!, bonus nomor telfon jihan, cicilan 0%, dijamin lulus 3 tahun
S0000252	Disewakan Sweater Sangat bagus	96000.00	anda harus jadi prioritas!, miliki saat ini juga, hari ini terakhir, dapatkan kesempatan, juga ngedate bareng jihan
S0000253	Dijual Sweater Nomor 1 seIndinesia	87000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, beli 1 gratis 10 , cicilan 0%, juga ngedate bareng jihan
S0000254	Menghadirkan Sepatu Nomor 1 seIndinesia	416000.00	beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi, dijamin basdat A, bonus piring cantik, tanpa tipu tipu
S0000255	Menghadirkan Kunci Mobil Tidak Jelek	274000.00	gapunya? gakeren lo!, mau tunggu sampai kapan!, gocengan doang, langsung tanpa DP, tanpa tipu tipu
S0000256	Disewakan Kemeja Keren sekali	15000.00	ayo jangan sampai kehabisan!, miliki saat ini juga, beli 1 gratis 10 , dapatkan kesempatan, tanpa tipu tipu
S0000257	[WTS] Sepatu Berkualitas	695000.00	anda harus jadi prioritas!, tunggu apa lagi, beli 1 gratis 10 , tanpa diundi, dijamin lulus 3 tahun
S0000258	Disewakan Jaket Berkualitas	54000.00	ayo jangan sampai kehabisan!, dapatkan segera, bonus kelereng juga, gak datang  2 kali, tanpa tipu tipu
S0000259	Dijual Celana Internasional	53000.00	anda harus jadi prioritas!, nanti!, dijamin basdat A, bonus piring cantik, fast respond!
S0000260	Dibeli sekarang juga Vasmina Internasional	893000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, hari ini terakhir, langsung tanpa DP, juga ngedate bareng jihan
S0000261	[WTS] HP Berkualitas	753000.00	ayo jangan sampai kehabisan!, mau tunggu sampai kapan!, hari ini terakhir, langsung tanpa DP, tanpa basa basi
S0000262	Dengan ini adanya Vasmina Biasa saja	31000.00	gapunya? gakeren lo!, beli sekarang, jangan nanti, bonus nomor telfon jihan, cicilan 0%, juga ngedate bareng jihan
S0000263	Dijual Jeans Sangat bagus	95000.00	gapunya? gakeren lo!, nanti!, gocengan doang, cicilan 0%, fast respond!
S0000264	[WTS] Sepatu Nomor 1 seIndinesia	415000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, beli 1 gratis 10 , tanpa diundi, fast respond!
S0000265	Menghadirkan Kemeja Nomor 1 seIndinesia	59000.00	ayo jangan sampai kehabisan!, nanti!, bonus nomor telfon jihan, tanpa diundi, tanpa basa basi
S0000266	Menghadirkan HP Keren sekali	24000.00	beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera, dijamin basdat A, langsung tanpa DP, dijamin lulus 3 tahun
S0000267	Dijual Baju Keren sekali	17000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, gocengan doang, cicilan 0%, tanpa basa basi
S0000268	Disewakan Baju Pantai Tidak Jelek	75000.00	ayo jangan sampai kehabisan!, mau tunggu sampai kapan!, gocengan doang, bonus piring cantik, fast respond!
S0000269	Menghadirkan Mobil Internasional	351000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, bonus nomor telfon jihan, bonus piring cantik, fast respond!
S0000270	Menghadirkan HP Tidak Jelek	82000.00	ayo jangan sampai kehabisan!, tunggu apa lagi, gocengan doang, cicilan 0%, juga ngedate bareng jihan
S0000271	Dijual Vasmina Biasa saja	83000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, gocengan doang, bonus piring cantik, dijamin lulus 3 tahun
S0000272	[WTS] Jeans Tidak ada tandingannya	74000.00	gapunya? gakeren lo!, miliki saat ini juga, beli 1 gratis 10 , gak datang  2 kali, fast respond!
S0000273	[WTS] Celana Biasa saja	393000.00	gapunya? gakeren lo!, miliki saat ini juga, beli 1 gratis 10 , cicilan 0%, tanpa tipu tipu
S0000274	Dijual Baju Keren sekali	743000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, gocengan doang, langsung tanpa DP, original asli!
S0000275	Dijual Baju Keren sekali	229000.00	anda harus jadi prioritas!, beli sekarang, jangan nanti, dijamin basdat A, dapatkan kesempatan, juga ngedate bareng jihan
S0000276	Dibeli sekarang juga Mobil Tidak Jelek	21000.00	ayo jangan sampai kehabisan!, nanti!, gocengan doang, gak datang  2 kali, juga ngedate bareng jihan
S0000277	Dengan ini adanya Kunci Mobil Sangat bagus	499000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, dijamin basdat A, langsung tanpa DP, tanpa tipu tipu
S0000278	Dengan ini adanya Mobil Tidak Jelek	27000.00	anda harus jadi prioritas!, miliki saat ini juga, bonus kelereng juga, gak datang  2 kali, original asli!
S0000279	Dengan ini adanya Sepatu Tidak ada tandingannya	687000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, bonus kelereng juga, dapatkan kesempatan, fast respond!
S0000280	Disewakan Sepatu Biasa saja	362000.00	anda harus jadi prioritas!, dapatkan segera, beli 1 gratis 10 , gak datang  2 kali, dijamin lulus 3 tahun
S0000281	Dibeli sekarang juga Vasmina Berkualitas	913000.00	ayo jangan sampai kehabisan!, dapatkan segera, beli 1 gratis 10 , bonus piring cantik, original asli!
S0000282	Menghadirkan Jeans Tidak ada tandingannya	632000.00	anda harus jadi prioritas!, miliki saat ini juga, bonus nomor telfon jihan, cicilan 0%, tanpa basa basi
S0000283	[WTS] Sweater Biasa saja	938000.00	anda harus jadi prioritas!, mau tunggu sampai kapan!, hari ini terakhir, cicilan 0%, dijamin lulus 3 tahun
S0000284	Dijual Sepatu Tidak Jelek	345000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, bonus kelereng juga, langsung tanpa DP, tanpa tipu tipu
S0000285	Dibeli sekarang juga Vasmina Keren sekali	478000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, dijamin basdat A, gak datang  2 kali, juga ngedate bareng jihan
S0000286	Menghadirkan Kunci Mobil Biasa saja	273000.00	gapunya? gakeren lo!, miliki saat ini juga, gocengan doang, gak datang  2 kali, fast respond!
S0000287	Dijual Baju Internasional	95000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, beli 1 gratis 10 , cicilan 0%, original asli!
S0000288	[WTS] Kunci Mobil Biasa saja	166000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, beli 1 gratis 10 , tanpa diundi, juga ngedate bareng jihan
S0000289	[WTS] Jaket Internasional	49000.00	anda harus jadi prioritas!, tunggu apa lagi, bonus kelereng juga, cicilan 0%, original asli!
S0000290	[WTS] Sweater Tidak Jelek	89000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, dijamin basdat A, dapatkan kesempatan, fast respond!
S0000291	[WTS] Mobil Keren sekali	65000.00	beli sekarang dan dapatkan keuntungan maksimal, nanti!, gocengan doang, langsung tanpa DP, original asli!
S0000292	Dijual Baju Pantai Biasa saja	13000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, gocengan doang, cicilan 0%, tanpa basa basi
S0000293	Dijual Baju Nomor 1 seIndinesia	32000.00	beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga, bonus nomor telfon jihan, bonus piring cantik, tanpa basa basi
S0000294	[WTS] HP Tidak ada tandingannya	37000.00	ayo jangan sampai kehabisan!, beli sekarang, jangan nanti, dijamin basdat A, langsung tanpa DP, fast respond!
S0000295	Menghadirkan Jeans Tak terkalahkan	89000.00	beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti, bonus nomor telfon jihan, cicilan 0%, original asli!
S0000296	Dengan ini adanya Baju Pantai Biasa saja	142000.00	ayo jangan sampai kehabisan!, mau tunggu sampai kapan!, dijamin basdat A, dapatkan kesempatan, tanpa tipu tipu
S0000297	Dengan ini adanya Mobil Tak terkalahkan	34000.00	beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!, bonus kelereng juga, langsung tanpa DP, tanpa tipu tipu
S0000298	Dibeli sekarang juga Sepatu Tidak Jelek	329000.00	anda harus jadi prioritas!, miliki saat ini juga, beli 1 gratis 10 , gak datang  2 kali, original asli!
S0000299	Dijual Kunci Mobil Tidak Jelek	83000.00	ayo jangan sampai kehabisan!, miliki saat ini juga, beli 1 gratis 10 , bonus piring cantik, tanpa tipu tipu
S0000300	Dibeli sekarang juga Kunci Mobil Keren sekali	131000.00	ayo jangan sampai kehabisan!, nanti!, dijamin basdat A, gak datang  2 kali, juga ngedate bareng jihan
P0000001	Paket Data XL 5GB	59000.00	Dapat direfund, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000002	Pulsa Telkomsel 20ribu	26000.00	Dapat direfund, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000003	Pulsa Indosat 40ribu	45000.00	Dapat direfund, digunakan ketika dibutuhkan, ayo jangan sampai kehabisan!, dapatkan segera
P0000004	Pulsa Tri 3 70ribu	79000.00	Dapat direfund, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000005	Paket Data Halo 9GB	96000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, tunggu apa lagi
P0000006	Pulsa Smartfren 10ribu	16000.00	Dapat direfund, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000007	Pulsa Hepi 60ribu	64000.00	Jaminan 100%, Bisa dinego, gapunya? gakeren lo!, beli sekarang, jangan nanti
P0000008	Pulsa IAM3 40ribu	45000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, dapatkan segera
P0000009	Paket Data Mentari 2GB	26000.00	Dapat direfund, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000010	Pulsa Kartu As 80ribu	89000.00	Dapat ditransfer saldo, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, nanti!
P0000011	Pulsa XL 60ribu	66000.00	Dapat ditransfer saldo, anda pasti butuh ini, ayo jangan sampai kehabisan!, miliki saat ini juga
P0000012	Pulsa Telkomsel 50ribu	57000.00	Dapat ditransfer saldo, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000013	Paket Data Indosat 9GB	93000.00	Jaminan 100%, Bisa dinego, anda harus jadi prioritas!, tunggu apa lagi
P0000014	Pulsa Tri 3 20ribu	21000.00	Dapat direfund, Bisa dinego, anda harus jadi prioritas!, nanti!
P0000015	Pulsa Halo 60ribu	63000.00	Dapat direfund, anda pasti butuh ini, ayo jangan sampai kehabisan!, mau tunggu sampai kapan!
P0000016	Pulsa Smartfren 30ribu	36000.00	Dapat direfund, Bisa dinego, ayo jangan sampai kehabisan!, mau tunggu sampai kapan!
P0000017	Paket Data Hepi 1GB	17000.00	Jaminan 100%, anda pasti butuh ini, ayo jangan sampai kehabisan!, nanti!
P0000018	Pulsa IAM3 50ribu	55000.00	Dapat ditransfer saldo, anda pasti butuh ini, ayo jangan sampai kehabisan!, miliki saat ini juga
P0000019	Pulsa Mentari 80ribu	81000.00	Jaminan 100%, digunakan ketika dibutuhkan, gapunya? gakeren lo!, beli sekarang, jangan nanti
P0000020	Pulsa Kartu As 10ribu	11000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, nanti!
P0000021	Paket Data XL 8GB	86000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, mau tunggu sampai kapan!
P0000022	Pulsa Telkomsel 90ribu	91000.00	Dapat direfund, digunakan ketika dibutuhkan, gapunya? gakeren lo!, dapatkan segera
P0000023	Pulsa Indosat 70ribu	76000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, miliki saat ini juga
P0000024	Pulsa Tri 3 10ribu	12000.00	Jaminan 100%, Bisa dinego, ayo jangan sampai kehabisan!, mau tunggu sampai kapan!
P0000025	Paket Data Halo 7GB	75000.00	Dapat ditransfer saldo, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000026	Pulsa Smartfren 50ribu	51000.00	Jaminan 100%, digunakan ketika dibutuhkan, anda harus jadi prioritas!, mau tunggu sampai kapan!
P0000027	Pulsa Hepi 30ribu	39000.00	Dapat ditransfer saldo, anda pasti butuh ini, gapunya? gakeren lo!, tunggu apa lagi
P0000028	Pulsa IAM3 90ribu	93000.00	Dapat direfund, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, miliki saat ini juga
P0000029	Paket Data Mentari 6GB	64000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, nanti!
P0000030	Pulsa Kartu As 80ribu	82000.00	Bisa digunakan kapanpun, Bisa dinego, gapunya? gakeren lo!, mau tunggu sampai kapan!
P0000031	Pulsa XL 20ribu	25000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000032	Pulsa Telkomsel 80ribu	88000.00	Dapat ditransfer saldo, anda pasti butuh ini, gapunya? gakeren lo!, dapatkan segera
P0000033	Paket Data Indosat 9GB	97000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000034	Pulsa Tri 3 20ribu	26000.00	Dapat ditransfer saldo, Bisa dinego, gapunya? gakeren lo!, mau tunggu sampai kapan!
P0000035	Pulsa Halo 90ribu	92000.00	Jaminan 100%, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000036	Pulsa Smartfren 30ribu	34000.00	Dapat direfund, Bisa dinego, gapunya? gakeren lo!, mau tunggu sampai kapan!
P0000037	Paket Data Hepi 3GB	32000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, nanti!
P0000038	Pulsa IAM3 80ribu	85000.00	Dapat ditransfer saldo, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000039	Pulsa Mentari 30ribu	39000.00	Jaminan 100%, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000040	Pulsa Kartu As 10ribu	14000.00	Jaminan 100%, digunakan ketika dibutuhkan, anda harus jadi prioritas!, tunggu apa lagi
P0000041	Paket Data XL 4GB	46000.00	Dapat ditransfer saldo, digunakan ketika dibutuhkan, anda harus jadi prioritas!, nanti!
P0000042	Pulsa Telkomsel 10ribu	12000.00	Dapat direfund, Bisa dinego, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000043	Pulsa Indosat 10ribu	15000.00	Dapat direfund, Bisa dinego, gapunya? gakeren lo!, beli sekarang, jangan nanti
P0000044	Pulsa Tri 3 40ribu	48000.00	Bisa digunakan kapanpun, Bisa dinego, anda harus jadi prioritas!, mau tunggu sampai kapan!
P0000045	Paket Data Halo 8GB	83000.00	Dapat ditransfer saldo, anda pasti butuh ini, ayo jangan sampai kehabisan!, beli sekarang, jangan nanti
P0000048	Pulsa IAM3 20ribu	23000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000049	Paket Data Mentari 4GB	45000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000050	Pulsa Kartu As 30ribu	39000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, anda harus jadi prioritas!, nanti!
P0000051	Pulsa XL 40ribu	49000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera
P0000052	Pulsa Telkomsel 10ribu	18000.00	Dapat direfund, digunakan ketika dibutuhkan, anda harus jadi prioritas!, tunggu apa lagi
P0000053	Paket Data Indosat 7GB	74000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000054	Pulsa Tri 3 80ribu	89000.00	Dapat direfund, anda pasti butuh ini, ayo jangan sampai kehabisan!, miliki saat ini juga
P0000055	Pulsa Halo 50ribu	56000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, tunggu apa lagi
P0000056	Pulsa Smartfren 20ribu	23000.00	Dapat direfund, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000057	Paket Data Hepi 7GB	76000.00	Dapat direfund, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, tunggu apa lagi
P0000058	Pulsa IAM3 10ribu	15000.00	Dapat direfund, digunakan ketika dibutuhkan, ayo jangan sampai kehabisan!, dapatkan segera
P0000059	Pulsa Mentari 10ribu	14000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, dapatkan segera
P0000060	Pulsa Kartu As 60ribu	64000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, gapunya? gakeren lo!, tunggu apa lagi
P0000061	Paket Data XL 7GB	76000.00	Dapat ditransfer saldo, Bisa dinego, anda harus jadi prioritas!, nanti!
P0000062	Pulsa Telkomsel 70ribu	79000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, miliki saat ini juga
P0000063	Pulsa Indosat 20ribu	29000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, anda harus jadi prioritas!, nanti!
P0000064	Pulsa Tri 3 10ribu	15000.00	Jaminan 100%, anda pasti butuh ini, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000065	Paket Data Halo 4GB	41000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera
P0000066	Pulsa Smartfren 60ribu	67000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, tunggu apa lagi
P0000067	Pulsa Hepi 30ribu	38000.00	Bisa digunakan kapanpun, anda pasti butuh ini, gapunya? gakeren lo!, dapatkan segera
P0000068	Pulsa IAM3 60ribu	65000.00	Dapat ditransfer saldo, Bisa dinego, anda harus jadi prioritas!, dapatkan segera
P0000069	Paket Data Mentari 2GB	27000.00	Dapat direfund, anda pasti butuh ini, anda harus jadi prioritas!, nanti!
P0000070	Pulsa Kartu As 20ribu	25000.00	Jaminan 100%, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera
P0000071	Pulsa XL 30ribu	37000.00	Dapat direfund, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000072	Pulsa Telkomsel 90ribu	95000.00	Dapat direfund, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000073	Paket Data Indosat 2GB	24000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000074	Pulsa Tri 3 70ribu	72000.00	Jaminan 100%, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, nanti!
P0000075	Pulsa Halo 80ribu	89000.00	Dapat direfund, Bisa dinego, gapunya? gakeren lo!, beli sekarang, jangan nanti
P0000076	Pulsa Smartfren 90ribu	99000.00	Dapat ditransfer saldo, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000077	Paket Data Hepi 5GB	51000.00	Dapat direfund, digunakan ketika dibutuhkan, gapunya? gakeren lo!, beli sekarang, jangan nanti
P0000078	Pulsa IAM3 50ribu	53000.00	Jaminan 100%, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000079	Pulsa Mentari 40ribu	48000.00	Jaminan 100%, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000080	Pulsa Kartu As 70ribu	79000.00	Jaminan 100%, digunakan ketika dibutuhkan, gapunya? gakeren lo!, dapatkan segera
P0000081	Paket Data XL 2GB	23000.00	Bisa digunakan kapanpun, Bisa dinego, gapunya? gakeren lo!, nanti!
P0000082	Pulsa Telkomsel 90ribu	97000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000083	Pulsa Indosat 40ribu	49000.00	Dapat direfund, digunakan ketika dibutuhkan, gapunya? gakeren lo!, mau tunggu sampai kapan!
P0000084	Pulsa Tri 3 10ribu	18000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, nanti!
P0000085	Paket Data Halo 2GB	26000.00	Jaminan 100%, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera
P0000086	Pulsa Smartfren 30ribu	38000.00	Jaminan 100%, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000087	Pulsa Hepi 50ribu	53000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, mau tunggu sampai kapan!
P0000088	Pulsa IAM3 70ribu	77000.00	Bisa digunakan kapanpun, Bisa dinego, gapunya? gakeren lo!, dapatkan segera
P0000089	Paket Data Mentari 6GB	64000.00	Dapat direfund, Bisa dinego, anda harus jadi prioritas!, dapatkan segera
P0000090	Pulsa Kartu As 90ribu	92000.00	Dapat ditransfer saldo, anda pasti butuh ini, ayo jangan sampai kehabisan!, mau tunggu sampai kapan!
P0000091	Pulsa XL 40ribu	41000.00	Dapat direfund, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000092	Pulsa Telkomsel 50ribu	54000.00	Dapat direfund, digunakan ketika dibutuhkan, anda harus jadi prioritas!, nanti!
P0000093	Paket Data Indosat 4GB	48000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, gapunya? gakeren lo!, miliki saat ini juga
P0000094	Pulsa Tri 3 80ribu	87000.00	Dapat direfund, anda pasti butuh ini, ayo jangan sampai kehabisan!, dapatkan segera
P0000095	Pulsa Halo 30ribu	36000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, nanti!
P0000096	Pulsa Smartfren 40ribu	43000.00	Bisa digunakan kapanpun, anda pasti butuh ini, ayo jangan sampai kehabisan!, nanti!
P0000097	Paket Data Hepi 5GB	54000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, dapatkan segera
P0000098	Pulsa IAM3 80ribu	89000.00	Dapat direfund, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000099	Pulsa Mentari 70ribu	74000.00	Dapat direfund, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000100	Pulsa Kartu As 30ribu	35000.00	Dapat direfund, anda pasti butuh ini, anda harus jadi prioritas!, nanti!
P0000101	Paket Data XL 7GB	79000.00	Jaminan 100%, digunakan ketika dibutuhkan, ayo jangan sampai kehabisan!, tunggu apa lagi
P0000102	Pulsa Telkomsel 50ribu	58000.00	Dapat ditransfer saldo, Bisa dinego, gapunya? gakeren lo!, tunggu apa lagi
P0000103	Pulsa Indosat 40ribu	49000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, dapatkan segera
P0000104	Pulsa Tri 3 70ribu	72000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, gapunya? gakeren lo!, dapatkan segera
P0000105	Paket Data Halo 6GB	64000.00	Dapat ditransfer saldo, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000106	Pulsa Smartfren 20ribu	25000.00	Dapat direfund, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000107	Pulsa Hepi 30ribu	37000.00	Dapat ditransfer saldo, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera
P0000108	Pulsa IAM3 20ribu	21000.00	Dapat ditransfer saldo, Bisa dinego, anda harus jadi prioritas!, tunggu apa lagi
P0000109	Paket Data Mentari 8GB	84000.00	Bisa digunakan kapanpun, anda pasti butuh ini, gapunya? gakeren lo!, mau tunggu sampai kapan!
P0000110	Pulsa Kartu As 10ribu	17000.00	Dapat ditransfer saldo, anda pasti butuh ini, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000111	Pulsa XL 50ribu	55000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, ayo jangan sampai kehabisan!, mau tunggu sampai kapan!
P0000112	Pulsa Telkomsel 50ribu	56000.00	Jaminan 100%, digunakan ketika dibutuhkan, gapunya? gakeren lo!, miliki saat ini juga
P0000113	Paket Data Indosat 6GB	67000.00	Bisa digunakan kapanpun, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000114	Pulsa Tri 3 90ribu	95000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, mau tunggu sampai kapan!
P0000115	Pulsa Halo 30ribu	39000.00	Jaminan 100%, anda pasti butuh ini, anda harus jadi prioritas!, nanti!
P0000116	Pulsa Smartfren 90ribu	96000.00	Dapat direfund, Bisa dinego, anda harus jadi prioritas!, nanti!
P0000117	Paket Data Hepi 6GB	68000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, dapatkan segera
P0000118	Pulsa IAM3 60ribu	66000.00	Jaminan 100%, anda pasti butuh ini, anda harus jadi prioritas!, miliki saat ini juga
P0000119	Pulsa Mentari 70ribu	74000.00	Bisa digunakan kapanpun, anda pasti butuh ini, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000120	Pulsa Kartu As 60ribu	61000.00	Dapat direfund, digunakan ketika dibutuhkan, anda harus jadi prioritas!, miliki saat ini juga
P0000121	Paket Data XL 6GB	61000.00	Bisa digunakan kapanpun, Bisa dinego, gapunya? gakeren lo!, miliki saat ini juga
P0000122	Pulsa Telkomsel 40ribu	41000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000123	Pulsa Indosat 40ribu	42000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, tunggu apa lagi
P0000124	Pulsa Tri 3 10ribu	11000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, mau tunggu sampai kapan!
P0000125	Paket Data Halo 3GB	37000.00	Jaminan 100%, anda pasti butuh ini, anda harus jadi prioritas!, tunggu apa lagi
P0000126	Pulsa Smartfren 70ribu	77000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, miliki saat ini juga
P0000127	Pulsa Hepi 30ribu	32000.00	Dapat direfund, digunakan ketika dibutuhkan, anda harus jadi prioritas!, tunggu apa lagi
P0000128	Pulsa IAM3 40ribu	48000.00	Dapat direfund, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000129	Paket Data Mentari 1GB	11000.00	Bisa digunakan kapanpun, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000130	Pulsa Kartu As 70ribu	73000.00	Jaminan 100%, Bisa dinego, ayo jangan sampai kehabisan!, nanti!
P0000131	Pulsa XL 70ribu	74000.00	Jaminan 100%, anda pasti butuh ini, ayo jangan sampai kehabisan!, miliki saat ini juga
P0000132	Pulsa Telkomsel 80ribu	88000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, nanti!
P0000133	Paket Data Indosat 3GB	35000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, dapatkan segera
P0000134	Pulsa Tri 3 70ribu	79000.00	Dapat direfund, Bisa dinego, ayo jangan sampai kehabisan!, nanti!
P0000135	Pulsa Halo 30ribu	32000.00	Dapat direfund, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, tunggu apa lagi
P0000136	Pulsa Smartfren 10ribu	17000.00	Dapat direfund, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000137	Paket Data Hepi 5GB	58000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000138	Pulsa IAM3 10ribu	17000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, nanti!
P0000139	Pulsa Mentari 10ribu	14000.00	Dapat direfund, Bisa dinego, anda harus jadi prioritas!, mau tunggu sampai kapan!
P0000140	Pulsa Kartu As 40ribu	48000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000141	Paket Data XL 5GB	53000.00	Dapat ditransfer saldo, digunakan ketika dibutuhkan, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000142	Pulsa Telkomsel 80ribu	85000.00	Dapat direfund, anda pasti butuh ini, ayo jangan sampai kehabisan!, beli sekarang, jangan nanti
P0000143	Pulsa Indosat 60ribu	64000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, gapunya? gakeren lo!, miliki saat ini juga
P0000144	Pulsa Tri 3 70ribu	72000.00	Jaminan 100%, anda pasti butuh ini, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000145	Paket Data Halo 7GB	72000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, nanti!
P0000146	Pulsa Smartfren 80ribu	89000.00	Dapat direfund, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000147	Pulsa Hepi 10ribu	18000.00	Bisa digunakan kapanpun, anda pasti butuh ini, ayo jangan sampai kehabisan!, tunggu apa lagi
P0000148	Pulsa IAM3 20ribu	23000.00	Bisa digunakan kapanpun, Bisa dinego, gapunya? gakeren lo!, nanti!
P0000149	Paket Data Mentari 1GB	15000.00	Bisa digunakan kapanpun, anda pasti butuh ini, ayo jangan sampai kehabisan!, tunggu apa lagi
P0000150	Pulsa Kartu As 50ribu	58000.00	Jaminan 100%, anda pasti butuh ini, anda harus jadi prioritas!, miliki saat ini juga
P0000151	Pulsa XL 30ribu	38000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000152	Pulsa Telkomsel 20ribu	29000.00	Jaminan 100%, Bisa dinego, beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera
P0000153	Paket Data Indosat 2GB	23000.00	Dapat ditransfer saldo, digunakan ketika dibutuhkan, gapunya? gakeren lo!, beli sekarang, jangan nanti
P0000154	Pulsa Tri 3 30ribu	35000.00	Bisa digunakan kapanpun, Bisa dinego, gapunya? gakeren lo!, tunggu apa lagi
P0000155	Pulsa Halo 50ribu	52000.00	Dapat direfund, anda pasti butuh ini, anda harus jadi prioritas!, miliki saat ini juga
P0000156	Pulsa Smartfren 60ribu	65000.00	Dapat direfund, digunakan ketika dibutuhkan, gapunya? gakeren lo!, nanti!
P0000157	Paket Data Hepi 3GB	36000.00	Dapat direfund, anda pasti butuh ini, gapunya? gakeren lo!, nanti!
P0000158	Pulsa IAM3 20ribu	23000.00	Jaminan 100%, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000159	Pulsa Mentari 20ribu	29000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, tunggu apa lagi
P0000160	Pulsa Kartu As 90ribu	92000.00	Dapat direfund, digunakan ketika dibutuhkan, ayo jangan sampai kehabisan!, nanti!
P0000161	Paket Data XL 1GB	13000.00	Jaminan 100%, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000162	Pulsa Telkomsel 50ribu	52000.00	Dapat direfund, Bisa dinego, ayo jangan sampai kehabisan!, nanti!
P0000163	Pulsa Indosat 80ribu	82000.00	Dapat ditransfer saldo, digunakan ketika dibutuhkan, gapunya? gakeren lo!, dapatkan segera
P0000164	Pulsa Tri 3 40ribu	48000.00	Jaminan 100%, Bisa dinego, gapunya? gakeren lo!, mau tunggu sampai kapan!
P0000165	Paket Data Halo 7GB	77000.00	Dapat direfund, anda pasti butuh ini, gapunya? gakeren lo!, nanti!
P0000166	Pulsa Smartfren 50ribu	56000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000167	Pulsa Hepi 10ribu	14000.00	Bisa digunakan kapanpun, Bisa dinego, ayo jangan sampai kehabisan!, beli sekarang, jangan nanti
P0000168	Pulsa IAM3 40ribu	41000.00	Bisa digunakan kapanpun, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, nanti!
P0000169	Paket Data Mentari 7GB	77000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, anda harus jadi prioritas!, beli sekarang, jangan nanti
P0000170	Pulsa Kartu As 50ribu	58000.00	Jaminan 100%, anda pasti butuh ini, gapunya? gakeren lo!, mau tunggu sampai kapan!
P0000171	Pulsa XL 70ribu	77000.00	Jaminan 100%, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000172	Pulsa Telkomsel 50ribu	59000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, mau tunggu sampai kapan!
P0000173	Paket Data Indosat 3GB	33000.00	Bisa digunakan kapanpun, Bisa dinego, ayo jangan sampai kehabisan!, beli sekarang, jangan nanti
P0000174	Pulsa Tri 3 20ribu	23000.00	Dapat direfund, anda pasti butuh ini, anda harus jadi prioritas!, nanti!
P0000175	Pulsa Halo 30ribu	33000.00	Dapat ditransfer saldo, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, nanti!
P0000176	Pulsa Smartfren 90ribu	99000.00	Jaminan 100%, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, nanti!
P0000177	Paket Data Hepi 5GB	51000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000178	Pulsa IAM3 10ribu	11000.00	Dapat direfund, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, beli sekarang, jangan nanti
P0000179	Pulsa Mentari 70ribu	78000.00	Jaminan 100%, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, nanti!
P0000180	Pulsa Kartu As 10ribu	17000.00	Jaminan 100%, anda pasti butuh ini, anda harus jadi prioritas!, miliki saat ini juga
P0000181	Paket Data XL 1GB	18000.00	Bisa digunakan kapanpun, anda pasti butuh ini, gapunya? gakeren lo!, beli sekarang, jangan nanti
P0000182	Pulsa Telkomsel 60ribu	68000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000183	Pulsa Indosat 70ribu	79000.00	Jaminan 100%, Bisa dinego, ayo jangan sampai kehabisan!, miliki saat ini juga
P0000184	Pulsa Tri 3 40ribu	41000.00	Dapat ditransfer saldo, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, dapatkan segera
P0000185	Paket Data Halo 8GB	84000.00	Dapat direfund, digunakan ketika dibutuhkan, gapunya? gakeren lo!, dapatkan segera
P0000186	Pulsa Smartfren 40ribu	44000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, anda harus jadi prioritas!, dapatkan segera
P0000187	Pulsa Hepi 70ribu	73000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, gapunya? gakeren lo!, miliki saat ini juga
P0000188	Pulsa IAM3 50ribu	56000.00	Jaminan 100%, digunakan ketika dibutuhkan, gapunya? gakeren lo!, beli sekarang, jangan nanti
P0000189	Paket Data Mentari 1GB	17000.00	Jaminan 100%, Bisa menjadi barang berguna bagi kehidupan, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000190	Pulsa Kartu As 30ribu	31000.00	Dapat direfund, Bisa dinego, anda harus jadi prioritas!, dapatkan segera
P0000191	Pulsa XL 10ribu	12000.00	Dapat direfund, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000192	Pulsa Telkomsel 20ribu	27000.00	Dapat ditransfer saldo, Bisa dinego, gapunya? gakeren lo!, beli sekarang, jangan nanti
P0000193	Paket Data Indosat 1GB	19000.00	Jaminan 100%, Bisa dinego, anda harus jadi prioritas!, mau tunggu sampai kapan!
P0000194	Pulsa Tri 3 20ribu	29000.00	Bisa digunakan kapanpun, Bisa menjadi barang berguna bagi kehidupan, ayo jangan sampai kehabisan!, mau tunggu sampai kapan!
P0000195	Pulsa Halo 20ribu	23000.00	Dapat direfund, anda pasti butuh ini, beli sekarang dan dapatkan keuntungan maksimal, mau tunggu sampai kapan!
P0000196	Pulsa Smartfren 10ribu	16000.00	Dapat ditransfer saldo, digunakan ketika dibutuhkan, beli sekarang dan dapatkan keuntungan maksimal, miliki saat ini juga
P0000197	Paket Data Hepi 4GB	43000.00	Bisa digunakan kapanpun, Bisa dinego, ayo jangan sampai kehabisan!, beli sekarang, jangan nanti
P0000198	Pulsa IAM3 80ribu	86000.00	Dapat ditransfer saldo, Bisa dinego, anda harus jadi prioritas!, dapatkan segera
P0000199	Pulsa Mentari 70ribu	75000.00	Jaminan 100%, digunakan ketika dibutuhkan, gapunya? gakeren lo!, tunggu apa lagi
P0000200	Pulsa Kartu As 90ribu	91000.00	Bisa digunakan kapanpun, digunakan ketika dibutuhkan, ayo jangan sampai kehabisan!, miliki saat ini juga
\.


--
-- Data for Name: produk_pulsa; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY produk_pulsa (kode_produk, nominal) FROM stdin;
P0000001	50
P0000002	20
P0000003	40
P0000004	70
P0000005	90
P0000006	10
P0000007	60
P0000008	40
P0000009	20
P0000010	80
P0000011	60
P0000012	50
P0000013	90
P0000014	20
P0000015	60
P0000016	30
P0000017	10
P0000018	50
P0000019	80
P0000020	10
P0000021	80
P0000022	90
P0000023	70
P0000024	10
P0000025	70
P0000026	50
P0000027	30
P0000028	90
P0000029	60
P0000030	80
P0000031	20
P0000032	80
P0000033	90
P0000034	20
P0000035	90
P0000036	30
P0000037	30
P0000038	80
P0000039	30
P0000040	10
P0000041	40
P0000042	10
P0000043	10
P0000044	40
P0000045	80
P0000046	70
P0000047	40
P0000048	20
P0000049	40
P0000050	30
P0000051	40
P0000052	10
P0000053	70
P0000054	80
P0000055	50
P0000056	20
P0000057	70
P0000058	10
P0000059	10
P0000060	60
P0000061	70
P0000062	70
P0000063	20
P0000064	10
P0000065	40
P0000066	60
P0000067	30
P0000068	60
P0000069	20
P0000070	20
P0000071	30
P0000072	90
P0000073	20
P0000074	70
P0000075	80
P0000076	90
P0000077	50
P0000078	50
P0000079	40
P0000080	70
P0000081	20
P0000082	90
P0000083	40
P0000084	10
P0000085	20
P0000086	30
P0000087	50
P0000088	70
P0000089	60
P0000090	90
P0000091	40
P0000092	50
P0000093	40
P0000094	80
P0000095	30
P0000096	40
P0000097	50
P0000098	80
P0000099	70
P0000100	30
P0000101	70
P0000102	50
P0000103	40
P0000104	70
P0000105	60
P0000106	20
P0000107	30
P0000108	20
P0000109	80
P0000110	10
P0000111	50
P0000112	50
P0000113	60
P0000114	90
P0000115	30
P0000116	90
P0000117	60
P0000118	60
P0000119	70
P0000120	60
P0000121	60
P0000122	40
P0000123	40
P0000124	10
P0000125	30
P0000126	70
P0000127	30
P0000128	40
P0000129	10
P0000130	70
P0000131	70
P0000132	80
P0000133	30
P0000134	70
P0000135	30
P0000136	10
P0000137	50
P0000138	10
P0000139	10
P0000140	40
P0000141	50
P0000142	80
P0000143	60
P0000144	70
P0000145	70
P0000146	80
P0000147	10
P0000148	20
P0000149	10
P0000150	50
P0000151	30
P0000152	20
P0000153	20
P0000154	30
P0000155	50
P0000156	60
P0000157	30
P0000158	20
P0000159	20
P0000160	90
P0000161	10
P0000162	50
P0000163	80
P0000164	40
P0000165	70
P0000166	50
P0000167	10
P0000168	40
P0000169	70
P0000170	50
P0000171	70
P0000172	50
P0000173	30
P0000174	20
P0000175	30
P0000176	90
P0000177	50
P0000178	10
P0000179	70
P0000180	10
P0000181	10
P0000182	60
P0000183	70
P0000184	40
P0000185	80
P0000186	40
P0000187	70
P0000188	50
P0000189	10
P0000190	30
P0000191	10
P0000192	20
P0000193	10
P0000194	20
P0000195	20
P0000196	10
P0000197	40
P0000198	80
P0000199	70
P0000200	90
\.


--
-- Data for Name: promo; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY promo (id, deskripsi, periode_awal, periode_akhir, kode) FROM stdin;
R00001	Diskon 50% All item	2016-04-26	2016-05-03	SEMUASETENGAH
R00002	Diskon 50000 setiap pembelian	2016-08-05	2016-08-08	MAYANGOCAP
R00003	Cashback 100000 kapanpun	2016-03-18	2016-03-28	POLISICEPEK
R00004	Diskon 20% + 40% LIMITED!	2016-04-13	2016-04-25	BANYAKDISKON
R00005	Potongan harga 10% tanpa ppn	2016-04-30	2016-05-06	KURANGIPPN
\.


--
-- Data for Name: promo_produk; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY promo_produk (id_promo, kode_produk) FROM stdin;
R00002	S0000143
R00004	P0000189
R00005	S0000201
R00001	P0000190
R00002	P0000090
R00003	P0000126
R00003	P0000164
R00001	S0000097
R00004	S0000294
R00003	S0000205
R00001	S0000143
R00002	P0000127
R00001	P0000062
R00001	P0000029
R00001	P0000038
R00005	S0000103
R00005	S0000032
R00004	P0000088
R00004	P0000179
R00003	S0000226
R00003	S0000143
R00005	S0000192
R00003	P0000177
R00005	S0000048
R00003	S0000019
R00003	S0000118
R00001	S0000204
R00005	S0000173
R00004	S0000253
R00004	S0000170
R00003	S0000053
R00001	S0000130
R00004	P0000057
R00003	P0000089
R00005	S0000059
R00003	S0000279
R00005	P0000081
R00001	S0000071
R00003	S0000240
R00001	S0000197
R00005	S0000160
R00005	P0000035
R00004	S0000036
R00002	P0000129
R00002	P0000035
R00004	P0000190
R00002	P0000067
R00003	S0000194
R00001	S0000221
R00002	P0000190
R00001	S0000024
R00001	S0000103
R00004	P0000058
R00004	S0000148
R00002	S0000246
R00002	P0000188
R00004	P0000028
R00005	S0000149
R00002	S0000232
R00001	S0000111
R00001	P0000116
R00005	P0000066
R00002	S0000017
R00001	P0000121
R00004	S0000132
R00005	S0000121
R00001	P0000103
R00003	S0000086
R00004	P0000115
R00003	S0000287
R00003	S0000190
R00005	S0000231
R00005	S0000034
R00003	S0000201
R00003	P0000065
\.


--
-- Data for Name: shipped_produk; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY shipped_produk (kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto) FROM stdin;
S0000008	SK008	Ongebzu	t	66	f	1	9	17	90000.00	S0000008-image.jpg
S0000048	SK048	Nisgasod	t	59	t	3	9	19	132000.00	S0000048-image.jpg
S0000076	SK076	Teocesuh	t	25	t	3	10	11	74000.00	S0000076-image.jpg
S0000026	SK026	Uciwguc	t	47	f	3	9	18	13000.00	S0000026-image.jpg
S0000050	SK050	Azevimut	t	55	f	2	7	12	6000.00	S0000050-image.jpg
S0000015	SK015	Laetpem	t	70	f	2	6	15	548000.00	S0000015-image.jpg
S0000043	SK043	Ewelimleh	t	26	t	3	8	16	131000.00	S0000043-image.jpg
S0000078	SK078	Teocesuh	t	13	f	1	7	17	44000.00	S0000078-image.jpg
S0000049	SK049	Azevimut	t	2	t	1	9	17	590000.00	S0000049-image.jpg
S0000022	SK022	Puseza	t	77	t	2	9	11	116000.00	S0000022-image.jpg
S0000061	SK061	Mahzegug	t	47	t	3	9	19	50000.00	S0000061-image.jpg
S0000021	SK021	Tonone	t	61	t	1	8	14	60000.00	S0000021-image.jpg
S0000020	SK020	Tonone	t	4	t	2	10	16	607000.00	S0000020-image.jpg
S0000060	SK060	Zafjomvo	t	58	f	1	9	18	19000.00	S0000060-image.jpg
S0000005	SK005	Newbimmig	f	41	f	1	5	16	956000.00	S0000005-image.jpg
S0000036	SK036	Ivsatdof	t	37	f	2	8	14	38000.00	S0000036-image.jpg
S0000001	SK001	Netfugab	t	12	t	2	8	13	344000.00	S0000001-image.jpg
S0000027	SK027	Uciwguc	t	72	f	2	10	11	340000.00	S0000027-image.jpg
S0000003	SK003	Netfugab	t	64	t	3	7	17	22000.00	S0000003-image.jpg
S0000052	SK052	Weezed	t	22	t	1	9	13	159000.00	S0000052-image.jpg
S0000069	SK069	Rufarowo	t	15	f	2	7	17	954000.00	S0000069-image.jpg
S0000063	SK063	Mahzegug	t	86	f	2	8	17	283000.00	S0000063-image.jpg
S0000057	SK057	Zonsawne	t	61	t	1	10	18	956000.00	S0000057-image.jpg
S0000072	SK072	Umuduuna	t	50	f	2	10	20	570000.00	S0000072-image.jpg
S0000007	SK007	Ongebzu	t	66	t	1	5	18	241000.00	S0000007-image.jpg
S0000077	SK077	Teocesuh	f	29	f	2	7	19	861000.00	S0000077-image.jpg
S0000044	SK044	Ewelimleh	t	0	f	1	9	11	771000.00	S0000044-image.jpg
S0000006	SK006	Newbimmig	t	7	f	3	6	11	412000.00	S0000006-image.jpg
S0000054	SK054	Weezed	t	4	f	2	5	15	58000.00	S0000054-image.jpg
S0000062	SK062	Mahzegug	t	45	f	3	7	19	26000.00	S0000062-image.jpg
S0000067	SK067	Rufarowo	t	59	t	1	10	18	183000.00	S0000067-image.jpg
S0000068	SK068	Rufarowo	t	9	f	1	9	16	509000.00	S0000068-image.jpg
S0000032	SK032	Favofsi	t	81	f	2	7	18	644000.00	S0000032-image.jpg
S0000010	SK010	Onuegbiw	t	80	t	2	5	11	94000.00	S0000010-image.jpg
S0000059	SK059	Zafjomvo	f	45	f	1	5	16	64000.00	S0000059-image.jpg
S0000056	SK056	Zonsawne	t	8	t	3	10	14	51000.00	S0000056-image.jpg
S0000028	SK028	Zesuzid	t	11	t	1	8	18	164000.00	S0000028-image.jpg
S0000013	SK013	Laetpem	t	65	t	2	5	20	964000.00	S0000013-image.jpg
S0000033	SK033	Favofsi	t	70	f	1	5	18	370000.00	S0000033-image.jpg
S0000030	SK030	Zesuzid	t	45	t	1	5	16	142000.00	S0000030-image.jpg
S0000046	SK046	Nisgasod	t	23	t	2	5	15	163000.00	S0000046-image.jpg
S0000058	SK058	Zafjomvo	t	24	t	3	6	11	638000.00	S0000058-image.jpg
S0000011	SK011	Onuegbiw	f	44	t	2	7	19	81000.00	S0000011-image.jpg
S0000029	SK029	Zesuzid	f	69	t	2	7	11	78000.00	S0000029-image.jpg
S0000002	SK002	Netfugab	t	13	t	3	6	19	347000.00	S0000002-image.jpg
S0000023	SK023	Puseza	f	55	f	3	6	17	331000.00	S0000023-image.jpg
S0000025	SK025	Uciwguc	t	66	t	2	9	17	86000.00	S0000025-image.jpg
S0000053	SK053	Weezed	f	7	f	2	7	11	87000.00	S0000053-image.jpg
S0000071	SK071	Umuduuna	f	78	f	1	7	11	917000.00	S0000071-image.jpg
S0000024	SK024	Puseza	t	5	f	1	6	14	81000.00	S0000024-image.jpg
S0000066	SK066	Binjesra	t	80	t	2	9	16	983000.00	S0000066-image.jpg
S0000019	SK019	Tonone	t	25	t	2	8	20	27000.00	S0000019-image.jpg
S0000018	SK018	Ezjeza	t	80	f	3	10	19	464000.00	S0000018-image.jpg
S0000070	SK070	Umuduuna	t	32	t	1	9	15	536000.00	S0000070-image.jpg
S0000037	SK037	Kerehu	t	79	t	1	10	14	378000.00	S0000037-image.jpg
S0000012	SK012	Onuegbiw	t	84	t	2	9	12	90000.00	S0000012-image.jpg
S0000039	SK039	Kerehu	t	90	t	3	9	13	7000.00	S0000039-image.jpg
S0000042	SK042	Lobasil	t	3	f	1	8	14	172000.00	S0000042-image.jpg
S0000040	SK040	Lobasil	t	84	t	2	8	13	39000.00	S0000040-image.jpg
S0000075	SK075	Jeijsig	t	17	t	2	6	13	638000.00	S0000075-image.jpg
S0000004	SK004	Newbimmig	t	58	t	3	10	12	89000.00	S0000004-image.jpg
S0000055	SK055	Zonsawne	t	89	t	1	8	16	441000.00	S0000055-image.jpg
S0000065	SK065	Binjesra	f	40	t	2	7	19	480000.00	S0000065-image.jpg
S0000031	SK031	Favofsi	t	22	t	1	8	18	54000.00	S0000031-image.jpg
S0000016	SK016	Ezjeza	t	35	t	3	9	16	61000.00	S0000016-image.jpg
S0000064	SK064	Binjesra	t	54	t	3	9	14	9000.00	S0000064-image.jpg
S0000014	SK014	Laetpem	t	23	f	3	7	12	24000.00	S0000014-image.jpg
S0000034	SK034	Ivsatdof	t	84	t	1	10	12	461000.00	S0000034-image.jpg
S0000074	SK074	Jeijsig	t	30	t	3	5	14	63000.00	S0000074-image.jpg
S0000073	SK073	Jeijsig	t	60	t	1	10	18	39000.00	S0000073-image.jpg
S0000047	SK047	Nisgasod	f	75	t	2	7	14	916000.00	S0000047-image.jpg
S0000038	SK038	Kerehu	t	62	t	1	10	18	9000.00	S0000038-image.jpg
S0000009	SK009	Ongebzu	t	11	f	3	7	20	292000.00	S0000009-image.jpg
S0000035	SK035	Ivsatdof	f	89	f	2	9	19	591000.00	S0000035-image.jpg
S0000097	SK017	Zuzreku	t	40	t	3	8	13	953000.00	S0000097-image.jpg
S0000113	SK033	Ekviamu	f	81	f	3	9	20	7000.00	S0000113-image.jpg
S0000156	SK076	Fotavsig	t	26	t	3	10	13	77000.00	S0000156-image.jpg
S0000146	SK066	Tohimi	t	69	t	3	5	16	240000.00	S0000146-image.jpg
S0000120	SK040	Sekgizveb	t	92	t	1	6	12	407000.00	S0000120-image.jpg
S0000084	SK004	Cebvolsi	t	74	t	3	5	14	671000.00	S0000084-image.jpg
S0000085	SK005	Ulmezle	t	74	t	2	10	17	859000.00	S0000085-image.jpg
S0000137	SK057	Denhosjil	f	14	t	3	6	15	66000.00	S0000137-image.jpg
S0000105	SK025	Zovkumer	t	80	f	1	10	13	87000.00	S0000105-image.jpg
S0000148	SK068	Podpijzaz	t	65	t	2	9	15	766000.00	S0000148-image.jpg
S0000128	SK048	Gegufa	t	67	t	3	9	19	22000.00	S0000128-image.jpg
S0000087	SK007	Ulmezle	t	71	f	3	8	11	52000.00	S0000087-image.jpg
S0000126	SK046	Nadcezpip	t	63	f	3	8	13	68000.00	S0000126-image.jpg
S0000095	SK015	Tesozoun	f	81	f	2	8	20	819000.00	S0000095-image.jpg
S0000104	SK024	Zovkumer	t	71	f	1	7	11	363000.00	S0000104-image.jpg
S0000127	SK047	Gegufa	t	59	t	2	5	18	759000.00	S0000127-image.jpg
S0000109	SK029	Wanuwa	t	46	t	1	10	17	590000.00	S0000109-image.jpg
S0000150	SK070	Podpijzaz	t	35	f	2	9	13	82000.00	S0000150-image.jpg
S0000111	SK031	Wanuwa	t	61	t	2	10	11	87000.00	S0000111-image.jpg
S0000121	SK041	Halaga	t	64	t	1	9	14	988000.00	S0000121-image.jpg
S0000082	SK002	Cebvolsi	t	11	t	3	5	11	142000.00	S0000082-image.jpg
S0000094	SK014	Tesozoun	t	53	t	2	7	12	359000.00	S0000094-image.jpg
S0000144	SK064	Jucujra	t	53	f	2	8	17	182000.00	S0000144-image.jpg
S0000081	SK001	Rihizaf	t	75	f	1	10	11	753000.00	S0000081-image.jpg
S0000080	SK080	Rihizaf	t	81	f	1	8	11	37000.00	S0000080-image.jpg
S0000079	SK079	Rihizaf	t	73	t	1	7	13	79000.00	S0000079-image.jpg
S0000130	SK050	Racubuus	t	37	t	3	9	15	22000.00	S0000130-image.jpg
S0000117	SK037	Goopifeb	t	44	f	3	5	18	17000.00	S0000117-image.jpg
S0000125	SK045	Nadcezpip	f	36	f	3	8	13	23000.00	S0000125-image.jpg
S0000149	SK069	Podpijzaz	f	19	f	1	8	16	90000.00	S0000149-image.jpg
S0000123	SK043	Halaga	t	65	f	1	9	11	742000.00	S0000123-image.jpg
S0000098	SK018	Zuzreku	t	77	f	3	5	17	683000.00	S0000098-image.jpg
S0000089	SK009	Leobizek	f	91	f	3	10	16	39000.00	S0000089-image.jpg
S0000143	SK063	Jucujra	f	39	f	3	8	11	28000.00	S0000143-image.jpg
S0000116	SK036	Goopifeb	t	71	f	2	5	12	39000.00	S0000116-image.jpg
S0000147	SK067	Tohimi	t	70	t	3	9	11	137000.00	S0000147-image.jpg
S0000142	SK062	Jucujra	t	17	t	2	10	13	151000.00	S0000142-image.jpg
S0000115	SK035	Goopifeb	t	30	t	3	6	15	173000.00	S0000115-image.jpg
S0000103	SK023	Zovkumer	t	66	t	2	8	19	40000.00	S0000103-image.jpg
S0000140	SK060	Icjakop	t	58	f	2	7	11	354000.00	S0000140-image.jpg
S0000088	SK008	Leobizek	t	6	t	3	8	13	82000.00	S0000088-image.jpg
S0000107	SK027	Vulkewmeg	f	73	f	2	9	11	31000.00	S0000107-image.jpg
S0000153	SK073	Nefpezi	t	69	f	3	9	11	263000.00	S0000153-image.jpg
S0000099	SK019	Zuzreku	t	23	f	3	8	17	29000.00	S0000099-image.jpg
S0000090	SK010	Leobizek	t	80	f	2	6	16	46000.00	S0000090-image.jpg
S0000136	SK056	Denhosjil	t	79	t	2	5	17	334000.00	S0000136-image.jpg
S0000112	SK032	Ekviamu	t	39	t	3	6	19	76000.00	S0000112-image.jpg
S0000091	SK011	Nekusuj	t	82	t	1	8	16	71000.00	S0000091-image.jpg
S0000154	SK074	Fotavsig	t	90	t	1	8	18	734000.00	S0000154-image.jpg
S0000155	SK075	Fotavsig	f	85	t	3	10	18	33000.00	S0000155-image.jpg
S0000139	SK059	Icjakop	t	85	t	2	5	15	78000.00	S0000139-image.jpg
S0000093	SK013	Nekusuj	t	36	t	1	5	12	574000.00	S0000093-image.jpg
S0000133	SK053	Ciemica	t	27	t	3	7	14	863000.00	S0000133-image.jpg
S0000132	SK052	Racubuus	t	89	f	3	7	18	20000.00	S0000132-image.jpg
S0000096	SK016	Tesozoun	t	29	f	3	6	17	90000.00	S0000096-image.jpg
S0000108	SK028	Vulkewmeg	t	56	f	1	7	19	63000.00	S0000108-image.jpg
S0000110	SK030	Wanuwa	t	56	t	3	10	14	90000.00	S0000110-image.jpg
S0000122	SK042	Halaga	t	11	f	1	10	17	38000.00	S0000122-image.jpg
S0000124	SK044	Nadcezpip	t	33	t	2	6	15	17000.00	S0000124-image.jpg
S0000119	SK039	Sekgizveb	f	84	t	3	5	20	6000.00	S0000119-image.jpg
S0000157	SK077	Girnudike	t	89	t	2	6	18	30000.00	S0000157-image.jpg
S0000151	SK071	Nefpezi	t	58	t	1	8	20	28000.00	S0000151-image.jpg
S0000138	SK058	Denhosjil	t	15	t	1	9	11	23000.00	S0000138-image.jpg
S0000141	SK061	Icjakop	t	63	f	3	10	19	323000.00	S0000141-image.jpg
S0000135	SK055	Ciemica	t	69	f	2	10	18	38000.00	S0000135-image.jpg
S0000129	SK049	Gegufa	t	52	t	1	10	12	39000.00	S0000129-image.jpg
S0000092	SK012	Nekusuj	t	46	t	3	9	11	93000.00	S0000092-image.jpg
S0000145	SK065	Tohimi	t	46	t	1	5	11	81000.00	S0000145-image.jpg
S0000106	SK026	Vulkewmeg	t	43	t	3	7	16	906000.00	S0000106-image.jpg
S0000083	SK003	Cebvolsi	f	76	t	2	8	13	441000.00	S0000083-image.jpg
S0000101	SK021	Lilhava	f	66	t	3	7	15	61000.00	S0000101-image.jpg
S0000134	SK054	Ciemica	t	32	f	2	8	13	16000.00	S0000134-image.jpg
S0000118	SK038	Sekgizveb	t	50	t	2	9	12	366000.00	S0000118-image.jpg
S0000102	SK022	Lilhava	t	41	t	1	5	19	70000.00	S0000102-image.jpg
S0000114	SK034	Ekviamu	t	4	f	2	6	16	869000.00	S0000114-image.jpg
S0000171	SK011	Uhcowej	t	53	f	2	6	11	53000.00	S0000171-image.jpg
S0000198	SK038	Akutodror	t	53	f	2	9	15	22000.00	S0000198-image.jpg
S0000197	SK037	Akutodror	f	54	f	1	10	18	47000.00	S0000197-image.jpg
S0000224	SK064	Ihkeadu	t	31	f	1	9	15	93000.00	S0000224-image.jpg
S0000225	SK065	Ihkeadu	t	89	f	2	9	19	793000.00	S0000225-image.jpg
S0000221	SK061	Fapapo	f	27	f	1	9	15	744000.00	S0000221-image.jpg
S0000166	SK006	Udaumdog	t	78	t	1	9	18	91000.00	S0000166-image.jpg
S0000226	SK066	Masvebje	t	66	t	3	7	15	973000.00	S0000226-image.jpg
S0000220	SK060	Fapapo	t	64	t	3	7	20	632000.00	S0000220-image.jpg
S0000173	SK013	Sansegesu	f	0	t	2	10	19	324000.00	S0000173-image.jpg
S0000161	SK001	Galufef	f	76	f	3	10	19	64000.00	S0000161-image.jpg
S0000190	SK030	Tuavted	t	33	t	3	10	16	739000.00	S0000190-image.jpg
S0000206	SK046	Renroos	t	71	f	1	9	18	921000.00	S0000206-image.jpg
S0000162	SK002	Galufef	t	95	f	1	8	13	447000.00	S0000162-image.jpg
S0000191	SK031	Tuavted	f	18	t	1	5	11	240000.00	S0000191-image.jpg
S0000229	SK069	Sokahig	t	48	t	3	6	16	753000.00	S0000229-image.jpg
S0000213	SK053	Voewisod	t	36	f	3	5	13	21000.00	S0000213-image.jpg
S0000188	SK028	Sujoze	t	21	f	3	6	16	38000.00	S0000188-image.jpg
S0000195	SK035	Cuulaseb	t	56	f	3	10	15	157000.00	S0000195-image.jpg
S0000170	SK010	Uhcowej	t	68	f	1	6	19	24000.00	S0000170-image.jpg
S0000228	SK068	Masvebje	t	27	t	1	10	11	988000.00	S0000228-image.jpg
S0000217	SK057	Nocanad	t	19	t	3	10	18	7000.00	S0000217-image.jpg
S0000165	SK005	Divvemdaz	t	22	t	1	9	20	562000.00	S0000165-image.jpg
S0000234	SK074	Amdicru	t	91	f	2	5	20	24000.00	S0000234-image.jpg
S0000215	SK055	Zuhicuto	f	51	f	3	5	13	281000.00	S0000215-image.jpg
S0000208	SK048	Hecgusnum	t	36	t	3	10	18	520000.00	S0000208-image.jpg
S0000200	SK040	Ovozge	t	2	t	1	6	16	60000.00	S0000200-image.jpg
S0000233	SK073	Amdicru	f	74	f	1	7	17	913000.00	S0000233-image.jpg
S0000158	SK078	Girnudike	t	79	f	1	5	20	447000.00	S0000158-image.jpg
S0000194	SK034	Cuulaseb	t	32	f	3	7	15	91000.00	S0000194-image.jpg
S0000205	SK045	Renroos	t	22	t	1	8	12	323000.00	S0000205-image.jpg
S0000209	SK049	Hecgusnum	f	88	t	3	8	15	83000.00	S0000209-image.jpg
S0000169	SK009	Uhcowej	t	14	t	2	10	14	267000.00	S0000169-image.jpg
S0000232	SK072	Amdicru	t	64	t	3	8	11	94000.00	S0000232-image.jpg
S0000185	SK025	Movwukine	f	58	f	1	7	11	30000.00	S0000185-image.jpg
S0000211	SK051	Voewisod	t	33	t	2	7	18	121000.00	S0000211-image.jpg
S0000216	SK056	Zuhicuto	t	58	f	2	5	13	278000.00	S0000216-image.jpg
S0000160	SK080	Galufef	t	23	t	3	6	12	47000.00	S0000160-image.jpg
S0000219	SK059	Nocanad	t	26	t	3	7	17	887000.00	S0000219-image.jpg
S0000204	SK044	Zibewiw	t	48	f	2	10	19	884000.00	S0000204-image.jpg
S0000207	SK047	Renroos	t	77	f	2	8	11	54000.00	S0000207-image.jpg
S0000202	SK042	Zibewiw	t	6	t	3	8	15	76000.00	S0000202-image.jpg
S0000203	SK043	Zibewiw	f	56	f	1	8	20	912000.00	S0000203-image.jpg
S0000177	SK017	Nadagra	t	16	f	1	7	19	437000.00	S0000177-image.jpg
S0000167	SK007	Udaumdog	f	82	f	2	6	16	141000.00	S0000167-image.jpg
S0000196	SK036	Akutodror	t	35	t	1	10	13	363000.00	S0000196-image.jpg
S0000159	SK079	Girnudike	t	75	f	3	5	12	9000.00	S0000159-image.jpg
S0000230	SK070	Sokahig	t	85	f	2	8	16	724000.00	S0000230-image.jpg
S0000163	SK003	Divvemdaz	t	16	t	3	8	19	529000.00	S0000163-image.jpg
S0000174	SK014	Sansegesu	t	40	t	2	8	14	42000.00	S0000174-image.jpg
S0000223	SK063	Ihkeadu	t	89	t	1	6	12	167000.00	S0000223-image.jpg
S0000172	SK012	Sansegesu	t	53	t	3	9	20	647000.00	S0000172-image.jpg
S0000193	SK033	Cuulaseb	t	3	t	2	8	15	123000.00	S0000193-image.jpg
S0000189	SK029	Sujoze	t	85	f	2	9	19	6000.00	S0000189-image.jpg
S0000183	SK023	Difarbaw	t	71	t	3	9	11	90000.00	S0000183-image.jpg
S0000187	SK027	Sujoze	t	5	t	1	7	19	31000.00	S0000187-image.jpg
S0000222	SK062	Fapapo	t	19	f	1	6	18	562000.00	S0000222-image.jpg
S0000180	SK020	Saolina	t	76	f	3	5	13	57000.00	S0000180-image.jpg
S0000214	SK054	Zuhicuto	t	63	t	3	8	15	433000.00	S0000214-image.jpg
S0000236	SK076	Kahmuso	t	78	t	3	5	12	564000.00	S0000236-image.jpg
S0000199	SK039	Ovozge	t	89	t	1	6	11	76000.00	S0000199-image.jpg
S0000184	SK024	Movwukine	t	1	t	2	5	15	20000.00	S0000184-image.jpg
S0000164	SK004	Divvemdaz	t	56	t	2	9	19	13000.00	S0000164-image.jpg
S0000231	SK071	Sokahig	t	26	f	2	10	17	769000.00	S0000231-image.jpg
S0000192	SK032	Tuavted	t	87	t	3	8	11	570000.00	S0000192-image.jpg
S0000218	SK058	Nocanad	t	74	t	3	5	19	239000.00	S0000218-image.jpg
S0000182	SK022	Difarbaw	t	12	t	2	9	15	74000.00	S0000182-image.jpg
S0000235	SK075	Kahmuso	t	51	t	2	8	18	48000.00	S0000235-image.jpg
S0000178	SK018	Saolina	t	65	t	3	6	11	77000.00	S0000178-image.jpg
S0000175	SK015	Nadagra	t	75	t	3	8	20	91000.00	S0000175-image.jpg
S0000186	SK026	Movwukine	t	44	f	3	10	15	76000.00	S0000186-image.jpg
S0000201	SK041	Ovozge	t	77	t	3	6	18	729000.00	S0000201-image.jpg
S0000168	SK008	Udaumdog	t	36	f	3	5	19	56000.00	S0000168-image.jpg
S0000181	SK021	Difarbaw	t	35	t	3	9	12	547000.00	S0000181-image.jpg
S0000179	SK019	Saolina	f	42	f	3	9	19	32000.00	S0000179-image.jpg
S0000246	SK006	Dalnedi	t	49	t	1	8	11	38000.00	S0000246-image.jpg
S0000263	SK023	Diktufina	f	96	t	1	6	12	90000.00	S0000263-image.jpg
S0000275	SK035	Zogjowoj	f	66	f	3	7	15	224000.00	S0000275-image.jpg
S0000295	SK055	Nivumat	t	90	t	3	6	15	84000.00	S0000295-image.jpg
S0000264	SK024	Diktufina	t	62	t	2	7	17	410000.00	S0000264-image.jpg
S0000291	SK051	Hozguna	t	92	t	2	8	13	60000.00	S0000291-image.jpg
S0000299	SK059	Gimmaniko	f	0	t	3	9	11	78000.00	S0000299-image.jpg
S0000268	SK028	Zamafhiz	t	52	t	1	10	19	70000.00	S0000268-image.jpg
S0000289	SK049	Hozguna	t	8	t	1	5	20	44000.00	S0000289-image.jpg
S0000296	SK056	Nivumat	t	8	f	2	6	18	137000.00	S0000296-image.jpg
S0000285	SK045	Gazebzi	t	37	f	2	7	14	473000.00	S0000285-image.jpg
S0000176	SK016	Nadagra	t	42	f	3	9	19	338000.00	S0000176-image.jpg
S0000271	SK031	Gasomo	t	70	t	1	10	15	78000.00	S0000271-image.jpg
S0000212	SK052	Voewisod	t	7	f	3	6	13	77000.00	S0000212-image.jpg
S0000272	SK032	Gasomo	t	51	t	1	10	13	69000.00	S0000272-image.jpg
S0000297	SK057	Nivumat	t	83	f	3	8	16	29000.00	S0000297-image.jpg
S0000247	SK007	Uguciis	t	83	t	1	8	20	82000.00	S0000247-image.jpg
S0000258	SK018	Ritkezil	t	21	f	2	9	12	49000.00	S0000258-image.jpg
S0000277	SK037	Zeprinbu	t	19	t	1	5	19	494000.00	S0000277-image.jpg
S0000210	SK050	Hecgusnum	t	74	t	2	6	16	77000.00	S0000210-image.jpg
S0000259	SK019	Vicgonzop	t	64	t	2	5	11	48000.00	S0000259-image.jpg
S0000248	SK008	Uguciis	t	28	f	1	9	15	28000.00	S0000248-image.jpg
S0000045	SK045	Ewelimleh	t	20	f	3	5	18	217000.00	S0000045-image.jpg
S0000260	SK020	Vicgonzop	t	66	f	2	10	20	888000.00	S0000260-image.jpg
S0000261	SK021	Vicgonzop	t	77	f	1	9	18	748000.00	S0000261-image.jpg
S0000267	SK027	Tukeci	t	12	f	3	7	13	12000.00	S0000267-image.jpg
S0000280	SK040	Cudsale	t	73	t	3	7	16	357000.00	S0000280-image.jpg
S0000283	SK043	Gazebzi	t	62	t	2	8	11	933000.00	S0000283-image.jpg
S0000239	SK079	Publotweb	f	28	f	2	8	18	8000.00	S0000239-image.jpg
S0000270	SK030	Zamafhiz	t	24	f	1	7	19	77000.00	S0000270-image.jpg
S0000238	SK078	Publotweb	t	4	t	2	5	20	74000.00	S0000238-image.jpg
S0000276	SK036	Zogjowoj	t	87	f	3	6	15	16000.00	S0000276-image.jpg
S0000237	SK077	Kahmuso	t	47	t	2	8	13	920000.00	S0000237-image.jpg
S0000249	SK009	Uguciis	t	67	f	2	10	16	68000.00	S0000249-image.jpg
S0000086	SK006	Ulmezle	t	49	f	3	6	19	53000.00	S0000086-image.jpg
S0000051	SK051	Azevimut	t	69	f	2	6	17	261000.00	S0000051-image.jpg
S0000252	SK012	Efuwumubi	t	1	f	2	5	12	91000.00	S0000252-image.jpg
S0000294	SK054	Celseuh	t	43	f	1	7	20	32000.00	S0000294-image.jpg
S0000041	SK041	Lobasil	f	7	f	3	10	18	544000.00	S0000041-image.jpg
S0000273	SK033	Gasomo	t	28	t	2	8	12	388000.00	S0000273-image.jpg
S0000288	SK048	Jazodoh	t	3	f	1	6	17	161000.00	S0000288-image.jpg
S0000242	SK002	Kalgenit	t	0	f	1	5	19	63000.00	S0000242-image.jpg
S0000243	SK003	Kalgenit	t	67	f	1	7	20	32000.00	S0000243-image.jpg
S0000284	SK044	Gazebzi	t	9	f	2	5	11	340000.00	S0000284-image.jpg
S0000241	SK001	Kalgenit	t	75	t	2	7	12	36000.00	S0000241-image.jpg
S0000265	SK025	Tukeci	t	73	t	2	6	14	54000.00	S0000265-image.jpg
S0000293	SK053	Celseuh	f	49	f	2	6	13	27000.00	S0000293-image.jpg
S0000240	SK080	Publotweb	t	28	f	1	6	18	76000.00	S0000240-image.jpg
S0000253	SK013	Ijiozazag	t	93	t	2	9	13	82000.00	S0000253-image.jpg
S0000100	SK020	Lilhava	t	1	t	3	5	12	632000.00	S0000100-image.jpg
S0000286	SK046	Jazodoh	t	32	t	2	6	19	268000.00	S0000286-image.jpg
S0000227	SK067	Masvebje	f	50	t	1	8	16	83000.00	S0000227-image.jpg
S0000152	SK072	Nefpezi	t	65	f	1	6	13	31000.00	S0000152-image.jpg
S0000281	SK041	Cudsale	f	14	t	2	7	11	908000.00	S0000281-image.jpg
S0000244	SK004	Dalnedi	t	17	t	2	5	19	11000.00	S0000244-image.jpg
S0000266	SK026	Tukeci	t	34	f	3	9	11	19000.00	S0000266-image.jpg
S0000131	SK051	Racubuus	f	81	f	3	7	20	129000.00	S0000131-image.jpg
S0000254	SK014	Ijiozazag	t	20	t	2	9	11	411000.00	S0000254-image.jpg
S0000250	SK010	Efuwumubi	t	76	t	3	7	17	52000.00	S0000250-image.jpg
S0000279	SK039	Zeprinbu	t	76	f	2	7	17	682000.00	S0000279-image.jpg
S0000298	SK058	Gimmaniko	t	24	t	2	8	19	324000.00	S0000298-image.jpg
S0000278	SK038	Zeprinbu	t	2	f	1	10	18	22000.00	S0000278-image.jpg
S0000274	SK034	Zogjowoj	t	12	t	3	6	14	738000.00	S0000274-image.jpg
S0000262	SK022	Diktufina	t	27	t	1	10	13	26000.00	S0000262-image.jpg
S0000300	SK060	Gimmaniko	t	37	t	2	8	16	126000.00	S0000300-image.jpg
S0000256	SK016	Ritkezil	t	24	t	2	7	17	10000.00	S0000256-image.jpg
S0000282	SK042	Cudsale	t	10	t	3	7	18	627000.00	S0000282-image.jpg
S0000257	SK017	Ritkezil	f	68	f	3	5	12	690000.00	S0000257-image.jpg
S0000287	SK047	Jazodoh	f	39	f	3	8	13	90000.00	S0000287-image.jpg
S0000245	SK005	Dalnedi	f	17	t	3	7	12	887000.00	S0000245-image.jpg
S0000292	SK052	Celseuh	t	11	t	3	10	12	8000.00	S0000292-image.jpg
S0000251	SK011	Efuwumubi	f	4	f	3	6	16	612000.00	S0000251-image.jpg
S0000269	SK029	Zamafhiz	f	97	f	2	7	15	346000.00	S0000269-image.jpg
S0000290	SK050	Hozguna	t	46	t	1	6	14	84000.00	S0000290-image.jpg
S0000017	SK017	Ezjeza	f	88	f	1	9	11	723000.00	S0000017-image.jpg
S0000255	SK015	Ijiozazag	t	29	t	2	9	11	269000.00	S0000255-image.jpg
\.


--
-- Data for Name: sub_kategori; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY sub_kategori (kode, kode_kategori, nama) FROM stdin;
SK001	K01	Atasan
SK002	K01	Celana
SK003	K01	Dress
SK004	K01	Outerwear
SK005	K01	Setelan
SK006	K01	Batik Wanita
SK007	K01	Pakaian Dalam Wanita
SK008	K01	Tas
SK009	K01	Sepatu
SK010	K01	Jam Tangan
SK011	K01	Perhiasan
SK012	K01	Aksesoris
SK013	K01	Aksesoris Rambut
SK014	K01	Perlengkapan Couple
SK015	K01	Baju Tidur
SK016	K01	Perlengkapan Jahit
SK017	K02	Jam Tangan
SK018	K02	Baju Tidur
SK019	K02	Pakaian Dalam Pria
SK020	K02	Outerwear
SK021	K02	Sepatu
SK022	K02	Tas
SK023	K02	Perhiasan Fashion
SK024	K02	Aksesoris
SK025	K02	Celana
SK026	K02	Batik Pria
SK027	K02	Atasan
SK028	K03	Outerwear
SK029	K03	Setelan Muslim
SK030	K03	Dress
SK031	K03	Scarf
SK032	K03	Baju Muslim Anak
SK033	K03	Atasan
SK034	K03	Aksesoris Jilbab
SK035	K03	Bawahan
SK036	K03	Perlengkapan Ibadah
SK037	K04	Perhiasan Anak
SK038	K04	Sepatu Anak Perempuan
SK039	K04	Aksesoris Rambut Anak
SK040	K04	Aksesoris Anak
SK041	K04	Tas Anak
SK042	K04	Sepatu Anak Laki
SK043	K04	laki
SK044	K04	Pakaian Anak Perempuan
SK045	K05	Pakaian Anak Laki
SK046	K05	Laki
SK047	K05	Kosmetik
SK048	K05	Perawatan Wajah
SK049	K05	Perawatan Tangan, Kaki dan Kuku
SK050	K05	Perawatan Rambut
SK051	K05	Perawatan Mata
SK052	K05	Styling Rambut
SK053	K05	Peralatan Make Up
SK054	K06	Grooming
SK055	K06	Mandi & Perawatan Tubuh
SK056	K06	Telinga
SK057	K06	Kesehatan Wanita
SK058	K06	Obat & Alat Kesehatan
SK059	K06	Health Products
SK060	K06	Kesehatan Gigi & Mulut
SK061	K06	Diet & Vitamin
SK062	K06	Kesehatan Mata
SK063	K07	Perlengkapan Medis
SK064	K08	Kesehatan Lainnya
SK065	K09	Aksesoris Bayi
SK066	K10	Kamar Tidur
SK067	K11	Handphone
SK068	K12	Laptop
SK069	K13	Komputer
SK070	K14	TV
SK071	K15	Kamera
SK072	K16	Aksesoris Mobil
SK073	K17	Basket
SK074	K18	Musik
SK075	K19	Peralatan Dapur
SK076	K20	Alat Tulis
SK077	K21	Boneka
SK078	K21	Figure
SK079	K22	Makanan
SK080	K22	Minuman
\.


--
-- Data for Name: toko; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY toko (nama, deskripsi, slogan, lokasi, email_penjual) FROM stdin;
Netfugab	Jaminan 100%, beli sekarang dan dapatkan keuntungan maksimal, tanpa basa basi	Siapapun siap tidak menyerah	800 Ridgewood Drive?	evkosa@seh.net
Newbimmig	Dapat direfund, gapunya? gakeren lo!, fast respond!	Kita bisa tidak menyerah	Bardstown, KY 40004	ugsava@ubruffal.co.uk
Ongebzu	Bisa digunakan kapanpun, beli sekarang dan dapatkan keuntungan maksimal, tanpa tipu tipu	Siapapun siap benar	8218 Court St.?	wed@ihienucus.com
Onuegbiw	Bisa digunakan kapanpun, beli sekarang dan dapatkan keuntungan maksimal, tanpa tipu tipu	Anda siap menerima segalanya	Hoffman Estates, IL 60169	rofik@uz.edu
Laetpem	Jaminan 100%, beli sekarang dan dapatkan keuntungan maksimal, dijamin lulus 3 tahun	Kita siap menerima segalanya	665 Kingston Dr.?	nanotat@huthu.co.uk
Ezjeza	Bisa digunakan kapanpun, anda harus jadi prioritas!, juga ngedate bareng jihan	Semua bisa benar sukses	Glendora, CA 91740	oh@nip.org
Tonone	Dapat ditransfer saldo, anda harus jadi prioritas!, juga ngedate bareng jihan	Kita dapat tidak menyerah	895 Pumpkin Hill Dr.?	migtunor@gu.edu
Puseza	Dapat ditransfer saldo, gapunya? gakeren lo!, dijamin lulus 3 tahun	Kita akan jadi jutawan	Little Falls, NJ 07424	zuzro@finapore.org
Uciwguc	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, tanpa tipu tipu	Semua akan tidak menyerah	8114 Roberts Street?	niltafli@jeewedi.io
Zesuzid	Jaminan 100%, gapunya? gakeren lo!, tanpa tipu tipu	Anda ingin tidak menyerah	Encino, CA 91316	egven@di.net
Favofsi	Dapat direfund, gapunya? gakeren lo!, tanpa basa basi	Seluruhnya bisa tidak menyerah	819 Smoky Hollow Lane?	ikwuwne@kap.co.uk
Ivsatdof	Jaminan 100%, anda harus jadi prioritas!, tanpa basa basi	Kita akan menerima segalanya	Woodstock, GA 30188	pid@ke.io
Kerehu	Jaminan 100%, beli sekarang dan dapatkan keuntungan maksimal, dijamin lulus 3 tahun	Siapapun siap tidak menyerah	37 S. Brewery Dr.?	topa@geztik.org
Lobasil	Bisa digunakan kapanpun, anda harus jadi prioritas!, tanpa tipu tipu	Anda akan dapat barang bagus	Glendale Heights, IL 60139	liev@ravwu.co.uk
Ewelimleh	Dapat ditransfer saldo, anda harus jadi prioritas!, dijamin lulus 3 tahun	Kita dapat menerima segalanya	9698 Cedar Swamp Street?	fi@idarof.gov
Nisgasod	Bisa digunakan kapanpun, anda harus jadi prioritas!, juga ngedate bareng jihan	Siapapun akan jadi jutawan	Saint Louis, MO 63109	he@kodizi.co.uk
Azevimut	Dapat ditransfer saldo, anda harus jadi prioritas!, tanpa basa basi	Semua ingin menerima segalanya	117 Brookside Ave.?	caoko@jufonima.org
Weezed	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, juga ngedate bareng jihan	Semua akan benar sukses	West Lafayette, IN 47906	fiuco@po.gov
Zonsawne	Dapat ditransfer saldo, gapunya? gakeren lo!, tanpa tipu tipu	Seluruhnya dapat benar sukses	44 N. Branch Lane?	pijejaze@bop.com
Zafjomvo	Dapat ditransfer saldo, ayo jangan sampai kehabisan!, juga ngedate bareng jihan	Seluruhnya ingin menerima segalanya	Glen Allen, VA 23059	ces@fefeit.edu
Mahzegug	Dapat direfund, anda harus jadi prioritas!, juga ngedate bareng jihan	Seluruhnya siap benar sukses	152 El Dorado Court?	nocmafi@judda.net
Binjesra	Bisa digunakan kapanpun, gapunya? gakeren lo!, fast respond!	Semua siap menerima segalanya	Lacey, WA 98503	vama@zi.net
Rufarowo	Dapat direfund, ayo jangan sampai kehabisan!, original asli!	Siapapun ingin tidak menyerah	491 N. College Court?	wiofu@ruafvu.gov
Umuduuna	Dapat ditransfer saldo, ayo jangan sampai kehabisan!, fast respond!	Anda ingin benar	Middleton, WI 53562	mavvo@jeniv.org
Jeijsig	Bisa digunakan kapanpun, ayo jangan sampai kehabisan!, juga ngedate bareng jihan	Seluruhnya ingin benar	638 High Road?	fazgif@dir.co.uk
Teocesuh	Jaminan 100%, ayo jangan sampai kehabisan!, fast respond!	Kita ingin menerima segalanya	Wooster, OH 44691	adwior@kionanu.net
Rihizaf	Dapat direfund, gapunya? gakeren lo!, dijamin lulus 3 tahun	Anda siap benar sukses	8 Ketch Harbour Lane?	luzub@ep.org
Cebvolsi	Jaminan 100%, beli sekarang dan dapatkan keuntungan maksimal, tanpa basa basi	Seluruhnya bisa benar sukses	Bayonne, NJ 07002	cor@jawip.co.uk
Ulmezle	Bisa digunakan kapanpun, ayo jangan sampai kehabisan!, dijamin lulus 3 tahun	Anda dapat menerima segalanya	3 Paris Hill Ave.?	lopvu@zudpaki.co.uk
Leobizek	Dapat ditransfer saldo, beli sekarang dan dapatkan keuntungan maksimal, original asli!	Seluruhnya akan jadi jutawan	Fall River, MA 02720	po@cahgamuji.io
Nekusuj	Jaminan 100%, ayo jangan sampai kehabisan!, dijamin lulus 3 tahun	Seluruhnya ingin tidak menyerah	30 Race Rd.?	facehfe@pidmu.com
Tesozoun	Bisa digunakan kapanpun, beli sekarang dan dapatkan keuntungan maksimal, tanpa tipu tipu	Siapapun ingin benar	Commack, NY 11725	mek@funsako.edu
Zuzreku	Jaminan 100%, gapunya? gakeren lo!, fast respond!	Siapapun bisa menerima segalanya	47 Cedar Ave.?	buahe@ismulgam.org
Lilhava	Bisa digunakan kapanpun, gapunya? gakeren lo!, fast respond!	Semua ingin dapat barang bagus	Brick, NJ 08723	ovzedi@azpapvuc.net
Zovkumer	Jaminan 100%, ayo jangan sampai kehabisan!, original asli!	Anda bisa benar	19 Surrey St.?	ci@hom.com
Vulkewmeg	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, juga ngedate bareng jihan	Seluruhnya siap benar	Findlay, OH 45840	dupbajov@lidhugmal.io
Wanuwa	Jaminan 100%, anda harus jadi prioritas!, juga ngedate bareng jihan	Semua ingin tidak menyerah	9963 Wayne St.?	how@rizaur.edu
Ekviamu	Bisa digunakan kapanpun, ayo jangan sampai kehabisan!, tanpa basa basi	Kita bisa dapat barang bagus	Jamestown, NY 14701	efuepiec@ow.gov
Goopifeb	Dapat direfund, gapunya? gakeren lo!, juga ngedate bareng jihan	Seluruhnya ingin benar sukses	8516 Snake Hill Street?	azakopibo@fuew.gov
Sekgizveb	Dapat ditransfer saldo, ayo jangan sampai kehabisan!, original asli!	Siapapun ingin benar	Point Pleasant Beach, NJ 08742	he@huow.co.uk
Halaga	Dapat ditransfer saldo, ayo jangan sampai kehabisan!, tanpa tipu tipu	Anda ingin benar	78 Parker Ave.?	fi@wadbiwlo.gov
Nadcezpip	Dapat ditransfer saldo, beli sekarang dan dapatkan keuntungan maksimal, tanpa basa basi	Anda dapat tidak menyerah	Natick, MA 01760	pisopu@fomabako.co.uk
Gegufa	Jaminan 100%, anda harus jadi prioritas!, tanpa tipu tipu	Siapapun dapat dapat barang bagus	984 Elm Ave.?	ut@movpuwom.net
Racubuus	Dapat direfund, ayo jangan sampai kehabisan!, tanpa tipu tipu	Kita ingin jadi jutawan	Port Charlotte, FL 33952	detcogu@war.edu
Ciemica	Dapat direfund, ayo jangan sampai kehabisan!, juga ngedate bareng jihan	Semua bisa benar	9 Theatre Lane?	uzpude@fisojelev.io
Denhosjil	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, fast respond!	Siapapun bisa menerima segalanya	Buffalo Grove, IL 60089	uwbew@wircihhat.gov
Icjakop	Bisa digunakan kapanpun, beli sekarang dan dapatkan keuntungan maksimal, tanpa tipu tipu	Seluruhnya siap dapat barang bagus	493 Bishop Street?	bebuh@sot.gov
Jucujra	Dapat ditransfer saldo, beli sekarang dan dapatkan keuntungan maksimal, juga ngedate bareng jihan	Anda siap dapat barang bagus	Corona, NY 11368	zab@eje.net
Tohimi	Dapat direfund, ayo jangan sampai kehabisan!, dijamin lulus 3 tahun	Kita bisa benar sukses	10 W. Windfall Ave.?	adoujkum@gavro.net
Podpijzaz	Dapat ditransfer saldo, gapunya? gakeren lo!, juga ngedate bareng jihan	Anda ingin benar	Port Jefferson Station, NY 11776	jezrubki@masna.co.uk
Nefpezi	Bisa digunakan kapanpun, ayo jangan sampai kehabisan!, dijamin lulus 3 tahun	Seluruhnya bisa tidak menyerah	18 Pacific Circle?	gap@wemihir.org
Fotavsig	Dapat direfund, ayo jangan sampai kehabisan!, tanpa basa basi	Kita ingin dapat barang bagus	Yonkers, NY 10701	pinku@kerro.edu
Girnudike	Bisa digunakan kapanpun, ayo jangan sampai kehabisan!, tanpa basa basi	Kita bisa benar	24 Devonshire Ave.?	renicul@pazuse.net
Galufef	Bisa digunakan kapanpun, anda harus jadi prioritas!, dijamin lulus 3 tahun	Semua dapat benar	Howell, NJ 07731	rigic@lob.net
Divvemdaz	Bisa digunakan kapanpun, ayo jangan sampai kehabisan!, tanpa tipu tipu	Seluruhnya ingin dapat barang bagus	7754 Chapel Street?	susgijic@at.org
Udaumdog	Jaminan 100%, anda harus jadi prioritas!, juga ngedate bareng jihan	Semua akan menerima segalanya	Iowa City, IA 52240	cirif@eciipi.edu
Uhcowej	Bisa digunakan kapanpun, ayo jangan sampai kehabisan!, tanpa basa basi	Anda dapat jadi jutawan	9801 Andover Street?	ko@fuvlujom.edu
Sansegesu	Dapat direfund, ayo jangan sampai kehabisan!, tanpa basa basi	Siapapun bisa benar	Muskogee, OK 74403	gukeroc@juihade.co.uk
Nadagra	Dapat direfund, gapunya? gakeren lo!, original asli!	Seluruhnya siap tidak menyerah	39 Carpenter St.?	copzocnid@lu.gov
Saolina	Bisa digunakan kapanpun, ayo jangan sampai kehabisan!, tanpa basa basi	Seluruhnya siap jadi jutawan	Gloucester, MA 01930	dafhol@asawike.org
Difarbaw	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, original asli!	Semua siap benar	8006 Randall Mill St.?	jazuggo@zeb.co.uk
Movwukine	Bisa digunakan kapanpun, ayo jangan sampai kehabisan!, juga ngedate bareng jihan	Siapapun akan benar sukses	Lombard, IL 60148	ovhoco@pe.org
Sujoze	Jaminan 100%, beli sekarang dan dapatkan keuntungan maksimal, tanpa tipu tipu	Seluruhnya siap jadi jutawan	128 Durham St.?	baftoj@li.gov
Tuavted	Bisa digunakan kapanpun, beli sekarang dan dapatkan keuntungan maksimal, juga ngedate bareng jihan	Semua bisa tidak menyerah	Port Saint Lucie, FL 34952	zobeh@les.gov
Cuulaseb	Dapat direfund, ayo jangan sampai kehabisan!, dijamin lulus 3 tahun	Semua bisa menerima segalanya	7755 South Deerfield Street?	leztigjod@rehenas.edu
Akutodror	Bisa digunakan kapanpun, beli sekarang dan dapatkan keuntungan maksimal, juga ngedate bareng jihan	Siapapun dapat benar	Panama City, FL 32404	wedujiw@guh.edu
Ovozge	Jaminan 100%, anda harus jadi prioritas!, tanpa tipu tipu	Anda siap benar	8508 East George Dr.?	lalenir@tajuwne.net
Zibewiw	Jaminan 100%, anda harus jadi prioritas!, tanpa basa basi	Kita akan dapat barang bagus	Wayne, NJ 07470	lidkoc@ze.gov
Renroos	Bisa digunakan kapanpun, gapunya? gakeren lo!, fast respond!	Anda bisa tidak menyerah	8923 Hall Lane?	ankez@dusfug.org
Hecgusnum	Dapat ditransfer saldo, gapunya? gakeren lo!, tanpa tipu tipu	Kita dapat jadi jutawan	Champlin, MN 55316	fajigag@igwuz.io
Voewisod	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, tanpa tipu tipu	Semua siap benar	20 Golf Dr.?	bopoc@meksiw.net
Zuhicuto	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, juga ngedate bareng jihan	Kita dapat dapat barang bagus	Crofton, MD 21114	kuleboj@ozwecwav.co.uk
Nocanad	Dapat ditransfer saldo, gapunya? gakeren lo!, tanpa basa basi	Kita bisa dapat barang bagus	62 Shipley St.?	zenu@morwulok.edu
Fapapo	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, fast respond!	Anda bisa tidak menyerah	Chandler, AZ 85224	jak@kevkupdof.co.uk
Ihkeadu	Dapat direfund, anda harus jadi prioritas!, tanpa basa basi	Seluruhnya dapat menerima segalanya	7788 Pleasant Rd.?	bugaw@or.edu
Masvebje	Dapat direfund, gapunya? gakeren lo!, original asli!	Siapapun dapat dapat barang bagus	Elkton, MD 21921	cepwasfa@habik.co.uk
Sokahig	Jaminan 100%, gapunya? gakeren lo!, dijamin lulus 3 tahun	Siapapun akan benar	830 Beech Street?	viz@nevhu.net
Amdicru	Bisa digunakan kapanpun, gapunya? gakeren lo!, juga ngedate bareng jihan	Siapapun siap jadi jutawan	Georgetown, SC 29440	mus@uja.gov
Kahmuso	Jaminan 100%, gapunya? gakeren lo!, tanpa tipu tipu	Anda siap jadi jutawan	96 Depot Lane?	milcetwo@zutenjo.edu
Publotweb	Bisa digunakan kapanpun, anda harus jadi prioritas!, tanpa tipu tipu	Anda dapat menerima segalanya	Catonsville, MD 21228	dis@pavejus.edu
Kalgenit	Jaminan 100%, anda harus jadi prioritas!, juga ngedate bareng jihan	Semua akan dapat barang bagus	8099 Rock Maple Ave.?	ihi@gabtanto.org
Dalnedi	Jaminan 100%, ayo jangan sampai kehabisan!, tanpa basa basi	Anda dapat dapat barang bagus	Barrington, IL 60010	ruhtad@vavihza.net
Uguciis	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, juga ngedate bareng jihan	Anda siap benar sukses	68 Baker Rd.?	coje@wuf.io
Efuwumubi	Jaminan 100%, gapunya? gakeren lo!, juga ngedate bareng jihan	Kita dapat dapat barang bagus	Carlisle, PA 17013	pedfur@ze.co.uk
Ijiozazag	Dapat ditransfer saldo, anda harus jadi prioritas!, tanpa tipu tipu	Anda ingin menerima segalanya	97 Henry Dr.?	jedmew@cihuwde.co.uk
Ritkezil	Dapat direfund, beli sekarang dan dapatkan keuntungan maksimal, fast respond!	Semua akan benar sukses	Saint Joseph, MI 49085	pacajti@uj.edu
Vicgonzop	Dapat ditransfer saldo, ayo jangan sampai kehabisan!, fast respond!	Anda ingin benar	5 S. Hilldale Ave.?	ohijazren@izfu.net
Diktufina	Dapat ditransfer saldo, anda harus jadi prioritas!, juga ngedate bareng jihan	Seluruhnya siap benar	Terre Haute, IN 47802	wihut@judhindiz.net
Tukeci	Bisa digunakan kapanpun, beli sekarang dan dapatkan keuntungan maksimal, tanpa basa basi	Seluruhnya bisa jadi jutawan	9811 Arcadia Street?	zoju@zururub.net
Zamafhiz	Bisa digunakan kapanpun, gapunya? gakeren lo!, original asli!	Anda bisa menerima segalanya	Dothan, AL 36301	akehihi@zoji.gov
Gasomo	Bisa digunakan kapanpun, anda harus jadi prioritas!, original asli!	Kita siap tidak menyerah	6 Wild Horse Lane?	vezi@jocow.co.uk
Zogjowoj	Bisa digunakan kapanpun, beli sekarang dan dapatkan keuntungan maksimal, tanpa tipu tipu	Seluruhnya ingin menerima segalanya	Maplewood, NJ 07040	haul@roama.net
Zeprinbu	Dapat ditransfer saldo, anda harus jadi prioritas!, dijamin lulus 3 tahun	Kita dapat benar sukses	9899 Goldfield Drive?	mehrulihu@riribkuk.net
Cudsale	Jaminan 100%, beli sekarang dan dapatkan keuntungan maksimal, tanpa basa basi	Semua bisa jadi jutawan	West Palm Beach, FL 33404	tagu@ajaehagis.com
Gazebzi	Dapat direfund, gapunya? gakeren lo!, juga ngedate bareng jihan	Seluruhnya siap menerima segalanya	679 Evergreen Street?	bu@diadu.gov
Jazodoh	Jaminan 100%, anda harus jadi prioritas!, tanpa tipu tipu	Kita akan jadi jutawan	Chapel Hill, NC 27516	do@ki.io
Hozguna	Bisa digunakan kapanpun, anda harus jadi prioritas!, tanpa basa basi	Kita akan benar	624 Ashley Ave.?	puz@feumu.net
Celseuh	Dapat ditransfer saldo, gapunya? gakeren lo!, tanpa tipu tipu	Siapapun dapat benar sukses	Rockville Centre, NY 11570	deem@tuvir.co.uk
Nivumat	Dapat ditransfer saldo, anda harus jadi prioritas!, dijamin lulus 3 tahun	Kita ingin benar sukses	30 Shirley St.?	ta@elbuple.net
Gimmaniko	Bisa digunakan kapanpun, gapunya? gakeren lo!, tanpa tipu tipu	Siapapun bisa benar sukses	Petersburg, VA 23803	ac@nettiguj.edu
\.


--
-- Data for Name: toko_jasa_kirim; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY toko_jasa_kirim (nama_toko, jasa_kirim) FROM stdin;
Netfugab	JNE YES
Netfugab	JNE OKE
Newbimmig	JNE OKE
Newbimmig	POS PAKET BIASA
Ongebzu	TIKI REGULER
Ongebzu	WAHANA
Onuegbiw	POS PAKET BIASA
Onuegbiw	PAHAL
Laetpem	POS PAKET KILAT
Laetpem	JNE REGULER
Ezjeza	WAHANA
Ezjeza	JNE OKE
Tonone	J&T EXPRESS
Tonone	POS PAKET BIASA
Puseza	PAHAL
Puseza	WAHANA
Uciwguc	LION PARCEL
Uciwguc	PAHAL
Zesuzid	JNE REGULER
Zesuzid	LION PARCEL
Favofsi	JNE YES
Favofsi	JNE OKE
Ivsatdof	JNE OKE
Ivsatdof	POS PAKET BIASA
Kerehu	TIKI REGULER
Kerehu	WAHANA
Lobasil	POS PAKET BIASA
Lobasil	PAHAL
Ewelimleh	POS PAKET KILAT
Ewelimleh	JNE REGULER
Nisgasod	WAHANA
Nisgasod	JNE OKE
Azevimut	J&T EXPRESS
Azevimut	POS PAKET BIASA
Weezed	PAHAL
Weezed	WAHANA
Zonsawne	LION PARCEL
Zonsawne	PAHAL
Zafjomvo	JNE REGULER
Zafjomvo	PAHAL
Mahzegug	JNE YES
Mahzegug	JNE OKE
Binjesra	JNE OKE
Binjesra	POS PAKET BIASA
Rufarowo	TIKI REGULER
Rufarowo	WAHANA
Umuduuna	POS PAKET BIASA
Umuduuna	PAHAL
Jeijsig	POS PAKET KILAT
Jeijsig	JNE REGULER
Teocesuh	WAHANA
Teocesuh	JNE OKE
Rihizaf	J&T EXPRESS
Rihizaf	POS PAKET BIASA
Cebvolsi	PAHAL
Cebvolsi	WAHANA
Ulmezle	LION PARCEL
Ulmezle	PAHAL
Leobizek	JNE REGULER
Leobizek	PAHAL
Nekusuj	JNE YES
Nekusuj	JNE OKE
Tesozoun	JNE OKE
Tesozoun	POS PAKET BIASA
Zuzreku	TIKI REGULER
Zuzreku	WAHANA
Lilhava	POS PAKET BIASA
Lilhava	PAHAL
Zovkumer	POS PAKET KILAT
Zovkumer	JNE REGULER
Vulkewmeg	WAHANA
Vulkewmeg	JNE OKE
Wanuwa	J&T EXPRESS
Wanuwa	POS PAKET BIASA
Ekviamu	PAHAL
Ekviamu	WAHANA
Goopifeb	LION PARCEL
Goopifeb	PAHAL
Sekgizveb	JNE REGULER
Sekgizveb	JNE YES
Halaga	JNE YES
Halaga	JNE OKE
Nadcezpip	JNE OKE
Nadcezpip	POS PAKET BIASA
Gegufa	TIKI REGULER
Gegufa	WAHANA
Racubuus	POS PAKET BIASA
Racubuus	PAHAL
Ciemica	POS PAKET KILAT
Ciemica	JNE REGULER
Denhosjil	WAHANA
Denhosjil	JNE OKE
Icjakop	J&T EXPRESS
Icjakop	POS PAKET BIASA
Jucujra	PAHAL
Jucujra	WAHANA
Tohimi	LION PARCEL
Tohimi	PAHAL
Podpijzaz	JNE YES
Podpijzaz	JNE REGULER
Nefpezi	JNE YES
Nefpezi	JNE OKE
Fotavsig	JNE OKE
Fotavsig	POS PAKET BIASA
Girnudike	TIKI REGULER
Girnudike	WAHANA
Galufef	POS PAKET BIASA
Galufef	PAHAL
Divvemdaz	POS PAKET KILAT
Divvemdaz	JNE REGULER
Udaumdog	WAHANA
Udaumdog	JNE OKE
Uhcowej	J&T EXPRESS
Uhcowej	POS PAKET BIASA
Sansegesu	PAHAL
Sansegesu	WAHANA
Nadagra	LION PARCEL
Nadagra	PAHAL
Saolina	JNE YES
Saolina	JNE REGULER
Difarbaw	JNE YES
Difarbaw	JNE OKE
Movwukine	JNE OKE
Movwukine	POS PAKET BIASA
Sujoze	TIKI REGULER
Sujoze	WAHANA
Tuavted	POS PAKET BIASA
Tuavted	PAHAL
Cuulaseb	POS PAKET KILAT
Cuulaseb	JNE REGULER
Akutodror	WAHANA
Akutodror	JNE OKE
Ovozge	J&T EXPRESS
Ovozge	POS PAKET BIASA
Zibewiw	PAHAL
Zibewiw	WAHANA
Renroos	LION PARCEL
Renroos	PAHAL
Hecgusnum	JNE YES
Hecgusnum	JNE REGULER
Voewisod	JNE YES
Voewisod	JNE OKE
Zuhicuto	JNE OKE
Zuhicuto	POS PAKET BIASA
Nocanad	TIKI REGULER
Nocanad	WAHANA
Fapapo	POS PAKET BIASA
Fapapo	PAHAL
Ihkeadu	POS PAKET KILAT
Ihkeadu	JNE REGULER
Masvebje	WAHANA
Masvebje	JNE OKE
Sokahig	J&T EXPRESS
Sokahig	POS PAKET BIASA
Amdicru	PAHAL
Amdicru	WAHANA
Kahmuso	LION PARCEL
Kahmuso	PAHAL
Publotweb	JNE YES
Publotweb	JNE REGULER
Kalgenit	JNE YES
Kalgenit	JNE OKE
Dalnedi	JNE OKE
Dalnedi	POS PAKET BIASA
Uguciis	TIKI REGULER
Uguciis	WAHANA
Efuwumubi	POS PAKET BIASA
Efuwumubi	PAHAL
Ijiozazag	POS PAKET KILAT
Ijiozazag	JNE REGULER
Ritkezil	WAHANA
Ritkezil	JNE OKE
Vicgonzop	J&T EXPRESS
Vicgonzop	POS PAKET BIASA
Diktufina	PAHAL
Diktufina	WAHANA
Tukeci	LION PARCEL
Tukeci	PAHAL
Zamafhiz	JNE YES
Zamafhiz	JNE REGULER
Gasomo	JNE YES
Gasomo	JNE OKE
Zogjowoj	JNE OKE
Zogjowoj	POS PAKET BIASA
Zeprinbu	TIKI REGULER
Zeprinbu	WAHANA
Cudsale	POS PAKET BIASA
Cudsale	PAHAL
Gazebzi	POS PAKET KILAT
Gazebzi	JNE REGULER
Jazodoh	WAHANA
Jazodoh	JNE OKE
Hozguna	J&T EXPRESS
Hozguna	POS PAKET BIASA
Celseuh	PAHAL
Celseuh	WAHANA
Nivumat	LION PARCEL
Nivumat	PAHAL
Gimmaniko	JNE YES
Gimmaniko	JNE REGULER
\.


--
-- Data for Name: transaksi_pulsa; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY transaksi_pulsa (no_invoice, tanggal, waktu_bayar, status, total_bayar, email_pembeli, nominal, nomor, kode_produk) FROM stdin;
V000000001	2016-10-03	2016-10-05 06:49:27	2	17000.00	le@lopdooc.com	10	08931254	P0000110
V000000002	2016-09-04	2016-09-06 01:57:36	2	65000.00	jude@fok.edu	60	08668432	P0000156
V000000003	2016-10-03	2016-10-05 04:17:55	1	58000.00	war@fohrahsin.net	50	08858341	P0000137
V000000004	2016-09-16	2016-09-18 17:45:53	2	85000.00	socoma@laomigez.io	80	08199696	P0000038
V000000005	2016-08-14	2016-08-16 04:16:01	1	77000.00	zup@ab.com	70	08866879	P0000169
V000000006	2016-09-21	2016-09-23 08:54:40	2	12000.00	vedowur@loowojac.io	10	08351514	P0000042
V000000007	2016-08-30	2016-09-01 06:57:57	2	95000.00	idudovje@wadmisre.co.uk	90	08419689	P0000072
V000000008	2016-10-16	2016-10-18 14:58:09	2	37000.00	il@caguri.edu	30	08871981	P0000071
V000000009	2016-08-28	2016-08-30 11:57:59	1	59000.00	jukuctig@bucolza.org	50	08545766	P0000001
V000000010	2016-08-08	2016-08-10 08:04:41	2	26000.00	cel@map.io	20	08247131	P0000085
V000000011	2016-10-03	2016-10-05 12:10:38	2	34000.00	remuw@jopuru.io	30	08747144	P0000036
V000000012	2016-08-17	2016-08-18 23:45:11	2	29000.00	teuzu@va.net	20	08439362	P0000152
V000000013	2016-09-23	2016-09-25 19:42:50	1	85000.00	tomva@kej.gov	80	08919344	P0000142
V000000014	2016-08-16	2016-08-18 20:48:46	1	26000.00	ho@jesopi.gov	20	08539994	P0000034
V000000015	2016-07-31	2016-08-02 09:44:03	1	59000.00	igupu@ogezegpod.gov	50	08497917	P0000172
V000000016	2016-09-12	2016-09-14 00:34:52	2	49000.00	ceiwosak@kucuaj.net	40	08311378	P0000083
V000000017	2016-10-03	2016-10-05 08:05:21	2	45000.00	ho@jesopi.gov	40	08127933	P0000049
V000000018	2016-07-25	2016-07-27 21:09:47	2	75000.00	kanaz@mema.org	70	08267632	P0000025
V000000019	2016-08-22	2016-08-24 20:27:26	2	19000.00	goufaut@luwab.co.uk	10	08789741	P0000193
V000000020	2016-09-20	2016-09-22 19:37:25	1	44000.00	joboav@un.io	40	08171172	P0000186
V000000021	2016-09-19	2016-09-20 23:13:22	1	43000.00	gurab@suvelelub.io	40	08865157	P0000096
V000000022	2016-10-10	2016-10-12 19:02:39	1	18000.00	ige@ocwope.net	10	08186943	P0000181
V000000023	2016-09-19	2016-09-21 20:05:15	1	58000.00	cel@map.io	50	08585233	P0000170
V000000024	2016-08-25	2016-08-27 14:26:31	2	84000.00	anowuw@sabapmi.org	80	08742239	P0000185
V000000025	2016-09-22	2016-09-24 05:43:35	2	51000.00	inu@fo.gov	50	08324624	P0000177
V000000026	2016-07-31	2016-08-02 07:28:05	1	64000.00	uduhet@pomwic.org	60	08824692	P0000029
V000000027	2016-08-05	2016-08-07 07:09:18	2	26000.00	inu@fo.gov	20	08465871	P0000034
V000000028	2016-10-27	2016-10-29 03:57:57	2	78000.00	tog@siv.org	70	08138485	P0000179
V000000029	2016-10-15	2016-10-17 12:15:01	2	77000.00	vobvi@vo.org	70	08169848	P0000046
V000000030	2016-07-29	2016-07-31 18:33:35	1	39000.00	ze@erejanek.com	30	08582779	P0000050
V000000031	2016-10-19	2016-10-21 09:03:44	2	74000.00	it@kaf.io	70	08511326	P0000119
V000000032	2016-07-24	2016-07-26 12:54:09	2	33000.00	vacawhep@kalagwo.com	30	08896729	P0000175
V000000033	2016-10-17	2016-10-19 08:06:12	2	43000.00	paweifa@gusak.com	40	08761921	P0000096
V000000034	2016-07-22	2016-07-24 20:15:20	2	17000.00	noge@wib.com	10	08251633	P0000180
V000000035	2016-09-20	2016-09-22 17:03:02	2	48000.00	hu@zusrusiw.edu	40	08497319	P0000044
V000000036	2016-08-09	2016-08-11 06:45:44	2	93000.00	powjajrak@el.io	90	08937423	P0000028
V000000037	2016-09-12	2016-09-14 00:11:33	2	58000.00	malvo@gigdelkun.com	50	08938383	P0000170
V000000038	2016-07-26	2016-07-28 21:34:31	1	26000.00	zi@mimpu.com	20	08711492	P0000009
V000000039	2016-09-22	2016-09-24 13:18:56	2	15000.00	tomva@kej.gov	10	08136312	P0000149
V000000040	2016-07-24	2016-07-26 03:39:16	1	64000.00	if@azo.gov	60	08844646	P0000105
V000000041	2016-10-26	2016-10-28 01:26:04	1	79000.00	noge@wib.com	70	08628478	P0000183
V000000042	2016-08-16	2016-08-18 07:41:06	1	34000.00	ejoha@acsudo.net	30	08133969	P0000036
V000000043	2016-08-22	2016-08-24 20:16:05	1	24000.00	jujrecwo@veofowe.com	20	08958487	P0000073
V000000044	2016-07-29	2016-07-31 21:40:50	2	49000.00	dup@ejmameh.io	40	08319216	P0000083
V000000045	2016-07-25	2016-07-27 10:50:40	1	29000.00	kehec@ebbucja.net	20	08455636	P0000159
V000000046	2016-10-13	2016-10-15 11:01:08	1	35000.00	costoja@waw.gov	30	08519292	P0000154
V000000047	2016-08-28	2016-08-30 19:54:44	2	24000.00	tol@tukon.edu	20	08257733	P0000073
V000000048	2016-08-29	2016-08-31 13:25:15	2	95000.00	hemvofvod@osulojmov.com	90	08773921	P0000072
V000000049	2016-09-03	2016-09-05 06:53:56	1	95000.00	obofac@fel.edu	90	08977541	P0000114
V000000050	2016-10-25	2016-10-27 01:50:17	1	58000.00	costoja@waw.gov	50	08768324	P0000137
V000000051	2016-09-16	2016-09-19 10:45:03	1	72000.00	vobvi@vo.org	70	08357298	P0000074
V000000052	2016-09-25	2016-09-27 19:13:34	1	76000.00	ez@upzi.com	70	08196976	P0000023
V000000053	2016-07-27	2016-07-29 10:58:34	2	25000.00	kub@etdav.org	20	08682481	P0000031
V000000054	2016-08-20	2016-08-22 01:11:18	2	41000.00	no@ugu.com	40	08443197	P0000065
V000000055	2016-10-26	2016-10-28 09:54:55	1	82000.00	ige@ocwope.net	80	08461161	P0000030
V000000056	2016-10-02	2016-10-04 13:30:20	2	89000.00	ikruj@sapanti.com	80	08325569	P0000098
V000000057	2016-08-17	2016-08-19 04:55:25	1	49000.00	udne@donazmuv.io	40	08774163	P0000083
V000000058	2016-08-07	2016-08-09 08:12:47	2	91000.00	jiwuol@vi.io	90	08676737	P0000022
V000000059	2016-09-12	2016-09-14 21:28:35	2	23000.00	liru@op.co.uk	20	08793563	P0000174
V000000060	2016-09-02	2016-09-04 17:25:55	1	72000.00	kibiri@koftere.io	70	08369353	P0000145
V000000061	2016-10-11	2016-10-13 21:00:47	2	26000.00	tog@siv.org	20	08291721	P0000034
V000000062	2016-09-14	2016-09-16 03:04:08	2	65000.00	socoma@laomigez.io	60	08175657	P0000068
V000000063	2016-08-31	2016-09-02 20:33:56	2	88000.00	zu@guji.gov	80	08396967	P0000032
V000000064	2016-07-21	2016-07-23 08:31:41	1	25000.00	ra@midgabe.edu	20	08898554	P0000031
V000000065	2016-08-08	2016-08-10 09:10:56	2	56000.00	uphagfa@reluba.io	50	08342844	P0000188
V000000066	2016-09-16	2016-09-18 15:18:20	2	67000.00	jawantu@siziin.io	60	08137596	P0000113
V000000067	2016-08-28	2016-08-30 16:49:29	1	33000.00	meiru@dofigoj.org	30	08146869	P0000173
V000000068	2016-09-09	2016-09-11 06:44:42	2	82000.00	tol@tukon.edu	80	08911833	P0000163
V000000069	2016-10-17	2016-10-19 18:57:19	2	76000.00	ijages@lomulel.io	70	08127164	P0000057
V000000070	2016-09-01	2016-09-02 23:44:09	2	15000.00	kehu@pom.edu	10	08149818	P0000058
V000000071	2016-08-28	2016-08-30 06:39:51	1	31000.00	kibiri@koftere.io	30	08333312	P0000190
V000000072	2016-10-19	2016-10-21 15:42:56	2	91000.00	is@fe.com	90	08797595	P0000022
V000000073	2016-08-15	2016-08-17 07:14:48	1	64000.00	ta@roc.io	60	08732421	P0000143
V000000074	2016-09-21	2016-09-23 15:25:37	1	77000.00	mewugek@sojkatta.io	70	08388578	P0000126
V000000075	2016-10-04	2016-10-06 11:51:34	1	99000.00	deretjaw@gek.edu	90	08321646	P0000076
V000000076	2016-09-23	2016-09-25 13:31:10	1	91000.00	eptew@os.org	90	08988499	P0000022
V000000077	2016-09-05	2016-09-07 16:45:03	1	84000.00	adaerop@ono.net	80	08918993	P0000185
V000000078	2016-09-10	2016-09-12 11:27:52	1	36000.00	ivnu@viset.edu	30	08137592	P0000016
V000000079	2016-09-16	2016-09-18 00:35:07	1	56000.00	eto@cenjiv.co.uk	50	08335583	P0000188
V000000080	2016-09-19	2016-09-21 03:51:00	1	49000.00	oceob@codetcuh.com	40	08144939	P0000083
V000000081	2016-07-20	2016-07-23 19:07:19	1	76000.00	halilil@wuturo.net	70	08245644	P0000061
V000000082	2016-08-08	2016-08-10 19:21:45	1	83000.00	costoja@waw.gov	80	08783312	P0000045
V000000083	2016-08-30	2016-09-01 16:19:58	1	82000.00	ruemo@ru.co.uk	80	08963446	P0000163
V000000084	2016-07-25	2016-07-27 23:06:14	1	27000.00	co@lep.gov	20	08583345	P0000069
V000000085	2016-08-21	2016-08-23 08:34:07	2	32000.00	na@tazevze.com	30	08977569	P0000135
V000000086	2016-10-01	2016-10-03 20:23:33	2	55000.00	getat@cotehef.org	50	08631164	P0000111
V000000087	2016-09-26	2016-09-28 20:34:12	1	18000.00	paweifa@gusak.com	10	08599636	P0000147
V000000088	2016-08-20	2016-08-22 12:00:51	1	21000.00	aveju@ezihocle.co.uk	20	08365267	P0000108
V000000089	2016-07-27	2016-07-28 23:44:31	1	54000.00	tegesemo@roenvuj.edu	50	08396231	P0000092
V000000090	2016-10-26	2016-10-28 21:11:31	2	27000.00	subzola@kacjis.edu	20	08615544	P0000192
V000000091	2016-08-26	2016-08-28 16:14:31	1	37000.00	jibloc@ol.co.uk	30	08318711	P0000107
V000000092	2016-07-22	2016-07-24 20:50:30	1	88000.00	ijo@sisfopbup.org	80	08311841	P0000032
V000000093	2016-09-26	2016-09-28 21:56:18	1	86000.00	subzola@kacjis.edu	80	08326515	P0000198
V000000094	2016-09-22	2016-09-24 20:41:02	2	27000.00	ewnor@koc.org	20	08414372	P0000192
V000000095	2016-09-23	2016-09-26 01:26:03	1	32000.00	ba@nod.gov	30	08947685	P0000127
V000000096	2016-08-07	2016-08-09 09:32:00	2	18000.00	laligni@hukru.io	10	08482218	P0000181
V000000097	2016-07-21	2016-07-23 18:41:08	2	76000.00	uwa@tagokraw.gov	70	08538124	P0000023
V000000098	2016-10-22	2016-10-24 10:53:08	2	36000.00	ku@titdotres.gov	30	08549393	P0000157
V000000099	2016-09-19	2016-09-21 15:56:15	1	48000.00	enuji@benkutja.gov	40	08949915	P0000093
V000000100	2016-10-18	2016-10-20 21:02:08	1	79000.00	ap@gavakeog.gov	70	08976255	P0000080
V000000101	2016-08-30	2016-08-31 23:12:12	1	89000.00	cileiji@nek.edu	80	08527622	P0000010
V000000102	2016-08-22	2016-08-24 12:20:51	1	14000.00	uk@kiko.co.uk	10	08533544	P0000040
V000000103	2016-10-18	2016-10-20 09:02:25	1	11000.00	je@eno.net	10	08147244	P0000124
V000000104	2016-09-12	2016-09-14 02:35:08	2	92000.00	ku@titdotres.gov	90	08518792	P0000160
V000000105	2016-10-11	2016-10-13 21:55:32	1	64000.00	ijages@lomulel.io	60	08446828	P0000060
V000000106	2016-09-19	2016-09-21 17:35:11	2	58000.00	hovarwuh@sup.io	50	08822643	P0000137
V000000107	2016-08-30	2016-09-01 18:53:36	2	85000.00	dup@nazepad.net	80	08152958	P0000038
V000000108	2016-09-09	2016-09-11 03:50:27	1	93000.00	ji@fogukhu.com	90	08515392	P0000013
V000000109	2016-09-07	2016-09-09 12:30:16	2	58000.00	fiw@mamko.co.uk	50	08359427	P0000137
V000000110	2016-09-07	2016-09-09 05:15:55	1	64000.00	sud@ul.org	60	08158345	P0000060
V000000111	2016-08-20	2016-08-22 00:25:24	2	23000.00	cel@map.io	20	08154265	P0000081
V000000112	2016-09-28	2016-09-30 18:44:03	2	31000.00	ze@erejanek.com	30	08996247	P0000190
V000000113	2016-08-02	2016-08-04 02:26:49	1	32000.00	evisep@uwre.net	30	08395456	P0000127
V000000114	2016-09-04	2016-09-06 20:09:38	1	17000.00	vobvi@vo.org	10	08794457	P0000110
V000000115	2016-09-23	2016-09-25 08:54:15	1	39000.00	idfolo@kori.co.uk	30	08499839	P0000039
V000000116	2016-08-20	2016-08-22 01:48:27	2	25000.00	udne@donazmuv.io	20	08254493	P0000031
V000000117	2016-09-23	2016-09-25 09:51:53	2	56000.00	owagij@gingetek.io	50	08237134	P0000188
V000000118	2016-08-12	2016-08-14 10:18:21	1	26000.00	jejme@me.com	20	08699492	P0000009
V000000119	2016-09-04	2016-09-06 08:46:12	1	12000.00	ige@ocwope.net	10	08236799	P0000024
V000000120	2016-07-21	2016-07-23 03:54:16	2	12000.00	hu@eha.co.uk	10	08483781	P0000191
V000000121	2016-08-23	2016-08-25 06:19:28	1	21000.00	leluwi@honwi.com	20	08362256	P0000014
V000000122	2016-10-14	2016-10-16 19:24:06	2	59000.00	weheb@isoho.com	50	08794279	P0000001
V000000123	2016-08-14	2016-08-16 07:33:39	1	88000.00	ponuv@howup.co.uk	80	08652285	P0000132
V000000124	2016-09-24	2016-09-26 05:59:34	1	72000.00	upu@owfogna.com	70	08818537	P0000104
V000000125	2016-08-13	2016-08-15 23:03:46	2	64000.00	jiwuol@vi.io	60	08293728	P0000029
V000000126	2016-08-26	2016-08-28 14:06:24	2	89000.00	opevar@utfebe.com	80	08996651	P0000098
V000000127	2016-10-13	2016-10-15 09:49:44	1	15000.00	fura@upipu.gov	10	08844425	P0000064
V000000128	2016-08-14	2016-08-16 20:33:50	2	36000.00	oficaznaf@lomu.org	30	08361536	P0000157
V000000129	2016-08-11	2016-08-13 08:51:39	1	23000.00	dimkerra@lebog.co.uk	20	08992996	P0000158
V000000130	2016-07-23	2016-07-25 04:08:11	2	23000.00	murgupo@hiiluhe.net	20	08923419	P0000153
V000000131	2016-09-10	2016-09-12 09:40:32	1	39000.00	liru@op.co.uk	30	08412341	P0000039
V000000132	2016-08-26	2016-08-28 14:24:33	1	51000.00	il@caguri.edu	50	08537533	P0000177
V000000133	2016-09-30	2016-10-02 07:23:46	1	12000.00	cileiji@nek.edu	10	08897528	P0000024
V000000134	2016-09-05	2016-09-07 12:35:58	2	11000.00	ijages@lomulel.io	10	08789673	P0000178
V000000135	2016-10-27	2016-10-29 10:55:05	2	18000.00	jibloc@ol.co.uk	10	08928612	P0000181
V000000136	2016-10-11	2016-10-13 08:04:55	2	93000.00	ca@jon.io	90	08333471	P0000013
V000000137	2016-10-20	2016-10-22 18:57:32	1	42000.00	sijtu@ja.io	40	08513888	P0000123
V000000138	2016-10-22	2016-10-24 06:36:51	1	17000.00	kehec@ebbucja.net	10	08397842	P0000136
V000000139	2016-10-25	2016-10-27 12:08:04	1	97000.00	heip@cani.com	90	08575721	P0000033
V000000140	2016-09-24	2016-09-26 11:53:23	2	79000.00	war@fohrahsin.net	70	08362623	P0000062
V000000141	2016-08-28	2016-08-30 02:39:32	1	35000.00	if@azo.gov	30	08838678	P0000133
V000000142	2016-09-19	2016-09-21 03:39:14	1	68000.00	fiw@mamko.co.uk	60	08867147	P0000117
V000000143	2016-09-05	2016-09-07 07:24:45	2	23000.00	wemufmi@vigurija.gov	20	08699276	P0000148
V000000144	2016-07-28	2016-07-30 14:56:58	2	56000.00	ciwimnim@wolihvo.io	50	08543935	P0000188
V000000145	2016-07-25	2016-07-27 13:05:26	1	36000.00	hed@cingi.org	30	08528472	P0000095
V000000146	2016-10-27	2016-10-29 04:57:54	2	64000.00	rulsiw@lecwuzla.gov	60	08533611	P0000105
V000000147	2016-09-08	2016-09-10 02:07:58	1	26000.00	dimkerra@lebog.co.uk	20	08563639	P0000002
V000000148	2016-09-14	2016-09-16 19:11:40	1	14000.00	getat@cotehef.org	10	08727525	P0000139
V000000149	2016-10-12	2016-10-14 05:06:22	2	29000.00	ba@nod.gov	20	08597513	P0000152
V000000150	2016-10-13	2016-10-15 14:39:35	1	77000.00	hu@zusrusiw.edu	70	08784767	P0000171
V000000151	2016-08-17	2016-08-19 12:25:51	1	37000.00	uk@kiko.co.uk	30	08278846	P0000071
V000000152	2016-08-19	2016-08-21 15:29:12	2	58000.00	papa@jorisuz.co.uk	50	08834999	P0000150
V000000153	2016-08-20	2016-08-22 12:21:36	1	36000.00	ji@fogukhu.com	30	08957122	P0000016
V000000154	2016-10-26	2016-10-28 02:20:59	1	64000.00	jiwuol@vi.io	60	08678299	P0000060
V000000155	2016-08-09	2016-08-11 14:31:06	1	77000.00	kub@etdav.org	70	08482746	P0000126
V000000156	2016-10-11	2016-10-13 01:41:45	1	48000.00	jibloc@ol.co.uk	40	08636143	P0000079
V000000157	2016-07-27	2016-07-29 06:19:29	2	92000.00	ivduse@zuhew.org	90	08127265	P0000160
V000000158	2016-10-09	2016-10-11 08:39:44	1	65000.00	dah@cuz.org	60	08714379	P0000156
V000000159	2016-08-07	2016-08-09 08:28:52	2	45000.00	cobkom@bo.org	40	08171288	P0000049
V000000160	2016-10-14	2016-10-16 00:31:20	2	89000.00	nifu@lotedpoh.gov	80	08544741	P0000146
V000000161	2016-09-01	2016-09-03 10:55:19	1	12000.00	ju@zo.com	10	08695989	P0000191
V000000162	2016-10-15	2016-10-18 21:47:37	1	16000.00	ji@kozko.co.uk	10	08432728	P0000006
V000000163	2016-10-13	2016-10-15 12:40:37	2	85000.00	gurezi@folcib.gov	80	08652368	P0000038
V000000164	2016-08-12	2016-08-14 16:17:24	1	59000.00	igupu@ogezegpod.gov	50	08248172	P0000172
V000000165	2016-10-03	2016-10-05 01:59:45	1	68000.00	murgupo@hiiluhe.net	60	08497374	P0000182
V000000166	2016-07-24	2016-07-26 09:07:59	2	96000.00	if@azo.gov	90	08398567	P0000116
V000000167	2016-09-11	2016-09-13 20:45:43	2	15000.00	opevar@utfebe.com	10	08944283	P0000058
V000000168	2016-08-01	2016-08-03 07:16:37	2	96000.00	vanesu@juwhuzu.gov	90	08719294	P0000005
V000000169	2016-09-18	2016-09-20 17:54:11	1	43000.00	sepmajbo@racupmib.io	40	08586878	P0000197
V000000170	2016-09-26	2016-09-28 01:35:18	1	23000.00	vedowur@loowojac.io	20	08241133	P0000174
V000000171	2016-10-13	2016-10-15 17:46:17	1	41000.00	nifu@lotedpoh.gov	40	08829743	P0000065
V000000172	2016-08-15	2016-08-17 23:09:31	2	41000.00	ta@roc.io	40	08959494	P0000122
V000000173	2016-09-07	2016-09-09 15:49:04	1	14000.00	evomobmi@sepete.com	10	08345882	P0000040
V000000174	2016-10-21	2016-10-23 23:04:21	1	64000.00	uvogew@onumi.io	60	08826861	P0000143
V000000175	2016-09-28	2016-09-30 21:07:56	1	11000.00	ivnu@viset.edu	10	08158777	P0000178
V000000176	2016-10-26	2016-10-28 21:34:17	1	63000.00	ponuv@howup.co.uk	60	08656951	P0000015
V000000177	2016-10-06	2016-10-09 05:01:22	1	48000.00	cobkom@bo.org	40	08375174	P0000128
V000000178	2016-09-20	2016-09-22 03:03:14	2	72000.00	nu@erajod.org	70	08871145	P0000145
V000000179	2016-08-03	2016-08-05 11:07:42	2	82000.00	ejiflo@amdof.io	80	08916256	P0000163
V000000180	2016-09-02	2016-09-04 11:17:45	2	15000.00	cito@no.edu	10	08997579	P0000064
V000000181	2016-08-18	2016-08-20 03:58:33	1	48000.00	vedowur@loowojac.io	40	08144132	P0000093
V000000182	2016-08-08	2016-08-10 04:59:23	2	18000.00	eptew@os.org	10	08578887	P0000147
V000000183	2016-09-27	2016-09-29 07:10:52	2	82000.00	kon@dow.edu	80	08464932	P0000163
V000000184	2016-09-13	2016-09-15 03:53:13	1	11000.00	ditka@bajwo.gov	10	08943332	P0000020
V000000185	2016-09-06	2016-09-08 06:19:10	2	15000.00	pidcem@bucfauru.com	10	08641537	P0000149
V000000186	2016-09-24	2016-09-26 04:01:26	1	32000.00	is@fe.com	30	08456437	P0000135
V000000187	2016-08-27	2016-08-29 22:40:58	1	11000.00	ginid@unfioc.org	10	08686494	P0000178
V000000188	2016-08-30	2016-09-01 21:04:15	1	57000.00	paejne@com.edu	50	08565718	P0000012
V000000189	2016-08-25	2016-08-27 09:22:26	1	15000.00	cafvadpoj@miwoda.co.uk	10	08348293	P0000149
V000000190	2016-09-27	2016-09-29 01:46:43	1	66000.00	ilam@hadjaus.org	60	08182663	P0000118
V000000191	2016-09-04	2016-09-06 17:24:08	2	23000.00	adaerop@ono.net	20	08922645	P0000148
V000000192	2016-08-10	2016-08-12 22:22:42	1	66000.00	ciraofi@wikvuut.io	60	08559115	P0000118
V000000193	2016-08-29	2016-08-31 10:57:52	2	12000.00	rohoci@muza.io	10	08279343	P0000024
V000000194	2016-10-08	2016-10-10 19:56:16	2	75000.00	faopidu@uhi.com	70	08493595	P0000199
V000000195	2016-08-29	2016-08-31 03:05:56	1	89000.00	juciddu@difuci.org	80	08177313	P0000075
V000000196	2016-09-03	2016-09-05 04:15:59	2	11000.00	il@up.co.uk	10	08121245	P0000124
V000000197	2016-10-13	2016-10-15 14:32:40	2	39000.00	sulovsi@ewigi.edu	30	08337129	P0000115
V000000198	2016-10-09	2016-10-11 13:46:03	2	91000.00	jibloc@ol.co.uk	90	08629322	P0000200
V000000199	2016-09-29	2016-10-01 09:23:37	1	53000.00	gigat@saf.co.uk	50	08318877	P0000078
V000000200	2016-10-16	2016-10-18 22:23:43	1	16000.00	ciznamluf@penkono.gov	10	08478752	P0000196
V000000201	2016-07-24	2016-07-26 05:23:35	1	73000.00	ta@roc.io	70	08168123	P0000187
V000000202	2016-09-20	2016-09-22 01:35:59	2	65000.00	faopidu@uhi.com	60	08466488	P0000156
V000000203	2016-08-30	2016-09-01 05:56:27	2	11000.00	wano@liwjumu.com	10	08342441	P0000020
V000000204	2016-09-27	2016-09-29 05:46:37	2	29000.00	lefosmo@fawriil.co.uk	20	08954428	P0000063
V000000205	2016-08-06	2016-08-07 23:17:17	2	78000.00	hu@eha.co.uk	70	08969796	P0000179
V000000206	2016-10-27	2016-10-29 01:22:36	1	15000.00	inu@fo.gov	10	08736574	P0000064
V000000207	2016-10-04	2016-10-06 15:11:07	2	25000.00	tipdabit@ogazov.com	20	08259439	P0000070
V000000208	2016-09-18	2016-09-20 07:06:36	2	77000.00	jawantu@siziin.io	70	08534165	P0000165
V000000209	2016-09-10	2016-09-12 13:22:11	1	41000.00	evomobmi@sepete.com	40	08583964	P0000122
V000000210	2016-10-22	2016-10-24 10:38:42	1	79000.00	tata@meetpe.gov	70	08865375	P0000134
V000000211	2016-09-11	2016-09-13 08:52:05	2	61000.00	nimzin@guznerepi.edu	60	08335968	P0000121
V000000212	2016-09-08	2016-09-10 03:34:32	1	18000.00	ditka@bajwo.gov	10	08372771	P0000084
V000000213	2016-08-13	2016-08-14 23:16:48	2	29000.00	tufilco@luri.edu	20	08654958	P0000159
V000000214	2016-08-12	2016-08-14 08:34:46	2	14000.00	jiwuol@vi.io	10	08229288	P0000139
V000000215	2016-10-06	2016-10-08 06:45:54	1	21000.00	sivugal@opnulaj.gov	20	08149936	P0000014
V000000216	2016-09-27	2016-09-29 17:10:01	2	39000.00	woatva@wodlopwep.edu	30	08284994	P0000115
V000000217	2016-07-23	2016-07-25 19:15:17	2	34000.00	arpil@nasvunub.com	30	08752962	P0000036
V000000218	2016-09-28	2016-09-30 00:22:46	2	57000.00	tu@pevu.edu	50	08195681	P0000012
V000000219	2016-08-21	2016-08-23 04:07:45	1	41000.00	evisep@uwre.net	40	08928646	P0000065
V000000220	2016-09-07	2016-09-09 20:00:53	2	38000.00	sadhupac@zon.edu	30	08675399	P0000086
V000000221	2016-08-06	2016-08-07 23:28:30	1	64000.00	tata@meetpe.gov	60	08358773	P0000007
V000000222	2016-08-02	2016-08-04 06:00:45	2	52000.00	ponuv@howup.co.uk	50	08556582	P0000155
V000000223	2016-08-11	2016-08-13 14:29:58	2	64000.00	socoma@laomigez.io	60	08581288	P0000105
V000000224	2016-09-22	2016-09-24 19:37:40	1	67000.00	imsebap@oci.io	60	08866315	P0000113
V000000225	2016-07-26	2016-07-28 03:51:54	2	79000.00	miwesa@ros.org	70	08169553	P0000134
V000000226	2016-10-26	2016-10-28 08:15:57	1	99000.00	deretjaw@gek.edu	90	08542937	P0000076
V000000227	2016-08-02	2016-08-04 07:33:09	1	36000.00	miwesa@ros.org	30	08168171	P0000016
V000000228	2016-10-13	2016-10-15 06:49:28	1	96000.00	evomobmi@sepete.com	90	08375866	P0000116
V000000229	2016-10-05	2016-10-07 16:34:40	2	52000.00	owagij@gingetek.io	50	08113928	P0000155
V000000230	2016-10-15	2016-10-17 06:55:54	2	67000.00	ummi@dur.gov	60	08637271	P0000066
V000000231	2016-10-18	2016-10-20 13:12:58	2	75000.00	nograhcuc@kauno.edu	70	08743946	P0000025
V000000232	2016-07-24	2016-07-26 04:11:34	2	48000.00	ikruj@sapanti.com	40	08165636	P0000164
V000000233	2016-09-19	2016-09-21 16:13:13	2	54000.00	vacawhep@kalagwo.com	50	08292937	P0000092
V000000234	2016-10-08	2016-10-10 03:24:37	2	64000.00	tumog@heovazor.edu	60	08962253	P0000143
V000000235	2016-10-01	2016-10-03 00:41:44	1	93000.00	ap@gavakeog.gov	90	08261818	P0000028
V000000236	2016-09-01	2016-09-03 01:48:43	2	52000.00	pidcem@bucfauru.com	50	08575127	P0000162
V000000237	2016-07-31	2016-08-02 18:06:24	2	79000.00	it@kaf.io	70	08181131	P0000062
V000000238	2016-10-26	2016-10-28 00:01:34	2	92000.00	ginid@unfioc.org	90	08251512	P0000035
V000000239	2016-10-28	2016-10-30 02:59:38	1	64000.00	jotugve@oclekuju.io	60	08464988	P0000060
V000000240	2016-08-13	2016-08-15 05:27:46	1	46000.00	satme@lir.gov	40	08253316	P0000041
V000000241	2016-08-16	2016-08-18 07:13:16	1	49000.00	ceiwosak@kucuaj.net	40	08862531	P0000083
V000000242	2016-08-14	2016-08-16 09:51:20	1	66000.00	fub@nuwok.io	60	08544677	P0000118
V000000243	2016-10-10	2016-10-12 04:36:41	2	68000.00	fiom@si.org	60	08694344	P0000182
V000000244	2016-10-23	2016-10-25 07:29:11	1	26000.00	paejne@com.edu	20	08334866	P0000002
V000000245	2016-09-02	2016-09-04 07:11:35	1	73000.00	kodon@jeej.com	70	08979671	P0000130
V000000246	2016-10-07	2016-10-09 02:49:00	2	79000.00	guz@tuve.com	70	08473383	P0000101
V000000247	2016-07-31	2016-08-02 16:55:00	1	14000.00	bavit@ji.org	10	08234686	P0000040
V000000248	2016-09-01	2016-09-03 15:02:28	2	78000.00	sivugal@opnulaj.gov	70	08226169	P0000179
V000000249	2016-10-23	2016-10-25 20:41:53	2	17000.00	ji@kozko.co.uk	10	08435611	P0000180
V000000250	2016-07-30	2016-08-01 01:39:06	2	41000.00	paejne@com.edu	40	08282272	P0000168
V000000251	2016-07-29	2016-07-31 14:17:01	1	67000.00	tinubwi@ewo.com	60	08735511	P0000066
V000000252	2016-09-17	2016-09-19 06:13:28	2	89000.00	ponuv@howup.co.uk	80	08358821	P0000146
V000000253	2016-10-03	2016-10-05 06:27:50	2	32000.00	sepmajbo@racupmib.io	30	08579174	P0000127
V000000254	2016-10-25	2016-10-27 19:29:14	2	29000.00	ceiwosak@kucuaj.net	20	08482142	P0000194
V000000255	2016-08-10	2016-08-12 09:34:20	2	57000.00	nograhcuc@kauno.edu	50	08528633	P0000012
V000000256	2016-10-15	2016-10-17 19:17:02	2	33000.00	loh@cetumi.com	30	08728554	P0000173
V000000257	2016-09-08	2016-09-09 23:41:45	1	41000.00	pidcem@bucfauru.com	40	08238889	P0000184
V000000258	2016-10-03	2016-10-05 19:27:03	1	11000.00	jude@fok.edu	10	08232129	P0000124
V000000259	2016-08-14	2016-08-16 01:52:26	2	18000.00	jotugve@oclekuju.io	10	08861719	P0000052
V000000260	2016-09-13	2016-09-15 22:17:05	1	27000.00	gukum@ficoofo.co.uk	20	08156118	P0000192
V000000261	2016-08-13	2016-08-15 10:40:18	1	56000.00	obofac@fel.edu	50	08821275	P0000166
V000000262	2016-09-19	2016-09-21 07:49:11	1	39000.00	vobvi@vo.org	30	08477195	P0000039
V000000263	2016-10-03	2016-10-05 22:31:00	1	11000.00	va@bebim.net	10	08754747	P0000129
V000000264	2016-07-23	2016-07-25 01:00:25	2	38000.00	liru@op.co.uk	30	08757951	P0000067
V000000265	2016-09-28	2016-09-30 05:11:00	1	85000.00	teuzu@va.net	80	08673363	P0000142
V000000266	2016-08-20	2016-08-22 09:48:41	1	81000.00	waruk@tu.org	80	08479284	P0000019
V000000267	2016-09-21	2016-09-23 05:14:09	1	93000.00	gita@parwaj.net	90	08186619	P0000028
V000000268	2016-10-22	2016-10-24 22:33:06	1	48000.00	gita@parwaj.net	40	08727119	P0000093
V000000269	2016-09-11	2016-09-13 13:26:59	1	29000.00	sivugal@opnulaj.gov	20	08183197	P0000194
V000000270	2016-08-12	2016-08-14 05:52:21	2	74000.00	juciddu@difuci.org	70	08346494	P0000099
V000000271	2016-10-08	2016-10-10 01:01:38	1	76000.00	erfi@zokiw.co.uk	70	08751111	P0000061
V000000272	2016-10-28	2016-10-30 21:42:40	1	58000.00	vacawhep@kalagwo.com	50	08247476	P0000137
V000000273	2016-08-29	2016-08-31 20:15:04	2	99000.00	canehiler@enlo.edu	90	08652447	P0000176
V000000274	2016-08-07	2016-08-09 20:33:41	2	74000.00	mocsib@cirgo.io	70	08533629	P0000131
V000000275	2016-08-18	2016-08-20 03:29:29	2	64000.00	it@kaf.io	60	08722467	P0000060
V000000276	2016-07-31	2016-08-02 00:17:51	1	14000.00	satme@lir.gov	10	08589896	P0000139
V000000277	2016-10-07	2016-10-09 20:31:47	1	64000.00	gigat@saf.co.uk	60	08471848	P0000105
V000000278	2016-10-04	2016-10-06 14:49:44	2	77000.00	eptew@os.org	70	08148284	P0000046
V000000279	2016-10-07	2016-10-09 11:00:05	1	75000.00	jidfow@ditiduze.com	70	08868718	P0000199
V000000280	2016-07-27	2016-07-29 12:28:54	2	77000.00	nu@erajod.org	70	08567441	P0000165
V000000281	2016-09-02	2016-09-04 07:08:51	2	29000.00	jujrecwo@veofowe.com	20	08517481	P0000194
V000000282	2016-09-27	2016-09-29 19:21:46	1	39000.00	gukum@ficoofo.co.uk	30	08678117	P0000050
V000000283	2016-08-24	2016-08-26 11:22:24	1	89000.00	hed@cingi.org	80	08931756	P0000098
V000000284	2016-08-03	2016-08-05 03:27:15	2	79000.00	it@kaf.io	70	08373383	P0000004
V000000285	2016-09-09	2016-09-11 14:05:44	1	91000.00	ivduse@zuhew.org	90	08486288	P0000200
V000000286	2016-08-21	2016-08-23 06:12:16	2	77000.00	guz@tuve.com	70	08172531	P0000165
V000000287	2016-08-18	2016-08-20 07:54:41	2	57000.00	jejme@me.com	50	08976518	P0000012
V000000288	2016-10-22	2016-10-24 03:39:20	2	59000.00	ceiwosak@kucuaj.net	50	08823163	P0000172
V000000289	2016-08-21	2016-08-23 22:33:17	1	49000.00	ho@jesopi.gov	40	08977919	P0000051
V000000290	2016-08-03	2016-08-05 08:41:25	1	29000.00	fura@upipu.gov	20	08675959	P0000194
V000000291	2016-10-01	2016-10-03 04:21:43	2	14000.00	tol@tukon.edu	10	08936914	P0000040
V000000292	2016-10-27	2016-10-29 03:05:41	1	32000.00	papa@jorisuz.co.uk	30	08991253	P0000127
V000000293	2016-10-24	2016-10-26 05:14:12	1	42000.00	fokjar@ril.gov	40	08739758	P0000047
V000000294	2016-08-29	2016-08-31 12:03:57	1	35000.00	dipcoc@ziszipmes.io	30	08738777	P0000133
V000000295	2016-09-29	2016-10-01 21:59:41	2	14000.00	hu@zusrusiw.edu	10	08165356	P0000167
V000000296	2016-09-13	2016-09-16 09:33:40	2	97000.00	vim@kieh.gov	90	08983367	P0000033
V000000297	2016-07-22	2016-07-24 00:41:31	2	51000.00	ejoha@acsudo.net	50	08896391	P0000077
V000000298	2016-08-08	2016-08-10 05:27:41	2	63000.00	tuvnej@le.io	60	08758876	P0000015
V000000299	2016-10-07	2016-10-09 08:31:37	2	14000.00	hisudo@ohlu.edu	10	08291631	P0000059
V000000300	2016-09-19	2016-09-21 07:24:54	1	21000.00	ditka@bajwo.gov	20	08776853	P0000014
V000000301	2016-09-14	2016-09-16 04:57:00	2	61000.00	je@eno.net	60	08313313	P0000121
V000000302	2016-08-13	2016-08-15 17:08:01	2	17000.00	oceob@codetcuh.com	10	08338725	P0000189
V000000303	2016-07-28	2016-07-30 14:49:38	2	93000.00	il@up.co.uk	90	08147255	P0000013
V000000304	2016-09-10	2016-09-12 16:07:58	2	36000.00	gukum@ficoofo.co.uk	30	08663797	P0000157
V000000305	2016-10-01	2016-10-03 02:54:00	2	35000.00	lefosmo@fawriil.co.uk	30	08398717	P0000100
V000000306	2016-09-17	2016-09-19 07:17:13	1	18000.00	jujrecwo@veofowe.com	10	08858165	P0000084
V000000307	2016-08-08	2016-08-10 21:33:16	2	35000.00	mir@ru.org	30	08514883	P0000154
V000000308	2016-07-25	2016-07-27 20:45:01	2	15000.00	ejoha@acsudo.net	10	08522153	P0000064
V000000309	2016-10-21	2016-10-23 15:13:23	1	16000.00	tocepu@wela.co.uk	10	08217653	P0000196
V000000310	2016-07-23	2016-07-25 00:54:21	1	56000.00	rofuhbe@vetidi.co.uk	50	08919286	P0000188
V000000311	2016-09-29	2016-10-01 07:44:53	1	85000.00	upu@owfogna.com	80	08277783	P0000038
V000000312	2016-07-20	2016-07-22 01:37:23	1	23000.00	costoja@waw.gov	20	08672975	P0000056
V000000313	2016-09-17	2016-09-19 02:59:32	2	17000.00	fiw@mamko.co.uk	10	08527611	P0000138
V000000314	2016-08-02	2016-08-04 01:36:32	2	88000.00	kodon@jeej.com	80	08785357	P0000132
V000000315	2016-07-21	2016-07-23 22:26:00	2	21000.00	teuzu@va.net	20	08683286	P0000014
V000000316	2016-08-04	2016-08-06 13:41:10	1	76000.00	ur@ru.com	70	08426435	P0000057
V000000317	2016-07-31	2016-08-02 14:36:30	2	31000.00	igupu@ogezegpod.gov	30	08337134	P0000190
V000000318	2016-07-27	2016-07-29 19:02:08	2	48000.00	efuis@uc.co.uk	40	08265221	P0000164
V000000319	2016-10-17	2016-10-19 04:54:13	2	41000.00	wano@liwjumu.com	40	08349357	P0000065
V000000320	2016-08-20	2016-08-22 18:37:54	1	64000.00	mezhi@te.edu	60	08667947	P0000060
V000000321	2016-09-18	2016-09-20 10:59:47	2	93000.00	ra@midgabe.edu	90	08845235	P0000028
V000000322	2016-09-17	2016-09-19 17:42:22	2	17000.00	jiwuol@vi.io	10	08489753	P0000136
V000000323	2016-07-23	2016-07-25 11:32:58	2	25000.00	fo@pa.io	20	08269242	P0000031
V000000324	2016-10-05	2016-10-07 01:55:05	2	66000.00	teuzu@va.net	60	08552355	P0000118
V000000325	2016-09-14	2016-09-16 13:50:57	1	79000.00	tu@pevu.edu	70	08613236	P0000183
V000000326	2016-10-04	2016-10-06 05:54:22	1	11000.00	ige@ocwope.net	10	08343774	P0000020
V000000327	2016-10-05	2016-10-07 07:39:46	1	26000.00	eto@cenjiv.co.uk	20	08955428	P0000085
V000000328	2016-09-01	2016-09-03 09:08:45	1	16000.00	evisep@uwre.net	10	08574433	P0000196
V000000329	2016-08-20	2016-08-22 16:29:53	2	77000.00	ceiwosak@kucuaj.net	70	08465683	P0000169
V000000330	2016-10-07	2016-10-09 08:36:05	1	57000.00	gaupe@uf.org	50	08372315	P0000012
V000000331	2016-09-20	2016-09-22 14:09:35	1	93000.00	kodon@jeej.com	90	08252847	P0000028
V000000332	2016-07-26	2016-07-27 23:33:04	1	39000.00	zu@guji.gov	30	08163155	P0000050
V000000333	2016-08-01	2016-08-03 11:52:51	1	36000.00	pagmu@ka.edu	30	08136914	P0000095
V000000334	2016-09-07	2016-09-09 13:36:41	2	13000.00	rofuhbe@vetidi.co.uk	10	08997961	P0000161
V000000335	2016-10-17	2016-10-19 11:16:09	2	12000.00	ur@ru.com	10	08917666	P0000024
V000000336	2016-10-06	2016-10-08 08:08:54	1	75000.00	zeb@fuz.io	70	08394232	P0000025
V000000337	2016-09-03	2016-09-05 03:52:57	2	58000.00	il@up.co.uk	50	08169532	P0000150
V000000338	2016-08-02	2016-08-04 02:49:41	2	36000.00	cileiji@nek.edu	30	08742882	P0000016
V000000339	2016-08-18	2016-08-20 22:54:12	1	96000.00	leluwi@honwi.com	90	08116411	P0000116
V000000340	2016-08-26	2016-08-28 08:12:13	1	99000.00	ced@ego.net	90	08389839	P0000076
V000000341	2016-08-21	2016-08-23 07:01:13	2	54000.00	enuji@benkutja.gov	50	08349682	P0000092
V000000342	2016-08-02	2016-08-04 09:38:25	1	93000.00	obofac@fel.edu	90	08185624	P0000028
V000000343	2016-09-03	2016-09-05 04:45:43	1	53000.00	bod@zij.edu	50	08532315	P0000087
V000000344	2016-10-16	2016-10-18 04:39:51	2	64000.00	ejoha@acsudo.net	60	08212292	P0000105
V000000345	2016-10-13	2016-10-15 12:14:52	1	29000.00	tocepu@wela.co.uk	20	08339349	P0000152
V000000346	2016-10-12	2016-10-14 00:37:08	2	51000.00	jukuctig@bucolza.org	50	08958523	P0000077
V000000347	2016-10-26	2016-10-28 15:41:30	1	39000.00	zup@ab.com	30	08483947	P0000027
V000000348	2016-07-22	2016-07-24 05:49:22	1	53000.00	kehec@ebbucja.net	50	08585478	P0000087
V000000349	2016-08-03	2016-08-05 20:32:55	2	74000.00	hemvofvod@osulojmov.com	70	08489344	P0000053
V000000350	2016-08-16	2016-08-18 16:38:14	1	58000.00	meiru@dofigoj.org	50	08214541	P0000102
V000000351	2016-08-22	2016-08-24 00:08:41	1	72000.00	rofuhbe@vetidi.co.uk	70	08919298	P0000145
V000000352	2016-10-06	2016-10-08 20:35:53	2	41000.00	laligni@hukru.io	40	08615898	P0000065
V000000353	2016-08-08	2016-08-10 15:06:54	1	96000.00	cel@map.io	90	08987967	P0000005
V000000354	2016-10-09	2016-10-11 12:06:10	1	54000.00	rulsiw@lecwuzla.gov	50	08774518	P0000092
V000000355	2016-09-25	2016-09-27 12:47:57	2	53000.00	ji@kozko.co.uk	50	08922789	P0000087
V000000356	2016-08-23	2016-08-25 18:14:57	1	67000.00	gurezi@folcib.gov	60	08557625	P0000113
V000000357	2016-07-23	2016-07-25 22:01:58	2	42000.00	efuis@uc.co.uk	40	08364689	P0000123
V000000358	2016-07-22	2016-07-24 08:05:55	2	36000.00	sepmajbo@racupmib.io	30	08668944	P0000095
V000000359	2016-08-10	2016-08-12 01:16:30	2	92000.00	nograhcuc@kauno.edu	90	08318747	P0000160
V000000360	2016-09-16	2016-09-18 04:54:33	1	35000.00	jawantu@siziin.io	30	08578292	P0000133
V000000361	2016-08-18	2016-08-20 02:32:49	2	48000.00	je@eno.net	40	08213156	P0000079
V000000362	2016-10-23	2016-10-26 13:58:27	1	92000.00	veizovo@dewuv.org	90	08923585	P0000160
V000000363	2016-08-25	2016-08-27 22:43:40	2	48000.00	eto@cenjiv.co.uk	40	08837724	P0000093
V000000364	2016-09-18	2016-09-20 03:21:50	2	77000.00	si@behab.co.uk	70	08361932	P0000169
V000000365	2016-09-10	2016-09-12 01:13:46	1	46000.00	ba@nod.gov	40	08181328	P0000041
V000000366	2016-08-22	2016-08-24 21:13:25	1	29000.00	teuzu@va.net	20	08288613	P0000159
V000000367	2016-07-29	2016-07-31 16:27:19	2	55000.00	tu@pevu.edu	50	08461746	P0000111
V000000368	2016-10-15	2016-10-17 15:03:16	2	17000.00	imsebap@oci.io	10	08766756	P0000017
V000000369	2016-09-18	2016-09-21 15:02:59	2	23000.00	eto@cenjiv.co.uk	20	08745436	P0000148
V000000370	2016-10-22	2016-10-24 20:15:33	2	41000.00	rohoci@muza.io	40	08662985	P0000184
V000000371	2016-08-04	2016-08-06 12:26:46	1	15000.00	bod@zij.edu	10	08194494	P0000043
V000000372	2016-08-17	2016-08-19 04:10:58	1	14000.00	jumfel@nulueh.net	10	08779833	P0000059
V000000373	2016-09-26	2016-09-28 07:39:00	1	48000.00	sivugal@opnulaj.gov	40	08744436	P0000140
V000000374	2016-09-15	2016-09-17 21:58:43	2	45000.00	gaupe@uf.org	40	08523561	P0000008
V000000375	2016-10-01	2016-10-03 15:37:41	1	55000.00	map@levuv.io	50	08164254	P0000111
V000000376	2016-10-11	2016-10-13 16:25:03	1	26000.00	sepmajbo@racupmib.io	20	08295419	P0000034
V000000377	2016-08-19	2016-08-21 19:12:21	1	41000.00	ija@ub.net	40	08536849	P0000065
V000000378	2016-10-17	2016-10-19 02:23:23	1	92000.00	oceob@codetcuh.com	90	08154771	P0000090
V000000379	2016-08-17	2016-08-20 10:24:43	2	26000.00	loh@cetumi.com	20	08188178	P0000002
V000000380	2016-09-22	2016-09-24 11:22:56	1	41000.00	ditka@bajwo.gov	40	08339342	P0000065
V000000381	2016-10-04	2016-10-06 11:21:39	2	54000.00	sijtu@ja.io	50	08183195	P0000097
V000000382	2016-10-26	2016-10-28 23:08:42	1	41000.00	su@ri.net	40	08252229	P0000122
V000000383	2016-10-07	2016-10-09 07:24:37	1	15000.00	cin@dorpaz.org	10	08392656	P0000043
V000000384	2016-09-12	2016-09-14 22:37:25	1	29000.00	kodon@jeej.com	20	08579765	P0000194
V000000385	2016-10-07	2016-10-09 05:53:08	2	87000.00	oficaznaf@lomu.org	80	08153998	P0000094
V000000386	2016-09-20	2016-09-22 14:58:54	2	14000.00	tog@siv.org	10	08976641	P0000139
V000000387	2016-10-20	2016-10-22 03:21:50	2	29000.00	ivduse@zuhew.org	20	08738321	P0000152
V000000388	2016-07-28	2016-07-30 12:54:17	1	61000.00	heip@cani.com	60	08766893	P0000121
V000000389	2016-09-08	2016-09-09 23:21:45	2	86000.00	ejiflo@amdof.io	80	08573353	P0000021
V000000390	2016-09-10	2016-09-12 04:06:39	1	11000.00	satme@lir.gov	10	08793828	P0000124
V000000391	2016-10-24	2016-10-26 05:23:26	2	36000.00	uc@zepunu.org	30	08156245	P0000016
V000000392	2016-10-25	2016-10-27 13:37:37	2	38000.00	pi@kekbu.io	30	08221319	P0000151
V000000393	2016-10-13	2016-10-15 12:17:55	1	26000.00	ciznamluf@penkono.gov	20	08132656	P0000002
V000000394	2016-10-19	2016-10-21 07:34:15	1	63000.00	uc@rebufjen.edu	60	08258342	P0000015
V000000395	2016-08-27	2016-08-29 06:53:51	2	41000.00	anowuw@sabapmi.org	40	08847161	P0000168
V000000396	2016-10-10	2016-10-12 03:33:23	2	49000.00	hesnaji@ve.net	40	08156656	P0000083
V000000397	2016-10-12	2016-10-14 14:31:29	2	58000.00	if@azo.gov	50	08413285	P0000150
V000000398	2016-10-18	2016-10-20 00:29:18	1	72000.00	leluwi@honwi.com	70	08815444	P0000104
V000000399	2016-09-05	2016-09-07 05:03:00	1	29000.00	ejiflo@amdof.io	20	08538442	P0000152
V000000400	2016-10-15	2016-10-17 18:15:35	2	39000.00	ta@roc.io	30	08627184	P0000027
V000000401	2016-09-19	2016-09-21 05:56:41	1	64000.00	vobvi@vo.org	60	08727794	P0000105
V000000402	2016-07-24	2016-07-26 11:43:41	1	23000.00	hemvofvod@osulojmov.com	20	08492844	P0000048
V000000403	2016-09-10	2016-09-12 04:16:07	1	65000.00	zut@sazul.co.uk	60	08622293	P0000068
V000000404	2016-10-09	2016-10-11 16:29:34	1	77000.00	meiru@dofigoj.org	70	08629719	P0000171
V000000405	2016-10-22	2016-10-24 21:00:04	1	64000.00	ho@jesopi.gov	60	08562548	P0000007
V000000406	2016-09-30	2016-10-02 06:25:08	2	91000.00	laligni@hukru.io	90	08471446	P0000200
V000000407	2016-08-05	2016-08-07 22:06:29	2	43000.00	vuim@zadjul.co.uk	40	08928524	P0000096
V000000408	2016-08-05	2016-08-07 22:11:55	2	97000.00	tuvnej@le.io	90	08385319	P0000033
V000000409	2016-08-05	2016-08-07 13:37:54	2	11000.00	ditka@bajwo.gov	10	08199738	P0000129
V000000410	2016-09-13	2016-09-15 09:15:58	2	68000.00	zeb@fuz.io	60	08478172	P0000182
V000000411	2016-10-26	2016-10-28 06:51:16	2	86000.00	war@fohrahsin.net	80	08589845	P0000198
V000000412	2016-07-28	2016-07-30 19:25:11	1	57000.00	war@fohrahsin.net	50	08569937	P0000012
V000000413	2016-09-21	2016-09-22 23:33:45	1	93000.00	uduhet@pomwic.org	90	08585892	P0000028
V000000414	2016-09-22	2016-09-24 04:20:02	2	48000.00	socoma@laomigez.io	40	08182865	P0000128
V000000415	2016-10-17	2016-10-19 22:57:05	1	11000.00	jotugve@oclekuju.io	10	08436277	P0000178
V000000416	2016-10-03	2016-10-05 19:54:29	2	21000.00	ikruj@sapanti.com	20	08851876	P0000014
V000000417	2016-09-05	2016-09-07 21:48:37	1	64000.00	rohoci@muza.io	60	08661241	P0000105
V000000418	2016-10-13	2016-10-15 16:58:39	1	39000.00	leluwi@honwi.com	30	08675562	P0000115
V000000419	2016-07-27	2016-07-29 15:09:07	1	17000.00	vanesu@juwhuzu.gov	10	08884754	P0000110
V000000420	2016-09-12	2016-09-14 02:06:52	2	95000.00	zonur@vocumotoz.net	90	08398584	P0000072
V000000421	2016-10-09	2016-10-11 05:41:43	1	61000.00	uc@zepunu.org	60	08273495	P0000120
V000000422	2016-10-06	2016-10-08 22:13:08	2	74000.00	jukuctig@bucolza.org	70	08142958	P0000119
V000000423	2016-07-23	2016-07-25 20:55:25	2	12000.00	po@fahida.co.uk	10	08874254	P0000042
V000000424	2016-07-29	2016-07-31 18:52:17	1	36000.00	fiw@mamko.co.uk	30	08618941	P0000157
V000000425	2016-09-27	2016-09-29 20:27:20	1	72000.00	tog@siv.org	70	08411248	P0000145
V000000426	2016-08-21	2016-08-22 23:55:37	1	16000.00	mezhi@te.edu	10	08747399	P0000196
V000000427	2016-08-23	2016-08-25 10:17:24	1	13000.00	ku@titdotres.gov	10	08682186	P0000161
V000000428	2016-09-12	2016-09-14 21:17:12	1	41000.00	tocepu@wela.co.uk	40	08936621	P0000091
V000000429	2016-08-22	2016-08-24 09:42:30	2	35000.00	no@ugu.com	30	08871399	P0000154
V000000430	2016-07-21	2016-07-23 17:04:42	2	17000.00	ciznamluf@penkono.gov	10	08349762	P0000138
V000000431	2016-09-09	2016-09-11 20:50:35	1	39000.00	sivugal@opnulaj.gov	30	08491288	P0000115
V000000432	2016-09-16	2016-09-18 08:39:00	2	23000.00	paejne@com.edu	20	08345936	P0000048
V000000433	2016-10-27	2016-10-29 13:38:42	1	82000.00	lefosmo@fawriil.co.uk	80	08673353	P0000163
V000000434	2016-08-05	2016-08-07 11:04:31	2	55000.00	tol@tukon.edu	50	08422158	P0000111
V000000435	2016-10-08	2016-10-10 12:35:34	1	46000.00	dup@ejmameh.io	40	08445263	P0000041
V000000436	2016-09-04	2016-09-06 02:14:32	1	92000.00	cel@map.io	90	08729712	P0000035
V000000437	2016-08-02	2016-08-04 11:20:50	2	66000.00	cej@juz.co.uk	60	08229955	P0000011
V000000438	2016-08-05	2016-08-07 13:21:49	2	37000.00	jejme@me.com	30	08548458	P0000071
V000000439	2016-10-11	2016-10-13 05:39:12	2	55000.00	vacawhep@kalagwo.com	50	08118258	P0000018
V000000440	2016-09-15	2016-09-17 04:25:09	2	33000.00	pagmu@ka.edu	30	08973711	P0000175
V000000441	2016-09-12	2016-09-13 23:52:23	2	39000.00	veizovo@dewuv.org	30	08429423	P0000039
V000000442	2016-08-21	2016-08-23 07:10:59	2	33000.00	dimkerra@lebog.co.uk	30	08725595	P0000173
V000000443	2016-08-08	2016-08-10 08:57:28	1	79000.00	na@tazevze.com	70	08369528	P0000183
V000000444	2016-10-19	2016-10-21 05:55:12	1	99000.00	ji@ihmomih.io	90	08211186	P0000076
V000000445	2016-08-08	2016-08-10 09:19:38	2	91000.00	jotugve@oclekuju.io	90	08318359	P0000200
V000000446	2016-10-05	2016-10-07 05:17:09	2	39000.00	jazurot@kentefe.net	30	08771169	P0000050
V000000447	2016-09-23	2016-09-25 10:36:22	1	49000.00	vuc@ifef.net	40	08982589	P0000103
V000000448	2016-07-31	2016-08-02 09:47:49	2	55000.00	su@ri.net	50	08215185	P0000111
V000000449	2016-09-10	2016-09-12 17:51:54	1	45000.00	rofuhbe@vetidi.co.uk	40	08948312	P0000003
V000000450	2016-08-20	2016-08-22 02:35:25	1	17000.00	subzola@kacjis.edu	10	08872666	P0000017
V000000451	2016-10-23	2016-10-25 11:46:03	1	75000.00	dimkerra@lebog.co.uk	70	08841495	P0000025
V000000452	2016-10-10	2016-10-12 08:41:00	2	61000.00	sadhupac@zon.edu	60	08144188	P0000120
V000000453	2016-09-15	2016-09-17 10:03:29	1	56000.00	anowuw@sabapmi.org	50	08531542	P0000055
V000000454	2016-09-13	2016-09-15 16:52:29	1	75000.00	imsebap@oci.io	70	08549149	P0000199
V000000455	2016-08-18	2016-08-20 10:34:50	2	43000.00	upu@owfogna.com	40	08297217	P0000096
V000000456	2016-10-04	2016-10-05 23:30:20	2	93000.00	jodluf@soklizkec.org	90	08524942	P0000028
V000000457	2016-10-21	2016-10-23 00:44:00	2	48000.00	malvo@gigdelkun.com	40	08361957	P0000093
V000000458	2016-08-27	2016-08-29 13:47:52	1	53000.00	hazut@ezhofid.co.uk	50	08934687	P0000141
V000000459	2016-10-22	2016-10-24 19:22:52	1	77000.00	kon@dow.edu	70	08638499	P0000169
V000000460	2016-10-28	2016-10-30 20:16:07	2	96000.00	sulovsi@ewigi.edu	90	08629599	P0000116
V000000461	2016-09-12	2016-09-14 01:20:28	2	74000.00	zup@ab.com	70	08141266	P0000099
V000000462	2016-10-22	2016-10-24 15:29:13	2	23000.00	paejne@com.edu	20	08531546	P0000048
V000000463	2016-07-29	2016-07-31 21:28:59	1	37000.00	arpil@nasvunub.com	30	08488725	P0000107
V000000464	2016-10-11	2016-10-13 00:29:23	1	48000.00	sepmajbo@racupmib.io	40	08584753	P0000140
V000000465	2016-09-08	2016-09-10 18:31:30	1	93000.00	ze@erejanek.com	90	08546445	P0000028
V000000466	2016-07-29	2016-07-31 05:41:38	2	82000.00	woatva@wodlopwep.edu	80	08713343	P0000163
V000000467	2016-10-16	2016-10-18 13:42:52	1	11000.00	nimzin@guznerepi.edu	10	08724429	P0000124
V000000468	2016-09-11	2016-09-13 08:31:55	2	49000.00	idfolo@kori.co.uk	40	08721378	P0000051
V000000469	2016-08-16	2016-08-18 13:02:16	1	18000.00	powjajrak@el.io	10	08525273	P0000052
V000000470	2016-09-29	2016-10-01 17:42:44	2	15000.00	lud@wubra.gov	10	08329969	P0000149
V000000471	2016-09-14	2016-09-16 13:35:45	2	92000.00	wano@liwjumu.com	90	08756994	P0000090
V000000472	2016-09-25	2016-09-27 20:35:18	2	42000.00	ikruj@sapanti.com	40	08542953	P0000047
V000000473	2016-10-02	2016-10-04 12:56:22	1	92000.00	fo@pa.io	90	08562249	P0000090
V000000474	2016-08-07	2016-08-08 23:28:32	2	11000.00	jaolo@fihotnem.net	10	08941642	P0000124
V000000475	2016-08-16	2016-08-18 07:10:43	2	76000.00	sivugal@opnulaj.gov	70	08568983	P0000057
V000000476	2016-08-31	2016-09-02 01:06:56	2	63000.00	sivugal@opnulaj.gov	60	08729213	P0000015
V000000477	2016-07-20	2016-07-23 22:54:32	2	48000.00	ikruj@sapanti.com	40	08377416	P0000164
V000000478	2016-07-28	2016-07-29 23:40:34	2	78000.00	tocepu@wela.co.uk	70	08731967	P0000179
V000000479	2016-10-03	2016-10-05 11:46:27	2	76000.00	uduhet@pomwic.org	70	08118432	P0000023
V000000480	2016-09-23	2016-09-25 13:01:43	2	43000.00	po@fahida.co.uk	40	08567686	P0000197
V000000481	2016-07-24	2016-07-26 15:41:50	2	77000.00	uduhet@pomwic.org	70	08897253	P0000171
V000000482	2016-08-20	2016-08-22 16:26:28	1	16000.00	jaolo@fihotnem.net	10	08658321	P0000006
V000000483	2016-10-27	2016-10-29 04:57:14	1	27000.00	zi@mimpu.com	20	08136393	P0000069
V000000484	2016-09-28	2016-09-30 20:58:57	1	61000.00	kehec@ebbucja.net	60	08833513	P0000120
V000000485	2016-07-22	2016-07-24 22:05:07	1	89000.00	esoli@velmurap.io	80	08761987	P0000075
V000000486	2016-10-12	2016-10-14 20:07:45	2	18000.00	il@up.co.uk	10	08226618	P0000052
V000000487	2016-08-24	2016-08-27 05:43:34	2	51000.00	deretjaw@gek.edu	50	08719238	P0000026
V000000488	2016-08-13	2016-08-15 06:24:49	2	54000.00	pufvep@zagicmac.io	50	08619412	P0000097
V000000489	2016-09-25	2016-09-27 20:56:55	2	39000.00	kub@etdav.org	30	08612275	P0000050
V000000490	2016-08-15	2016-08-17 02:05:02	1	99000.00	sud@ul.org	90	08724485	P0000176
V000000491	2016-10-18	2016-10-20 10:04:52	1	55000.00	adaerop@ono.net	50	08872975	P0000018
V000000492	2016-08-02	2016-08-03 23:48:53	2	89000.00	socoma@laomigez.io	80	08939219	P0000054
V000000493	2016-10-07	2016-10-09 02:58:07	2	64000.00	dimkerra@lebog.co.uk	60	08872683	P0000029
V000000494	2016-08-30	2016-09-01 13:32:51	1	96000.00	teuzu@va.net	90	08925478	P0000116
V000000495	2016-10-09	2016-10-11 19:48:40	2	23000.00	eto@cenjiv.co.uk	20	08623926	P0000158
V000000496	2016-08-14	2016-08-16 06:49:28	1	23000.00	ivduse@zuhew.org	20	08429346	P0000056
V000000497	2016-08-17	2016-08-19 12:15:22	2	29000.00	anowuw@sabapmi.org	20	08855669	P0000194
V000000498	2016-08-04	2016-08-06 02:51:30	2	86000.00	povioge@na.gov	80	08178431	P0000021
V000000499	2016-10-21	2016-10-23 12:21:38	1	54000.00	si@behab.co.uk	50	08599658	P0000097
V000000500	2016-08-06	2016-08-08 06:57:06	2	82000.00	zeb@fuz.io	80	08961886	P0000163
V000000501	2016-09-04	2016-09-06 19:14:22	1	59000.00	hu@eha.co.uk	50	08139152	P0000001
V000000502	2016-09-02	2016-09-04 22:48:52	1	72000.00	ivnu@viset.edu	70	08558815	P0000144
V000000503	2016-09-16	2016-09-18 16:43:24	1	86000.00	jumfel@nulueh.net	80	08786645	P0000198
V000000504	2016-08-18	2016-08-20 19:12:58	1	52000.00	teguc@pohon.io	50	08137674	P0000162
V000000505	2016-08-20	2016-08-22 15:40:16	2	45000.00	ew@egzo.gov	40	08337738	P0000003
V000000506	2016-07-26	2016-07-28 10:00:33	1	92000.00	teuzu@va.net	90	08653292	P0000035
V000000507	2016-07-26	2016-07-28 17:49:21	1	12000.00	goufaut@luwab.co.uk	10	08922441	P0000024
V000000508	2016-10-10	2016-10-12 17:42:06	1	23000.00	ku@titdotres.gov	20	08964249	P0000195
V000000509	2016-08-27	2016-08-29 15:36:15	1	18000.00	wimfi@derwejgen.co.uk	10	08658236	P0000052
V000000510	2016-07-21	2016-07-23 03:29:02	1	45000.00	miwesa@ros.org	40	08817879	P0000008
V000000511	2016-09-07	2016-09-09 14:23:42	1	45000.00	jidfow@ditiduze.com	40	08283144	P0000003
V000000512	2016-10-08	2016-10-09 23:39:57	1	81000.00	ca@jon.io	80	08756424	P0000019
V000000513	2016-07-22	2016-07-24 19:57:11	1	64000.00	ivduse@zuhew.org	60	08723613	P0000007
V000000514	2016-07-25	2016-07-27 04:32:29	2	77000.00	ige@ocwope.net	70	08813792	P0000088
V000000515	2016-08-17	2016-08-19 00:22:41	1	84000.00	ofi@veubuda.org	80	08587268	P0000185
V000000516	2016-10-10	2016-10-12 13:17:01	2	64000.00	powjajrak@el.io	60	08339932	P0000143
V000000517	2016-08-30	2016-09-01 20:55:45	1	46000.00	hed@cingi.org	40	08746982	P0000041
V000000518	2016-10-03	2016-10-05 19:56:26	1	23000.00	oficaznaf@lomu.org	20	08383696	P0000153
V000000519	2016-10-12	2016-10-14 06:59:06	1	79000.00	wano@liwjumu.com	70	08299725	P0000062
V000000520	2016-07-20	2016-07-22 16:01:56	1	32000.00	gukum@ficoofo.co.uk	30	08286226	P0000037
V000000521	2016-09-17	2016-09-19 01:11:40	2	67000.00	powjajrak@el.io	60	08668954	P0000113
V000000522	2016-08-08	2016-08-10 05:42:51	2	49000.00	jaolo@fihotnem.net	40	08161679	P0000051
V000000523	2016-09-28	2016-09-30 21:19:23	2	82000.00	papa@jorisuz.co.uk	80	08154151	P0000163
V000000524	2016-09-01	2016-09-03 10:14:11	2	17000.00	povioge@na.gov	10	08694157	P0000138
V000000525	2016-10-05	2016-10-07 22:43:04	1	18000.00	ofi@veubuda.org	10	08432866	P0000084
V000000526	2016-07-27	2016-07-29 18:25:17	1	26000.00	po@fahida.co.uk	20	08388174	P0000002
V000000527	2016-09-15	2016-09-17 04:03:44	2	35000.00	rulsiw@lecwuzla.gov	30	08445999	P0000133
V000000528	2016-08-09	2016-08-11 10:25:10	1	39000.00	se@tedu.io	30	08832124	P0000027
V000000529	2016-09-22	2016-09-24 21:19:43	2	14000.00	sadhupac@zon.edu	10	08297518	P0000059
V000000530	2016-08-14	2016-08-16 04:24:44	2	99000.00	zu@guji.gov	90	08589393	P0000076
V000000531	2016-08-15	2016-08-17 05:08:07	1	58000.00	zonur@vocumotoz.net	50	08141299	P0000137
V000000532	2016-08-20	2016-08-22 02:04:55	1	42000.00	sivugal@opnulaj.gov	40	08861668	P0000047
V000000533	2016-08-15	2016-08-17 03:54:42	1	74000.00	if@azo.gov	70	08368848	P0000053
V000000534	2016-09-13	2016-09-15 19:56:04	1	29000.00	socoma@laomigez.io	20	08665116	P0000063
V000000535	2016-10-17	2016-10-19 15:12:58	1	44000.00	fub@nuwok.io	40	08586658	P0000186
V000000536	2016-10-19	2016-10-21 04:02:57	2	88000.00	papa@jorisuz.co.uk	80	08187653	P0000132
V000000537	2016-07-21	2016-07-23 06:52:23	1	66000.00	uc@zepunu.org	60	08371141	P0000011
V000000538	2016-10-25	2016-10-27 00:50:24	1	43000.00	tumog@heovazor.edu	40	08298889	P0000096
V000000539	2016-10-05	2016-10-07 03:17:15	2	77000.00	ginid@unfioc.org	70	08384757	P0000088
V000000540	2016-09-22	2016-09-24 22:05:22	2	96000.00	hesnaji@ve.net	90	08999991	P0000005
V000000541	2016-08-17	2016-08-19 20:17:56	1	25000.00	ponuv@howup.co.uk	20	08196944	P0000031
V000000542	2016-10-16	2016-10-18 06:14:45	1	27000.00	halilil@wuturo.net	20	08682268	P0000192
V000000543	2016-10-03	2016-10-05 10:21:47	1	15000.00	tuvnej@le.io	10	08424533	P0000149
V000000544	2016-08-03	2016-08-05 22:51:03	2	58000.00	tomva@kej.gov	50	08573337	P0000150
V000000545	2016-10-04	2016-10-06 19:13:07	1	68000.00	jidfow@ditiduze.com	60	08254764	P0000182
V000000546	2016-07-20	2016-07-22 03:19:56	2	15000.00	heip@cani.com	10	08431432	P0000064
V000000547	2016-08-15	2016-08-17 13:07:57	2	12000.00	vanesu@juwhuzu.gov	10	08913285	P0000042
V000000548	2016-07-31	2016-08-02 15:04:19	1	27000.00	vedowur@loowojac.io	20	08895213	P0000192
V000000549	2016-07-26	2016-07-28 03:05:00	2	23000.00	il@caguri.edu	20	08973941	P0000056
V000000550	2016-08-01	2016-08-03 07:24:55	2	74000.00	sadhupac@zon.edu	70	08613335	P0000119
V000000551	2016-10-08	2016-10-10 04:34:19	1	37000.00	mir@ru.org	30	08152568	P0000071
V000000552	2016-09-13	2016-09-15 00:10:56	1	34000.00	oceob@codetcuh.com	30	08319971	P0000036
V000000553	2016-10-05	2016-10-07 14:59:19	2	45000.00	ciraofi@wikvuut.io	40	08483118	P0000008
V000000554	2016-07-22	2016-07-23 23:13:49	1	55000.00	giwjufu@mahuh.net	50	08551813	P0000111
V000000555	2016-09-08	2016-09-11 15:28:55	1	99000.00	ku@titdotres.gov	90	08298828	P0000176
V000000556	2016-08-26	2016-08-28 01:29:16	1	58000.00	kehu@pom.edu	50	08428539	P0000137
V000000557	2016-09-30	2016-10-02 18:29:46	2	37000.00	papa@jorisuz.co.uk	30	08514119	P0000107
V000000558	2016-09-26	2016-09-28 07:05:58	1	33000.00	vanesu@juwhuzu.gov	30	08435441	P0000173
V000000559	2016-07-23	2016-07-25 08:06:02	1	64000.00	goufaut@luwab.co.uk	60	08676687	P0000007
V000000560	2016-09-30	2016-10-02 22:18:10	1	65000.00	nimzin@guznerepi.edu	60	08119982	P0000068
V000000561	2016-08-18	2016-08-20 03:27:01	2	16000.00	evomobmi@sepete.com	10	08289769	P0000196
V000000562	2016-09-20	2016-09-21 23:58:03	2	29000.00	adaerop@ono.net	20	08333441	P0000063
V000000563	2016-09-03	2016-09-05 18:20:19	2	23000.00	opevar@utfebe.com	20	08215435	P0000148
V000000564	2016-09-20	2016-09-21 23:46:57	1	53000.00	zut@sazul.co.uk	50	08793893	P0000078
V000000565	2016-09-05	2016-09-07 04:36:21	2	77000.00	ze@erejanek.com	70	08262981	P0000165
V000000566	2016-09-02	2016-09-04 01:58:55	2	68000.00	liru@op.co.uk	60	08325126	P0000117
V000000567	2016-10-25	2016-10-27 07:13:40	2	97000.00	ija@ub.net	90	08573483	P0000033
V000000568	2016-08-27	2016-08-30 18:42:19	1	38000.00	mireg@nevwacko.edu	30	08563925	P0000086
V000000569	2016-09-25	2016-09-27 08:44:09	1	59000.00	ba@nod.gov	50	08119577	P0000001
V000000570	2016-09-06	2016-09-08 16:26:29	1	77000.00	se@tedu.io	70	08722843	P0000126
V000000571	2016-09-17	2016-09-19 16:46:07	1	17000.00	cito@no.edu	10	08362855	P0000138
V000000572	2016-07-28	2016-07-30 00:40:25	2	41000.00	kanaz@mema.org	40	08846321	P0000122
V000000573	2016-10-21	2016-10-23 15:34:28	2	35000.00	ciraofi@wikvuut.io	30	08285247	P0000100
V000000574	2016-09-16	2016-09-18 09:08:24	1	24000.00	ji@kozko.co.uk	20	08485436	P0000073
V000000575	2016-09-02	2016-09-04 06:25:13	2	72000.00	ijo@sisfopbup.org	70	08446291	P0000144
V000000576	2016-08-01	2016-08-03 14:15:01	2	72000.00	tinubwi@ewo.com	70	08759477	P0000074
V000000577	2016-10-12	2016-10-14 17:44:16	1	58000.00	uc@rebufjen.edu	50	08318883	P0000170
V000000578	2016-09-09	2016-09-11 19:18:58	1	91000.00	wimfi@derwejgen.co.uk	90	08576681	P0000200
V000000579	2016-10-23	2016-10-25 16:47:24	1	79000.00	pufvep@zagicmac.io	70	08721779	P0000101
V000000580	2016-10-12	2016-10-14 22:38:41	2	86000.00	arpil@nasvunub.com	80	08586644	P0000198
V000000581	2016-10-28	2016-10-30 22:05:40	1	49000.00	mezhi@te.edu	40	08836271	P0000103
V000000582	2016-10-06	2016-10-07 23:21:39	1	65000.00	tocepu@wela.co.uk	60	08793352	P0000156
V000000583	2016-10-20	2016-10-22 21:53:03	2	15000.00	costoja@waw.gov	10	08484513	P0000058
V000000584	2016-10-11	2016-10-13 00:13:07	2	79000.00	is@fe.com	70	08418932	P0000080
V000000585	2016-08-07	2016-08-09 06:32:44	1	74000.00	tu@pevu.edu	70	08697788	P0000119
V000000586	2016-09-25	2016-09-27 06:16:48	1	67000.00	nimzin@guznerepi.edu	60	08774928	P0000066
V000000587	2016-08-14	2016-08-16 20:22:19	1	52000.00	kanaz@mema.org	50	08124658	P0000162
V000000588	2016-09-02	2016-09-04 10:42:00	2	27000.00	sijtu@ja.io	20	08723823	P0000192
V000000589	2016-09-10	2016-09-12 08:49:13	1	65000.00	je@eno.net	60	08915896	P0000156
V000000590	2016-09-19	2016-09-21 16:26:37	2	58000.00	murgupo@hiiluhe.net	50	08729466	P0000102
V000000591	2016-10-09	2016-10-11 23:55:52	2	64000.00	ewnor@koc.org	60	08381665	P0000060
V000000592	2016-10-10	2016-10-12 03:04:10	1	68000.00	se@tedu.io	60	08815654	P0000117
V000000593	2016-09-04	2016-09-06 00:18:27	1	64000.00	paejne@com.edu	60	08594387	P0000007
V000000594	2016-07-30	2016-08-01 08:04:20	1	57000.00	tu@pevu.edu	50	08163428	P0000012
V000000595	2016-08-31	2016-09-02 15:55:37	1	92000.00	owagij@gingetek.io	90	08112556	P0000160
V000000596	2016-09-23	2016-09-24 23:54:03	1	11000.00	zonur@vocumotoz.net	10	08587558	P0000178
V000000597	2016-08-08	2016-08-10 18:50:51	2	78000.00	povioge@na.gov	70	08713315	P0000179
V000000598	2016-09-30	2016-10-02 22:10:09	1	65000.00	enuji@benkutja.gov	60	08589114	P0000068
V000000599	2016-09-25	2016-09-27 23:15:29	1	24000.00	teguc@pohon.io	20	08756138	P0000073
V000000600	2016-07-21	2016-07-23 08:23:23	2	26000.00	cuberuta@biepi.co.uk	20	08995754	P0000002
V000000601	2016-07-29	2016-07-31 00:35:01	1	84000.00	waruk@tu.org	80	08722641	P0000109
V000000602	2016-07-22	2016-07-24 02:43:29	1	11000.00	kibiri@koftere.io	10	08157474	P0000129
V000000603	2016-08-22	2016-08-23 23:14:49	1	89000.00	tata@meetpe.gov	80	08996376	P0000098
V000000604	2016-09-30	2016-10-02 17:52:43	1	33000.00	ku@titdotres.gov	30	08772418	P0000173
V000000605	2016-07-25	2016-07-28 03:40:08	2	16000.00	ji@fogukhu.com	10	08292715	P0000006
V000000606	2016-07-24	2016-07-26 06:04:26	2	76000.00	eptew@os.org	70	08365948	P0000057
V000000607	2016-07-31	2016-08-02 08:02:26	1	89000.00	ji@kozko.co.uk	80	08288825	P0000098
V000000608	2016-08-03	2016-08-05 17:56:34	2	82000.00	ciraofi@wikvuut.io	80	08376418	P0000030
V000000609	2016-08-19	2016-08-21 06:59:31	1	43000.00	uc@zepunu.org	40	08224559	P0000096
V000000610	2016-09-06	2016-09-08 08:13:31	2	89000.00	laligni@hukru.io	80	08592678	P0000054
V000000611	2016-09-17	2016-09-19 14:04:33	1	93000.00	loh@cetumi.com	90	08769348	P0000028
V000000612	2016-08-22	2016-08-24 19:30:12	2	27000.00	jiwuol@vi.io	20	08261498	P0000192
V000000613	2016-10-14	2016-10-16 12:35:53	1	29000.00	liru@op.co.uk	20	08549896	P0000159
V000000614	2016-09-13	2016-09-15 17:59:13	1	89000.00	po@fahida.co.uk	80	08419755	P0000075
V000000615	2016-09-07	2016-09-09 14:45:57	1	67000.00	powjajrak@el.io	60	08569262	P0000066
V000000616	2016-10-20	2016-10-22 10:13:39	1	84000.00	efo@ran.com	80	08822677	P0000185
V000000617	2016-08-17	2016-08-19 15:51:28	1	27000.00	igupu@ogezegpod.gov	20	08562559	P0000069
V000000618	2016-09-13	2016-09-15 00:28:12	2	11000.00	owagij@gingetek.io	10	08555441	P0000178
V000000619	2016-10-08	2016-10-10 01:53:32	2	72000.00	si@behab.co.uk	70	08223397	P0000144
V000000620	2016-10-26	2016-10-28 10:29:42	2	53000.00	canehiler@enlo.edu	50	08847637	P0000087
V000000621	2016-08-09	2016-08-11 04:38:47	2	18000.00	tog@siv.org	10	08218475	P0000052
V000000622	2016-09-30	2016-10-02 23:04:51	2	39000.00	tipdabit@ogazov.com	30	08432388	P0000050
V000000623	2016-09-13	2016-09-15 13:42:27	1	53000.00	eribip@dim.edu	50	08237186	P0000087
V000000624	2016-07-20	2016-07-22 15:02:18	2	51000.00	mezhi@te.edu	50	08739418	P0000177
V000000625	2016-08-08	2016-08-10 02:30:30	2	29000.00	inu@fo.gov	20	08932388	P0000152
V000000626	2016-09-08	2016-09-10 20:31:48	1	13000.00	ijages@lomulel.io	10	08161538	P0000161
V000000627	2016-10-05	2016-10-07 18:11:53	1	26000.00	cito@no.edu	20	08286842	P0000085
V000000628	2016-08-25	2016-08-27 16:31:14	2	35000.00	esoli@velmurap.io	30	08673367	P0000154
V000000629	2016-09-08	2016-09-10 16:01:02	1	96000.00	inu@fo.gov	90	08968293	P0000005
V000000630	2016-08-03	2016-08-05 13:07:55	1	77000.00	oceob@codetcuh.com	70	08282549	P0000169
V000000631	2016-07-28	2016-07-30 15:56:24	1	56000.00	jukuctig@bucolza.org	50	08853537	P0000166
V000000632	2016-07-27	2016-07-29 13:31:59	2	48000.00	sepmajbo@racupmib.io	40	08363682	P0000093
V000000633	2016-09-29	2016-10-01 19:01:46	1	41000.00	guz@tuve.com	40	08297774	P0000122
V000000634	2016-10-24	2016-10-25 23:19:23	2	48000.00	fub@nuwok.io	40	08657866	P0000164
V000000635	2016-08-04	2016-08-06 04:12:24	2	27000.00	inu@fo.gov	20	08383529	P0000192
V000000636	2016-09-01	2016-09-03 18:47:43	2	85000.00	waruk@tu.org	80	08217861	P0000038
V000000637	2016-08-03	2016-08-05 02:20:30	2	25000.00	ige@ocwope.net	20	08921958	P0000106
V000000638	2016-08-22	2016-08-24 06:44:31	1	46000.00	jiwuol@vi.io	40	08111129	P0000041
V000000639	2016-09-22	2016-09-24 04:23:40	1	73000.00	hovarwuh@sup.io	70	08478135	P0000130
V000000640	2016-07-21	2016-07-23 23:04:12	1	39000.00	ewebeduc@heceh.co.uk	30	08347115	P0000050
V000000641	2016-08-24	2016-08-26 11:42:01	1	72000.00	tuvnej@le.io	70	08873348	P0000145
V000000642	2016-09-21	2016-09-23 13:48:20	2	38000.00	fiw@mamko.co.uk	30	08175399	P0000067
V000000643	2016-08-04	2016-08-05 23:17:30	2	42000.00	remuw@jopuru.io	40	08716673	P0000047
V000000644	2016-10-12	2016-10-14 06:13:21	2	17000.00	uk@kiko.co.uk	10	08394379	P0000189
V000000645	2016-08-06	2016-08-08 10:39:00	1	76000.00	costoja@waw.gov	70	08739169	P0000023
V000000646	2016-08-18	2016-08-20 17:19:59	1	96000.00	arpil@nasvunub.com	90	08395319	P0000116
V000000647	2016-09-24	2016-09-26 02:43:20	2	48000.00	tocepu@wela.co.uk	40	08336641	P0000079
V000000648	2016-10-13	2016-10-15 19:56:22	1	49000.00	remuw@jopuru.io	40	08172251	P0000103
V000000649	2016-09-18	2016-09-20 07:19:52	1	45000.00	faopidu@uhi.com	40	08739219	P0000003
V000000650	2016-10-27	2016-10-29 01:14:07	2	55000.00	efuis@uc.co.uk	50	08968181	P0000111
V000000651	2016-07-26	2016-07-28 02:49:06	1	76000.00	giwjufu@mahuh.net	70	08925238	P0000061
V000000652	2016-09-05	2016-09-07 07:28:58	1	86000.00	gurezi@folcib.gov	80	08727293	P0000021
V000000653	2016-10-16	2016-10-18 08:14:32	1	17000.00	tipdabit@ogazov.com	10	08929898	P0000017
V000000654	2016-09-10	2016-09-12 14:30:08	1	59000.00	zut@sazul.co.uk	50	08992792	P0000172
V000000655	2016-08-18	2016-08-21 13:21:42	1	11000.00	tuvnej@le.io	10	08389282	P0000129
V000000656	2016-09-27	2016-09-29 19:12:51	2	14000.00	eribip@dim.edu	10	08877392	P0000139
V000000657	2016-09-21	2016-09-23 04:12:10	1	79000.00	kibiri@koftere.io	70	08993934	P0000101
V000000658	2016-07-20	2016-07-22 20:43:44	1	61000.00	jidfow@ditiduze.com	60	08517627	P0000120
V000000659	2016-09-21	2016-09-24 06:31:02	1	21000.00	gurezi@folcib.gov	20	08392752	P0000108
V000000660	2016-08-22	2016-08-25 03:37:22	1	11000.00	pagmu@ka.edu	10	08114625	P0000124
V000000661	2016-10-06	2016-10-08 20:56:44	2	58000.00	fokjar@ril.gov	50	08131335	P0000170
V000000662	2016-08-13	2016-08-15 09:41:33	1	43000.00	gaupe@uf.org	40	08133497	P0000096
V000000663	2016-07-24	2016-07-26 19:50:13	2	23000.00	lupadiv@ge.com	20	08616923	P0000195
V000000664	2016-09-29	2016-10-01 21:46:49	2	74000.00	teuzu@va.net	70	08782948	P0000119
V000000665	2016-08-30	2016-09-01 04:38:24	1	39000.00	obofac@fel.edu	30	08484975	P0000050
V000000666	2016-09-03	2016-09-05 20:04:34	2	74000.00	jumovbot@ul.edu	70	08456238	P0000053
V000000667	2016-07-24	2016-07-26 18:32:47	2	64000.00	noge@wib.com	60	08711561	P0000060
V000000668	2016-10-01	2016-10-03 12:19:31	1	59000.00	malvo@gigdelkun.com	50	08779683	P0000001
V000000669	2016-07-21	2016-07-23 06:55:35	1	15000.00	woatva@wodlopwep.edu	10	08413486	P0000064
V000000670	2016-10-04	2016-10-06 00:49:52	2	41000.00	ku@titdotres.gov	40	08161631	P0000065
V000000671	2016-10-03	2016-10-04 23:20:57	2	23000.00	cin@dorpaz.org	20	08926458	P0000081
V000000672	2016-09-12	2016-09-14 07:17:53	2	85000.00	zu@guji.gov	80	08995128	P0000142
V000000673	2016-09-28	2016-09-30 07:35:26	2	89000.00	gurab@suvelelub.io	80	08225445	P0000098
V000000674	2016-09-15	2016-09-17 13:12:35	2	85000.00	is@fe.com	80	08473624	P0000142
V000000675	2016-09-01	2016-09-03 21:29:06	2	89000.00	neloke@lu.edu	80	08558386	P0000054
V000000676	2016-09-09	2016-09-11 03:04:58	1	26000.00	tog@siv.org	20	08143122	P0000034
V000000677	2016-10-05	2016-10-07 21:09:06	1	73000.00	uwa@tagokraw.gov	70	08326725	P0000130
V000000678	2016-10-23	2016-10-25 20:04:03	2	53000.00	oceob@codetcuh.com	50	08589544	P0000078
V000000679	2016-10-17	2016-10-19 08:46:31	1	59000.00	re@wumop.com	50	08612463	P0000001
V000000680	2016-09-19	2016-09-21 19:27:25	1	51000.00	vacawhep@kalagwo.com	50	08336979	P0000026
V000000681	2016-08-13	2016-08-15 18:04:30	2	24000.00	laligni@hukru.io	20	08864627	P0000073
V000000682	2016-07-22	2016-07-24 03:18:30	2	75000.00	fiw@mamko.co.uk	70	08758516	P0000199
V000000683	2016-09-27	2016-09-29 15:38:48	2	48000.00	hu@eha.co.uk	40	08276391	P0000164
V000000684	2016-08-27	2016-08-28 23:20:00	1	11000.00	halilil@wuturo.net	10	08325684	P0000020
V000000685	2016-10-09	2016-10-11 11:20:09	1	23000.00	ez@upzi.com	20	08967641	P0000081
V000000686	2016-08-20	2016-08-22 22:34:01	1	45000.00	obofac@fel.edu	40	08873866	P0000008
V000000687	2016-09-04	2016-09-06 16:21:01	1	43000.00	costoja@waw.gov	40	08144558	P0000096
V000000688	2016-07-28	2016-07-30 03:49:17	1	23000.00	hesnaji@ve.net	20	08962821	P0000048
V000000689	2016-07-22	2016-07-24 18:20:33	1	76000.00	po@fahida.co.uk	70	08983669	P0000023
V000000690	2016-08-15	2016-08-17 16:28:00	1	56000.00	gucas@vi.net	50	08663689	P0000112
V000000691	2016-07-31	2016-08-02 03:31:44	2	15000.00	kub@etdav.org	10	08731491	P0000064
V000000692	2016-08-09	2016-08-11 07:48:45	2	26000.00	hisudo@ohlu.edu	20	08242282	P0000002
V000000693	2016-08-02	2016-08-04 21:39:59	2	21000.00	co@lep.gov	20	08622276	P0000108
V000000694	2016-08-30	2016-09-01 00:01:05	2	26000.00	evisep@uwre.net	20	08885119	P0000002
V000000695	2016-10-04	2016-10-06 22:59:20	2	21000.00	socoma@laomigez.io	20	08378459	P0000014
V000000696	2016-09-19	2016-09-21 09:36:25	2	21000.00	guz@tuve.com	20	08943529	P0000014
V000000697	2016-08-05	2016-08-07 20:39:36	2	66000.00	jude@fok.edu	60	08647969	P0000011
V000000698	2016-10-03	2016-10-05 18:12:30	1	14000.00	ur@ru.com	10	08638549	P0000059
V000000699	2016-09-15	2016-09-18 14:09:22	2	45000.00	nifu@lotedpoh.gov	40	08226177	P0000049
V000000700	2016-07-23	2016-07-25 04:49:50	1	74000.00	erfi@zokiw.co.uk	70	08766523	P0000131
V000000701	2016-10-20	2016-10-22 00:31:42	1	58000.00	ji@kozko.co.uk	50	08633246	P0000102
V000000702	2016-10-25	2016-10-27 12:30:17	2	26000.00	miwesa@ros.org	20	08534183	P0000085
V000000703	2016-08-16	2016-08-18 05:45:21	2	55000.00	kanaz@mema.org	50	08213266	P0000018
V000000704	2016-07-26	2016-07-28 13:26:14	2	45000.00	if@azo.gov	40	08631744	P0000008
V000000705	2016-10-15	2016-10-17 06:23:22	2	43000.00	efo@ran.com	40	08946846	P0000096
V000000706	2016-08-01	2016-08-03 11:49:54	2	56000.00	tufilco@luri.edu	50	08766156	P0000112
V000000707	2016-07-21	2016-07-23 18:33:07	2	88000.00	ba@nod.gov	80	08821161	P0000032
V000000708	2016-08-29	2016-08-31 04:15:23	1	29000.00	gurab@suvelelub.io	20	08471289	P0000152
V000000709	2016-08-21	2016-08-23 18:38:07	2	89000.00	lefosmo@fawriil.co.uk	80	08263696	P0000098
V000000710	2016-07-28	2016-07-30 19:32:11	2	25000.00	if@azo.gov	20	08445345	P0000031
V000000711	2016-10-13	2016-10-15 05:42:54	2	39000.00	dipcoc@ziszipmes.io	30	08679185	P0000050
V000000712	2016-07-22	2016-07-24 12:44:53	2	79000.00	aveju@ezihocle.co.uk	70	08421618	P0000080
V000000713	2016-10-15	2016-10-17 17:25:43	1	74000.00	vig@dehede.org	70	08349245	P0000099
V000000714	2016-10-06	2016-10-08 05:33:51	2	77000.00	evdivcu@jak.io	70	08162396	P0000046
V000000715	2016-08-18	2016-08-20 04:05:39	2	59000.00	loh@cetumi.com	50	08854316	P0000001
V000000716	2016-08-21	2016-08-23 04:58:35	2	21000.00	sadhupac@zon.edu	20	08388572	P0000014
V000000717	2016-08-25	2016-08-27 21:58:03	2	82000.00	kanaz@mema.org	80	08281377	P0000163
V000000718	2016-09-26	2016-09-28 18:34:07	2	95000.00	jotugve@oclekuju.io	90	08685969	P0000114
V000000719	2016-09-21	2016-09-23 01:18:05	2	92000.00	ew@egzo.gov	90	08731172	P0000090
V000000720	2016-08-03	2016-08-05 08:36:24	2	73000.00	ijo@sisfopbup.org	70	08577376	P0000187
V000000721	2016-10-08	2016-10-10 18:42:27	2	64000.00	mewugek@sojkatta.io	60	08983344	P0000007
V000000722	2016-08-11	2016-08-13 04:45:02	1	51000.00	ji@kozko.co.uk	50	08772152	P0000077
V000000723	2016-10-21	2016-10-22 23:29:18	2	56000.00	fasepjev@ogafi.net	50	08869961	P0000188
V000000724	2016-09-09	2016-09-11 10:31:12	2	72000.00	fasepjev@ogafi.net	70	08963894	P0000144
V000000725	2016-09-27	2016-09-29 22:06:04	1	23000.00	uk@kiko.co.uk	20	08518194	P0000195
V000000726	2016-08-28	2016-08-31 22:00:30	2	68000.00	idudovje@wadmisre.co.uk	60	08672529	P0000117
V000000727	2016-09-30	2016-10-02 21:09:14	1	48000.00	pi@kekbu.io	40	08531782	P0000093
V000000728	2016-08-21	2016-08-24 21:40:23	1	12000.00	oficaznaf@lomu.org	10	08976814	P0000042
V000000729	2016-10-17	2016-10-19 13:03:11	2	89000.00	kon@dow.edu	80	08168778	P0000146
V000000730	2016-10-23	2016-10-24 23:37:37	1	83000.00	efo@ran.com	80	08493626	P0000045
V000000731	2016-08-08	2016-08-09 23:29:52	1	41000.00	obofac@fel.edu	40	08511779	P0000184
V000000732	2016-08-14	2016-08-16 10:19:07	1	36000.00	tol@tukon.edu	30	08684798	P0000016
V000000733	2016-08-25	2016-08-27 13:47:54	1	17000.00	le@lopdooc.com	10	08926985	P0000138
V000000734	2016-07-23	2016-07-25 02:57:15	2	61000.00	tipdabit@ogazov.com	60	08812838	P0000120
V000000735	2016-09-04	2016-09-06 14:19:27	1	79000.00	liru@op.co.uk	70	08443491	P0000183
V000000736	2016-10-11	2016-10-13 08:56:21	1	32000.00	hazut@ezhofid.co.uk	30	08329522	P0000127
V000000737	2016-10-17	2016-10-19 04:29:49	1	74000.00	goufaut@luwab.co.uk	70	08544567	P0000099
V000000738	2016-10-13	2016-10-15 12:54:19	1	66000.00	subzola@kacjis.edu	60	08965467	P0000011
V000000739	2016-09-16	2016-09-18 13:06:11	2	88000.00	ginid@unfioc.org	80	08262796	P0000132
V000000740	2016-08-14	2016-08-16 00:39:03	1	41000.00	ivduse@zuhew.org	40	08951484	P0000184
V000000741	2016-07-27	2016-07-29 17:05:29	2	16000.00	uduhet@pomwic.org	10	08315599	P0000006
V000000742	2016-09-10	2016-09-12 16:17:52	1	21000.00	efuis@uc.co.uk	20	08459238	P0000014
V000000743	2016-09-13	2016-09-15 15:18:37	2	91000.00	idfolo@kori.co.uk	90	08851316	P0000022
V000000744	2016-09-01	2016-09-02 23:47:54	2	52000.00	sadhupac@zon.edu	50	08636233	P0000155
V000000745	2016-08-22	2016-08-24 03:14:29	2	54000.00	tu@pevu.edu	50	08737755	P0000092
V000000746	2016-10-10	2016-10-12 13:17:20	2	48000.00	it@kaf.io	40	08115967	P0000093
V000000747	2016-10-07	2016-10-09 15:27:52	1	56000.00	oficaznaf@lomu.org	50	08153143	P0000188
V000000748	2016-10-22	2016-10-24 12:30:10	1	11000.00	udne@donazmuv.io	10	08738659	P0000124
V000000749	2016-10-23	2016-10-25 01:23:23	1	58000.00	uduhet@pomwic.org	50	08129869	P0000137
V000000750	2016-08-08	2016-08-10 20:56:15	2	17000.00	le@lopdooc.com	10	08571646	P0000017
V000000751	2016-09-22	2016-09-24 15:30:10	2	15000.00	je@eno.net	10	08762249	P0000149
V000000752	2016-08-07	2016-08-09 20:55:20	1	74000.00	hovarwuh@sup.io	70	08263541	P0000119
V000000753	2016-09-15	2016-09-17 19:33:20	1	58000.00	idudovje@wadmisre.co.uk	50	08635125	P0000102
V000000754	2016-10-20	2016-10-22 09:00:37	2	26000.00	hu@zusrusiw.edu	20	08219673	P0000002
V000000755	2016-09-10	2016-09-12 21:31:21	2	64000.00	ikruj@sapanti.com	60	08651355	P0000060
V000000756	2016-10-10	2016-10-12 12:22:50	1	48000.00	idudovje@wadmisre.co.uk	40	08941957	P0000044
V000000757	2016-07-27	2016-07-29 19:26:25	1	25000.00	na@tazevze.com	20	08858366	P0000106
V000000758	2016-07-20	2016-07-22 01:25:51	2	14000.00	aveju@ezihocle.co.uk	10	08874929	P0000059
V000000759	2016-09-24	2016-09-26 03:36:57	2	58000.00	dup@nazepad.net	50	08578862	P0000170
V000000760	2016-10-28	2016-10-30 00:11:39	1	51000.00	jawantu@siziin.io	50	08976744	P0000026
V000000761	2016-08-31	2016-09-02 01:41:24	1	45000.00	inu@fo.gov	40	08312346	P0000008
V000000762	2016-07-29	2016-07-31 20:04:37	2	64000.00	pagmu@ka.edu	60	08238663	P0000060
V000000763	2016-10-17	2016-10-19 10:32:04	2	93000.00	cileiji@nek.edu	90	08516228	P0000013
V000000764	2016-08-28	2016-08-30 18:50:02	1	57000.00	ejoha@acsudo.net	50	08138819	P0000012
V000000765	2016-09-07	2016-09-09 14:48:53	1	67000.00	ummi@dur.gov	60	08592257	P0000113
V000000766	2016-10-01	2016-10-03 12:03:57	2	37000.00	nepic@zolozwuf.gov	30	08331374	P0000071
V000000767	2016-08-20	2016-08-22 12:20:34	1	23000.00	sud@ul.org	20	08427313	P0000081
V000000768	2016-08-17	2016-08-19 18:46:32	1	79000.00	igupu@ogezegpod.gov	70	08453455	P0000080
V000000769	2016-09-04	2016-09-06 02:51:51	1	97000.00	re@wumop.com	90	08977667	P0000082
V000000770	2016-08-13	2016-08-15 04:36:26	2	72000.00	it@kaf.io	70	08417391	P0000144
V000000771	2016-07-23	2016-07-25 12:13:39	1	21000.00	socoma@laomigez.io	20	08552141	P0000108
V000000772	2016-10-26	2016-10-29 17:36:45	2	23000.00	evisep@uwre.net	20	08264387	P0000195
V000000773	2016-10-16	2016-10-18 09:42:01	1	68000.00	ceiwosak@kucuaj.net	60	08638254	P0000117
V000000774	2016-07-30	2016-08-02 06:38:08	1	77000.00	esoli@velmurap.io	70	08956163	P0000171
V000000775	2016-10-13	2016-10-15 02:21:55	1	77000.00	liru@op.co.uk	70	08184389	P0000126
V000000776	2016-08-31	2016-09-02 01:45:57	2	17000.00	pi@kekbu.io	10	08129497	P0000138
V000000777	2016-08-11	2016-08-13 05:35:06	2	53000.00	efuis@uc.co.uk	50	08647966	P0000087
V000000778	2016-08-03	2016-08-05 07:37:27	1	45000.00	se@tedu.io	40	08852539	P0000049
V000000779	2016-09-26	2016-09-28 09:30:16	2	78000.00	ca@jon.io	70	08236486	P0000179
V000000780	2016-10-21	2016-10-23 16:06:02	2	38000.00	ponuv@howup.co.uk	30	08994754	P0000086
V000000781	2016-08-05	2016-08-07 04:13:11	2	82000.00	jibloc@ol.co.uk	80	08197813	P0000030
V000000782	2016-09-08	2016-09-10 10:48:43	1	11000.00	woatva@wodlopwep.edu	10	08195466	P0000129
V000000783	2016-08-06	2016-08-08 21:21:43	2	82000.00	liru@op.co.uk	80	08894742	P0000163
V000000784	2016-08-25	2016-08-27 00:54:04	1	56000.00	tog@siv.org	50	08537455	P0000166
V000000785	2016-10-20	2016-10-22 00:50:03	1	11000.00	jawantu@siziin.io	10	08565818	P0000178
V000000786	2016-09-18	2016-09-20 07:44:58	1	59000.00	meiru@dofigoj.org	50	08225931	P0000172
V000000787	2016-08-04	2016-08-06 04:33:18	1	77000.00	ilam@hadjaus.org	70	08463157	P0000046
V000000788	2016-08-22	2016-08-24 03:20:24	1	12000.00	laligni@hukru.io	10	08725779	P0000042
V000000789	2016-10-02	2016-10-03 23:47:45	1	48000.00	deretjaw@gek.edu	40	08366773	P0000093
V000000790	2016-08-14	2016-08-16 14:38:21	1	85000.00	pidcem@bucfauru.com	80	08428369	P0000142
V000000791	2016-10-05	2016-10-07 23:10:23	2	65000.00	zup@ab.com	60	08958572	P0000068
V000000792	2016-09-09	2016-09-11 23:01:27	2	13000.00	sulovsi@ewigi.edu	10	08193484	P0000161
V000000793	2016-08-06	2016-08-08 16:43:33	2	64000.00	hu@zusrusiw.edu	60	08847936	P0000105
V000000794	2016-10-01	2016-10-03 05:32:57	2	26000.00	jumovbot@ul.edu	20	08194139	P0000009
V000000795	2016-08-29	2016-08-31 06:40:21	1	77000.00	fura@upipu.gov	70	08288862	P0000171
V000000796	2016-08-25	2016-08-27 19:53:38	1	74000.00	hemvofvod@osulojmov.com	70	08216423	P0000131
V000000797	2016-10-25	2016-10-27 08:28:12	1	23000.00	uphagfa@reluba.io	20	08349376	P0000148
V000000798	2016-09-18	2016-09-20 07:20:23	1	31000.00	leluwi@honwi.com	30	08282463	P0000190
V000000799	2016-08-07	2016-08-09 01:28:59	1	41000.00	waruk@tu.org	40	08593318	P0000065
V000000800	2016-09-03	2016-09-05 05:16:54	1	74000.00	vuc@ifef.net	70	08135354	P0000119
V000000801	2016-09-07	2016-09-09 01:58:52	2	61000.00	ceiwosak@kucuaj.net	60	08931172	P0000121
V000000802	2016-10-28	2016-10-30 13:50:56	1	29000.00	vobvi@vo.org	20	08223292	P0000063
V000000803	2016-07-31	2016-08-02 15:08:24	1	91000.00	le@lopdooc.com	90	08717731	P0000200
V000000804	2016-08-13	2016-08-15 10:55:43	2	86000.00	eptew@os.org	80	08992488	P0000021
V000000805	2016-08-12	2016-08-14 02:29:47	2	17000.00	nimzin@guznerepi.edu	10	08645554	P0000110
V000000806	2016-10-19	2016-10-21 04:36:11	2	38000.00	lefosmo@fawriil.co.uk	30	08384674	P0000086
V000000807	2016-08-18	2016-08-20 10:31:27	2	64000.00	ewnor@koc.org	60	08561797	P0000007
V000000808	2016-10-14	2016-10-16 04:23:30	2	36000.00	ewebeduc@heceh.co.uk	30	08996593	P0000157
V000000809	2016-08-28	2016-08-30 10:09:09	1	18000.00	juciddu@difuci.org	10	08961272	P0000084
V000000810	2016-08-20	2016-08-22 00:45:14	2	17000.00	fasepjev@ogafi.net	10	08161623	P0000138
V000000811	2016-09-28	2016-09-29 23:59:46	2	89000.00	giwjufu@mahuh.net	80	08644749	P0000054
V000000812	2016-08-02	2016-08-04 23:09:21	1	84000.00	zu@guji.gov	80	08159696	P0000185
V000000813	2016-08-12	2016-08-14 15:00:10	1	56000.00	uk@kiko.co.uk	50	08131241	P0000055
V000000814	2016-10-03	2016-10-05 03:41:55	1	48000.00	uwa@tagokraw.gov	40	08133933	P0000164
V000000815	2016-09-01	2016-09-03 06:44:55	2	26000.00	oceob@codetcuh.com	20	08784466	P0000002
V000000816	2016-10-16	2016-10-18 23:07:25	2	48000.00	mireg@nevwacko.edu	40	08599945	P0000079
V000000817	2016-07-21	2016-07-23 18:58:35	1	45000.00	ho@jesopi.gov	40	08845329	P0000049
V000000818	2016-09-30	2016-10-02 14:02:55	2	17000.00	mocsib@cirgo.io	10	08799318	P0000189
V000000819	2016-08-26	2016-08-28 08:55:23	1	41000.00	kehu@pom.edu	40	08682949	P0000168
V000000820	2016-10-23	2016-10-25 02:58:08	1	56000.00	ciwimnim@wolihvo.io	50	08824165	P0000188
V000000821	2016-08-16	2016-08-19 13:54:23	1	34000.00	uphagfa@reluba.io	30	08734874	P0000036
V000000822	2016-07-22	2016-07-24 07:41:08	1	72000.00	eribip@dim.edu	70	08883837	P0000145
V000000823	2016-09-06	2016-09-08 03:38:36	2	41000.00	ciraofi@wikvuut.io	40	08187975	P0000091
V000000824	2016-08-06	2016-08-08 13:34:09	2	58000.00	jujrecwo@veofowe.com	50	08417468	P0000150
V000000825	2016-09-28	2016-09-30 01:04:14	1	77000.00	hesnaji@ve.net	70	08391313	P0000169
V000000826	2016-09-07	2016-09-09 21:49:38	1	11000.00	solodsi@vinepmid.org	10	08139546	P0000178
V000000827	2016-08-24	2016-08-26 11:11:27	1	16000.00	zonur@vocumotoz.net	10	08744242	P0000006
V000000828	2016-08-27	2016-08-29 17:09:11	1	48000.00	se@tedu.io	40	08528566	P0000140
V000000829	2016-09-19	2016-09-21 00:07:18	2	72000.00	powjajrak@el.io	70	08789447	P0000144
V000000830	2016-10-04	2016-10-06 21:42:22	1	11000.00	ivnu@viset.edu	10	08243584	P0000020
V000000831	2016-10-20	2016-10-22 21:03:32	1	52000.00	war@fohrahsin.net	50	08293334	P0000162
V000000832	2016-09-21	2016-09-23 18:39:02	2	56000.00	tipdabit@ogazov.com	50	08511311	P0000112
V000000833	2016-10-06	2016-10-08 04:01:17	1	36000.00	vacawhep@kalagwo.com	30	08315197	P0000095
V000000834	2016-08-18	2016-08-20 12:41:43	2	51000.00	efo@ran.com	50	08487984	P0000177
V000000835	2016-09-16	2016-09-18 02:46:42	2	56000.00	le@lopdooc.com	50	08784287	P0000166
V000000836	2016-09-02	2016-09-04 06:08:39	2	92000.00	paweifa@gusak.com	90	08551149	P0000035
V000000837	2016-10-16	2016-10-18 14:29:43	1	58000.00	se@tedu.io	50	08299541	P0000137
V000000838	2016-07-24	2016-07-26 01:25:36	2	76000.00	ijages@lomulel.io	70	08619914	P0000061
V000000839	2016-10-04	2016-10-06 14:51:26	1	29000.00	nimzin@guznerepi.edu	20	08162537	P0000152
V000000840	2016-07-31	2016-08-02 12:45:51	2	41000.00	ju@zo.com	40	08279588	P0000168
V000000841	2016-09-29	2016-10-01 00:52:27	2	18000.00	ciraofi@wikvuut.io	10	08952362	P0000052
V000000842	2016-10-24	2016-10-26 00:56:26	1	89000.00	ciraofi@wikvuut.io	80	08568969	P0000098
V000000843	2016-07-23	2016-07-25 14:18:04	2	25000.00	mezhi@te.edu	20	08463571	P0000070
V000000844	2016-09-21	2016-09-23 15:52:00	1	45000.00	gukum@ficoofo.co.uk	40	08976539	P0000049
V000000845	2016-10-11	2016-10-14 03:33:53	1	14000.00	il@caguri.edu	10	08489124	P0000167
V000000846	2016-09-14	2016-09-16 13:32:33	2	11000.00	goufaut@luwab.co.uk	10	08176933	P0000020
V000000847	2016-08-02	2016-08-04 22:45:13	2	64000.00	ji@fogukhu.com	60	08458498	P0000089
V000000848	2016-07-21	2016-07-23 08:20:44	1	96000.00	jotugve@oclekuju.io	90	08356473	P0000005
V000000849	2016-10-21	2016-10-23 19:36:01	1	77000.00	costoja@waw.gov	70	08248299	P0000088
V000000850	2016-10-17	2016-10-19 21:16:04	2	23000.00	arpil@nasvunub.com	20	08387212	P0000174
V000000851	2016-08-01	2016-08-03 05:09:50	1	92000.00	il@caguri.edu	90	08638782	P0000160
V000000852	2016-07-30	2016-08-01 06:40:28	1	95000.00	jumfel@nulueh.net	90	08451316	P0000114
V000000853	2016-09-12	2016-09-14 17:40:14	1	32000.00	tu@pevu.edu	30	08126619	P0000037
V000000854	2016-10-04	2016-10-06 12:15:36	1	88000.00	tumog@heovazor.edu	80	08155224	P0000132
V000000855	2016-10-13	2016-10-15 10:52:23	2	74000.00	jawantu@siziin.io	70	08126768	P0000099
V000000856	2016-09-13	2016-09-15 05:44:30	2	72000.00	faopidu@uhi.com	70	08923448	P0000074
V000000857	2016-08-04	2016-08-06 13:03:06	2	41000.00	dup@ejmameh.io	40	08682598	P0000168
V000000858	2016-09-03	2016-09-06 20:19:57	2	39000.00	efuis@uc.co.uk	30	08252238	P0000027
V000000859	2016-09-05	2016-09-07 06:09:04	1	29000.00	miwesa@ros.org	20	08622256	P0000159
V000000860	2016-08-10	2016-08-12 03:26:17	1	89000.00	jukuctig@bucolza.org	80	08933286	P0000075
V000000861	2016-09-10	2016-09-12 02:20:32	1	23000.00	gucas@vi.net	20	08687794	P0000195
V000000862	2016-07-21	2016-07-23 17:30:00	1	81000.00	dup@ejmameh.io	80	08989936	P0000019
V000000863	2016-08-10	2016-08-12 03:03:09	1	63000.00	lefosmo@fawriil.co.uk	60	08793553	P0000015
V000000864	2016-09-01	2016-09-03 14:38:00	2	29000.00	ewe@manog.co.uk	20	08439794	P0000159
V000000865	2016-07-22	2016-07-24 02:51:17	2	99000.00	map@levuv.io	90	08459749	P0000176
V000000866	2016-08-26	2016-08-28 15:15:02	2	37000.00	gaupe@uf.org	30	08388354	P0000125
V000000867	2016-10-19	2016-10-21 16:46:22	2	91000.00	uwa@tagokraw.gov	90	08541818	P0000022
V000000868	2016-08-07	2016-08-09 10:29:01	1	17000.00	nu@erajod.org	10	08844173	P0000110
V000000869	2016-07-20	2016-07-22 12:04:15	2	51000.00	uphagfa@reluba.io	50	08987518	P0000077
V000000870	2016-10-11	2016-10-13 18:11:03	2	18000.00	uduhet@pomwic.org	10	08914929	P0000181
V000000871	2016-09-05	2016-09-07 05:09:23	2	32000.00	pufvep@zagicmac.io	30	08765127	P0000037
V000000872	2016-08-12	2016-08-14 07:41:05	2	54000.00	nu@erajod.org	50	08284873	P0000092
V000000873	2016-08-12	2016-08-14 04:46:54	2	76000.00	malvo@gigdelkun.com	70	08131829	P0000057
V000000874	2016-10-26	2016-10-28 13:39:13	1	21000.00	ewe@manog.co.uk	20	08875224	P0000108
V000000875	2016-10-01	2016-10-03 12:26:20	1	29000.00	va@bebim.net	20	08462999	P0000063
V000000876	2016-10-16	2016-10-18 14:01:46	2	41000.00	igupu@ogezegpod.gov	40	08249493	P0000091
V000000877	2016-09-12	2016-09-14 19:30:48	2	29000.00	dup@nazepad.net	20	08165249	P0000194
V000000878	2016-10-17	2016-10-19 14:37:40	2	39000.00	kodon@jeej.com	30	08193455	P0000039
V000000879	2016-09-30	2016-10-02 06:35:36	1	51000.00	wimfi@derwejgen.co.uk	50	08655159	P0000026
V000000880	2016-08-20	2016-08-22 04:04:14	2	26000.00	pufvep@zagicmac.io	20	08993841	P0000002
V000000881	2016-09-14	2016-09-16 10:28:20	2	51000.00	sisi@povzurudo.io	50	08783664	P0000077
V000000882	2016-10-25	2016-10-27 06:29:51	1	52000.00	papa@jorisuz.co.uk	50	08633399	P0000162
V000000883	2016-10-25	2016-10-27 03:42:51	2	72000.00	tegesemo@roenvuj.edu	70	08259129	P0000074
V000000884	2016-08-05	2016-08-07 06:34:07	1	25000.00	vuc@ifef.net	20	08346573	P0000070
V000000885	2016-10-06	2016-10-08 11:38:19	1	96000.00	hu@eha.co.uk	90	08639268	P0000116
V000000886	2016-09-24	2016-09-26 06:46:02	2	61000.00	evomobmi@sepete.com	60	08378972	P0000120
V000000887	2016-07-21	2016-07-23 18:27:27	1	79000.00	aveju@ezihocle.co.uk	70	08378937	P0000134
V000000888	2016-08-24	2016-08-25 23:52:18	1	89000.00	costoja@waw.gov	80	08492881	P0000146
V000000889	2016-08-13	2016-08-15 22:44:35	2	79000.00	paweifa@gusak.com	70	08176296	P0000062
V000000890	2016-09-10	2016-09-12 19:00:12	1	48000.00	mireg@nevwacko.edu	40	08451552	P0000128
V000000891	2016-09-27	2016-09-29 16:10:53	1	33000.00	vacawhep@kalagwo.com	30	08393375	P0000173
V000000892	2016-09-05	2016-09-07 21:07:22	2	36000.00	ceiwosak@kucuaj.net	30	08827281	P0000095
V000000893	2016-08-28	2016-08-30 05:22:27	2	49000.00	tu@pevu.edu	40	08685743	P0000103
V000000894	2016-09-19	2016-09-21 03:16:27	1	77000.00	gukum@ficoofo.co.uk	70	08217435	P0000126
V000000895	2016-08-18	2016-08-20 07:44:40	1	77000.00	evisep@uwre.net	70	08287767	P0000088
V000000896	2016-08-13	2016-08-15 06:47:33	1	93000.00	if@azo.gov	90	08729494	P0000013
V000000897	2016-07-23	2016-07-25 10:31:51	1	79000.00	jawantu@siziin.io	70	08554942	P0000101
V000000898	2016-09-10	2016-09-12 18:40:16	1	19000.00	mewugek@sojkatta.io	10	08586554	P0000193
V000000899	2016-08-04	2016-08-07 19:45:26	1	85000.00	jidfow@ditiduze.com	80	08862237	P0000038
V000000900	2016-10-08	2016-10-10 09:27:21	1	27000.00	woatva@wodlopwep.edu	20	08867196	P0000192
V000000901	2016-08-15	2016-08-17 01:38:14	1	93000.00	ijo@sisfopbup.org	90	08673586	P0000013
V000000902	2016-08-18	2016-08-20 19:45:37	1	92000.00	vobvi@vo.org	90	08317844	P0000035
V000000903	2016-09-12	2016-09-14 12:43:17	2	48000.00	gurezi@folcib.gov	40	08136948	P0000164
V000000904	2016-10-15	2016-10-17 20:36:38	2	12000.00	jotugve@oclekuju.io	10	08855854	P0000042
V000000905	2016-08-10	2016-08-12 06:33:09	1	37000.00	rulsiw@lecwuzla.gov	30	08482149	P0000125
V000000906	2016-09-05	2016-09-07 03:35:20	1	11000.00	evdivcu@jak.io	10	08783187	P0000178
V000000907	2016-08-26	2016-08-28 19:28:11	2	23000.00	cileiji@nek.edu	20	08588538	P0000048
V000000908	2016-10-24	2016-10-26 07:27:22	2	26000.00	ji@ihmomih.io	20	08633949	P0000085
V000000909	2016-09-19	2016-09-21 03:33:38	2	92000.00	neloke@lu.edu	90	08294321	P0000160
V000000910	2016-09-16	2016-09-18 05:09:50	2	42000.00	murgupo@hiiluhe.net	40	08782188	P0000047
V000000911	2016-09-21	2016-09-23 15:59:28	1	45000.00	fokjar@ril.gov	40	08931766	P0000008
V000000912	2016-09-06	2016-09-08 11:39:40	1	42000.00	eribip@dim.edu	40	08545636	P0000047
V000000913	2016-10-17	2016-10-19 13:19:28	1	23000.00	cej@juz.co.uk	20	08932785	P0000148
V000000914	2016-10-26	2016-10-28 02:52:45	1	78000.00	it@kaf.io	70	08752819	P0000179
V000000915	2016-09-19	2016-09-20 23:18:29	1	86000.00	cuberuta@biepi.co.uk	80	08897836	P0000021
V000000916	2016-08-21	2016-08-23 14:58:04	2	52000.00	uduhet@pomwic.org	50	08955948	P0000162
V000000917	2016-08-12	2016-08-14 11:36:07	2	14000.00	jiwuol@vi.io	10	08219498	P0000059
V000000918	2016-09-23	2016-09-25 22:37:25	2	56000.00	tumog@heovazor.edu	50	08631555	P0000112
V000000919	2016-08-03	2016-08-05 07:36:22	2	74000.00	anowuw@sabapmi.org	70	08883471	P0000099
V000000920	2016-09-21	2016-09-23 05:37:21	1	74000.00	vim@kieh.gov	70	08236745	P0000099
V000000921	2016-10-15	2016-10-17 09:02:47	2	48000.00	lud@wubra.gov	40	08985337	P0000079
V000000922	2016-08-24	2016-08-26 16:10:04	2	18000.00	remuw@jopuru.io	10	08245929	P0000181
V000000923	2016-09-13	2016-09-15 06:51:00	1	85000.00	ijages@lomulel.io	80	08669324	P0000142
V000000924	2016-10-18	2016-10-20 10:26:42	1	33000.00	tog@siv.org	30	08968297	P0000173
V000000925	2016-08-31	2016-09-02 14:35:40	1	89000.00	nimzin@guznerepi.edu	80	08997186	P0000098
V000000926	2016-07-30	2016-08-01 09:00:15	1	77000.00	uphagfa@reluba.io	70	08776869	P0000126
V000000927	2016-07-22	2016-07-24 04:30:43	2	15000.00	waruk@tu.org	10	08644693	P0000058
V000000928	2016-09-30	2016-10-02 04:32:58	2	41000.00	sijtu@ja.io	40	08916194	P0000122
V000000929	2016-08-03	2016-08-05 19:41:12	2	76000.00	sadhupac@zon.edu	70	08175332	P0000061
V000000930	2016-10-13	2016-10-15 09:18:19	2	24000.00	dimkerra@lebog.co.uk	20	08771131	P0000073
V000000931	2016-09-21	2016-09-23 16:22:30	1	34000.00	jujrecwo@veofowe.com	30	08941515	P0000036
V000000932	2016-10-19	2016-10-22 06:18:25	2	34000.00	deretjaw@gek.edu	30	08812734	P0000036
V000000933	2016-08-26	2016-08-28 20:25:54	1	29000.00	il@up.co.uk	20	08627685	P0000063
V000000934	2016-08-10	2016-08-12 00:30:49	2	72000.00	kon@dow.edu	70	08216256	P0000104
V000000935	2016-08-07	2016-08-09 15:27:11	2	84000.00	faopidu@uhi.com	80	08999816	P0000185
V000000936	2016-08-22	2016-08-24 22:19:25	1	17000.00	hovarwuh@sup.io	10	08433769	P0000110
V000000937	2016-10-05	2016-10-07 13:25:48	1	91000.00	no@ugu.com	90	08436166	P0000022
V000000938	2016-09-12	2016-09-14 11:57:52	1	66000.00	ciwimnim@wolihvo.io	60	08327499	P0000118
V000000939	2016-07-29	2016-07-31 19:57:51	2	91000.00	zup@ab.com	90	08337442	P0000022
V000000940	2016-10-05	2016-10-07 13:23:45	2	93000.00	dipcoc@ziszipmes.io	90	08646952	P0000028
V000000941	2016-10-18	2016-10-19 23:18:50	2	58000.00	uc@rebufjen.edu	50	08366566	P0000137
V000000942	2016-09-16	2016-09-18 18:36:34	1	82000.00	kon@dow.edu	80	08125399	P0000163
V000000943	2016-09-20	2016-09-22 09:59:37	2	48000.00	dipcoc@ziszipmes.io	40	08245398	P0000140
V000000944	2016-07-31	2016-08-02 11:05:24	1	54000.00	pi@kekbu.io	50	08286479	P0000097
V000000945	2016-10-02	2016-10-04 09:40:56	1	48000.00	ra@midgabe.edu	40	08283732	P0000044
V000000946	2016-07-25	2016-07-27 13:24:47	1	27000.00	goufaut@luwab.co.uk	20	08417448	P0000069
V000000947	2016-10-22	2016-10-24 17:34:51	1	35000.00	joboav@un.io	30	08346456	P0000133
V000000948	2016-08-17	2016-08-20 22:58:10	1	61000.00	fo@pa.io	60	08193686	P0000121
V000000949	2016-09-30	2016-10-02 16:40:42	2	15000.00	remuw@jopuru.io	10	08133869	P0000043
V000000950	2016-08-20	2016-08-22 06:07:34	1	64000.00	fub@nuwok.io	60	08397187	P0000007
V000000951	2016-07-24	2016-07-26 12:04:22	1	11000.00	jemoh@kawbur.gov	10	08967219	P0000020
V000000952	2016-07-31	2016-08-02 21:42:16	1	11000.00	waruk@tu.org	10	08125585	P0000129
V000000953	2016-10-02	2016-10-04 07:47:56	1	12000.00	dimkerra@lebog.co.uk	10	08645916	P0000191
V000000954	2016-08-10	2016-08-12 18:40:56	2	56000.00	hesnaji@ve.net	50	08558646	P0000166
V000000955	2016-07-30	2016-08-01 11:39:48	1	38000.00	eptew@os.org	30	08911611	P0000151
V000000956	2016-10-09	2016-10-11 19:13:15	2	89000.00	if@azo.gov	80	08451349	P0000146
V000000957	2016-07-21	2016-07-23 08:47:47	2	27000.00	war@fohrahsin.net	20	08447346	P0000192
V000000958	2016-08-03	2016-08-05 14:09:24	1	49000.00	sulovsi@ewigi.edu	40	08993517	P0000083
V000000959	2016-08-30	2016-09-01 16:13:47	1	43000.00	papa@jorisuz.co.uk	40	08232487	P0000096
V000000960	2016-10-08	2016-10-10 00:22:12	1	14000.00	sud@ul.org	10	08753187	P0000040
V000000961	2016-08-09	2016-08-11 01:57:33	1	37000.00	eptew@os.org	30	08537962	P0000125
V000000962	2016-09-10	2016-09-12 10:23:01	1	57000.00	giwjufu@mahuh.net	50	08926681	P0000012
V000000963	2016-07-23	2016-07-25 17:17:16	2	25000.00	ruemo@ru.co.uk	20	08455594	P0000070
V000000964	2016-10-26	2016-10-28 05:12:03	2	26000.00	kehu@pom.edu	20	08297199	P0000034
V000000965	2016-09-24	2016-09-26 15:06:44	1	82000.00	hazut@ezhofid.co.uk	80	08859714	P0000030
V000000966	2016-10-24	2016-10-26 18:58:05	1	13000.00	ced@ego.net	10	08564628	P0000161
V000000967	2016-08-06	2016-08-08 10:34:08	2	74000.00	kibiri@koftere.io	70	08621785	P0000053
V000000968	2016-08-01	2016-08-03 00:19:44	2	36000.00	meiru@dofigoj.org	30	08918848	P0000095
V000000969	2016-09-21	2016-09-23 15:48:54	1	12000.00	jotugve@oclekuju.io	10	08185422	P0000191
V000000970	2016-08-30	2016-09-01 17:16:46	2	11000.00	cito@no.edu	10	08892115	P0000020
V000000971	2016-09-06	2016-09-08 05:59:57	1	72000.00	gita@parwaj.net	70	08387944	P0000145
V000000972	2016-09-24	2016-09-26 09:07:51	2	42000.00	hu@eha.co.uk	40	08225142	P0000047
V000000973	2016-08-29	2016-08-31 06:46:33	2	16000.00	ku@titdotres.gov	10	08975273	P0000196
V000000974	2016-09-01	2016-09-03 18:08:40	1	57000.00	wemufmi@vigurija.gov	50	08884471	P0000012
V000000975	2016-07-30	2016-08-01 20:24:47	2	45000.00	ewe@manog.co.uk	40	08517286	P0000008
V000000976	2016-07-26	2016-07-28 01:26:27	1	51000.00	vig@dehede.org	50	08156784	P0000177
V000000977	2016-08-31	2016-09-02 09:56:52	1	99000.00	fiom@si.org	90	08573734	P0000176
V000000978	2016-10-06	2016-10-08 20:44:58	1	64000.00	owagij@gingetek.io	60	08256115	P0000143
V000000979	2016-08-25	2016-08-27 03:02:18	1	77000.00	rohoci@muza.io	70	08936273	P0000088
V000000980	2016-10-11	2016-10-13 03:44:58	2	41000.00	ivduse@zuhew.org	40	08999766	P0000122
V000000981	2016-08-31	2016-09-02 20:05:18	2	42000.00	ija@ub.net	40	08227284	P0000123
V000000982	2016-09-24	2016-09-26 18:48:58	1	79000.00	fura@upipu.gov	70	08761938	P0000080
V000000983	2016-08-19	2016-08-21 06:25:13	1	48000.00	liru@op.co.uk	40	08217424	P0000164
V000000984	2016-09-23	2016-09-25 10:21:55	1	32000.00	ciraofi@wikvuut.io	30	08546516	P0000135
V000000985	2016-07-25	2016-07-27 19:25:33	1	96000.00	powjajrak@el.io	90	08528619	P0000116
V000000986	2016-08-07	2016-08-09 05:02:07	1	95000.00	tufilco@luri.edu	90	08672785	P0000072
V000000987	2016-07-29	2016-07-31 22:02:08	2	83000.00	kibiri@koftere.io	80	08234695	P0000045
V000000988	2016-09-08	2016-09-10 20:23:57	2	75000.00	gabiro@sez.net	70	08675778	P0000199
V000000989	2016-08-24	2016-08-26 15:15:30	2	35000.00	ija@ub.net	30	08841591	P0000133
V000000990	2016-08-24	2016-08-26 10:10:34	2	95000.00	ciznamluf@penkono.gov	90	08259172	P0000114
V000000991	2016-09-22	2016-09-24 01:12:24	1	89000.00	jidfow@ditiduze.com	80	08481484	P0000098
V000000992	2016-10-17	2016-10-19 11:01:06	1	88000.00	is@fe.com	80	08961134	P0000132
V000000993	2016-09-27	2016-09-29 04:27:52	1	31000.00	lupadiv@ge.com	30	08863722	P0000190
V000000994	2016-09-05	2016-09-07 00:24:20	1	77000.00	jumfel@nulueh.net	70	08718244	P0000171
V000000995	2016-09-07	2016-09-09 17:33:08	2	14000.00	uc@zepunu.org	10	08748467	P0000040
V000000996	2016-09-24	2016-09-26 21:23:09	2	23000.00	kodon@jeej.com	20	08313723	P0000174
V000000997	2016-10-07	2016-10-09 13:34:54	1	32000.00	uphagfa@reluba.io	30	08512543	P0000037
V000000998	2016-10-24	2016-10-26 15:46:03	2	51000.00	ige@ocwope.net	50	08557876	P0000177
V000000999	2016-08-10	2016-08-12 12:07:59	1	89000.00	gurab@suvelelub.io	80	08182593	P0000054
V000001000	2016-08-18	2016-08-20 13:31:56	1	89000.00	sadhupac@zon.edu	80	08113123	P0000054
\.


--
-- Data for Name: transaksi_shipped; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY transaksi_shipped (no_invoice, tanggal, waktu_bayar, status, total_bayar, email_pembeli, nama_toko, alamat_kirim, biaya_kirim, no_resi, nama_jasa_kirim) FROM stdin;
V000000001	2016-09-04	2016-09-04 07:54:55	3	1786000.00	uduhet@pomwic.org	Ulmezle	Glendora, CA 91740	40000.00	RESI000000000001	LION PARCEL
V000000002	2016-08-27	2016-08-27 17:12:27	3	631000.00	cuberuta@biepi.co.uk	Nadagra	8020 Lawrence Rd.?	35000.00	RESI000000000002	PAHAL
V000000003	2016-09-28	2016-12-30 07:54:55	1	1314000.00	je@eno.net	Ihkeadu	El Paso, TX 79930	96000.00	\N	POS PAKET KILAT
V000000004	2016-10-25	2016-10-25 02:40:07	2	508000.00	meiru@dofigoj.org	Diktufina	East Haven, CT 06512	49000.00	\N	PAHAL
V000000005	2016-10-13	2016-10-13 08:00:10	4	345000.00	kodon@jeej.com	Gimmaniko	Elk Grove Village, IL 60007	40000.00	RESI000000000005	JNE REGULER
V000000006	2016-10-23	2016-10-23 10:00:03	3	320000.00	sijtu@ja.io	Cuulaseb	Saint Louis, MO 63109	60000.00	RESI000000000006	POS PAKET KILAT
V000000007	2016-09-28	2016-09-28 23:05:34	4	308000.00	adaerop@ono.net	Hozguna	7388 Hill Field Court?	98000.00	RESI000000000007	J&T EXPRESS
V000000008	2016-10-01	2016-10-01 14:07:13	2	58000.00	ofi@veubuda.org	Sujoze	176 8th Ave.?	32000.00	\N	WAHANA
V000000009	2016-09-23	2016-09-23 20:38:01	2	607000.00	va@bebim.net	Hecgusnum	68 Riverside Dr.?	72000.00	\N	JNE REGULER
V000000010	2016-09-09	2016-09-09 20:00:11	4	83000.00	rofuhbe@vetidi.co.uk	Sujoze	Jackson Heights, NY 11372	63000.00	RESI000000000010	TIKI REGULER
V000000011	2016-10-20	2016-10-20 09:58:46	4	2115000.00	sadhupac@zon.edu	Halaga	Richmond, VA 23223	90000.00	RESI000000000011	JNE YES
V000000012	2016-08-04	2016-08-04 07:44:06	3	528000.00	il@caguri.edu	Nivumat	Fond Du Lac, WI 54935	50000.00	RESI000000000012	LION PARCEL
V000000013	2016-08-13	2016-12-30 07:54:55	1	197000.00	jaolo@fihotnem.net	Publotweb	Casselberry, FL 32707	56000.00	\N	JNE REGULER
V000000014	2016-10-22	2016-10-22 21:48:34	2	2223000.00	leluwi@honwi.com	Tuavted	Springfield Gardens, NY 11413	56000.00	\N	POS PAKET BIASA
V000000015	2016-07-21	2016-07-21 18:47:47	3	1735000.00	jiwuol@vi.io	Lobasil	8522 W. Pacific St.?	28000.00	RESI000000000015	PAHAL
V000000016	2016-09-22	2016-09-22 05:03:16	4	273000.00	mewugek@sojkatta.io	Uguciis	Saint Louis, MO 63109	63000.00	RESI000000000016	TIKI REGULER
V000000017	2016-10-07	2016-10-07 18:28:48	2	3717000.00	uphagfa@reluba.io	Newbimmig	San Antonio, TX 78213	42000.00	\N	POS PAKET BIASA
V000000018	2016-08-28	2016-08-28 00:07:16	2	2124000.00	gukum@ficoofo.co.uk	Gazebzi	491 Sherwood Ave.?	48000.00	\N	JNE REGULER
V000000019	2016-10-10	2016-12-30 07:54:55	1	1830000.00	sepmajbo@racupmib.io	Teocesuh	Teaneck, NJ 07666	56000.00	\N	WAHANA
V000000020	2016-09-24	2016-09-24 14:56:39	2	1399000.00	ikruj@sapanti.com	Favofsi	9973 John Road?	25000.00	\N	JNE OKE
V000000021	2016-08-30	2016-08-30 04:55:18	2	229000.00	jumfel@nulueh.net	Ulmezle	Saint Johns, FL 32259	21000.00	\N	PAHAL
V000000022	2016-07-21	2016-07-21 20:17:03	3	788000.00	ija@ub.net	Akutodror	Terre Haute, IN 47802	72000.00	RESI000000000022	WAHANA
V000000023	2016-10-06	2016-10-06 00:33:39	4	2109000.00	erfi@zokiw.co.uk	Lilhava	458 Second Dr.?	28000.00	RESI000000000023	PAHAL
V000000024	2016-09-06	2016-09-06 18:52:11	4	256000.00	goufaut@luwab.co.uk	Amdicru	Harlingen, TX 78552	56000.00	RESI000000000024	WAHANA
V000000025	2016-07-20	2016-07-20 04:44:01	2	234000.00	jaolo@fihotnem.net	Leobizek	7019 Vale St.?	32000.00	\N	JNE REGULER
V000000026	2016-09-11	2016-09-11 05:43:07	3	334000.00	faopidu@uhi.com	Voewisod	117 Brookside Ave.?	30000.00	RESI000000000026	JNE OKE
V000000027	2016-07-28	2016-07-28 20:47:07	2	1623000.00	miwesa@ros.org	Halaga	Fargo, ND 58102	75000.00	\N	JNE YES
V000000028	2016-09-18	2016-12-30 07:54:55	1	1580000.00	getat@cotehef.org	Halaga	Lake Charles, LA 70605	10000.00	\N	JNE OKE
V000000029	2016-10-11	2016-12-30 07:54:55	1	378000.00	tog@siv.org	Goopifeb	Port Saint Lucie, FL 34952	63000.00	\N	PAHAL
V000000030	2016-07-30	2016-07-30 15:15:49	3	1900000.00	obofac@fel.edu	Tonone	Summerville, SC 29483	112000.00	RESI000000000030	J&T EXPRESS
V000000031	2016-09-27	2016-09-27 04:18:34	3	1378000.00	zut@sazul.co.uk	Newbimmig	81 Hill Field St.?	21000.00	RESI000000000031	POS PAKET BIASA
V000000032	2016-10-18	2016-10-18 19:17:38	2	2469000.00	sisi@povzurudo.io	Cebvolsi	North Miami Beach, FL 33160	80000.00	\N	WAHANA
V000000033	2016-10-22	2016-10-22 19:19:19	4	278000.00	sivugal@opnulaj.gov	Movwukine	Centreville, VA 20120	15000.00	RESI000000000033	JNE OKE
V000000034	2016-08-02	2016-08-02 07:50:35	3	2202000.00	ditka@bajwo.gov	Divvemdaz	787 6th St.?	48000.00	RESI000000000034	JNE REGULER
V000000035	2016-10-18	2016-10-18 05:30:34	3	1146000.00	rulsiw@lecwuzla.gov	Sekgizveb	Southfield, MI 48076	40000.00	RESI000000000035	JNE REGULER
V000000036	2016-09-13	2016-09-13 16:36:20	2	903000.00	ginid@unfioc.org	Uhcowej	7497 Thomas Ave.?	98000.00	\N	J&T EXPRESS
V000000037	2016-08-06	2016-08-06 02:20:21	3	1116000.00	if@azo.gov	Amdicru	Muskogee, OK 74403	56000.00	RESI000000000037	PAHAL
V000000038	2016-10-28	2016-12-30 07:54:55	1	546000.00	tog@siv.org	Podpijzaz	Fond Du Lac, WI 54935	40000.00	\N	JNE REGULER
V000000039	2016-09-28	2016-09-28 22:31:23	4	1813000.00	ciraofi@wikvuut.io	Kahmuso	Phoenixville, PA 19460	28000.00	RESI000000000039	PAHAL
V000000040	2016-08-30	2016-08-30 14:11:02	3	162000.00	ze@erejanek.com	Zafjomvo	Columbus, GA 31904	32000.00	RESI000000000040	JNE REGULER
V000000041	2016-10-24	2016-10-24 16:54:36	4	2365000.00	evisep@uwre.net	Vicgonzop	Edison, NJ 08817	14000.00	RESI000000000041	POS PAKET BIASA
V000000042	2016-09-10	2016-12-30 07:54:55	1	3366000.00	mireg@nevwacko.edu	Cebvolsi	7136 Manchester Street?	48000.00	\N	WAHANA
V000000043	2016-09-25	2016-09-25 15:12:29	2	582000.00	ho@jesopi.gov	Cuulaseb	73 Lakewood Lane?	40000.00	\N	JNE REGULER
V000000044	2016-09-18	2016-09-18 19:04:58	2	83000.00	obofac@fel.edu	Sujoze	7019 Vale St.?	72000.00	\N	TIKI REGULER
V000000045	2016-08-19	2016-12-30 07:54:55	1	1874000.00	jumovbot@ul.edu	Ivsatdof	677 Wakehurst Dr.?	25000.00	\N	JNE OKE
V000000046	2016-07-26	2016-07-26 20:08:37	2	919000.00	il@up.co.uk	Tesozoun	5 S. Hilldale Ave.?	15000.00	\N	JNE OKE
V000000047	2016-10-08	2016-10-08 09:52:10	3	218000.00	giwjufu@mahuh.net	Uciwguc	Wake Forest, NC 27587	50000.00	RESI000000000047	LION PARCEL
V000000048	2016-10-13	2016-10-13 20:48:39	3	345000.00	ur@ru.com	Jucujra	Niles, MI 49120	56000.00	RESI000000000048	PAHAL
V000000049	2016-09-13	2016-09-13 16:48:06	2	2115000.00	heip@cani.com	Zeprinbu	939 Beaver Ridge St.?	64000.00	\N	WAHANA
V000000050	2016-09-30	2016-12-30 07:54:55	1	604000.00	tomva@kej.gov	Weezed	8909 Catherine St.?	35000.00	\N	PAHAL
V000000051	2016-10-09	2016-12-30 07:54:55	1	912000.00	nimzin@guznerepi.edu	Vicgonzop	Salisbury, MD 21801	56000.00	\N	J&T EXPRESS
V000000052	2016-09-29	2016-09-29 09:30:33	2	1138000.00	tuvnej@le.io	Hecgusnum	North Haven, CT 06473	32000.00	\N	JNE REGULER
V000000053	2016-10-18	2016-10-18 05:09:27	3	188000.00	rofuhbe@vetidi.co.uk	Publotweb	43 NW. Oakland St.?	64000.00	RESI000000000053	JNE REGULER
V000000054	2016-09-20	2016-09-20 06:32:16	2	607000.00	no@ugu.com	Mahzegug	Beloit, WI 53511	30000.00	\N	JNE YES
V000000055	2016-08-20	2016-08-20 16:11:53	2	119000.00	eptew@os.org	Sujoze	60 Summerhouse Lane?	80000.00	\N	WAHANA
V000000056	2016-08-11	2016-08-11 09:44:09	3	173000.00	povioge@na.gov	Saolina	845 West Carpenter Ave.?	24000.00	RESI000000000056	JNE REGULER
V000000057	2016-10-16	2016-10-16 04:01:38	2	216000.00	hesnaji@ve.net	Voewisod	96 West Fordham Ave.?	90000.00	\N	JNE YES
V000000058	2016-10-18	2016-10-18 16:16:44	4	1539000.00	nimzin@guznerepi.edu	Ezjeza	Manchester Township, NJ 08759	48000.00	RESI000000000058	WAHANA
V000000059	2016-09-13	2016-09-13 14:25:40	3	2911000.00	jude@fok.edu	Gazebzi	Casselberry, FL 32707	64000.00	RESI000000000059	JNE REGULER
V000000060	2016-07-31	2016-07-31 08:45:30	3	2863000.00	ku@titdotres.gov	Ezjeza	Southampton, PA 18966	30000.00	RESI000000000060	JNE OKE
V000000061	2016-08-15	2016-08-15 13:34:30	4	65000.00	il@up.co.uk	Tukeci	965 Van Dyke Dr.?	63000.00	RESI000000000061	PAHAL
V000000062	2016-07-25	2016-07-25 07:07:58	3	256000.00	va@bebim.net	Publotweb	Dundalk, MD 21222	40000.00	RESI000000000062	JNE REGULER
V000000063	2016-10-19	2016-10-19 09:35:59	3	3067000.00	tuvnej@le.io	Masvebje	42 E. La Sierra Ave.?	15000.00	RESI000000000063	JNE OKE
V000000064	2016-10-24	2016-12-30 07:54:55	1	58000.00	zu@guji.gov	Sujoze	Elizabethton, TN 37643	54000.00	\N	TIKI REGULER
V000000065	2016-10-12	2016-12-30 07:54:55	1	2358000.00	ponuv@howup.co.uk	Rihizaf	Glendora, CA 91740	28000.00	\N	POS PAKET BIASA
V000000066	2016-10-16	2016-10-16 22:20:09	2	884000.00	nu@erajod.org	Rihizaf	7013 West St Paul St.?	35000.00	\N	POS PAKET BIASA
V000000067	2016-10-04	2016-10-04 03:34:15	2	1132000.00	fo@pa.io	Hecgusnum	37 S. Brewery Dr.?	48000.00	\N	JNE REGULER
V000000068	2016-07-31	2016-07-31 15:54:46	3	173000.00	ejiflo@amdof.io	Saolina	Dracut, MA 01826	80000.00	RESI000000000068	JNE REGULER
V000000069	2016-10-23	2016-10-23 04:57:12	2	3165000.00	guz@tuve.com	Newbimmig	2 Primrose Drive?	56000.00	\N	POS PAKET BIASA
V000000070	2016-08-08	2016-08-08 03:21:35	3	132000.00	faopidu@uhi.com	Goopifeb	Hazleton, PA 18201	50000.00	RESI000000000070	LION PARCEL
V000000071	2016-08-29	2016-12-30 07:54:55	1	122000.00	rofuhbe@vetidi.co.uk	Nadcezpip	Hamburg, NY 14075	20000.00	\N	JNE OKE
V000000072	2016-08-19	2016-08-19 14:05:08	2	1932000.00	cej@juz.co.uk	Tonone	9801 Andover Street?	63000.00	\N	POS PAKET BIASA
V000000073	2016-08-29	2016-08-29 06:20:25	2	1466000.00	pufvep@zagicmac.io	Wanuwa	Oak Lawn, IL 60453	21000.00	\N	POS PAKET BIASA
V000000074	2016-09-30	2016-09-30 14:12:06	4	433000.00	bavit@ji.org	Zamafhiz	25 Amherst Avenue?	32000.00	RESI000000000074	JNE REGULER
V000000075	2016-09-11	2016-09-11 05:38:50	3	141000.00	tegesemo@roenvuj.edu	Nefpezi	506 1st Lane?	90000.00	RESI000000000075	JNE YES
V000000076	2016-09-19	2016-09-19 18:12:54	2	1528000.00	kibiri@koftere.io	Zogjowoj	Frankfort, KY 40601	35000.00	\N	POS PAKET BIASA
V000000077	2016-10-21	2016-10-21 02:38:15	3	2368000.00	leluwi@honwi.com	Zonsawne	95 Mulberry Street?	70000.00	RESI000000000077	LION PARCEL
V000000078	2016-10-17	2016-10-17 16:57:59	3	452000.00	mireg@nevwacko.edu	Cuulaseb	8337 Edgefield St.?	24000.00	RESI000000000078	POS PAKET KILAT
V000000079	2016-10-02	2016-10-02 02:33:12	2	1624000.00	faopidu@uhi.com	Nocanad	Carmel, NY 10512	40000.00	\N	WAHANA
V000000080	2016-09-29	2016-09-29 22:18:03	3	1307000.00	ta@roc.io	Diktufina	Wilmette, IL 60091	72000.00	RESI000000000080	WAHANA
V000000081	2016-09-09	2016-09-09 16:45:30	4	2814000.00	efuis@uc.co.uk	Fapapo	45 South Roosevelt Lane?	63000.00	RESI000000000081	PAHAL
V000000082	2016-09-04	2016-09-04 23:06:18	2	3564000.00	cuberuta@biepi.co.uk	Tesozoun	8283 Mechanic St.?	25000.00	\N	JNE OKE
V000000083	2016-10-01	2016-10-01 04:08:00	4	2700000.00	uduhet@pomwic.org	Nocanad	37 S. Brewery Dr.?	72000.00	RESI000000000083	TIKI REGULER
V000000084	2016-09-28	2016-09-28 13:36:20	3	411000.00	ew@egzo.gov	Jucujra	293 Hartford Ave.?	14000.00	RESI000000000084	PAHAL
V000000085	2016-09-23	2016-09-23 13:03:06	3	189000.00	zup@ab.com	Leobizek	Point Pleasant Beach, NJ 08742	40000.00	RESI000000000085	JNE REGULER
V000000086	2016-08-07	2016-08-07 06:27:29	4	2832000.00	uduhet@pomwic.org	Gazebzi	108 Sleepy Hollow St.?	72000.00	RESI000000000086	POS PAKET KILAT
V000000087	2016-09-24	2016-12-30 07:54:55	1	2769000.00	obofac@fel.edu	Divvemdaz	Panama City, FL 32404	72000.00	\N	JNE REGULER
V000000088	2016-09-05	2016-09-05 13:04:48	4	138000.00	ap@gavakeog.gov	Leobizek	Jersey City, NJ 07302	48000.00	RESI000000000088	JNE REGULER
V000000089	2016-07-28	2016-12-30 07:54:55	1	2408000.00	eto@cenjiv.co.uk	Podpijzaz	Lithonia, GA 30038	24000.00	\N	JNE REGULER
V000000090	2016-10-15	2016-10-15 09:22:56	2	134000.00	zonur@vocumotoz.net	Voewisod	152 El Dorado Court?	15000.00	\N	JNE OKE
V000000091	2016-09-20	2016-09-20 04:32:29	2	186000.00	ez@upzi.com	Uguciis	Annandale, VA 22003	81000.00	\N	TIKI REGULER
V000000092	2016-08-21	2016-08-21 22:53:10	4	946000.00	getat@cotehef.org	Girnudike	8854 N. Smoky Hollow Drive?	63000.00	RESI000000000092	TIKI REGULER
V000000093	2016-08-17	2016-08-17 09:59:06	4	983000.00	ez@upzi.com	Vulkewmeg	Clayton, NC 27520	64000.00	RESI000000000093	WAHANA
V000000094	2016-08-25	2016-08-25 07:36:49	2	1970000.00	ju@zo.com	Renroos	20 S. Maiden Street?	40000.00	\N	LION PARCEL
V000000095	2016-09-07	2016-09-07 02:25:01	2	444000.00	jude@fok.edu	Goopifeb	Dundalk, MD 21222	35000.00	\N	PAHAL
V000000096	2016-10-12	2016-10-12 05:38:26	3	922000.00	teguc@pohon.io	Ulmezle	2 Vermont Court?	42000.00	RESI000000000096	PAHAL
V000000097	2016-10-05	2016-10-05 06:35:41	2	1827000.00	mir@ru.org	Podpijzaz	619 High Ridge Drive?	48000.00	\N	JNE REGULER
V000000098	2016-09-27	2016-09-27 11:31:35	2	1101000.00	no@ugu.com	Denhosjil	Clearwater, FL 33756	64000.00	\N	WAHANA
V000000099	2016-07-23	2016-07-23 11:02:09	2	1289000.00	fasepjev@ogafi.net	Tonone	Fargo, ND 58102	42000.00	\N	POS PAKET BIASA
V000000100	2016-09-03	2016-12-30 07:54:55	1	887000.00	pi@kekbu.io	Cebvolsi	Harrison Township, MI 48045	56000.00	\N	PAHAL
V000000101	2016-10-21	2016-10-21 22:17:14	2	1873000.00	kanaz@mema.org	Zeprinbu	877 Sunbeam Street?	32000.00	\N	WAHANA
V000000102	2016-08-26	2016-08-26 14:32:37	2	207000.00	joboav@un.io	Uguciis	60 Summerhouse Lane?	48000.00	\N	WAHANA
V000000103	2016-09-06	2016-09-06 16:59:35	2	869000.00	zup@ab.com	Gasomo	Mount Juliet, TN 37122	150000.00	\N	JNE YES
V000000104	2016-09-04	2016-09-04 05:48:44	2	345000.00	ju@zo.com	Efuwumubi	7958 Schoolhouse Drive?	35000.00	\N	PAHAL
V000000105	2016-08-08	2016-08-08 08:14:32	4	1230000.00	papa@jorisuz.co.uk	Lobasil	Easley, SC 29640	35000.00	RESI000000000105	PAHAL
V000000106	2016-08-27	2016-08-27 12:53:53	4	1161000.00	uduhet@pomwic.org	Renroos	7844 Thomas Dr.?	14000.00	RESI000000000106	PAHAL
V000000107	2016-10-02	2016-12-30 07:54:55	1	946000.00	povioge@na.gov	Girnudike	96 West Fordham Ave.?	63000.00	\N	TIKI REGULER
V000000108	2016-09-02	2016-09-02 13:01:46	2	218000.00	na@tazevze.com	Udaumdog	7844 Thomas Dr.?	40000.00	\N	WAHANA
V000000109	2016-08-27	2016-08-27 09:08:15	4	740000.00	mezhi@te.edu	Ritkezil	Panama City, FL 32404	56000.00	RESI000000000109	WAHANA
V000000110	2016-10-17	2016-10-17 17:38:57	4	102000.00	rulsiw@lecwuzla.gov	Racubuus	Elkton, MD 21921	56000.00	RESI000000000110	POS PAKET BIASA
V000000111	2016-08-25	2016-08-25 14:48:52	3	305000.00	faopidu@uhi.com	Leobizek	Springfield Gardens, NY 11413	24000.00	RESI000000000111	JNE REGULER
V000000112	2016-08-23	2016-08-23 19:49:41	4	1960000.00	eptew@os.org	Ewelimleh	80 S. Addison St.?	64000.00	RESI000000000112	JNE REGULER
V000000113	2016-08-13	2016-08-13 14:01:41	2	210000.00	igupu@ogezegpod.gov	Kalgenit	Port Jefferson Station, NY 11776	40000.00	\N	JNE OKE
V000000114	2016-09-08	2016-09-08 23:00:50	3	1990000.00	sivugal@opnulaj.gov	Binjesra	Tallahassee, FL 32303	63000.00	RESI000000000114	POS PAKET BIASA
V000000115	2016-08-12	2016-08-12 10:06:04	3	762000.00	cej@juz.co.uk	Denhosjil	Independence, KY 41051	35000.00	RESI000000000115	JNE OKE
V000000116	2016-09-11	2016-09-11 10:03:11	4	1422000.00	upu@owfogna.com	Nadagra	Tallahassee, FL 32303	60000.00	RESI000000000116	LION PARCEL
V000000117	2016-08-16	2016-12-30 07:54:55	1	276000.00	bavit@ji.org	Onuegbiw	296 County Ave.?	35000.00	\N	PAHAL
V000000118	2016-07-23	2016-07-23 06:31:16	4	1221000.00	lupadiv@ge.com	Nadagra	Buffalo Grove, IL 60089	49000.00	RESI000000000118	PAHAL
V000000119	2016-08-10	2016-12-30 07:54:55	1	77000.00	hed@cingi.org	Girnudike	Valparaiso, IN 46383	56000.00	\N	WAHANA
V000000120	2016-09-03	2016-09-03 21:58:47	2	166000.00	va@bebim.net	Tukeci	165 El Dorado Ave.?	49000.00	\N	PAHAL
V000000121	2016-09-20	2016-12-30 07:54:55	1	290000.00	pidcem@bucfauru.com	Weezed	Wooster, OH 44691	24000.00	\N	WAHANA
V000000122	2016-09-06	2016-09-06 13:26:37	4	3735000.00	udne@donazmuv.io	Sokahig	2 Liberty Lane?	98000.00	RESI000000000122	J&T EXPRESS
V000000123	2016-08-16	2016-08-16 16:31:51	2	580000.00	zu@guji.gov	Ewelimleh	44 Addison St.?	84000.00	\N	POS PAKET KILAT
V000000124	2016-08-30	2016-08-30 17:17:31	2	1533000.00	wano@liwjumu.com	Ovozge	Shrewsbury, MA 01545	49000.00	\N	POS PAKET BIASA
V000000125	2016-10-05	2016-10-05 07:55:53	3	1135000.00	cel@map.io	Sekgizveb	Dubuque, IA 52001	48000.00	RESI000000000125	JNE REGULER
V000000126	2016-07-26	2016-07-26 13:50:20	4	791000.00	cuberuta@biepi.co.uk	Gegufa	Encino, CA 91316	72000.00	RESI000000000126	TIKI REGULER
V000000127	2016-10-19	2016-10-19 00:00:52	4	410000.00	ju@zo.com	Udaumdog	Willoughby, OH 44094	40000.00	RESI000000000127	WAHANA
V000000128	2016-08-20	2016-12-30 07:54:55	1	999000.00	mezhi@te.edu	Vicgonzop	Flemington, NJ 08822	84000.00	\N	J&T EXPRESS
V000000129	2016-07-29	2016-12-30 07:54:55	1	527000.00	boobaca@we.gov	Uciwguc	Oklahoma City, OK 73112	50000.00	\N	LION PARCEL
V000000130	2016-07-22	2016-07-22 02:09:18	3	1892000.00	fasepjev@ogafi.net	Vicgonzop	Lake Charles, LA 70605	14000.00	RESI000000000130	POS PAKET BIASA
V000000131	2016-09-29	2016-09-29 19:28:25	2	1351000.00	imsebap@oci.io	Sansegesu	Summerfield, FL 34491	63000.00	\N	PAHAL
V000000132	2016-08-17	2016-08-17 21:29:12	4	1663000.00	bavit@ji.org	Ovozge	Missoula, MT 59801	49000.00	RESI000000000132	POS PAKET BIASA
V000000133	2016-10-10	2016-10-10 19:18:23	2	1605000.00	oficaznaf@lomu.org	Ezjeza	62 Shipley St.?	10000.00	\N	JNE OKE
V000000134	2016-08-06	2016-08-06 08:52:24	4	1316000.00	dah@cuz.org	Fapapo	Faribault, MN 55021	49000.00	RESI000000000134	PAHAL
V000000135	2016-09-24	2016-09-24 10:26:51	2	50000.00	udne@donazmuv.io	Celseuh	8672 Manor Station St.?	56000.00	\N	PAHAL
V000000136	2016-10-06	2016-10-06 19:36:12	3	2012000.00	evdivcu@jak.io	Nadagra	8803 East Chapel Rd.?	35000.00	RESI000000000136	PAHAL
V000000137	2016-10-06	2016-10-06 05:55:15	4	2432000.00	uc@zepunu.org	Rufarowo	4 S. Lake View Drive?	54000.00	RESI000000000137	TIKI REGULER
V000000138	2016-08-10	2016-08-10 01:36:46	2	171000.00	cej@juz.co.uk	Nefpezi	7072 Yukon Ave.?	120000.00	\N	JNE YES
V000000139	2016-10-09	2016-10-09 18:39:28	3	1930000.00	teguc@pohon.io	Vulkewmeg	7958 Schoolhouse Drive?	35000.00	RESI000000000139	JNE OKE
V000000140	2016-08-13	2016-12-30 07:54:55	1	318000.00	sijtu@ja.io	Uguciis	Collegeville, PA 19426	36000.00	\N	TIKI REGULER
V000000141	2016-08-19	2016-08-19 21:21:43	4	420000.00	idfolo@kori.co.uk	Akutodror	7117 SE. Oklahoma Ave.?	20000.00	RESI000000000141	JNE OKE
V000000142	2016-09-06	2016-09-06 21:29:08	2	3292000.00	kehu@pom.edu	Gazebzi	44 Addison St.?	72000.00	\N	POS PAKET KILAT
V000000143	2016-09-17	2016-12-30 07:54:55	1	1320000.00	ji@fogukhu.com	Tonone	Amsterdam, NY 12010	70000.00	\N	J&T EXPRESS
V000000144	2016-08-02	2016-08-02 14:32:29	3	2003000.00	ruemo@ru.co.uk	Sansegesu	Strongsville, OH 44136	64000.00	RESI000000000144	WAHANA
V000000145	2016-09-05	2016-09-05 12:44:27	3	771000.00	nu@erajod.org	Jazodoh	112 Laurel St.?	32000.00	RESI000000000145	WAHANA
V000000146	2016-10-07	2016-10-07 22:26:06	2	1910000.00	gaupe@uf.org	Renroos	Petersburg, VA 23803	50000.00	\N	LION PARCEL
V000000147	2016-09-20	2016-12-30 07:54:55	1	288000.00	sivugal@opnulaj.gov	Azevimut	63 E. Arnold Lane?	70000.00	\N	J&T EXPRESS
V000000148	2016-08-30	2016-08-30 20:17:24	4	543000.00	sud@ul.org	Zesuzid	Champlin, MN 55316	40000.00	RESI000000000148	JNE REGULER
V000000149	2016-08-18	2016-08-18 08:16:40	3	1089000.00	cito@no.edu	Uciwguc	634 Homestead Court?	28000.00	RESI000000000149	PAHAL
V000000150	2016-08-30	2016-08-30 07:41:07	3	1275000.00	fasepjev@ogafi.net	Cudsale	Crofton, MD 21114	35000.00	RESI000000000150	POS PAKET BIASA
V000000151	2016-08-30	2016-08-30 13:28:48	2	2235000.00	pagmu@ka.edu	Divvemdaz	Atlanta, GA 30303	84000.00	\N	POS PAKET KILAT
V000000152	2016-10-16	2016-10-16 05:54:27	3	3480000.00	hisudo@ohlu.edu	Halaga	Elizabethtown, PA 17022	90000.00	RESI000000000152	JNE YES
V000000153	2016-10-02	2016-10-02 14:37:41	4	318000.00	cafvadpoj@miwoda.co.uk	Racubuus	West Roxbury, MA 02132	42000.00	RESI000000000153	POS PAKET BIASA
V000000154	2016-10-28	2016-12-30 07:54:55	1	1398000.00	jemoh@kawbur.gov	Sansegesu	Endicott, NY 13760	72000.00	\N	WAHANA
V000000155	2016-08-24	2016-08-24 22:42:11	3	225000.00	hu@eha.co.uk	Leobizek	Longwood, FL 32779	32000.00	RESI000000000155	JNE REGULER
V000000156	2016-07-29	2016-07-29 08:32:27	4	552000.00	vuc@ifef.net	Zovkumer	Findlay, OH 45840	32000.00	RESI000000000156	JNE REGULER
V000000157	2016-08-28	2016-12-30 07:54:55	1	2061000.00	nimzin@guznerepi.edu	Wanuwa	Shrewsbury, MA 01545	21000.00	\N	POS PAKET BIASA
V000000158	2016-10-25	2016-10-25 04:41:14	2	120000.00	tinubwi@ewo.com	Uguciis	Anderson, SC 29621	54000.00	\N	TIKI REGULER
V000000159	2016-09-21	2016-09-21 21:53:25	3	2688000.00	dup@nazepad.net	Nocanad	586 Howard Avenue?	90000.00	RESI000000000159	TIKI REGULER
V000000160	2016-07-30	2016-07-30 22:28:43	4	2832000.00	oceob@codetcuh.com	Zibewiw	433 Trenton Court?	42000.00	RESI000000000160	PAHAL
V000000161	2016-09-05	2016-09-05 15:32:33	3	3071000.00	zut@sazul.co.uk	Newbimmig	895 El Dorado Ave.?	35000.00	RESI000000000161	POS PAKET BIASA
V000000162	2016-09-06	2016-12-30 07:54:55	1	335000.00	ige@ocwope.net	Zesuzid	9811 Arcadia Street?	56000.00	\N	JNE REGULER
V000000163	2016-09-25	2016-09-25 02:50:05	4	160000.00	enuji@benkutja.gov	Voewisod	San Carlos, CA 94070	120000.00	RESI000000000163	JNE YES
V000000164	2016-10-07	2016-10-07 05:43:58	3	1754000.00	ruemo@ru.co.uk	Netfugab	8263 Hawthorne Lane?	105000.00	RESI000000000164	JNE YES
V000000165	2016-08-26	2016-08-26 22:44:55	2	671000.00	ced@ego.net	Tohimi	112 Laurel St.?	63000.00	\N	PAHAL
V000000166	2016-10-08	2016-10-08 17:34:50	2	815000.00	hed@cingi.org	Fotavsig	491 N. College Court?	15000.00	\N	JNE OKE
V000000167	2016-10-09	2016-12-30 07:54:55	1	1111000.00	nimzin@guznerepi.edu	Galufef	Commack, NY 11725	28000.00	\N	PAHAL
V000000168	2016-07-26	2016-07-26 20:53:42	4	236000.00	mewugek@sojkatta.io	Uciwguc	Cordova, TN 38016	60000.00	RESI000000000168	LION PARCEL
V000000169	2016-10-15	2016-12-30 07:54:55	1	138000.00	si@behab.co.uk	Ritkezil	10 Oak Valley Street?	30000.00	\N	JNE OKE
V000000170	2016-08-06	2016-08-06 14:34:47	2	801000.00	war@fohrahsin.net	Zesuzid	679 Evergreen Street?	72000.00	\N	JNE REGULER
V000000171	2016-09-24	2016-09-24 16:32:17	2	1442000.00	rulsiw@lecwuzla.gov	Ewelimleh	Romeoville, IL 60446	56000.00	\N	JNE REGULER
V000000172	2016-10-14	2016-12-30 07:54:55	1	297000.00	idudovje@wadmisre.co.uk	Gimmaniko	San Lorenzo, CA 94580	40000.00	\N	JNE REGULER
V000000173	2016-09-04	2016-09-04 19:07:40	2	150000.00	efuis@uc.co.uk	Celseuh	90 Euclid Lane?	35000.00	\N	PAHAL
V000000174	2016-08-30	2016-12-30 07:54:55	1	905000.00	ofi@veubuda.org	Icjakop	Rossville, GA 30741	56000.00	\N	POS PAKET BIASA
V000000175	2016-08-23	2016-08-23 03:40:20	4	152000.00	tufilco@luri.edu	Tukeci	Collegeville, PA 19426	49000.00	RESI000000000175	PAHAL
V000000176	2016-07-29	2016-07-29 13:59:25	4	1718000.00	wa@ejadu.gov	Cudsale	Falls Church, VA 22041	56000.00	RESI000000000176	POS PAKET BIASA
V000000177	2016-08-07	2016-12-30 07:54:55	1	172000.00	mezhi@te.edu	Mahzegug	Menasha, WI 54952	105000.00	\N	JNE YES
V000000178	2016-10-22	2016-10-22 03:28:11	3	2327000.00	erfi@zokiw.co.uk	Halaga	9963 Wayne St.?	40000.00	RESI000000000178	JNE OKE
V000000179	2016-08-03	2016-08-03 22:49:51	2	115000.00	joboav@un.io	Gegufa	Palm Beach Gardens, FL 33410	36000.00	\N	TIKI REGULER
V000000180	2016-09-13	2016-09-13 17:22:16	3	980000.00	ji@ihmomih.io	Nadagra	Kenosha, WI 53140	14000.00	RESI000000000180	PAHAL
V000000181	2016-07-30	2016-07-30 18:03:02	3	840000.00	jawantu@siziin.io	Akutodror	787 6th St.?	56000.00	RESI000000000181	WAHANA
V000000182	2016-10-11	2016-10-11 08:03:10	3	556000.00	va@bebim.net	Galufef	Alabaster, AL 35007	35000.00	RESI000000000182	POS PAKET BIASA
V000000183	2016-09-07	2016-09-07 10:25:38	3	194000.00	loh@cetumi.com	Onuegbiw	Oconomowoc, WI 53066	14000.00	RESI000000000183	POS PAKET BIASA
V000000184	2016-10-20	2016-10-20 00:48:28	2	3022000.00	fub@nuwok.io	Masvebje	Oak Lawn, IL 60453	30000.00	\N	JNE OKE
V000000185	2016-10-01	2016-10-01 08:26:24	4	2266000.00	ew@egzo.gov	Umuduuna	Longwood, FL 32779	49000.00	RESI000000000185	POS PAKET BIASA
V000000186	2016-08-07	2016-08-07 15:09:30	2	281000.00	tomva@kej.gov	Weezed	Elk Grove Village, IL 60007	35000.00	\N	PAHAL
V000000187	2016-10-14	2016-10-14 16:18:17	2	2632000.00	jidfow@ditiduze.com	Fapapo	7497 Thomas Ave.?	42000.00	\N	POS PAKET BIASA
V000000188	2016-09-25	2016-09-25 19:52:03	4	855000.00	wimfi@derwejgen.co.uk	Zuhicuto	96 Depot Lane?	30000.00	RESI000000000188	JNE OKE
V000000189	2016-08-18	2016-12-30 07:54:55	1	3344000.00	weheb@isoho.com	Kahmuso	Ashburn, VA 20147	80000.00	\N	LION PARCEL
V000000190	2016-09-29	2016-09-29 14:58:23	3	2142000.00	hed@cingi.org	Zeprinbu	Gurnee, IL 60031	45000.00	RESI000000000190	TIKI REGULER
V000000191	2016-09-16	2016-09-16 20:39:04	4	483000.00	vig@dehede.org	Racubuus	Cordova, TN 38016	56000.00	RESI000000000191	POS PAKET BIASA
V000000192	2016-08-25	2016-08-25 09:44:30	3	2614000.00	idfolo@kori.co.uk	Sansegesu	Bolingbrook, IL 60440	56000.00	RESI000000000192	WAHANA
V000000193	2016-09-12	2016-09-12 00:30:00	2	1160000.00	il@caguri.edu	Zibewiw	Mesa, AZ 85203	14000.00	\N	PAHAL
V000000194	2016-10-13	2016-12-30 07:54:55	1	175000.00	ta@roc.io	Publotweb	804 Foster Drive?	48000.00	\N	JNE REGULER
V000000195	2016-07-30	2016-07-30 05:13:20	3	1301000.00	ewe@manog.co.uk	Gazebzi	Peachtree City, GA 30269	72000.00	RESI000000000195	JNE REGULER
V000000196	2016-10-15	2016-12-30 07:54:55	1	833000.00	oficaznaf@lomu.org	Halaga	18 Pacific Circle?	105000.00	\N	JNE YES
V000000197	2016-08-18	2016-08-18 00:41:52	3	79000.00	ijages@lomulel.io	Sujoze	85 East Avenue?	72000.00	RESI000000000197	TIKI REGULER
V000000198	2016-09-23	2016-09-23 16:12:10	4	2103000.00	zonur@vocumotoz.net	Netfugab	East Orange, NJ 07017	50000.00	RESI000000000198	JNE OKE
V000000199	2016-09-21	2016-09-21 14:33:18	4	793000.00	zu@guji.gov	Puseza	506 1st Lane?	56000.00	RESI000000000199	PAHAL
V000000200	2016-10-10	2016-12-30 07:54:55	1	1657000.00	ejiflo@amdof.io	Hecgusnum	Westwood, NJ 07675	48000.00	\N	JNE REGULER
V000000201	2016-09-28	2016-09-28 12:00:00	2	1016000.00	tufilco@luri.edu	Binjesra	Billerica, MA 01821	49000.00	\N	POS PAKET BIASA
V000000202	2016-07-20	2016-07-20 02:45:04	2	775000.00	vanesu@juwhuzu.gov	Nekusuj	Clemmons, NC 27012	135000.00	\N	JNE YES
V000000203	2016-09-21	2016-09-21 00:29:41	3	172000.00	po@fahida.co.uk	Vulkewmeg	7072 Yukon Ave.?	30000.00	RESI000000000203	JNE OKE
V000000204	2016-09-17	2016-12-30 07:54:55	1	1335000.00	mocsib@cirgo.io	Rufarowo	Yuba City, CA 95993	48000.00	\N	WAHANA
V000000205	2016-08-12	2016-08-12 09:26:27	3	230000.00	ucetaeb@zola.co.uk	Zesuzid	541 Indian Spring Road?	40000.00	RESI000000000205	JNE REGULER
V000000206	2016-10-27	2016-10-27 23:39:43	3	2070000.00	jotugve@oclekuju.io	Wanuwa	Georgetown, SC 29440	84000.00	RESI000000000206	J&T EXPRESS
V000000207	2016-09-01	2016-09-01 07:33:42	4	1056000.00	ba@nod.gov	Podpijzaz	9963 Wayne St.?	56000.00	RESI000000000207	JNE REGULER
V000000208	2016-10-12	2016-10-12 04:30:04	4	1310000.00	su@ri.net	Zafjomvo	Georgetown, SC 29440	32000.00	RESI000000000208	JNE REGULER
V000000209	2016-08-05	2016-08-05 17:32:55	3	2090000.00	anowuw@sabapmi.org	Zonsawne	Chapel Hill, NC 27516	14000.00	RESI000000000209	PAHAL
V000000210	2016-08-18	2016-12-30 07:54:55	1	598000.00	nograhcuc@kauno.edu	Tohimi	Bergenfield, NJ 07621	30000.00	\N	LION PARCEL
V000000211	2016-07-26	2016-07-26 12:48:03	4	1940000.00	kehec@ebbucja.net	Zibewiw	Fernandina Beach, FL 32034	35000.00	RESI000000000211	PAHAL
V000000212	2016-10-03	2016-10-03 23:53:31	3	420000.00	sisi@povzurudo.io	Cuulaseb	Halethorpe, MD 21227	60000.00	RESI000000000212	POS PAKET KILAT
V000000213	2016-10-21	2016-12-30 07:54:55	1	2009000.00	cuberuta@biepi.co.uk	Kahmuso	Passaic, NJ 07055	60000.00	\N	LION PARCEL
V000000214	2016-07-30	2016-07-30 11:05:19	3	1638000.00	tegesemo@roenvuj.edu	Divvemdaz	East Haven, CT 06512	16000.00	RESI000000000214	JNE REGULER
V000000215	2016-08-08	2016-08-08 08:56:14	2	822000.00	rulsiw@lecwuzla.gov	Icjakop	697 Arlington Street?	126000.00	\N	J&T EXPRESS
V000000216	2016-07-29	2016-07-29 00:00:17	2	1806000.00	jukuctig@bucolza.org	Zibewiw	Perth Amboy, NJ 08861	64000.00	\N	WAHANA
V000000217	2016-08-18	2016-08-18 19:45:39	4	4227000.00	kon@dow.edu	Halaga	Owosso, MI 48867	90000.00	RESI000000000217	JNE YES
V000000218	2016-10-06	2016-10-06 08:36:43	3	230000.00	evdivcu@jak.io	Nadcezpip	108 Sleepy Hollow St.?	35000.00	RESI000000000218	POS PAKET BIASA
V000000219	2016-07-21	2016-12-30 07:54:55	1	2773000.00	igupu@ogezegpod.gov	Umuduuna	Mableton, GA 30126	56000.00	\N	POS PAKET BIASA
V000000220	2016-10-24	2016-10-24 01:17:49	2	416000.00	ginid@unfioc.org	Cuulaseb	Wisconsin Rapids, WI 54494	48000.00	\N	JNE REGULER
V000000221	2016-10-06	2016-10-06 10:13:47	4	157000.00	hewunul@rigfuda.org	Nadcezpip	42 NE. Nut Swamp Rd.?	30000.00	RESI000000000221	JNE OKE
V000000222	2016-08-11	2016-08-11 18:24:04	3	388000.00	le@lopdooc.com	Udaumdog	195 College St.?	48000.00	RESI000000000222	WAHANA
V000000223	2016-09-09	2016-09-09 20:46:02	2	1663000.00	papa@jorisuz.co.uk	Ovozge	68 Riverside Dr.?	140000.00	\N	J&T EXPRESS
V000000224	2016-10-05	2016-10-05 17:30:38	2	2801000.00	ijlo@vicot.com	Vulkewmeg	Rockville, MD 20850	80000.00	\N	WAHANA
V000000225	2016-08-08	2016-12-30 07:54:55	1	1079000.00	co@lep.gov	Zibewiw	Panama City, FL 32404	14000.00	\N	PAHAL
V000000226	2016-08-13	2016-08-13 05:16:56	4	1327000.00	ilam@hadjaus.org	Gasomo	9578 Campfire Rd.?	25000.00	RESI000000000226	JNE OKE
V000000227	2016-10-12	2016-10-12 13:48:41	4	1302000.00	tegesemo@roenvuj.edu	Favofsi	9383 Military Avenue?	20000.00	RESI000000000227	JNE OKE
V000000228	2016-10-02	2016-10-02 02:20:27	3	402000.00	cin@dorpaz.org	Efuwumubi	Cottage Grove, MN 55016	35000.00	RESI000000000228	PAHAL
V000000229	2016-08-18	2016-08-18 21:10:16	2	947000.00	hovarwuh@sup.io	Amdicru	81 Hill Field St.?	42000.00	\N	PAHAL
V000000230	2016-09-15	2016-09-15 14:51:51	4	1970000.00	igupu@ogezegpod.gov	Tuavted	Lombard, IL 60148	42000.00	RESI000000000230	POS PAKET BIASA
V000000231	2016-10-23	2016-12-30 07:54:55	1	1835000.00	sisi@povzurudo.io	Nekusuj	Niles, MI 49120	30000.00	\N	JNE YES
V000000232	2016-09-05	2016-12-30 07:54:55	1	106000.00	ap@gavakeog.gov	Ciemica	Windsor Mill, MD 21244	48000.00	\N	JNE REGULER
V000000233	2016-09-15	2016-09-15 05:18:37	2	162000.00	jude@fok.edu	Sujoze	Saint Johns, FL 32259	54000.00	\N	TIKI REGULER
V000000234	2016-09-11	2016-09-11 03:49:17	2	699000.00	cin@dorpaz.org	Newbimmig	176 8th Ave.?	56000.00	\N	POS PAKET BIASA
V000000235	2016-09-27	2016-09-27 15:59:55	2	1391000.00	meiru@dofigoj.org	Girnudike	7949 Roberts Street?	54000.00	\N	TIKI REGULER
V000000236	2016-09-26	2016-09-26 02:34:08	3	1960000.00	nograhcuc@kauno.edu	Ewelimleh	Doylestown, PA 18901	56000.00	RESI000000000236	JNE REGULER
V000000237	2016-10-14	2016-10-14 19:42:52	3	71000.00	bod@zij.edu	Celseuh	7566 Longbranch Road?	32000.00	RESI000000000237	WAHANA
V000000238	2016-10-17	2016-10-17 03:49:32	2	4501000.00	nifu@lotedpoh.gov	Zibewiw	Elk Grove Village, IL 60007	35000.00	\N	PAHAL
V000000239	2016-07-28	2016-12-30 07:54:55	1	928000.00	kub@etdav.org	Nocanad	134 St Paul Lane?	63000.00	\N	TIKI REGULER
V000000240	2016-08-13	2016-08-13 08:53:53	3	948000.00	mireg@nevwacko.edu	Zonsawne	895 El Dorado Ave.?	56000.00	RESI000000000240	PAHAL
V000000241	2016-07-23	2016-12-30 07:54:55	1	1194000.00	ap@gavakeog.gov	Zovkumer	830 Beech Street?	56000.00	\N	JNE REGULER
V000000242	2016-10-03	2016-10-03 03:12:02	3	226000.00	mocsib@cirgo.io	Denhosjil	Mount Juliet, TN 37122	48000.00	RESI000000000242	WAHANA
V000000243	2016-09-06	2016-09-06 23:01:11	2	970000.00	dup@ejmameh.io	Cebvolsi	Jackson Heights, NY 11372	49000.00	\N	PAHAL
V000000244	2016-08-28	2016-12-30 07:54:55	1	1326000.00	hisudo@ohlu.edu	Icjakop	9817 Hill St.?	35000.00	\N	POS PAKET BIASA
V000000245	2016-10-20	2016-10-20 23:12:39	3	3072000.00	zup@ab.com	Favofsi	Findlay, OH 45840	20000.00	RESI000000000245	JNE OKE
V000000246	2016-09-21	2016-09-21 15:37:06	3	2088000.00	ca@jon.io	Zeprinbu	Shrewsbury, MA 01545	54000.00	RESI000000000246	TIKI REGULER
V000000247	2016-10-02	2016-10-02 05:47:10	3	1062000.00	kibiri@koftere.io	Ivsatdof	8 Chapel St.?	63000.00	RESI000000000247	POS PAKET BIASA
V000000248	2016-08-29	2016-08-29 22:10:17	2	107000.00	ciwimnim@wolihvo.io	Publotweb	Harlingen, TX 78552	72000.00	\N	JNE REGULER
V000000249	2016-10-12	2016-12-30 07:54:55	1	2733000.00	mocsib@cirgo.io	Ciemica	Champlin, MN 55316	32000.00	\N	JNE REGULER
V000000250	2016-09-02	2016-09-02 18:04:43	3	3173000.00	liru@op.co.uk	Newbimmig	45 South Roosevelt Lane?	30000.00	RESI000000000250	JNE OKE
V000000251	2016-07-21	2016-12-30 07:54:55	1	758000.00	ji@ihmomih.io	Netfugab	98 Mulberry Street?	40000.00	\N	JNE OKE
V000000252	2016-08-03	2016-08-03 11:06:09	4	412000.00	uvogew@onumi.io	Gimmaniko	9963 Wayne St.?	24000.00	RESI000000000252	JNE REGULER
V000000253	2016-09-16	2016-09-16 07:36:21	3	2180000.00	ponuv@howup.co.uk	Renroos	Wilmette, IL 60091	90000.00	RESI000000000253	LION PARCEL
V000000254	2016-09-17	2016-09-17 14:03:05	4	1639000.00	nu@erajod.org	Sansegesu	35 Adams Dr.?	42000.00	RESI000000000254	PAHAL
V000000255	2016-09-22	2016-09-22 17:52:45	4	2245000.00	is@fe.com	Sokahig	700 Briarwood Street?	84000.00	RESI000000000255	J&T EXPRESS
V000000256	2016-10-24	2016-10-24 06:26:16	2	1463000.00	su@ri.net	Umuduuna	57 Harvard Drive?	63000.00	\N	PAHAL
V000000257	2016-10-01	2016-10-01 23:28:51	3	486000.00	dup@nazepad.net	Lobasil	8 Chapel St.?	28000.00	RESI000000000257	POS PAKET BIASA
V000000258	2016-10-01	2016-10-01 09:12:51	3	577000.00	teuzu@va.net	Icjakop	21 Helen Circle?	70000.00	RESI000000000258	J&T EXPRESS
V000000259	2016-09-26	2016-09-26 21:41:08	3	2863000.00	jawantu@siziin.io	Ezjeza	665 Kingston Dr.?	40000.00	RESI000000000259	JNE OKE
V000000260	2016-08-23	2016-08-23 02:19:33	3	558000.00	ijo@sisfopbup.org	Jazodoh	Paramus, NJ 07652	20000.00	RESI000000000260	JNE OKE
V000000261	2016-09-02	2016-09-02 00:29:40	2	833000.00	uk@kiko.co.uk	Halaga	585 Valley Court?	105000.00	\N	JNE YES
V000000262	2016-07-24	2016-07-24 18:35:14	2	1418000.00	faopidu@uhi.com	Jeijsig	Newport News, VA 23601	48000.00	\N	POS PAKET KILAT
V000000263	2016-07-20	2016-07-20 23:09:28	4	460000.00	uduhet@pomwic.org	Nivumat	Ridgecrest, CA 93555	70000.00	RESI000000000263	LION PARCEL
V000000264	2016-09-29	2016-09-29 00:40:09	3	2539000.00	vim@kieh.gov	Vicgonzop	7136 Manchester Street?	63000.00	RESI000000000264	POS PAKET BIASA
V000000265	2016-10-21	2016-10-21 07:33:08	3	2733000.00	vobvi@vo.org	Halaga	9811 Arcadia Street?	120000.00	RESI000000000265	JNE YES
V000000266	2016-09-01	2016-12-30 07:54:55	1	2290000.00	powjajrak@el.io	Sokahig	Marietta, GA 30008	112000.00	\N	J&T EXPRESS
V000000267	2016-08-19	2016-08-19 16:35:10	2	133000.00	povioge@na.gov	Celseuh	146 Woodside St.?	40000.00	\N	WAHANA
V000000268	2016-08-25	2016-08-25 04:29:23	2	1132000.00	weheb@isoho.com	Hecgusnum	634 Homestead Court?	24000.00	\N	JNE REGULER
V000000269	2016-10-04	2016-10-04 02:14:52	3	308000.00	esoli@velmurap.io	Hozguna	832 Green Street?	126000.00	RESI000000000269	J&T EXPRESS
V000000270	2016-09-03	2016-12-30 07:54:55	1	1172000.00	ditka@bajwo.gov	Nadagra	Woodside, NY 11377	70000.00	\N	LION PARCEL
V000000271	2016-10-14	2016-12-30 07:54:55	1	343000.00	eribip@dim.edu	Jucujra	Maplewood, NJ 07040	21000.00	\N	PAHAL
V000000272	2016-10-17	2016-10-17 23:14:28	2	242000.00	pagmu@ka.edu	Voewisod	94 53rd Street?	75000.00	\N	JNE YES
V000000273	2016-10-20	2016-10-20 10:33:51	2	1183000.00	leluwi@honwi.com	Difarbaw	9578 Campfire Rd.?	120000.00	\N	JNE YES
V000000274	2016-08-05	2016-08-05 08:09:21	4	852000.00	vuim@zadjul.co.uk	Ewelimleh	Murrells Inlet, SC 29576	72000.00	RESI000000000274	POS PAKET KILAT
V000000275	2016-10-19	2016-12-30 07:54:55	1	782000.00	ivduse@zuhew.org	Nadagra	2 Liberty Lane?	40000.00	\N	LION PARCEL
V000000276	2016-09-26	2016-12-30 07:54:55	1	2647000.00	ivduse@zuhew.org	Teocesuh	Langhorne, PA 19047	40000.00	\N	WAHANA
V000000277	2016-08-16	2016-08-16 08:42:47	4	322000.00	eribip@dim.edu	Publotweb	634 Homestead Court?	24000.00	RESI000000000277	JNE REGULER
V000000278	2016-07-31	2016-07-31 14:39:38	2	410000.00	fasepjev@ogafi.net	Udaumdog	Goldsboro, NC 27530	48000.00	\N	WAHANA
V000000279	2016-10-05	2016-10-05 10:32:17	4	110000.00	ija@ub.net	Movwukine	68 Riverside Dr.?	35000.00	RESI000000000279	JNE OKE
V000000280	2016-07-26	2016-07-26 07:05:45	4	2994000.00	tomva@kej.gov	Ewelimleh	8217 Creekside Dr.?	64000.00	RESI000000000280	JNE REGULER
V000000281	2016-08-16	2016-12-30 07:54:55	1	1161000.00	tegesemo@roenvuj.edu	Tohimi	685 Andover Road?	49000.00	\N	PAHAL
V000000282	2016-09-11	2016-09-11 23:15:16	3	928000.00	cafvadpoj@miwoda.co.uk	Nocanad	8 Ketch Harbour Lane?	81000.00	RESI000000000282	TIKI REGULER
V000000283	2016-09-14	2016-09-14 17:53:36	2	2474000.00	ap@gavakeog.gov	Cebvolsi	Amsterdam, NY 12010	28000.00	\N	PAHAL
V000000284	2016-08-30	2016-12-30 07:54:55	1	316000.00	va@bebim.net	Zesuzid	Barrington, IL 60010	56000.00	\N	JNE REGULER
V000000285	2016-08-06	2016-08-06 04:50:34	3	2400000.00	vedowur@loowojac.io	Rihizaf	Falls Church, VA 22041	28000.00	RESI000000000285	J&T EXPRESS
V000000286	2016-09-03	2016-09-03 14:31:15	2	2266000.00	jodluf@soklizkec.org	Umuduuna	46 Silver Spear St.?	42000.00	\N	POS PAKET BIASA
V000000287	2016-08-03	2016-08-03 06:24:36	4	4566000.00	ji@kozko.co.uk	Laetpem	Encino, CA 91316	60000.00	RESI000000000287	POS PAKET KILAT
V000000288	2016-10-11	2016-10-11 02:56:21	4	140000.00	po@fahida.co.uk	Sujoze	Westwood, NJ 07675	16000.00	RESI000000000288	WAHANA
V000000289	2016-09-28	2016-09-28 22:16:44	2	355000.00	rohoci@muza.io	Amdicru	7058 Illinois St.?	63000.00	\N	PAHAL
V000000290	2016-09-07	2016-09-07 02:55:47	4	140000.00	il@up.co.uk	Vulkewmeg	46 Silver Spear St.?	30000.00	RESI000000000290	JNE OKE
V000000291	2016-07-29	2016-07-29 15:18:33	4	631000.00	deretjaw@gek.edu	Nadagra	7019 Vale St.?	35000.00	RESI000000000291	PAHAL
V000000292	2016-09-28	2016-09-28 22:58:40	3	477000.00	solodsi@vinepmid.org	Diktufina	7252 Wild Rose St.?	21000.00	RESI000000000292	PAHAL
V000000293	2016-10-13	2016-12-30 07:54:55	1	2828000.00	ewebeduc@heceh.co.uk	Kahmuso	Eugene, OR 97402	35000.00	\N	PAHAL
V000000294	2016-09-02	2016-09-02 15:20:40	4	684000.00	jude@fok.edu	Tohimi	16 Bald Hill Ave.?	35000.00	RESI000000000294	PAHAL
V000000295	2016-07-29	2016-07-29 14:05:54	4	731000.00	ez@upzi.com	Nekusuj	Levittown, NY 11756	35000.00	RESI000000000295	JNE OKE
V000000296	2016-08-27	2016-08-27 20:52:00	4	2312000.00	si@behab.co.uk	Vicgonzop	Passaic, NJ 07055	35000.00	RESI000000000296	POS PAKET BIASA
V000000297	2016-07-19	2016-12-30 07:54:55	1	2372000.00	cel@map.io	Gazebzi	Port Saint Lucie, FL 34952	40000.00	\N	JNE REGULER
V000000298	2016-09-18	2016-09-18 03:17:20	4	1654000.00	subzola@kacjis.edu	Ijiozazag	Langhorne, PA 19047	32000.00	RESI000000000298	JNE REGULER
V000000299	2016-09-03	2016-09-03 20:45:07	3	1405000.00	jumovbot@ul.edu	Ritkezil	174 W. Kent Dr.?	20000.00	RESI000000000299	JNE OKE
V000000300	2016-10-01	2016-10-01 05:28:09	3	261000.00	nograhcuc@kauno.edu	Jazodoh	Muskegon, MI 49441	30000.00	RESI000000000300	JNE OKE
V000000301	2016-08-04	2016-08-04 21:21:49	4	1216000.00	vedowur@loowojac.io	Rufarowo	Palm City, FL 34990	48000.00	RESI000000000301	WAHANA
V000000302	2016-08-25	2016-12-30 07:54:55	1	1405000.00	po@fahida.co.uk	Efuwumubi	Brick, NJ 08723	35000.00	\N	PAHAL
V000000303	2016-08-26	2016-08-26 09:14:04	2	1320000.00	jodluf@soklizkec.org	Tonone	Rego Park, NY 11374	35000.00	\N	POS PAKET BIASA
V000000304	2016-08-24	2016-08-24 03:16:38	3	313000.00	ced@ego.net	Zesuzid	589 Arrowhead St.?	40000.00	RESI000000000304	JNE REGULER
V000000305	2016-09-06	2016-09-06 04:46:39	4	722000.00	fasepjev@ogafi.net	Zuzreku	585 Valley Court?	54000.00	RESI000000000305	TIKI REGULER
V000000306	2016-08-06	2016-08-06 05:16:43	3	2841000.00	ciraofi@wikvuut.io	Vulkewmeg	Shrewsbury, MA 01545	45000.00	RESI000000000306	JNE OKE
V000000307	2016-07-30	2016-07-30 02:04:28	3	2772000.00	uk@kiko.co.uk	Fapapo	Cary, NC 27511	42000.00	RESI000000000307	PAHAL
V000000308	2016-09-08	2016-09-08 19:42:07	3	1109000.00	na@tazevze.com	Tesozoun	51 Green Drive?	35000.00	RESI000000000308	JNE OKE
V000000309	2016-09-12	2016-09-12 09:06:53	2	175000.00	ijages@lomulel.io	Leobizek	Millington, TN 38053	64000.00	\N	JNE REGULER
V000000310	2016-09-16	2016-09-16 12:50:57	2	1932000.00	if@azo.gov	Tonone	267 Linda Road?	28000.00	\N	POS PAKET BIASA
V000000311	2016-10-18	2016-12-30 07:54:55	1	682000.00	uphagfa@reluba.io	Ivsatdof	9362 Vale Street?	28000.00	\N	POS PAKET BIASA
V000000312	2016-08-03	2016-08-03 08:50:48	3	2736000.00	heip@cani.com	Divvemdaz	8367 Lincoln Road?	40000.00	RESI000000000312	JNE REGULER
V000000313	2016-07-22	2016-12-30 07:54:55	1	242000.00	vig@dehede.org	Udaumdog	7255 Campfire Dr.?	35000.00	\N	JNE OKE
V000000314	2016-10-13	2016-10-13 02:46:36	3	2863000.00	bod@zij.edu	Ezjeza	8734 Winding Way Drive?	40000.00	RESI000000000314	WAHANA
V000000315	2016-07-30	2016-07-30 20:59:10	3	1051000.00	vacawhep@kalagwo.com	Zibewiw	16 Bank Court?	48000.00	RESI000000000315	WAHANA
V000000316	2016-08-12	2016-08-12 03:50:57	4	3597000.00	lupadiv@ge.com	Laetpem	Valparaiso, IN 46383	60000.00	RESI000000000316	POS PAKET KILAT
V000000317	2016-10-07	2016-10-07 09:24:49	4	176000.00	vuc@ifef.net	Goopifeb	Reading, MA 01867	70000.00	RESI000000000317	LION PARCEL
V000000318	2016-10-21	2016-10-21 15:08:54	4	826000.00	eto@cenjiv.co.uk	Zovkumer	Port Jefferson Station, NY 11776	36000.00	RESI000000000318	POS PAKET KILAT
V000000319	2016-07-27	2016-07-27 07:28:56	4	2368000.00	po@fahida.co.uk	Zonsawne	833 New Lane?	35000.00	RESI000000000319	PAHAL
V000000320	2016-10-05	2016-12-30 07:54:55	1	702000.00	ciwimnim@wolihvo.io	Rufarowo	2 Old Princess Rd.?	45000.00	\N	TIKI REGULER
V000000321	2016-09-14	2016-09-14 03:18:28	4	1624000.00	ju@zo.com	Nocanad	644 Sherwood Avenue?	45000.00	RESI000000000321	TIKI REGULER
V000000322	2016-09-10	2016-09-10 02:50:28	3	135000.00	gukum@ficoofo.co.uk	Nefpezi	21 Helen Circle?	10000.00	RESI000000000322	JNE OKE
V000000323	2016-08-03	2016-08-03 18:43:51	3	742000.00	jiwuol@vi.io	Cuulaseb	Harlingen, TX 78552	32000.00	RESI000000000323	JNE REGULER
V000000324	2016-08-05	2016-08-05 11:42:21	3	2221000.00	anowuw@sabapmi.org	Gazebzi	Windsor Mill, MD 21244	96000.00	RESI000000000324	POS PAKET KILAT
V000000325	2016-09-23	2016-09-23 13:32:24	3	2450000.00	giwjufu@mahuh.net	Fapapo	500 Center Lane?	35000.00	RESI000000000325	POS PAKET BIASA
V000000326	2016-08-12	2016-08-12 03:52:32	4	1751000.00	ruemo@ru.co.uk	Netfugab	26 Gainsway St.?	30000.00	RESI000000000326	JNE OKE
V000000327	2016-08-20	2016-08-20 13:22:24	4	453000.00	leluwi@honwi.com	Mahzegug	11 Gainsway Street?	60000.00	RESI000000000327	JNE YES
V000000328	2016-10-25	2016-10-25 06:21:01	2	756000.00	esoli@velmurap.io	Zuzreku	Oconomowoc, WI 53066	63000.00	\N	TIKI REGULER
V000000329	2016-09-30	2016-09-30 11:37:28	2	298000.00	jidfow@ditiduze.com	Voewisod	1 Whitemarsh Drive?	15000.00	\N	JNE OKE
V000000330	2016-07-22	2016-07-22 01:26:15	3	3253000.00	gigat@saf.co.uk	Rufarowo	Westmont, IL 60559	48000.00	RESI000000000330	WAHANA
V000000331	2016-07-23	2016-07-23 21:49:29	2	559000.00	ewebeduc@heceh.co.uk	Gasomo	98 Mulberry Street?	90000.00	\N	JNE YES
V000000332	2016-07-30	2016-07-30 20:41:32	4	2244000.00	uphagfa@reluba.io	Cebvolsi	7227 Creekside St.?	49000.00	RESI000000000332	PAHAL
V000000333	2016-09-23	2016-09-23 19:13:07	3	538000.00	weheb@isoho.com	Nadagra	8091 Nicolls Ave.?	49000.00	RESI000000000333	PAHAL
V000000334	2016-09-18	2016-12-30 07:54:55	1	534000.00	ku@titdotres.gov	Udaumdog	7651 Rockcrest Drive?	40000.00	\N	JNE OKE
V000000335	2016-09-25	2016-09-25 18:44:45	4	871000.00	pidcem@bucfauru.com	Zovkumer	9817 Hill St.?	96000.00	RESI000000000335	POS PAKET KILAT
V000000336	2016-09-28	2016-09-28 14:12:56	2	1428000.00	nimzin@guznerepi.edu	Gasomo	98 Mulberry Street?	45000.00	\N	JNE OKE
V000000337	2016-10-25	2016-10-25 00:38:52	3	1233000.00	tinubwi@ewo.com	Icjakop	444 Stillwater St.?	49000.00	RESI000000000337	POS PAKET BIASA
V000000338	2016-07-30	2016-12-30 07:54:55	1	494000.00	pidcem@bucfauru.com	Nivumat	Jackson Heights, NY 11372	60000.00	\N	LION PARCEL
V000000339	2016-08-03	2016-08-03 14:58:58	2	3072000.00	je@eno.net	Favofsi	860 Madison Avenue?	75000.00	\N	JNE YES
V000000340	2016-08-27	2016-08-27 04:34:52	2	209000.00	idudovje@wadmisre.co.uk	Racubuus	860 Madison Avenue?	56000.00	\N	POS PAKET BIASA
V000000341	2016-08-28	2016-08-28 22:16:07	2	327000.00	is@fe.com	Kalgenit	Levittown, NY 11756	50000.00	\N	JNE OKE
V000000342	2016-09-15	2016-12-30 07:54:55	1	343000.00	aveju@ezihocle.co.uk	Mahzegug	Pelham, AL 35124	120000.00	\N	JNE YES
V000000343	2016-09-24	2016-09-24 11:11:04	3	225000.00	dup@ejmameh.io	Leobizek	Ft Mitchell, KY 41017	40000.00	RESI000000000343	JNE REGULER
V000000344	2016-07-31	2016-07-31 20:46:02	2	3703000.00	is@fe.com	Sokahig	152 El Dorado Court?	28000.00	\N	J&T EXPRESS
V000000345	2016-07-20	2016-12-30 07:54:55	1	406000.00	lupadiv@ge.com	Netfugab	7013 West St Paul St.?	30000.00	\N	JNE YES
V000000346	2016-09-16	2016-09-16 15:04:25	3	1009000.00	hu@eha.co.uk	Jazodoh	7394 Monroe Dr.?	40000.00	RESI000000000346	WAHANA
V000000347	2016-10-05	2016-10-05 08:41:54	4	984000.00	ijlo@vicot.com	Binjesra	9973 John Road?	49000.00	RESI000000000347	POS PAKET BIASA
V000000348	2016-09-06	2016-12-30 07:54:55	1	1428000.00	fiom@si.org	Zeprinbu	Suite 80?	64000.00	\N	WAHANA
V000000349	2016-09-29	2016-09-29 02:39:44	2	2106000.00	zut@sazul.co.uk	Rufarowo	47 Cedar Ave.?	81000.00	\N	TIKI REGULER
V000000350	2016-09-04	2016-09-04 22:53:48	3	282000.00	ewebeduc@heceh.co.uk	Wanuwa	9144 Prince Street?	70000.00	RESI000000000350	J&T EXPRESS
V000000351	2016-09-07	2016-09-07 03:46:03	4	940000.00	idfolo@kori.co.uk	Dalnedi	5 Sunset St.?	45000.00	RESI000000000351	JNE OKE
V000000352	2016-09-14	2016-12-30 07:54:55	1	2784000.00	canehiler@enlo.edu	Ekviamu	278 Helen Court?	56000.00	\N	WAHANA
V000000353	2016-10-22	2016-10-22 19:13:24	3	705000.00	dup@nazepad.net	Sansegesu	702 Smoky Hollow St.?	24000.00	RESI000000000353	WAHANA
V000000354	2016-10-11	2016-12-30 07:54:55	1	602000.00	ikruj@sapanti.com	Uhcowej	Encino, CA 91316	84000.00	\N	J&T EXPRESS
V000000355	2016-10-26	2016-10-26 13:02:48	4	87000.00	pidcem@bucfauru.com	Celseuh	833 New Lane?	80000.00	RESI000000000355	WAHANA
V000000356	2016-08-07	2016-08-07 05:55:01	3	357000.00	ra@midgabe.edu	Lilhava	Louisville, KY 40207	42000.00	RESI000000000356	POS PAKET BIASA
V000000357	2016-09-10	2016-09-10 06:38:19	4	916000.00	tinubwi@ewo.com	Tohimi	778 Carpenter St.?	49000.00	RESI000000000357	PAHAL
V000000358	2016-09-29	2016-09-29 04:57:53	3	1374000.00	kanaz@mema.org	Jeijsig	7756 Highland St.?	72000.00	RESI000000000358	POS PAKET KILAT
V000000359	2016-08-18	2016-08-18 22:57:08	4	3597000.00	murgupo@hiiluhe.net	Laetpem	Oxnard, CA 93035	24000.00	RESI000000000359	POS PAKET KILAT
V000000360	2016-10-22	2016-10-22 21:03:28	2	131000.00	ju@zo.com	Tukeci	Elizabeth, NJ 07202	100000.00	\N	LION PARCEL
V000000361	2016-10-02	2016-10-02 08:28:55	4	2327000.00	wemufmi@vigurija.gov	Halaga	Apple Valley, CA 92307	35000.00	RESI000000000361	JNE OKE
V000000362	2016-08-15	2016-08-15 06:36:43	3	2492000.00	ijo@sisfopbup.org	Ihkeadu	Circle Pines, MN 55014	108000.00	RESI000000000362	POS PAKET KILAT
V000000363	2016-09-10	2016-09-10 08:26:09	4	1933000.00	wano@liwjumu.com	Nekusuj	Pottstown, PA 19464	105000.00	RESI000000000363	JNE YES
V000000364	2016-08-28	2016-08-28 09:43:56	3	2814000.00	hazut@ezhofid.co.uk	Fapapo	Chippewa Falls, WI 54729	42000.00	RESI000000000364	PAHAL
V000000365	2016-09-04	2016-09-04 01:25:18	3	259000.00	tipdabit@ogazov.com	Kalgenit	7117 SE. Oklahoma Ave.?	150000.00	RESI000000000365	JNE YES
V000000366	2016-10-04	2016-10-04 11:10:45	2	129000.00	solodsi@vinepmid.org	Racubuus	Mcminnville, TN 37110	56000.00	\N	PAHAL
V000000367	2016-08-17	2016-08-17 07:14:57	4	602000.00	ija@ub.net	Uhcowej	80 S. Addison St.?	112000.00	RESI000000000367	J&T EXPRESS
V000000368	2016-09-29	2016-09-29 19:46:21	2	2065000.00	war@fohrahsin.net	Favofsi	9 Theatre Lane?	25000.00	\N	JNE OKE
V000000369	2016-10-11	2016-10-11 17:23:01	3	176000.00	kon@dow.edu	Nivumat	Amsterdam, NY 12010	35000.00	RESI000000000369	PAHAL
V000000370	2016-09-01	2016-09-01 21:32:04	2	1507000.00	po@fahida.co.uk	Zogjowoj	9623 Manhattan Drive?	45000.00	\N	JNE OKE
V000000371	2016-10-09	2016-10-09 04:25:52	3	857000.00	ijo@sisfopbup.org	Sekgizveb	8825 Peninsula Avenue?	32000.00	RESI000000000371	JNE REGULER
V000000372	2016-09-04	2016-09-04 23:50:05	3	2978000.00	teuzu@va.net	Binjesra	825 East Fieldstone Ave.?	15000.00	RESI000000000372	JNE OKE
V000000373	2016-08-28	2016-08-28 16:28:25	2	171000.00	dipcoc@ziszipmes.io	Publotweb	Pelham, AL 35124	24000.00	\N	JNE REGULER
V000000374	2016-08-31	2016-08-31 06:36:16	3	2006000.00	ew@egzo.gov	Favofsi	21 Helen Circle?	60000.00	RESI000000000374	JNE YES
V000000375	2016-07-31	2016-12-30 07:54:55	1	1844000.00	uwa@tagokraw.gov	Ulmezle	800 Oxford St.?	60000.00	\N	LION PARCEL
V000000376	2016-09-29	2016-09-29 10:17:52	3	808000.00	ew@egzo.gov	Kerehu	8283 Mechanic St.?	81000.00	RESI000000000376	TIKI REGULER
V000000377	2016-08-06	2016-08-06 11:51:31	2	244000.00	su@ri.net	Nivumat	Petersburg, VA 23803	42000.00	\N	PAHAL
V000000378	2016-10-27	2016-10-27 20:22:47	4	101000.00	faopidu@uhi.com	Nadcezpip	7788 Pleasant Rd.?	25000.00	RESI000000000378	JNE OKE
V000000379	2016-07-24	2016-07-24 12:37:20	2	1865000.00	ciwimnim@wolihvo.io	Amdicru	227 Golf Lane?	72000.00	\N	WAHANA
V000000380	2016-10-19	2016-10-19 16:46:48	4	94000.00	ijages@lomulel.io	Nadcezpip	112 Laurel St.?	25000.00	RESI000000000380	JNE OKE
V000000381	2016-10-26	2016-10-26 13:47:38	3	378000.00	fura@upipu.gov	Goopifeb	Fond Du Lac, WI 54935	42000.00	RESI000000000381	PAHAL
V000000382	2016-09-02	2016-12-30 07:54:55	1	1275000.00	costoja@waw.gov	Cudsale	491 N. College Court?	70000.00	\N	POS PAKET BIASA
V000000383	2016-08-30	2016-12-30 07:54:55	1	172000.00	paweifa@gusak.com	Uguciis	9362 Vale Street?	48000.00	\N	WAHANA
V000000384	2016-08-14	2016-08-14 13:48:02	3	3006000.00	is@fe.com	Sokahig	60 Summerhouse Lane?	112000.00	RESI000000000384	J&T EXPRESS
V000000385	2016-09-27	2016-09-27 20:13:33	4	2516000.00	cej@juz.co.uk	Nocanad	134 St Paul Lane?	80000.00	RESI000000000385	WAHANA
V000000386	2016-07-25	2016-07-25 00:09:13	3	2978000.00	deretjaw@gek.edu	Binjesra	26 Gainsway St.?	28000.00	RESI000000000386	POS PAKET BIASA
V000000387	2016-09-23	2016-09-23 07:28:12	3	3957000.00	fub@nuwok.io	Tuavted	7252 Wild Rose St.?	35000.00	RESI000000000387	POS PAKET BIASA
V000000388	2016-10-15	2016-12-30 07:54:55	1	379000.00	hesnaji@ve.net	Puseza	Cottage Grove, MN 55016	16000.00	\N	WAHANA
V000000389	2016-10-25	2016-12-30 07:54:55	1	1986000.00	zu@guji.gov	Lilhava	8408 Bank St.?	70000.00	\N	POS PAKET BIASA
V000000390	2016-09-23	2016-09-23 19:41:30	2	1291000.00	ofi@veubuda.org	Efuwumubi	Glendora, CA 91740	21000.00	\N	PAHAL
V000000391	2016-08-15	2016-12-30 07:54:55	1	2245000.00	evomobmi@sepete.com	Sokahig	Greenfield, IN 46140	70000.00	\N	J&T EXPRESS
V000000392	2016-08-08	2016-08-08 17:31:54	2	2023000.00	paweifa@gusak.com	Fapapo	8468 Taylor St.?	42000.00	\N	PAHAL
V000000393	2016-10-14	2016-12-30 07:54:55	1	259000.00	ruemo@ru.co.uk	Galufef	Shrewsbury, MA 01545	28000.00	\N	PAHAL
V000000394	2016-10-14	2016-12-30 07:54:55	1	151000.00	boobaca@we.gov	Sujoze	510 East Rose St.?	56000.00	\N	WAHANA
V000000395	2016-08-21	2016-08-21 09:54:57	3	2807000.00	cafvadpoj@miwoda.co.uk	Tuavted	7755 South Deerfield Street?	42000.00	RESI000000000395	POS PAKET BIASA
V000000396	2016-09-14	2016-09-14 09:50:29	3	418000.00	jazurot@kentefe.net	Cuulaseb	842 Hilldale Dr.?	72000.00	RESI000000000396	JNE REGULER
V000000397	2016-08-06	2016-08-06 23:03:25	3	227000.00	ejiflo@amdof.io	Ovozge	8508 East George Dr.?	35000.00	RESI000000000397	POS PAKET BIASA
V000000398	2016-09-17	2016-09-17 01:52:29	3	891000.00	vuim@zadjul.co.uk	Denhosjil	Teaneck, NJ 07666	64000.00	RESI000000000398	WAHANA
V000000399	2016-10-18	2016-10-18 17:53:19	3	1215000.00	oficaznaf@lomu.org	Amdicru	912 Sherwood St.?	72000.00	RESI000000000399	WAHANA
V000000400	2016-09-10	2016-09-10 22:22:06	3	1296000.00	woatva@wodlopwep.edu	Zuhicuto	97 Henry Dr.?	30000.00	RESI000000000400	JNE OKE
V000000401	2016-07-27	2016-07-27 22:30:34	3	1522000.00	cito@no.edu	Laetpem	685 Andover Road?	72000.00	RESI000000000401	POS PAKET KILAT
V000000402	2016-09-25	2016-09-25 09:51:33	3	1663000.00	waruk@tu.org	Hecgusnum	Elizabeth, NJ 07202	56000.00	RESI000000000402	JNE REGULER
V000000403	2016-09-16	2016-09-16 01:00:52	3	1204000.00	bavit@ji.org	Fapapo	Waukegan, IL 60085	42000.00	RESI000000000403	PAHAL
V000000404	2016-09-23	2016-12-30 07:54:55	1	267000.00	ivnu@viset.edu	Movwukine	Westmont, IL 60559	40000.00	\N	JNE OKE
V000000405	2016-08-31	2016-08-31 19:21:39	3	1349000.00	jejme@me.com	Lilhava	Lebanon, PA 17042	56000.00	RESI000000000405	PAHAL
V000000406	2016-08-13	2016-12-30 07:54:55	1	294000.00	gaupe@uf.org	Rihizaf	Freeport, NY 11520	21000.00	\N	POS PAKET BIASA
V000000407	2016-09-21	2016-12-30 07:54:55	1	970000.00	miwesa@ros.org	Cebvolsi	8408 Fieldstone St.?	35000.00	\N	PAHAL
V000000408	2016-07-27	2016-12-30 07:54:55	1	2370000.00	ija@ub.net	Halaga	7949 Roberts Street?	45000.00	\N	JNE OKE
V000000409	2016-07-29	2016-07-29 19:08:59	3	874000.00	jodluf@soklizkec.org	Uhcowej	Barrington, IL 60010	70000.00	RESI000000000409	POS PAKET BIASA
V000000410	2016-09-27	2016-12-30 07:54:55	1	1122000.00	wimfi@derwejgen.co.uk	Cebvolsi	285 West Olive Ave.?	56000.00	\N	PAHAL
V000000411	2016-08-26	2016-12-30 07:54:55	1	576000.00	mewugek@sojkatta.io	Tohimi	90 Euclid Lane?	35000.00	\N	PAHAL
V000000412	2016-08-28	2016-08-28 13:25:36	3	1718000.00	uvogew@onumi.io	Cudsale	7827 W. Vine St.?	28000.00	RESI000000000412	PAHAL
V000000413	2016-10-08	2016-10-08 08:25:47	4	824000.00	enuji@benkutja.gov	Gimmaniko	8850 Illinois Street?	16000.00	RESI000000000413	JNE REGULER
V000000414	2016-07-28	2016-07-28 04:29:15	2	1386000.00	sepmajbo@racupmib.io	Fapapo	Gurnee, IL 60031	35000.00	\N	POS PAKET BIASA
V000000415	2016-07-23	2016-07-23 08:36:25	2	2952000.00	enuji@benkutja.gov	Amdicru	Statesville, NC 28625	32000.00	\N	WAHANA
V000000416	2016-09-22	2016-09-22 02:33:34	2	376000.00	gurab@suvelelub.io	Sansegesu	796 Poor House Lane?	48000.00	\N	WAHANA
V000000417	2016-09-30	2016-09-30 04:38:55	3	2625000.00	va@bebim.net	Ciemica	Chapel Hill, NC 27516	120000.00	RESI000000000417	POS PAKET KILAT
V000000418	2016-07-27	2016-07-27 19:08:48	4	604000.00	kanaz@mema.org	Weezed	Dothan, AL 36301	35000.00	RESI000000000418	PAHAL
V000000419	2016-09-18	2016-09-18 12:14:10	2	1751000.00	uphagfa@reluba.io	Difarbaw	New Haven, CT 06511	35000.00	\N	JNE OKE
V000000420	2016-09-09	2016-12-30 07:54:55	1	348000.00	co@lep.gov	Difarbaw	683 Briarwood Ave.?	30000.00	\N	JNE OKE
V000000421	2016-08-06	2016-08-06 17:54:41	4	186000.00	eto@cenjiv.co.uk	Zafjomvo	Southampton, PA 18966	32000.00	RESI000000000421	JNE REGULER
V000000422	2016-08-20	2016-08-20 03:23:33	4	1296000.00	cobkom@bo.org	Hecgusnum	64 N. Lyme Rd.?	56000.00	RESI000000000422	JNE REGULER
V000000423	2016-08-02	2016-08-02 04:13:15	2	1067000.00	vig@dehede.org	Icjakop	Oceanside, NY 11572	98000.00	\N	J&T EXPRESS
V000000424	2016-09-18	2016-09-18 15:18:11	4	1911000.00	ceiwosak@kucuaj.net	Renroos	El Paso, TX 79930	60000.00	RESI000000000424	LION PARCEL
V000000425	2016-08-05	2016-08-05 19:05:15	2	852000.00	sivugal@opnulaj.gov	Zamafhiz	Ottawa, IL 61350	24000.00	\N	JNE REGULER
V000000426	2016-08-03	2016-08-03 22:51:56	4	785000.00	is@fe.com	Nadagra	West Palm Beach, FL 33404	70000.00	RESI000000000426	LION PARCEL
V000000427	2016-08-21	2016-08-21 04:05:32	2	1197000.00	vuim@zadjul.co.uk	Ezjeza	24 Wall Ave.?	48000.00	\N	WAHANA
V000000428	2016-09-29	2016-09-29 19:29:40	3	430000.00	ji@ihmomih.io	Voewisod	Carlisle, PA 17013	60000.00	RESI000000000428	JNE YES
V000000429	2016-10-20	2016-10-20 01:17:38	3	1771000.00	jotugve@oclekuju.io	Fapapo	839 North Richardson Ave.?	28000.00	RESI000000000429	POS PAKET BIASA
V000000430	2016-08-17	2016-12-30 07:54:55	1	168000.00	socoma@laomigez.io	Rihizaf	8283 Mechanic St.?	70000.00	\N	J&T EXPRESS
V000000431	2016-08-20	2016-08-20 02:25:56	3	3090000.00	is@fe.com	Cudsale	68 Riverside Dr.?	14000.00	RESI000000000431	POS PAKET BIASA
V000000432	2016-08-04	2016-12-30 07:54:55	1	268000.00	tumog@heovazor.edu	Udaumdog	9383 Military Avenue?	20000.00	\N	JNE OKE
V000000433	2016-10-12	2016-12-30 07:54:55	1	1785000.00	rulsiw@lecwuzla.gov	Ulmezle	8217 Creekside Dr.?	40000.00	\N	LION PARCEL
V000000434	2016-10-08	2016-10-08 16:22:04	4	1808000.00	ditka@bajwo.gov	Nocanad	30 Race Rd.?	40000.00	RESI000000000434	WAHANA
V000000435	2016-10-01	2016-10-01 12:18:18	2	742000.00	erfi@zokiw.co.uk	Cuulaseb	62 Shipley St.?	84000.00	\N	POS PAKET KILAT
V000000436	2016-08-31	2016-08-31 12:53:58	2	267000.00	hewunul@rigfuda.org	Onuegbiw	165 El Dorado Ave.?	42000.00	\N	POS PAKET BIASA
V000000437	2016-09-13	2016-09-13 14:56:07	4	186000.00	joboav@un.io	Movwukine	2 Jones Street?	30000.00	RESI000000000437	JNE OKE
V000000438	2016-07-26	2016-07-26 20:02:57	2	1380000.00	eptew@os.org	Wanuwa	25 Amherst Avenue?	98000.00	\N	J&T EXPRESS
V000000439	2016-10-12	2016-10-12 19:58:56	3	432000.00	oceob@codetcuh.com	Saolina	Alabaster, AL 35007	32000.00	RESI000000000439	JNE REGULER
V000000440	2016-08-16	2016-08-16 19:21:40	2	803000.00	ewnor@koc.org	Ritkezil	178 Philmont St.?	56000.00	\N	WAHANA
V000000441	2016-08-17	2016-12-30 07:54:55	1	4920000.00	obofac@fel.edu	Masvebje	8825 Peninsula Avenue?	40000.00	\N	JNE OKE
V000000442	2016-09-27	2016-09-27 20:32:39	4	2805000.00	ikruj@sapanti.com	Vulkewmeg	Phoenixville, PA 19460	15000.00	RESI000000000442	JNE OKE
V000000443	2016-09-05	2016-09-05 04:14:17	4	1600000.00	vuc@ifef.net	Rihizaf	8810 Corona Street?	98000.00	RESI000000000443	J&T EXPRESS
V000000444	2016-08-23	2016-08-23 14:52:08	3	97000.00	owagij@gingetek.io	Sujoze	8923 Hall Lane?	40000.00	RESI000000000444	WAHANA
V000000445	2016-08-28	2016-12-30 07:54:55	1	1475000.00	vig@dehede.org	Favofsi	Harlingen, TX 78552	10000.00	\N	JNE OKE
V000000446	2016-10-20	2016-12-30 07:54:55	1	809000.00	ewebeduc@heceh.co.uk	Azevimut	Springfield Gardens, NY 11413	84000.00	\N	J&T EXPRESS
V000000447	2016-08-23	2016-08-23 12:25:23	3	1533000.00	papa@jorisuz.co.uk	Newbimmig	West Lafayette, IN 47906	63000.00	RESI000000000447	POS PAKET BIASA
V000000448	2016-10-18	2016-10-18 09:03:23	4	611000.00	povioge@na.gov	Laetpem	San Carlos, CA 94070	84000.00	RESI000000000448	POS PAKET KILAT
V000000449	2016-09-27	2016-09-27 07:31:42	4	110000.00	sisi@povzurudo.io	Movwukine	Ossining, NY 10562	15000.00	RESI000000000449	JNE OKE
V000000450	2016-10-26	2016-12-30 07:54:55	1	1314000.00	fiom@si.org	Ihkeadu	9899 Goldfield Drive?	64000.00	\N	JNE REGULER
V000000451	2016-08-10	2016-08-10 03:32:38	4	952000.00	uduhet@pomwic.org	Gasomo	9578 Campfire Rd.?	25000.00	RESI000000000451	JNE OKE
V000000452	2016-08-18	2016-08-18 06:13:19	2	1831000.00	sisi@povzurudo.io	Ivsatdof	Apple Valley, CA 92307	30000.00	\N	JNE OKE
V000000453	2016-09-25	2016-09-25 05:09:09	2	621000.00	ige@ocwope.net	Divvemdaz	89 E. Devonshire Ave.?	72000.00	\N	POS PAKET KILAT
V000000454	2016-08-22	2016-08-22 11:26:48	3	2450000.00	heip@cani.com	Fapapo	10 Oak Valley Street?	63000.00	RESI000000000454	POS PAKET BIASA
V000000455	2016-09-08	2016-09-08 17:57:32	4	1978000.00	jukuctig@bucolza.org	Zonsawne	7042 Rockaway Ave.?	50000.00	RESI000000000455	LION PARCEL
V000000456	2016-10-26	2016-10-26 13:56:54	2	912000.00	hewunul@rigfuda.org	Ewelimleh	685 Andover Road?	84000.00	\N	POS PAKET KILAT
V000000457	2016-09-10	2016-12-30 07:54:55	1	322000.00	sulovsi@ewigi.edu	Racubuus	Littleton, CO 80123	56000.00	\N	POS PAKET BIASA
V000000458	2016-08-04	2016-12-30 07:54:55	1	2977000.00	hisudo@ohlu.edu	Newbimmig	Woburn, MA 01801	30000.00	\N	JNE OKE
V000000459	2016-08-20	2016-08-20 21:58:30	2	753000.00	ciraofi@wikvuut.io	Sekgizveb	Summerville, SC 29483	64000.00	\N	JNE REGULER
V000000460	2016-09-08	2016-09-08 23:06:12	4	790000.00	lupadiv@ge.com	Kerehu	7350 Myrtle Lane?	45000.00	RESI000000000460	TIKI REGULER
V000000461	2016-09-16	2016-12-30 07:54:55	1	1221000.00	ruemo@ru.co.uk	Nadagra	Jackson Heights, NY 11372	40000.00	\N	LION PARCEL
V000000462	2016-09-06	2016-09-06 03:50:13	2	1127000.00	lud@wubra.gov	Azevimut	1 Bishop St.?	84000.00	\N	J&T EXPRESS
V000000463	2016-09-19	2016-09-19 00:39:08	2	120000.00	veizovo@dewuv.org	Publotweb	8672 Manor Station St.?	64000.00	\N	JNE REGULER
V000000464	2016-09-04	2016-12-30 07:54:55	1	157000.00	jemoh@kawbur.gov	Gasomo	Hoffman Estates, IL 60169	35000.00	\N	JNE OKE
V000000465	2016-08-03	2016-08-03 01:59:26	2	2178000.00	weheb@isoho.com	Nisgasod	9698 Cedar Swamp Street?	35000.00	\N	JNE OKE
V000000466	2016-09-25	2016-12-30 07:54:55	1	54000.00	kodon@jeej.com	Kerehu	Passaic, NJ 07055	32000.00	\N	WAHANA
V000000467	2016-09-19	2016-09-19 04:32:57	2	1327000.00	tuvnej@le.io	Gasomo	7949 Roberts Street?	90000.00	\N	JNE YES
V000000468	2016-10-18	2016-12-30 07:54:55	1	590000.00	idudovje@wadmisre.co.uk	Galufef	7136 Manchester Street?	21000.00	\N	PAHAL
V000000469	2016-09-16	2016-09-16 09:51:47	4	1127000.00	ditka@bajwo.gov	Azevimut	8850 Illinois Street?	42000.00	RESI000000000469	J&T EXPRESS
V000000470	2016-08-10	2016-08-10 07:08:46	2	690000.00	rofuhbe@vetidi.co.uk	Ijiozazag	25 Amherst Avenue?	96000.00	\N	POS PAKET KILAT
V000000471	2016-09-24	2016-12-30 07:54:55	1	297000.00	canehiler@enlo.edu	Gimmaniko	10 W. Windfall Ave.?	48000.00	\N	JNE REGULER
V000000472	2016-07-31	2016-07-31 08:25:50	2	572000.00	fasepjev@ogafi.net	Nefpezi	8408 Fieldstone St.?	90000.00	\N	JNE YES
V000000473	2016-10-23	2016-12-30 07:54:55	1	940000.00	nifu@lotedpoh.gov	Dalnedi	228 Pennington St.?	45000.00	\N	JNE OKE
V000000474	2016-09-06	2016-09-06 10:02:47	3	2784000.00	gaupe@uf.org	Ekviamu	227 Golf Lane?	48000.00	RESI000000000474	WAHANA
V000000475	2016-08-10	2016-08-10 11:13:46	4	136000.00	nu@erajod.org	Saolina	Saint Joseph, MI 49085	32000.00	RESI000000000475	JNE REGULER
V000000476	2016-07-23	2016-07-23 14:21:44	4	200000.00	enuji@benkutja.gov	Jeijsig	Oxford, MS 38655	96000.00	RESI000000000476	POS PAKET KILAT
V000000477	2016-08-08	2016-08-08 03:31:58	3	1335000.00	pidcem@bucfauru.com	Ijiozazag	208 Mountainview Rd.?	16000.00	RESI000000000477	JNE REGULER
V000000478	2016-09-30	2016-12-30 07:54:55	1	187000.00	ige@ocwope.net	Hozguna	South Plainfield, NJ 07080	14000.00	\N	POS PAKET BIASA
V000000479	2016-08-06	2016-08-06 17:41:51	2	3434000.00	opevar@utfebe.com	Renroos	368 Durham St.?	42000.00	\N	PAHAL
V000000480	2016-09-27	2016-12-30 07:54:55	1	606000.00	ponuv@howup.co.uk	Azevimut	984 Elm Ave.?	28000.00	\N	J&T EXPRESS
V000000481	2016-10-22	2016-10-22 21:58:36	4	268000.00	hewunul@rigfuda.org	Udaumdog	Middleton, WI 53562	32000.00	RESI000000000481	WAHANA
V000000482	2016-10-11	2016-10-11 09:01:14	3	794000.00	zup@ab.com	Ezjeza	Ambler, PA 19002	48000.00	RESI000000000482	WAHANA
V000000483	2016-09-13	2016-09-13 06:42:04	2	840000.00	vig@dehede.org	Ongebzu	Pottstown, PA 19464	40000.00	\N	WAHANA
V000000484	2016-10-25	2016-10-25 09:51:38	4	2975000.00	vig@dehede.org	Fapapo	96 West Fordham Ave.?	49000.00	RESI000000000484	POS PAKET BIASA
V000000485	2016-07-27	2016-12-30 07:54:55	1	106000.00	ijages@lomulel.io	Uguciis	491 N. College Court?	40000.00	\N	WAHANA
V000000486	2016-07-29	2016-07-29 06:42:38	2	1083000.00	ejiflo@amdof.io	Ijiozazag	Oklahoma City, OK 73112	72000.00	\N	JNE REGULER
V000000487	2016-09-29	2016-09-29 05:37:27	3	2720000.00	powjajrak@el.io	Ivsatdof	195 College St.?	25000.00	RESI000000000487	JNE OKE
V000000488	2016-10-13	2016-10-13 07:07:52	4	705000.00	leluwi@honwi.com	Sansegesu	Chelmsford, MA 01824	35000.00	RESI000000000488	PAHAL
V000000489	2016-08-31	2016-08-31 01:11:11	3	247000.00	uk@kiko.co.uk	Weezed	Levittown, NY 11756	28000.00	RESI000000000489	PAHAL
V000000490	2016-09-17	2016-09-17 13:38:37	2	60000.00	wa@ejadu.gov	Movwukine	7888 W. Plumb Branch Street?	10000.00	\N	JNE OKE
V000000491	2016-08-16	2016-08-16 07:52:25	3	2332000.00	kub@etdav.org	Ovozge	7255 Campfire Dr.?	49000.00	RESI000000000491	POS PAKET BIASA
V000000492	2016-10-24	2016-10-24 07:28:34	4	233000.00	jaolo@fihotnem.net	Uguciis	South Bend, IN 46614	54000.00	RESI000000000492	TIKI REGULER
V000000493	2016-09-13	2016-09-13 09:10:36	3	1779000.00	rofuhbe@vetidi.co.uk	Cebvolsi	644 Purple Finch Rd.?	24000.00	RESI000000000493	WAHANA
V000000494	2016-09-11	2016-12-30 07:54:55	1	1986000.00	cuberuta@biepi.co.uk	Lilhava	208 Mountainview Rd.?	35000.00	\N	POS PAKET BIASA
V000000495	2016-10-13	2016-10-13 11:46:29	2	387000.00	no@ugu.com	Tohimi	Oceanside, NY 11572	35000.00	\N	PAHAL
V000000496	2016-07-20	2016-07-20 00:27:11	4	3006000.00	nimzin@guznerepi.edu	Sokahig	8218 Court St.?	70000.00	RESI000000000496	J&T EXPRESS
V000000497	2016-08-27	2016-08-27 06:21:33	4	1660000.00	fub@nuwok.io	Gegufa	796 Poor House Lane?	45000.00	RESI000000000497	TIKI REGULER
V000000498	2016-10-12	2016-10-12 22:41:05	3	294000.00	po@fahida.co.uk	Uguciis	Wooster, OH 44691	45000.00	RESI000000000498	TIKI REGULER
V000000499	2016-08-30	2016-08-30 06:52:52	3	83000.00	jejme@me.com	Tukeci	Summerville, SC 29483	90000.00	RESI000000000499	LION PARCEL
V000000500	2016-08-10	2016-08-10 17:33:07	4	2772000.00	hovarwuh@sup.io	Fapapo	Wilmette, IL 60091	21000.00	RESI000000000500	PAHAL
\.


--
-- Data for Name: ulasan; Type: TABLE DATA; Schema: tokokeren; Owner: postgres
--

COPY ulasan (email_pembeli, kode_produk, tanggal, rating, komentar) FROM stdin;
halilil@wuturo.net	S0000001	2016-09-06	0	lumayan sih, tapi agak ngga worth it.. 
tocepu@wela.co.uk	S0000006	2016-09-21	1	Baru seminggu udah rusak.. 
guz@tuve.com	S0000011	2016-09-16	2	lumayan sih, tapi agak ngga worth it.. 
kehu@pom.edu	S0000016	2016-08-21	3	Awet, gan. 
na@tazevze.com	S0000021	2016-09-08	4	Ditunggu stock selanjutnya. 
tu@pevu.edu	S0000026	2016-09-14	5	Penjualnya kurang ramah tapi barang oke.. 
uc@zepunu.org	S0000031	2016-09-25	0	Jelek. Cepat rusak. 
sijtu@ja.io	S0000036	2016-08-05	1	Ngga suka. ga sesuai foto.. 
cel@map.io	S0000041	2016-08-08	2	lumayan sih, tapi agak ngga worth it.. 
teuzu@va.net	S0000046	2016-10-20	3	Good. Recomended.. 
hu@eha.co.uk	S0000051	2016-09-16	4	Barangnya bagus.. 
dimkerra@lebog.co.uk	S0000056	2016-08-02	5	Ditunggu stock selanjutnya. 
oficaznaf@lomu.org	S0000061	2016-09-08	0	Ngga suka. ga sesuai foto.. 
ucetaeb@zola.co.uk	S0000066	2016-10-21	1	Ada cacat nih gan barangnya.. 
ijages@lomulel.io	S0000071	2016-08-07	2	lumayan sih, tapi agak ngga worth it.. 
pidcem@bucfauru.com	S0000076	2016-08-30	3	Mau order lagi kak. 
anowuw@sabapmi.org	S0000081	2016-07-26	4	Sesuai sama harganya.. 
sadhupac@zon.edu	S0000086	2016-08-12	5	Cepat sampai barang oke. 
noge@wib.com	S0000091	2016-07-27	0	Unrecomended. Lama sampai. Barang tidak sesuai dengan foto.. 
po@fahida.co.uk	S0000096	2016-08-11	1	Ada cacat nih gan barangnya.. 
vanesu@juwhuzu.gov	S0000101	2016-08-02	2	lumayan sih, tapi agak ngga worth it.. 
sisi@povzurudo.io	S0000106	2016-07-26	3	Penjualnya kurang ramah tapi barang oke.. 
hovarwuh@sup.io	S0000111	2016-10-06	4	Ditunggu stock selanjutnya. 
inu@fo.gov	S0000116	2016-08-11	5	Ditunggu stock selanjutnya. 
pi@kekbu.io	S0000121	2016-08-22	0	lumayan sih, tapi agak ngga worth it.. 
subzola@kacjis.edu	S0000126	2016-08-01	1	lumayan sih, tapi agak ngga worth it.. 
si@behab.co.uk	S0000131	2016-08-21	2	lumayan sih, tapi agak ngga worth it.. 
ruemo@ru.co.uk	S0000136	2016-08-02	3	Good. Recomended.. 
pufvep@zagicmac.io	S0000141	2016-09-16	4	Lama sampai tapi barang oke. 
nu@erajod.org	S0000146	2016-09-16	5	Lama sampai tapi barang oke. 
jiwuol@vi.io	S0000151	2016-09-21	0	Baru seminggu udah rusak.. 
vuc@ifef.net	S0000156	2016-10-27	1	lumayan sih, tapi agak ngga worth it.. 
gaupe@uf.org	S0000161	2016-08-08	2	Ngga suka. ga sesuai foto.. 
dup@ejmameh.io	S0000166	2016-09-29	3	Ditunggu stock selanjutnya. 
erfi@zokiw.co.uk	S0000171	2016-08-18	4	Awet, gan. 
mewugek@sojkatta.io	S0000176	2016-08-07	5	Ditunggu stock selanjutnya. 
jawantu@siziin.io	S0000181	2016-07-30	0	lumayan sih, tapi agak ngga worth it.. 
ced@ego.net	S0000186	2016-09-19	1	Ada cacat nih gan barangnya.. 
eptew@os.org	S0000191	2016-08-27	2	lumayan sih, tapi agak ngga worth it.. 
jotugve@oclekuju.io	S0000196	2016-09-20	3	Sesuai sama harganya.. 
ze@erejanek.com	S0000201	2016-07-24	4	Awet, gan. 
vim@kieh.gov	S0000206	2016-07-21	5	Lumayan lah ya ini barang. 
evisep@uwre.net	S0000211	2016-08-23	0	Baru seminggu udah rusak.. 
hu@zusrusiw.edu	S0000216	2016-10-02	1	Ngga suka. ga sesuai foto.. 
ji@kozko.co.uk	S0000221	2016-10-13	2	lumayan sih, tapi agak ngga worth it.. 
il@up.co.uk	S0000226	2016-10-06	3	Cepat sampai, barang cihuy. 
obofac@fel.edu	S0000231	2016-09-01	4	Bagus banget. 
rohoci@muza.io	S0000236	2016-10-11	5	Penjualnya kurang ramah tapi barang oke.. 
adaerop@ono.net	S0000241	2016-08-05	0	Ada cacat nih gan barangnya.. 
waruk@tu.org	S0000246	2016-09-15	1	Jelek. Cepat rusak. 
eto@cenjiv.co.uk	S0000001	2016-08-04	2	Ngga suka. ga sesuai foto.. 
fiw@mamko.co.uk	S0000006	2016-08-21	3	Cepat sampai, barang cihuy. 
gukum@ficoofo.co.uk	S0000011	2016-08-22	4	Debest penjual dan barangnya.. 
ho@jesopi.gov	S0000016	2016-09-14	5	Debest penjual dan barangnya.. 
hemvofvod@osulojmov.com	S0000021	2016-10-07	0	Ngga suka. ga sesuai foto.. 
zu@guji.gov	S0000026	2016-10-19	1	lumayan sih, tapi agak ngga worth it.. 
uk@kiko.co.uk	S0000031	2016-09-01	2	Jelek. Cepat rusak. 
powjajrak@el.io	S0000036	2016-08-09	3	Cepat sampai barangnya. Trims. 
ige@ocwope.net	S0000041	2016-10-27	4	Barangnya bagus.. 
jodluf@soklizkec.org	S0000046	2016-10-15	5	Awet, gan. 
canehiler@enlo.edu	S0000051	2016-09-15	0	Baru seminggu udah rusak.. 
teguc@pohon.io	S0000056	2016-08-21	1	Unrecomended. Lama sampai. Barang tidak sesuai dengan foto.. 
zonur@vocumotoz.net	S0000061	2016-09-18	2	Unrecomended. Lama sampai. Barang tidak sesuai dengan foto.. 
povioge@na.gov	S0000066	2016-08-12	3	Cepat sampai, barang cihuy. 
jazurot@kentefe.net	S0000071	2016-10-17	4	Debest penjual dan barangnya.. 
ija@ub.net	S0000076	2016-09-24	5	Sesuai sama harganya.. 
udne@donazmuv.io	S0000081	2016-08-07	0	Ngga suka. ga sesuai foto.. 
imsebap@oci.io	S0000086	2016-10-24	1	Ada cacat nih gan barangnya.. 
ta@roc.io	S0000091	2016-10-23	2	Jelek. Cepat rusak. 
mir@ru.org	S0000096	2016-10-16	3	Ini barang tidak ada tandingannya. 
fura@upipu.gov	S0000101	2016-10-25	4	Lumayan lah ya ini barang. 
solodsi@vinepmid.org	S0000106	2016-09-23	5	Bagus banget. 
jumovbot@ul.edu	S0000111	2016-08-17	0	Unrecomended. Lama sampai. Barang tidak sesuai dengan foto.. 
ba@nod.gov	S0000116	2016-08-03	1	lumayan sih, tapi agak ngga worth it.. 
ur@ru.com	S0000121	2016-10-01	2	lumayan sih, tapi agak ngga worth it.. 
tipdabit@ogazov.com	S0000126	2016-08-27	3	Mau order lagi kak. 
tumog@heovazor.edu	S0000131	2016-10-05	4	Awet, gan. 
jidfow@ditiduze.com	S0000136	2016-10-06	5	Penjualnya kurang ramah tapi barang oke.. 
kub@etdav.org	S0000141	2016-08-07	0	lumayan sih, tapi agak ngga worth it.. 
ji@ihmomih.io	S0000146	2016-10-24	1	Jelek. Cepat rusak. 
loh@cetumi.com	S0000151	2016-08-24	2	Ada cacat nih gan barangnya.. 
nimzin@guznerepi.edu	S0000156	2016-07-29	3	Lama sampai tapi barang oke. 
uc@rebufjen.edu	S0000161	2016-10-15	4	Lumayan lah ya ini barang. 
ejoha@acsudo.net	S0000166	2016-07-26	5	Debest penjual dan barangnya.. 
gita@parwaj.net	S0000171	2016-08-16	0	Ada cacat nih gan barangnya.. 
ivduse@zuhew.org	S0000176	2016-10-25	1	Jelek. Cepat rusak. 
vobvi@vo.org	S0000181	2016-08-08	2	Unrecomended. Lama sampai. Barang tidak sesuai dengan foto.. 
cin@dorpaz.org	S0000186	2016-08-31	3	Sesuai sama harganya.. 
jujrecwo@veofowe.com	S0000191	2016-10-08	4	Lumayan lah ya ini barang. 
rulsiw@lecwuzla.gov	S0000196	2016-09-30	5	Cepat sampai, barang cihuy. 
cito@no.edu	S0000201	2016-07-28	0	Jelek. Cepat rusak. 
esoli@velmurap.io	S0000206	2016-08-06	1	Ngga suka. ga sesuai foto.. 
tomva@kej.gov	S0000211	2016-10-20	2	Baru seminggu udah rusak.. 
neloke@lu.edu	S0000216	2016-08-24	3	Sesuai sama harganya.. 
sud@ul.org	S0000221	2016-09-11	4	Cepat sampai, barang cihuy. 
war@fohrahsin.net	S0000226	2016-07-31	5	Cepat sampai barangnya. Trims. 
ilam@hadjaus.org	S0000231	2016-08-17	0	Baru seminggu udah rusak.. 
dipcoc@ziszipmes.io	S0000236	2016-09-20	1	Jelek. Cepat rusak. 
wano@liwjumu.com	S0000241	2016-09-11	2	Ngga suka. ga sesuai foto.. 
malvo@gigdelkun.com	S0000246	2016-07-31	3	Bagus banget. 
\.


--
-- Name: jasa_kirim_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY jasa_kirim
    ADD CONSTRAINT jasa_kirim_pkey PRIMARY KEY (nama);


--
-- Name: kategori_utama_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY kategori_utama
    ADD CONSTRAINT kategori_utama_pkey PRIMARY KEY (kode);


--
-- Name: keranjang_belanja_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY keranjang_belanja
    ADD CONSTRAINT keranjang_belanja_pkey PRIMARY KEY (pembeli, kode_produk);


--
-- Name: komentar_diskusi_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY komentar_diskusi
    ADD CONSTRAINT komentar_diskusi_pkey PRIMARY KEY (pengirim, penerima, waktu);


--
-- Name: list_item_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY list_item
    ADD CONSTRAINT list_item_pkey PRIMARY KEY (no_invoice, kode_produk);


--
-- Name: pelanggan_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY pelanggan
    ADD CONSTRAINT pelanggan_pkey PRIMARY KEY (email);


--
-- Name: pengguna_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (email);


--
-- Name: produk_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY produk
    ADD CONSTRAINT produk_pkey PRIMARY KEY (kode_produk);


--
-- Name: produk_pulsa_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY produk_pulsa
    ADD CONSTRAINT produk_pulsa_pkey PRIMARY KEY (kode_produk);


--
-- Name: promo_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY promo
    ADD CONSTRAINT promo_pkey PRIMARY KEY (id);


--
-- Name: promo_produk_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY promo_produk
    ADD CONSTRAINT promo_produk_pkey PRIMARY KEY (id_promo, kode_produk);


--
-- Name: shipped_produk_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY shipped_produk
    ADD CONSTRAINT shipped_produk_pkey PRIMARY KEY (kode_produk);


--
-- Name: sub_kategori_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY sub_kategori
    ADD CONSTRAINT sub_kategori_pkey PRIMARY KEY (kode);


--
-- Name: toko_jasa_kirim_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY toko_jasa_kirim
    ADD CONSTRAINT toko_jasa_kirim_pkey PRIMARY KEY (nama_toko, jasa_kirim);


--
-- Name: toko_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY toko
    ADD CONSTRAINT toko_pkey PRIMARY KEY (nama);


--
-- Name: transaksi_pulsa_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY transaksi_pulsa
    ADD CONSTRAINT transaksi_pulsa_pkey PRIMARY KEY (no_invoice);


--
-- Name: transaksi_shipped_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY transaksi_shipped
    ADD CONSTRAINT transaksi_shipped_pkey PRIMARY KEY (no_invoice);


--
-- Name: ulasan_pkey; Type: CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY ulasan
    ADD CONSTRAINT ulasan_pkey PRIMARY KEY (email_pembeli, kode_produk);


--
-- Name: check_point_trigger; Type: TRIGGER; Schema: tokokeren; Owner: postgres
--

CREATE TRIGGER check_point_trigger AFTER UPDATE ON transaksi_shipped FOR EACH ROW EXECUTE PROCEDURE public.check_point();


--
-- Name: upd_stok_trigger; Type: TRIGGER; Schema: tokokeren; Owner: postgres
--

CREATE TRIGGER upd_stok_trigger BEFORE INSERT OR DELETE OR UPDATE ON list_item FOR EACH ROW EXECUTE PROCEDURE public.stock_after_update();


--
-- Name: keranjang_belanja_kode_produk_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY keranjang_belanja
    ADD CONSTRAINT keranjang_belanja_kode_produk_fkey FOREIGN KEY (kode_produk) REFERENCES shipped_produk(kode_produk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: keranjang_belanja_pembeli_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY keranjang_belanja
    ADD CONSTRAINT keranjang_belanja_pembeli_fkey FOREIGN KEY (pembeli) REFERENCES pelanggan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: komentar_diskusi_penerima_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY komentar_diskusi
    ADD CONSTRAINT komentar_diskusi_penerima_fkey FOREIGN KEY (penerima) REFERENCES pelanggan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: komentar_diskusi_pengirim_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY komentar_diskusi
    ADD CONSTRAINT komentar_diskusi_pengirim_fkey FOREIGN KEY (pengirim) REFERENCES pelanggan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: list_item_kode_produk_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY list_item
    ADD CONSTRAINT list_item_kode_produk_fkey FOREIGN KEY (kode_produk) REFERENCES shipped_produk(kode_produk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: list_item_no_invoice_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY list_item
    ADD CONSTRAINT list_item_no_invoice_fkey FOREIGN KEY (no_invoice) REFERENCES transaksi_shipped(no_invoice) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pelanggan_email_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY pelanggan
    ADD CONSTRAINT pelanggan_email_fkey FOREIGN KEY (email) REFERENCES pengguna(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: produk_pulsa_kode_produk_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY produk_pulsa
    ADD CONSTRAINT produk_pulsa_kode_produk_fkey FOREIGN KEY (kode_produk) REFERENCES produk(kode_produk);


--
-- Name: promo_produk_id_promo_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY promo_produk
    ADD CONSTRAINT promo_produk_id_promo_fkey FOREIGN KEY (id_promo) REFERENCES promo(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: promo_produk_kode_produk_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY promo_produk
    ADD CONSTRAINT promo_produk_kode_produk_fkey FOREIGN KEY (kode_produk) REFERENCES produk(kode_produk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shipped_produk_kategori_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY shipped_produk
    ADD CONSTRAINT shipped_produk_kategori_fkey FOREIGN KEY (kategori) REFERENCES sub_kategori(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shipped_produk_kode_produk_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY shipped_produk
    ADD CONSTRAINT shipped_produk_kode_produk_fkey FOREIGN KEY (kode_produk) REFERENCES produk(kode_produk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shipped_produk_nama_toko_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY shipped_produk
    ADD CONSTRAINT shipped_produk_nama_toko_fkey FOREIGN KEY (nama_toko) REFERENCES toko(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sub_kategori_kode_kategori_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY sub_kategori
    ADD CONSTRAINT sub_kategori_kode_kategori_fkey FOREIGN KEY (kode_kategori) REFERENCES kategori_utama(kode) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: toko_email_penjual_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY toko
    ADD CONSTRAINT toko_email_penjual_fkey FOREIGN KEY (email_penjual) REFERENCES pelanggan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: toko_jasa_kirim_jasa_kirim_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY toko_jasa_kirim
    ADD CONSTRAINT toko_jasa_kirim_jasa_kirim_fkey FOREIGN KEY (jasa_kirim) REFERENCES jasa_kirim(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: toko_jasa_kirim_nama_toko_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY toko_jasa_kirim
    ADD CONSTRAINT toko_jasa_kirim_nama_toko_fkey FOREIGN KEY (nama_toko) REFERENCES toko(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_pulsa_email_pembeli_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY transaksi_pulsa
    ADD CONSTRAINT transaksi_pulsa_email_pembeli_fkey FOREIGN KEY (email_pembeli) REFERENCES pelanggan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_pulsa_kode_produk_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY transaksi_pulsa
    ADD CONSTRAINT transaksi_pulsa_kode_produk_fkey FOREIGN KEY (kode_produk) REFERENCES produk_pulsa(kode_produk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_shipped_email_pembeli_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY transaksi_shipped
    ADD CONSTRAINT transaksi_shipped_email_pembeli_fkey FOREIGN KEY (email_pembeli) REFERENCES pelanggan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_shipped_nama_toko_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY transaksi_shipped
    ADD CONSTRAINT transaksi_shipped_nama_toko_fkey FOREIGN KEY (nama_toko, nama_jasa_kirim) REFERENCES toko_jasa_kirim(nama_toko, jasa_kirim) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ulasan_email_pembeli_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY ulasan
    ADD CONSTRAINT ulasan_email_pembeli_fkey FOREIGN KEY (email_pembeli) REFERENCES pelanggan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ulasan_kode_produk_fkey; Type: FK CONSTRAINT; Schema: tokokeren; Owner: postgres
--

ALTER TABLE ONLY ulasan
    ADD CONSTRAINT ulasan_kode_produk_fkey FOREIGN KEY (kode_produk) REFERENCES shipped_produk(kode_produk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

