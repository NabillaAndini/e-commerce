

<?php include "headerAfterLogin.php";
if($_SESSION['role'] !== 'pembeli'){

  header('location: dashboard.php');
    echo' <script> location.replace("dashboard.php"); </script>';
    return;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
.navbar-inverse{
        background-color: #c0392b;
        color: #eeeeee !important;
      }

      .navbar-inverse .navbar-brand {
        color: #eeeeee !important;
      }
/*
  Side Navigation Menu V2, RWD
  ===================
  License:
  http://goo.gl/EaUPrt
  ===================
  Author: @PableraShow

 */

@charset "UTF-8";
@import url(http://fonts.googleapis.com/css?family=Open+Sans:300,400,700);
.form-style-4{
    width: 970px;
    font-size: 16px;
    background: #495C70;
    padding: 30px 30px 15px 30px;
    border: 5px solid #53687E;
    margin-left: 86px;
}
.form-style-4 input[type=submit],
.form-style-4 input[type=button],
.form-style-4 input[type=text],
.form-style-4 input[type=email],
.form-style-4 textarea,
.form-style-4 label
{
    font-family: Georgia, "Times New Roman", Times, serif;
    font-size: 16px;
    color: #fff;

}
.form-style-4 label {
    display:block;
    margin-bottom: 10px;
}
.form-style-4 label > span{
    display: inline-block;
    float: left;
    width: 150px;
}
.form-style-4 input[type=text],
.form-style-4 input[type=email]
{
    background: transparent;
    border: none;
    border-bottom: 1px dashed #83A4C5;
    width: 275px;
    outline: none;
    padding: 0px 0px 0px 0px;
    font-style: italic;
}
.form-style-4 textarea{
    font-style: italic;
    padding: 0px 0px 0px 0px;
    background: transparent;
    outline: none;
    border: none;
    border-bottom: 1px dashed #83A4C5;
    width: 275px;
    overflow: hidden;
    resize:none;
    height:20px;
}

.form-style-4 textarea:focus,
.form-style-4 input[type=text]:focus,
.form-style-4 input[type=email]:focus,
.form-style-4 input[type=email] :focus
{
    border-bottom: 1px dashed #D9FFA9;
}

.form-style-4 input[type=submit],
.form-style-4 input[type=button]{
    background: #576E86;
    border: none;
    padding: 8px 10px 8px 10px;
    border-radius: 5px;
    color: #A8BACE;
}
.form-style-4 input[type=submit]:hover,
.form-style-4 input[type=button]:hover{
background: #394D61;
}

body {
  font-family: 'Open Sans', sans-serif;
  font-weight: 300;
  line-height: 1.42em;
  color:#A7A1AE;
  background-color:#1F2739;
}

h1 {
  font-size:3em;
  font-weight: 300;
  line-height:1em;
  text-align: center;
  color: #4DC3FA;
}

h2 {
  font-size:1em;
  font-weight: 300;
  text-align: center;
  display: block;
  line-height:1em;
  padding-bottom: 2em;
  color: #FB667A;
}

h2 a {
  font-weight: 700;
  text-transform: uppercase;
  color: #FB667A;
  text-decoration: none;
}

.blue { color: #185875; font-size: 1.5em }
.yellow { color: #FB667A; font-size: 1.6em }

.container2 th h1 {
    font-weight: bold;
    font-size: 1em;
  text-align: left;
  color: #185875;
}

.container2 td {
    font-weight: normal;
    font-size: 1em;
  -webkit-box-shadow: 0 2px 2px -2px #0E1119;
     -moz-box-shadow: 0 2px 2px -2px #0E1119;
          box-shadow: 0 2px 2px -2px #0E1119;
}

.container2 {
    text-align: left;
    overflow: hidden;
    width: 85%;
    margin: 0 auto;
  display: table;
  padding: 0 0 8em 0;
}

.container2 td, .container2 th {
    padding-bottom: 2%;
    padding-top: 2%;
  padding-left:2%;
}

/* Background-color of the odd rows */
.container2 tr:nth-child(odd) {
    background-color: #323C50;
}

/* Background-color of the even rows */
.container2 tr:nth-child(even) {
    background-color: #2C3446;
}

.container2 th {
    background-color: #1F2739;
}

.container2 td:first-child { color: #FB667A; }

.container2 tr:hover {
   background-color: #464A52;
-webkit-box-shadow: 0 6px 6px -6px #0E1119;
     -moz-box-shadow: 0 6px 6px -6px #0E1119;
          box-shadow: 0 6px 6px -6px #0E1119;
}

.container2 td:hover {
  background-color: #FFF842;
  color: #403E10;
  font-weight: bold;

  box-shadow: #7F7C21 -1px 1px, #7F7C21 -2px 2px, #7F7C21 -3px 3px, #7F7C21 -4px 4px, #7F7C21 -5px 5px, #7F7C21 -6px 6px;
  transform: translate3d(6px, -6px, 0);

  transition-delay: 0s;
    transition-duration: 0.4s;
    transition-property: all;
  transition-timing-function: line;
}

@media (max-width: 800px) {
.container2 td:nth-child(4),
.container2 th:nth-child(4) { display: none; }
}

.form-style-1 {
    margin:10px auto;
    max-width: 400px;
    padding: 20px 12px 10px 20px;
    font: 13px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
}
.form-style-1 li {
    padding: 0;
    display: block;
    list-style: none;
    margin: 10px 0 0 0;
}
.form-style-1 label{
    margin:0 0 3px 0;
    padding:0px;
    display:block;
    font-weight: bold;
}
.form-style-1 input[type=text],
.form-style-1 input[type=date],
.form-style-1 input[type=datetime],
.form-style-1 input[type=number],
.form-style-1 input[type=search],
.form-style-1 input[type=time],
.form-style-1 input[type=url],
.form-style-1 input[type=email],
textarea,
select{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    border:1px solid #BEBEBE;
    padding: 7px;
    margin:0px;
    -webkit-transition: all 0.30s ease-in-out;
    -moz-transition: all 0.30s ease-in-out;
    -ms-transition: all 0.30s ease-in-out;
    -o-transition: all 0.30s ease-in-out;
    outline: none;
}
.form-style-1 input[type=text]:focus,
.form-style-1 input[type=date]:focus,
.form-style-1 input[type=datetime]:focus,
.form-style-1 input[type=number]:focus,
.form-style-1 input[type=search]:focus,
.form-style-1 input[type=time]:focus,
.form-style-1 input[type=url]:focus,
.form-style-1 input[type=email]:focus,
.form-style-1 textarea:focus,
.form-style-1 select:focus{
    -moz-box-shadow: 0 0 8px #88D5E9;
    -webkit-box-shadow: 0 0 8px #88D5E9;
    box-shadow: 0 0 8px #88D5E9;
    border: 1px solid #88D5E9;
}
.form-style-1 .field-divided{
    width: 49%;
}

.form-style-1 .field-long{
    width: 100%;
}
.form-style-1 .field-select{
    width: 100%;
}
.form-style-1 .field-textarea{
    height: 100px;
}
.form-style-1 input[type=submit], .form-style-1 input[type=button]{
    background: #4B99AD;
    padding: 8px 15px 8px 15px;
    border: none;
    color: #fff;
}
.form-style-1 input[type=submit]:hover, .form-style-1 input[type=button]:hover{
    background: #4691A4;
    box-shadow:none;
    -moz-box-shadow:none;
    -webkit-box-shadow:none;
}
.form-style-1 .required{
    color:red;
}
</style>
</head>
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12 text-center">
<h1><span class="blue"></span>Daftar<span class="blue"></span> <span class="yellow">Shipped Produk</pan></h1>

<?php
  //$db = pg_connect('host=localhost dbname=sirima user=b10 password=basdat123');
  include 'connect.php';

  $toko_valid = true;
  if(!isset($_GET['toko'])){
    $toko_valid = false;
  }else {
    pg_query("set search_path to tokokeren;");
    $total_berat = 0; ;
    $total_subtotal = 0;
    $query = "SELECT nama FROM TOKOKEREN.toko WHERE nama = '".$_GET['toko']."'";
  	$result = pg_query($query);
  	if (!$result) {
  			echo "Problem with query " . $query . "<br/>";
  			echo pg_last_error();
  			exit();
  	}

  	if(pg_num_rows($result) <= 0){
  		$toko_valid = false;
  	}else {
      $query = "SELECT kb.kode_produk, p.nama, kb.berat, kb.kuantitas, kb.harga, kb.sub_total, sp.stok FROM TOKOKEREN.produk p, TOKOKEREN.shipped_produk sp,  TOKOKEREN.keranjang_belanja kb  WHERE sp.kode_produk = p.kode_produk and p.kode_produk = kb.kode_produk and sp.nama_toko = '".$_GET['toko']."' and kb.pembeli = '".$_SESSION['email']."'";


    	$result = pg_query($query);
    	if (!$result) {
    			echo "Problem with query " . $query . "<br/>";
    			echo pg_last_error();
    			exit();
    	}
    }
  }



  if(isset($_POST['alamat_kirim']) and isset($_POST['jasa_kirim']) and isset($_POST['harga_kg']) and isset($_POST['total_ongkir']) and isset($_POST['total_harga'])){
    $has_error = false;
    $rows = array();
    while($row = pg_fetch_assoc($result)) {
      if(intval($row['stok'] < $row['kuantitas'])){
          $has_error = false;
          echo "<script>alert('Stok produk ".$row['kode_produk']." tidak mencukupi')</script>";
      }
      array_push($rows, $row);
    }

    if(!$has_error){
      $query = "SELECT count(*) as jumlah FROM TOKOKEREN.transaksi_shipped";
    	$result = pg_query($query);
    	if (!$result) {
    			echo "Problem with query " . $query . "<br/>";
    			echo pg_last_error();
    			exit();
    	}
      $no_invoice = sprintf('V%09d', intval(pg_fetch_assoc($result)['jumlah']) + 1);

      $query = "INSERT INTO TOKOKEREN.transaksi_shipped (no_invoice, tanggal, waktu_bayar, status,
                                                        total_bayar, email_pembeli, nama_toko, alamat_kirim, biaya_kirim, nama_jasa_kirim)
                       VALUES ('".$no_invoice."','".date('Y-m-d')."','".date('Y-m-d H:i:s')."',2,
                              '".$_POST['total_harga']."','".$_SESSION['email']."','".$_GET['toko']."',
                              '".$_POST['alamat_kirim']."','".$_POST['total_ongkir']."','".$_POST['jasa_kirim']."')";


      $result = pg_query($query);
    	if (!$result) {
    			echo "Problem with query " . $query . "<br/>";
    			echo pg_last_error();
    			exit();
    	}
      foreach ($rows as $row) {
        $query = "INSERT INTO TOKOKEREN.list_item (no_invoice, kode_produk, berat,
                                                  kuantitas, harga, sub_total)
                         VALUES ('".$no_invoice."','".$row['kode_produk']."','".$row['berat']."',
                                '".$row['kuantitas']."','".$row['harga']."','".$row['sub_total']."')";
        $result = pg_query($query);
      	if (!$result) {
      			echo "Problem with query " . $query . "<br/>";
      			echo pg_last_error();
      			exit();
      	}

        $query = "DELETE FROM TOKOKEREN.keranjang_belanja WHERE pembeli ='".$_SESSION['email']."' and kode_produk = '".$row['kode_produk']."'";
        $result = pg_query($query);
        if (!$result) {
      			echo "Problem with query " . $query . "<br/>";
      			echo pg_last_error();
      			exit();
      	}
      }
      echo "<script>alert('Sukses checkout, nomor invoice = ".$no_invoice."'); location.replace('index.php');</script>";
      return;
    }

  }
?>
<?php if(!$toko_valid): ?>>
  <?php
    $query = "SELECT nama_toko  FROM TOKOKEREN.keranjang_belanja k, TOKOKEREN.shipped_produk s WHERE pembeli ='".$_SESSION['email']."' and s.kode_produk = k.kode_produk";
    $result = pg_query($query);
    if (!$result) {
      $result = pg_query($query);
        echo "Problem with query " . $query . "<br/>";
        echo pg_last_error();
        exit();
    }
  ?>
  <?php if(pg_num_rows($result) > 0):?>
    <h3> Lihat keranjang belanja kamu dari toko: </h3>
    <ul>
      <?php while($row = pg_fetch_assoc($result)) { ?>
        <li><a href="cart.php?toko=<?php echo $row['nama_toko']; ?>"><?php echo $row['nama_toko']; ?></a></li>
      <?php } ?>
    </ul>
  <?php else: ?>
    <h3> Keranjang belanjang kamu kosong :( </h3>

  <?php endif; ?>
<?php elseif(pg_num_rows($result) > 0): ?>
    <table class="container2">
      <thead>
        <tr>
          <th><h1>Kode Produk</h1></th>
          <th><h1>Nama Produk</h1></th>
          <th><h1>Berat</h1></th>
          <th><h1>Kuantitas</h1></th>
          <th><h1>Harga</h1></th>

          <th>
          <h1>Subtotal</h1>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php while($row = pg_fetch_assoc($result)) {?>
          <tr>
            <td><?php echo $row['kode_produk']; ?></td>
            <td><?php echo $row['nama']; ?></td>
            <td><?php echo $row['berat']; ?></td>
            <td><?php echo $row['kuantitas']; ?></td>
            <td><?php echo $row['harga']; ?></td>
            <td><?php echo $row['sub_total']; ?></td>
            <?php $total_berat += ($row['berat'] * $row['kuantitas']);
                  $total_subtotal += $row['sub_total'];
            ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>
            </div>
          </div>


      <!-- Modal -->
    <form class="form-style-4" action="" method="post">
    <label for="alamat_kirim">
    <span>Alamat Kirim</span><input type="text" name="alamat_kirim" required="true" />
    </label>
    <label for="jasa_kirim">
      <span>Jasa Kirim</span>
    </label>
    <?php
      $query = "SELECT jasa_kirim FROM TOKOKEREN.toko_jasa_kirim WHERE nama_toko = '".$_GET['toko']."'";

    	$result = pg_query($query);
    	if (!$result) {
    			echo "Problem with query " . $query . "<br/>";
    			echo pg_last_error();
    			exit();
    	}
      $jasa_kirim = array();
    ?>
    <select required name="jasa_kirim" id="jasa_kirim" style="color: black">
      <option value="">Pilih Jasa Kirim</option>
      <?php while($row= pg_fetch_assoc($result)) { ?>
        <option value="<?php echo $row['jasa_kirim']; ?>"><?php echo $row['jasa_kirim']; ?></option>
      <?php


        $query = "SELECT lama_kirim, tarif FROM TOKOKEREN.jasa_kirim WHERE nama = '".$row['jasa_kirim']."'";

      	$result2 = pg_query($query);
      	if (!$result2) {
      			echo "Problem with query " . $query . "<br/>";
      			echo pg_last_error();
      			exit();
      	}
        $jasa_kirim[$row['jasa_kirim']] = pg_fetch_assoc($result2);
      ?>
      <?php }?>

    </select>
    </br></br>
    <label for="harga_kg">
    <span>Tarif per kg</span><input type="text"  id="harga_kg" disabled/>
    </label></br>
    <label for="total_ongkir">
    <span>Total Ongkir  </span><input type="text"  id="total_ongkir"  disabled />
    </label></br>
    <label for="total_harga">
    <span>Total yang harus dibayar  </span><input type="text" id="total_harga" disabled />
    </label></br>
    <label>
      <input type="text" name="harga_kg" id="harga_kg_real"  readonly hidden />
      <input type="text" name="total_ongkir" id="total_ongkir_real"  readonly hidden />
      <input type="text" name="total_harga" id="total_harga_real"  readonly hidden/>
    <span>&nbsp;</span><input type="submit" value="CHECKOUT" />
    </label>
  </form>
<?php else: ?>
  <h3> Keranjang Belanja Anda untuk Toko <?php echo $_GET['toko']; ?>  Kosong </h3>
<?php endif;  ?>
    </div>
  </div>
<?php include "footerAfterLogin.php"; ?>
<script>
  var jasa_kirims = <?php echo json_encode($jasa_kirim); ?>;
  var total_berat = <?php echo $total_berat; ?>;
  var total_subtotal = <?php echo $total_subtotal; ?>;
  $('#jasa_kirim').change(function(){
    var jasa_kirim = this.value;
    if(jasa_kirim == "" || !jasa_kirims[jasa_kirim]){
      $('#harga_kg').val("");
      $('#total_ongkir').val("");
      $('#total_harga').val("");
      $('#harga_kg_real').val("");
      $('#total_ongkir_real').val("");
      $('#total_harga_real').val("");
    }else {
      var harga_kg = parseInt(jasa_kirims[jasa_kirim]['tarif']);
      $('#harga_kg').val("Rp"+harga_kg);
      $('#total_ongkir').val("Rp"+harga_kg + " x " + total_berat + " kg = Rp" + (harga_kg*total_berat));
      $('#total_harga').val("Rp"+(harga_kg*total_berat) + " + Rp" + total_subtotal + " = Rp" + (harga_kg*total_berat+total_subtotal));


      $('#harga_kg_real').val(harga_kg);
      $('#total_ongkir_real').val((harga_kg*total_berat));
      $('#total_harga_real').val(total_subtotal);
    }

  });



</script>
