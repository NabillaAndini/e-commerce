<?php include "headerAfterLogin.php"; include 'connect.php';

  	if($_SESSION["role"] == "admin") {
		echo("<script> location.replace('errorAdmin.php'); </script>");
	}
?>

<div class="container" style="margin-top:35px">
    <?php
    	if(isset($_SESSION["masuk"])){
      		if($_SESSION["masuk"] == "success") {
      			unset($_SESSION['masuk']);
        			echo("<div id='messages' class= 'alert alert-success alert-dismissible' role='alert' style='background-color: #bdecf0; border: 1px solid #3dc7d4; font-family: 'Slabo 27px', serif;'>
              				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>&times;</button>
              				<div> Success! Your Product Has Been Added! </div>
            			</div>");
				}
		}
		?>

    <div class="row" style="background-color: #e54145; color: white;">
		<h2 id="judul" style="text-align: center; "> Add Shipped Product </h2>
	</div>
	<div class="row" id="formShipped">
        <div class="col-md-6">
			<form action="cekShipped.php">
				<div class="form-group">
					<label for="kode">Product Code:</label>
					<input type="text" class="form-control" id="kode" placeholder="Enter product code" name="kode" required oninvalid="this.setCustomValidity('Please insert the product code')" oninput="setCustomValidity('')">
				</div>
				<div class="form-group">
					<label for="name">Product Name:</label>
					<input type="text" class="form-control" id="name" placeholder="Enter product name" name="name" required oninvalid="this.setCustomValidity('Please insert the product name')" oninput="setCustomValidity('')">
				</div>
				<div class="form-group">
					<label for="harga">Price:</label>
					<input type="text" class="form-control" id="harga" placeholder="Enter product price" name="harga" required oninvalid="this.setCustomValidity('Please insert the product price')" oninput="setCustomValidity('')">
				</div>
				<div class="form-group">
					<label for="description">Description:</label>
					<textarea class="form-control" rows="5" id="desc" name="desc"></textarea>
				</div>
				<div class="form-group">
					<label for="subkategori">Sub-Category:</label>
						<?php
							$sql='SELECT * FROM TOKOKEREN.SUB_KATEGORI';
							$retval= pg_query($conn,$sql);

							echo"<select style='padding: 6px; color: black; border: none; margin-left: 5px; font-size: 18px; font-family: Philosopher, sans-serif;' name='kategori' required>";
								while($row= pg_fetch_assoc($retval))
								{
									echo "<option value=\"{$row['kode']}\">{$row['nama']}</option>";
								}
							echo "</select>";
						?>
				</div>
				<div class="form-group">
					<label for="stok">Insurance goods:</label>
					<label class="radio-inline">
		  				<input type="radio" name="optasuransi" required> Yes
					</label>
					<label class="radio-inline">
		  				<input type="radio" name="optasuransi" required> No
					</label>
				</div>
		</div>
		<div class="col-md-6">
				<div class="form-group">
					<label for="stok">Stock:</label>
					<input type="text" class="form-control" id="stok" placeholder="Enter product stock" name="stok" required oninvalid="this.setCustomValidity('Please insert the amount of stock')" oninput="setCustomValidity('')">
				</div>
				<div class="form-group">
					<label for="stok">New goods:</label>
					<label class="radio-inline">
		  				<input type="radio" name="optbaru" required> Yes
					</label>
					<label class="radio-inline">
		  				<input type="radio" name="optbaru" required> No
					</label>
				</div>
				<div class="form-group">
					<label for="minorder">Minimum order:</label>
					<input type="text" class="form-control" id="minorder" placeholder="Enter the number of minimum order " name="minorder" required oninvalid="this.setCustomValidity('Please insert the minimum order of your product')" oninput="setCustomValidity('')">
				</div>
				<div class="form-group">
					<label for="mingrosir">Minimum wholesale:</label>
					<input type="text" class="form-control" id="mingrosir" placeholder="Enter the number of minimum order for wholesale" name="mingrosir" required oninvalid="this.setCustomValidity('Please insert the minimum wholesale of your product')" oninput="setCustomValidity('')">
				</div>
				<div class="form-group">
					<label for="maxgrosir">Maximal wholesale:</label>
					<input type="text" class="form-control" id="maxgrosir" placeholder="Enter the number of maximal order for wholesale" name="maxgrosir" required oninvalid="this.setCustomValidity('Please insert the maximal wholesale of your product')" oninput="setCustomValidity('')">
				</div>
				<div class="form-group">
					<label for="hargaGrosir">Wholesale price:</label>
					<input type="text" class="form-control" id="hargaGrosir" placeholder="Enter the price for wholesale" name="hargaGrosir" required oninvalid="this.setCustomValidity('Please insert the price of wholesale buying')" oninput="setCustomValidity('')">
				</div>
				<div class="form-group">
					<label for="foto">Photo:</label>
		            <input type="file" name="file" id="file" required> <br>
		        </div>
		        <div style="background-color: #e54145; color: white; text-align: center; border: 1px solid white;">
					<button type="submit" class="btn btn-default" style="background: transparent;  border: none; font-size: 20px; font-family: 'Philosopher', sans-serif; padding-left: 190px; padding-right: 190px; color: white; outline: none;">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>


<?php include "footerAfterLogin.php"; ?>